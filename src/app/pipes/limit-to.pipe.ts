import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'limitTo'
})

export class LimitToPipe implements PipeTransform {
    transform(val, args) {
        if (args === undefined || !val) {
            return val;
        }

        if (val.length > args) {
            return val.substring(0, args) + '...';
        } else {
            return val;
        }
    }
}