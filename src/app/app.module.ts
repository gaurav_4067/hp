import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { MomentModule } from 'ngx-moment';
import { NgbDateAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { DragScrollModule } from 'ngx-drag-scroll';
import { NgxSocialShareModule } from 'ngx-social-share';
import { PlyrModule } from 'ngx-plyr';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PublicFooterComponent } from './public-layouts/footer/footer.component';
import { PublicNavbarComponent } from './public-layouts/navbar/navbar.component';
import { PublicHomeComponent } from './public-pages/home/home.component';
import { PublicAboutusComponent } from './public-pages/about-us/about-us.component';
import { PublicHowitworksComponent } from './public-pages/howitworks/howitworks.component';
import { PublicBecomePractitionerComponent } from './public-pages/become-practitioner/become-practitioner.component';
import { PublicContactusComponent } from './public-pages/contact-us/contact-us.component';
import { PublicFaqComponent } from './public-pages/faq/faq.component';
import { LoginModalComponent } from './public-pages/popup-modals/login-modal/login-modal.component';
import { SignupModalComponent } from './public-pages/popup-modals/sign-up-modal/sign-up-modal.component';
import { ForgotPasswordModalComponent } from './public-pages/popup-modals/forgot-password-modal/forgot-password-modal.component';
import { AuthHomeComponent } from './auth-layouts/home/auth-home.component';
import { AuthNavbarComponent } from './auth-layouts/navbar/navbar.component';
import { AuthSidebarComponent } from './auth-layouts/sidebar/sidebar.component';
import { AuthFooterComponent } from './auth-layouts/footer/footer.component';
import { DashboardComponent } from './auth-pages/dashboard/dashboard.component';
import { ChangePasswordComponent } from './auth-pages/common-pages/change-password/change-password.component';
import { EditProfileComponent } from './auth-pages/common-pages/edit-profile/edit-profile.component';
import { SessiononlyComponent } from './auth-pages/common-pages/sessiononly/sessiononly.component';
import { GeneralDashboardComponent } from './auth-pages/common-pages/general-dashboard/general-dashboard.component';
import { AllInterestsComponent } from './auth-pages/common-pages/all-interests/all-interests.component';

import { AdminDashboardComponent } from './auth-pages/admin-pages/admin-dashboard/admin-dashboard.component';
import { CustomerDashboardComponent } from './auth-pages/customer-pages/customer-dashboard/customer-dashboard.component';
import { CustomerProfileComponent } from './auth-pages/customer-pages/customer-profile/customer-profile.component';
import { BasicInfoComponent } from './auth-pages/customer-pages/basic-info/basic-info.component';
import { FirstPageComponent } from './auth-pages/customer-pages/basic-info/first-page/first-page.component';
import { SecondPageComponent } from './auth-pages/customer-pages/basic-info/second-page/second-page.component';
import { ThirdPageComponent } from './auth-pages/customer-pages/basic-info/third-page/third-page.component';
import { FourthPageComponent } from './auth-pages/customer-pages/basic-info/fourth-page/fourth-page.component';
import { FifthPageComponent } from './auth-pages/customer-pages/basic-info/fifth-page/fifth-page.component';
import { PractitionerDashboardComponent } from './auth-pages/practitioner-pages/practitioner-dashboard/practitioner-dashboard.component';
import { ApiService } from './services/ApiService';
import { ThirdPApiService } from './services/ThirdPApiService';
import { TokenInterceptorService } from './services/auth/TokenInterceptorService';
import { AuthGuardService } from './services/AuthGuardService';
import { CustomDateParserFormatter } from './services/datepickerAdapter';
import { AuthService } from './services/AuthService';
import { ChatService } from './services/ChatService';
import { GeneralService } from './services/GeneralService';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { PublicChangePasswordComponent } from './public-pages/change-password/change-password.component';
// Social Logins

import { MySubscriptionComponent } from './auth-pages/customer-pages/my-subscription/my-subscription.component';
import { CustomerPaymentComponent } from './auth-pages/customer-pages/customer-payment/customer-payment-component';
import { ViewInterestsComponent } from './auth-pages/customer-pages/view-interests/view-interests.component';
import { BillingInfoComponent } from './auth-pages/customer-pages/billing-info/billing-info.component';
import { BillingHistoryComponent } from './auth-pages/customer-pages/billing-info/billing-history/billing-history.component';
import { PaymentUpdateComponent } from './auth-pages/customer-pages/billing-info/payment-update/payment-update.component';
import { PaymentcreateComponent } from './auth-pages/customer-pages/billing-info/payment-create/payment-create.component';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { ChartsModule } from 'ng2-charts';
import { TabsModule } from 'ngx-bootstrap/tabs';
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';
import { ScoailLoginConfig } from './config/config';
import { PractitionerChatComponent } from './auth-pages/practitioner-pages/practitioner-chat/practitioner-chat.component';
import { CustomerSessionComponent } from './auth-pages/customer-pages/customer-session/customer-session.component';
import { CustomerChatComponent } from './auth-pages/customer-pages/customer-chat/customer-chat.component';
import { NotificationsComponent } from './auth-pages/practitioner-pages/notifications/notifications.component';
import { PractitionerSessionsComponent } from './auth-pages/practitioner-pages/practitioner-sessions/practitioner-sessions.component';
import { PractionerSessionComponent } from './auth-pages/practitioner-pages/practitioner-sessions/practioner-session/practioner-session.component';
import { AddSessionComponent } from './auth-pages/practitioner-pages/practitioner-sessions/add-session/add-session.component';
import { ConfirmSessionComponent } from './auth-pages/practitioner-pages/practitioner-sessions/confirm-session/confirm-session.component';
import { PaymentInfoComponent } from './auth-pages/payment-info/payment-info.component';
import { BankDetailsComponent } from './auth-pages/payment-info/bank-details/bank-details.component';
import { WithdrawHistoryComponent } from './auth-pages/payment-info/withdraw-history/withdraw-history.component';
import { ConnectAccountComponent } from './auth-pages/payment-info/connect-account/connect-account.component';
import { DocumentVerificationComponent } from './auth-pages/payment-info/document-verification/document-verification.component';
import { AddExpertiseComponent } from './auth-pages/add-expertise/add-expertise.component';
import { ViewExpertiseComponent } from './auth-pages/view-expertise/view-expertise.component';
import { ProfilePictureModalComponent } from './auth-pages/popup-modals/profile-picture-modal/profile-picture-modal.component';

import { ContactUsService } from './services/contact-service';

import { PractitionerSessionService } from './services/Practitioner-session.service';
import { SubscriptionPlansService } from './services/SubscriptionPlansService';
import { PractitionerScheduleService } from './services/practitioner-schedule.service';

import { DeleteChatComponent } from './auth-pages/customer-pages/delete-chat/delete-chat.component';
import { PractitionerScheduleComponent } from './auth-pages/practitioner-pages/practitioner-schedule/practitioner-schedule.component';
import { AvailablePatientComponent } from './auth-pages/practitioner-pages/available-patient/available-patient.component';
import { WeekSelectionComponent } from './auth-pages/practitioner-pages/practitioner-schedule/week-selection/week-selection.component';
import { BarChartComponent } from './auth-pages/common-pages/charts/bar-chart/bar-chart.component';
import { EditScheduleComponent } from './auth-pages/practitioner-pages/practitioner-schedule/edit-schedule/edit-schedule.component';

import { AddNewCustomerPaymentComponent } from './auth-pages/customer-pages/add-new-customer-payment/add-new-customer-payment.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BookTimePipe } from './auth-pages/customer-pages/customer-booking/booktimepipe';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { CustomerBookingComponent } from './auth-pages/customer-pages/customer-booking/customer-booking.component';
import { NgbdModalContent } from './auth-pages/customer-pages/customer-booking/ngbdmodalcontent.component';
import { PractitionerModalContent } from './auth-pages/common-pages/sessiononly/practitionermodalcontent.component';
import { ReportPractitionerComponent } from './auth-pages/practitioner-pages/practitioner-chat/report-practitioner/report-practitioner.component';
import { BlockCustomerComponent } from './auth-pages/customer-pages/block-customer/block-customer.component';
import { SwitchPractitionerComponent } from './auth-pages/practitioner-pages/practitioner-chat/switch-practitioner/switch-practitioner.component';
import { DataService } from './services/DataService';
import { AddPractionerComponent } from './auth-pages/add-practioner-pages/admin-add-practioner-component';
import { AddCustomerComponent } from './auth-pages/add-customer-pages/admin-add-customer-component';
import { PractitionerSessionComponent } from './auth-pages/practioner-dashboard-pages/practitioner-session.component';
import { FollowUpFormComponent } from './auth-pages/practitioner-pages/practitioner-chat/follow-up-form/follow-up-form.component';
import { CustomerStatsComponent } from './auth-pages/customer-pages/customer-stats/customer-stats.component';
import { MoodCalendarComponent } from './auth-pages/customer-pages/mood-calendar/mood-calendar.component';
import { LimitToPipe } from './pipes/limit-to.pipe';

import { StatsComponent } from './auth-pages/practitioner-pages/stats/stats.component';
import { SessionDetailsComponent } from './auth-pages/practitioner-pages/stats/session-details/session-details.component';
import { LineChartComponent } from './auth-pages/common-pages/charts/line-chart/line-chart.component';
import { DoughnutChartComponent } from './auth-pages/common-pages/charts/doughnut-chart/doughnut-chart.component';
import { ReviewPractitionerComponent } from './auth-pages/practitioner-pages/practitioner-chat/review-practitioner/review-practitioner.component';

import { RatingModule } from 'ngx-bootstrap/rating';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { AddSubscriptionPlanComponent } from './auth-pages/admin-pages/add-subscription/add-subscription-plan/add-subscription-plan.component';
import { AddInterestComponent } from './auth-pages/admin-pages/add-interest/add-interest.component';
import { DeleteInterestComponent } from './auth-pages/admin-pages/delete-interest/delete-interest.component';
import { InterestListComponent } from './auth-pages/admin-pages/interest-list/interest-list.component';
import { CustomerAboutusComponent } from './auth-pages/customer-pages/customer-aboutus/customer-aboutus.component';
import { HelpFaqComponent } from './auth-pages/common-pages/help-faq/help-faq.component';
import { TermsConditionComponent } from './auth-pages/common-pages/terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './public-pages/privacy-policy/privacy-policy.component';
import { CustomerPractitionerProfileComponent } from './auth-pages/customer-pages/customer-practitioner-profile/customer-practitioner-profile.component';
import { AdminPractitionerListComponent } from './auth-pages/admin-pages/admin-practitioner-list/admin-practitioner-list.component';
import { AdminCustomerListComponent } from './auth-pages/admin-pages/admin-customer-list/admin-customer-list.component';
import { ContactAdminComponent } from './auth-pages/contact-admin/contact-admin.component';
import { ClientNotesComponent } from './auth-pages/customer-pages/client-notes/client-notes.component';
import { SelectInterestComponent } from './auth-pages/customer-pages/select-interest/select-interest.component';
import { CustomerChatArchiveComponent } from './auth-pages/customer-pages/customer-chat-archive/customer-chat-archive.component';
import { PractitionerChatArchiveComponent } from './auth-pages/practitioner-pages/practitioner-chat-archive/practitioner-chat-archive.component';
import { SendMessageToPractitonerComponent } from './auth-pages/admin-pages/send-message-to-practitoner/send-message-to-practitoner.component';
import { SendMessageToCustomerComponent } from './auth-pages/admin-pages/send-message-to-customer/send-message-to-customer.component';
import { ManageNotificationComponent } from './auth-pages/admin-pages/manage-notification/manage-notification.component';
import { AdminStatsComponent } from './auth-pages/admin-pages/admin-stats/admin-stats.component';
import { TermsConditionsComponent } from './public-pages/terms-conditions/terms-conditions.component';
import { HeaderComponent } from './v2/header/header.component';
import { FooterComponent } from './v2/footer/footer.component';
import { ContactUsComponent } from './v2/contact-us/contact-us.component';
import { MyFavoritesComponent } from './v2/my-favorites/my-favorites.component';
import { HomepageComponent } from './v2/homepage/homepage.component';
import { SearchPractitionerComponent } from './v2/practitioner/search-practitioner/search-practitioner.component';
import { PractitionerProfileComponent } from './v2/practitioner/practitioner-profile/practitioner-profile.component';
import { SignupComponent } from './v2/signup/signup.component';
import { LoginComponent } from './v2/login/login.component';
import { LiveClassesComponent } from './v2/live-classes/live-classes.component';

import { PractitionersComponent } from './v2/view/practitioners/practitioners.component';
import { CoursesComponent } from './v2/view/courses/courses.component';
import { ClassesComponent } from './v2/view/classes/classes.component';
import { ArticlesComponent } from './v2/view/articles/articles.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MainBlogComponent } from './v2/blog/main-blog/main-blog.component';
import { PostBlogComponent } from './v2/blog/post-blog/post-blog.component';
import { SupportComponent } from './v2/support/support.component';
import { AboutUsComponent } from './v2/about-us/about-us.component';
import { PractitionerCarouselComponent } from './v2/carousel/practitioner-carousel/practitioner-carousel.component';
import { ArticleCarouselComponent } from './v2/carousel/article-carousel/article-carousel.component';
import { ClassCarouselComponent } from './v2/carousel/class-carousel/class-carousel.component';
import { CourseCarouselComponent } from './v2/carousel/course-carousel/course-carousel.component';
import { AllCourseComponent } from './v2/all-course/all-course.component';
import { AllArticleComponent } from './v2/all-article/all-article.component';
import { SearchComponent } from './v2/search/search.component';
import { LoginViewComponent } from './v2/view/login-view/login-view.component';
import { RatingComponent } from './v2/view/rating/rating.component';
import { DetailComponent } from './v2/all-course/detail/detail.component';
import { ClassDetailComponent } from './v2/live-classes/detail/detail.component';
import { InitiateComponent } from './v2/payment/initiate/initiate.component';
import { JoinFreeLiveClassComponent } from './v2/live-classes/modal/join-free-live-class/join-free-live-class.component';
import { JoinPaidLiveClassComponent } from './v2/live-classes/modal/join-paid-live-class/join-paid-live-class.component';
import { CardFormComponent } from './v2/payment/card-form/card-form.component';
import { SessionComponent } from './v2/session/session.component';
import { PurchaseCourseComponent } from './v2/all-course/purchase-course/purchase-course.component';
import { MyCourseComponent } from './v2/all-course/my-course/my-course.component';
import { SignupViewComponent } from './v2/view/signup-view/signup-view.component';
import { ConfirmModalComponent } from './v2/confirm-modal/confirm-modal.component';
import { DonateComponent } from './v2/practitioner/donate/donate.component';
import { CourseRatingComponent } from './v2/all-course/course-rating/course-rating.component';
import { SessionOverComponent } from './v2/session/session-over/session-over.component';
import { VideoPlayerComponent } from './v2/view/video-player/video-player.component';
import { VideoPlayerCarouselComponent } from './v2/carousel/video-player-carousel/video-player-carousel.component';
import { PractitionerSignupComponent } from './v2/view/practitioner-signup/practitioner-signup.component';
import { ForgotPasswordComponent } from './v2/forgot-password/forgot-password.component';
import { SocialMediaSignupComponent } from './v2/view/social-media-signup/social-media-signup.component';
import { ManageSubscriptionComponent } from './v2/manage-subscription/manage-subscription.component';
import {WelcomeCustomerComponent} from './v1/customer/welcome-customer/welcome-customer.component';
import { ExpertiseViewComponent } from './v1/common/view/expertise-view/expertise-view.component';
import { AuthHeaderComponent } from './v1/common/auth-header/auth-header.component';
import { AuthSideNavComponent } from './v1/common/auth-side-nav/auth-side-nav.component';
import { TestComponent } from './v1/customer/test/test.component';
import { YesnoModalComponent } from './v1/common/view/yesno-modal/yesno-modal.component';
import { LiveClassComponent } from './v1/customer/live-class/live-class.component';
import { BookSessionComponent } from './v2/session/book-session/book-session.component';
import { ManageMySubscriptionComponent } from './v1/customer/manage-subscription/manage-subscription.component';
import { ActiveFollowupFormComponent } from './v1/practitioner/active-followup-form/active-followup-form.component';
import { LiveClassCardComponent } from './v1/common/view/live-class-card/live-class-card.component';
import { LiveClassSesionComponent } from './v1/practitioner/live-class-sesion/live-class-sesion.component';
import { StripeCardViewComponent } from './v1/common/view/stripe-card-view/stripe-card-view.component';
import { AddCourseModalComponent } from './v1/practitioner/courses/add-course-modal/add-course-modal.component';
import { PractitionerCoursesComponent } from './v1/practitioner/courses/courses.component';
import { TncComponent } from './v1/practitioner/tnc/tnc.component';
import { AddLiveClassComponent } from './v1/common/view/add-live-class/add-live-class.component';
import { AddChatSessionComponent } from './v1/common/view/add-chat-session/add-chat-session.component';
import { AppointmentViewComponent } from './v1/common/view/appointment-view/appointment-view.component';
import { ScheduleSessionComponent } from './v1/practitioner/schedule-session/schedule-session.component';

@NgModule({
  declarations: [
    AppComponent,
    // Public pages
    PublicHomeComponent,
    PublicAboutusComponent,
    PublicHowitworksComponent,
    PublicBecomePractitionerComponent,
    PublicContactusComponent,
    PublicFaqComponent,
    PublicNavbarComponent,
    PublicFooterComponent,
    PublicChangePasswordComponent,
    // Authndication pages
    LoginModalComponent,
    SignupModalComponent,
    ForgotPasswordModalComponent,
    AuthHomeComponent,
    AuthNavbarComponent,
    AuthSidebarComponent,
    AuthFooterComponent,
    // Admin Pages
    AdminDashboardComponent,
    // Customer Pages
    CustomerDashboardComponent,
    CustomerProfileComponent,
    BasicInfoComponent,
    FirstPageComponent,
    SecondPageComponent,
    ThirdPageComponent,
    FourthPageComponent,
    FifthPageComponent,
    MySubscriptionComponent,
    CustomerPaymentComponent,
    ViewInterestsComponent,
    BillingInfoComponent,
    BillingHistoryComponent,
    PaymentcreateComponent,
    PaymentUpdateComponent,
    // Practitioner Pages
    PractitionerDashboardComponent,
    DashboardComponent,
    ChangePasswordComponent,
    EditProfileComponent,
    SessiononlyComponent,
    GeneralDashboardComponent,
    AllInterestsComponent,
    PractitionerChatComponent,
    CustomerSessionComponent,
    CustomerChatComponent,
    NotificationsComponent,
    PractitionerSessionsComponent,
    PractionerSessionComponent,
    AddSessionComponent,
    ConfirmSessionComponent,
    AddExpertiseComponent,
    ViewExpertiseComponent,
    PaymentInfoComponent,
    BankDetailsComponent,
    WithdrawHistoryComponent,
    ConnectAccountComponent,
    DocumentVerificationComponent,
    PractitionerScheduleComponent,
    ProfilePictureModalComponent,
    DeleteChatComponent,
    AvailablePatientComponent,
    WeekSelectionComponent,
    BarChartComponent,
    EditScheduleComponent,
    CustomerBookingComponent,
    NgbdModalContent,
    PractitionerModalContent,
    BookTimePipe,
    LimitToPipe,
    AddNewCustomerPaymentComponent,
    ReportPractitionerComponent,
    BlockCustomerComponent,
    SwitchPractitionerComponent,
    AddPractionerComponent,
    AddCustomerComponent,
    PractitionerSessionComponent,
    FollowUpFormComponent,
    StatsComponent,
    SessionDetailsComponent,
    LineChartComponent,
    DoughnutChartComponent,

    CustomerStatsComponent,
    MoodCalendarComponent,
    ReviewPractitionerComponent,
    AddSubscriptionPlanComponent,
    AddInterestComponent,
    DeleteInterestComponent,
    InterestListComponent,
    CustomerAboutusComponent,
    PrivacyPolicyComponent,
    HelpFaqComponent,
    TermsConditionComponent,
    CustomerPractitionerProfileComponent,
    AdminPractitionerListComponent,
    AdminCustomerListComponent,
    ContactAdminComponent,
    ClientNotesComponent,
    SelectInterestComponent,
    CustomerChatArchiveComponent,
    PractitionerChatArchiveComponent,
    SendMessageToPractitonerComponent,
    SendMessageToCustomerComponent,
    ManageNotificationComponent,
    AdminStatsComponent,
    TermsConditionsComponent,
    HeaderComponent,
    FooterComponent,
    ContactUsComponent,
    MyFavoritesComponent,
    HomepageComponent,
    SearchPractitionerComponent,
    PractitionerProfileComponent,
    SignupComponent,
    LoginComponent,
    LiveClassesComponent,
    PractitionersComponent,
    CoursesComponent,
    PractitionerCoursesComponent,
    ClassesComponent,
    ArticlesComponent,
    MainBlogComponent,
    PostBlogComponent,
    SupportComponent,
    AboutUsComponent,
    PractitionerCarouselComponent,
    ArticleCarouselComponent,
    ClassCarouselComponent,
    CourseCarouselComponent,
    AllCourseComponent,
    AllArticleComponent,
    SearchComponent,
    LoginViewComponent,
    RatingComponent,
    DetailComponent,
    ClassDetailComponent,
    InitiateComponent,
    JoinFreeLiveClassComponent,
    JoinPaidLiveClassComponent,
    CardFormComponent,
    SessionComponent,
    PurchaseCourseComponent,
    MyCourseComponent,
    SignupViewComponent,
    ConfirmModalComponent,
    DonateComponent,
    CourseRatingComponent,
    SessionOverComponent,
    VideoPlayerComponent,
    VideoPlayerCarouselComponent,
    PractitionerSignupComponent,
    ForgotPasswordComponent,
    SocialMediaSignupComponent,
    ManageSubscriptionComponent,
    ManageMySubscriptionComponent,
    WelcomeCustomerComponent,
    ExpertiseViewComponent,
    AuthHeaderComponent,
    AuthSideNavComponent,
    TestComponent,
    YesnoModalComponent,
    LiveClassComponent,
    BookSessionComponent,
    ActiveFollowupFormComponent,
    LiveClassCardComponent,
    LiveClassSesionComponent,
    StripeCardViewComponent,
    AddCourseModalComponent,
    TncComponent,
    AddLiveClassComponent,
    AddChatSessionComponent,
    AppointmentViewComponent,
    ScheduleSessionComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxSpinnerModule,
    PerfectScrollbarModule,
    NgxIntlTelInputModule,
    SelectDropDownModule,
    SocialLoginModule,
    PopoverModule.forRoot(),
    PickerModule,
    MomentModule,
    ChartsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    NgxSliderModule,
    TabsModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 8000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true,
      progressBar: true,
      progressAnimation: 'increasing',
    }),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(), 
    TimepickerModule.forRoot(),
    RatingModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    CarouselModule.forRoot(),
    InfiniteScrollModule,
    DragScrollModule,
    NgxSocialShareModule,
    PlyrModule,
    TypeaheadModule.forRoot()
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: ScoailLoginConfig.CREDENTIALS
      } as SocialAuthServiceConfig,
    }, AuthGuardService, AuthService, ApiService, ThirdPApiService, GeneralService, Title, ContactUsService,
    PractitionerSessionService, SubscriptionPlansService, DataService, PractitionerScheduleService, ChatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
