import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { AuthService } from 'src/app/services/AuthService';
import { ApiService } from 'src/app/services/ApiService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-public-faq-page',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
  providers: [AuthService]
})
export class PublicFaqComponent implements OnInit {
  public _token: any;
  breadCrumbItems: Array<{}>;
  isCollapsed: boolean;
  selectedItem: boolean = true;
  clickedItem: boolean = true;
  sub: Subscription;
  faqList: any = [];

  constructor(
    private router: Router,
    private authService: AuthService,
    private service: ApiService,
    private toastr: ToastrService,
    private loader: NgxSpinnerService,
  ) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(["/customer/welcome-customer"]);
    }
    this.loadData("Customer");
  }
  loadData(arg){
    this.service.getFAQ(arg).subscribe((response: any) => {
      this.faqList = response;
    },(error) => {
      this.toastr.error(error.statusText);
    });
  }
  callCustomer() {
    this.selectedItem = true;
    this.loadData("Customer");
  }
  callPractitioner() {
    this.selectedItem = false;
    this.loadData("Practitioner");
  }
  collapseIcon(event) {
    if (event.currentTarget.className.indexOf("fas fa-arrow-down") > -1) {
      event.currentTarget.className = "fas fa-arrow-up";
    } else if (event.currentTarget.className.indexOf("fas fa-arrow-up") > -1) {
      event.currentTarget.className = "fas fa-arrow-down"
    }
  }
}
