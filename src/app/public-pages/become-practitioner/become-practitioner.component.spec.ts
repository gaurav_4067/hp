import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicBecomePractitionerComponent } from './become-practitioner.component';

describe('PublicBecomePractitionerComponent', () => {
  let component: PublicBecomePractitionerComponent;
  let fixture: ComponentFixture<PublicBecomePractitionerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicBecomePractitionerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicBecomePractitionerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
