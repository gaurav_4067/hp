import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/AuthService';

@Component({
  selector: 'app-public-become-practitioner-page',
  templateUrl: './become-practitioner.component.html',
  styleUrls: ['./become-practitioner.component.scss'],
  providers: [AuthService]
})
export class PublicBecomePractitionerComponent implements OnInit {

  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(["/customer/welcome-customer"]);
    }
  }
}
