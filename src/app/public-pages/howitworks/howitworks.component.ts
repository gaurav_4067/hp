import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/AuthService';
import { ApiService } from 'src/app/services/ApiService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-public-howitworks-page',
  templateUrl: './howitworks.component.html',
  styleUrls: ['./howitworks.component.scss'],
  providers: [AuthService]
})
export class PublicHowitworksComponent implements OnInit {
  public _token: any;
  isContent: string;
  signupModel: any;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    public apiService: ApiService,
    public modalService: NgbModal,
  ) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(["/customer/welcome-customer"]);
    }
  }
  openSignupModal(modalContent:any, isWhere:string) {
    this.isContent = isWhere;
    this.signupModel = this.modalService.open(modalContent, {centered: true, scrollable: true,windowClass: 'modal-holder'});
  }
  closeSignupModal(Event:any){
    this.signupModel.close();
  }
}
