import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddCourseModalComponent } from './add-course-modal/add-course-modal.component';
@Component({
  selector: 'app-add-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class PractitionerCoursesComponent implements OnInit {

  constructor(private modalService:NgbModal) { }

  ngOnInit(): void {
  }

  openMe(){
    let modalRef = this.modalService.open(AddCourseModalComponent, { scrollable: true, centered: true, ariaLabelledBy: 'signup-confirm-title', size: 'lg', windowClass: 'confirm-modal' });
  }

}
