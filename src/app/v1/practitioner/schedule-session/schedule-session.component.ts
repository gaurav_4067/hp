import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schedule-session',
  templateUrl: './schedule-session.component.html',
  styleUrls: ['./schedule-session.component.scss']
})
export class ScheduleSessionComponent implements OnInit {
  minDate: Date = new Date();
  calenderDate: Date = new Date();
  bsValue: Date = new Date();

  constructor() { }

  ngOnInit(): void {
  }

  onValueChange(e){
    console.log(e);
  }
}
