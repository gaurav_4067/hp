import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveFollowupFormComponent } from './active-followup-form.component';

describe('ActiveFollowupFormComponent', () => {
  let component: ActiveFollowupFormComponent;
  let fixture: ComponentFixture<ActiveFollowupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActiveFollowupFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveFollowupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
