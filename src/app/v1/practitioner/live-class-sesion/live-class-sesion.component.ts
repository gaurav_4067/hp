import { Component, OnInit } from '@angular/core';
enum CardType { "UpcomingLiveClass", 'CompletedLiveClass'};
enum UserType { "Customer", 'Practitioner'};
@Component({
  selector: 'app-live-class-sesion',
  templateUrl: './live-class-sesion.component.html',
  styleUrls: ['./live-class-sesion.component.scss']
})
export class LiveClassSesionComponent implements OnInit {
  showUpcomingSession:boolean=true;
  cardType:CardType;
  userType:UserType;
  constructor() { }

  ngOnInit(): void {
  }

}
