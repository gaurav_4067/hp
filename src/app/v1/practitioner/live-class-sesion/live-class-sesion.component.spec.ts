import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveClassSesionComponent } from './live-class-sesion.component';

describe('LiveClassSesionComponent', () => {
  let component: LiveClassSesionComponent;
  let fixture: ComponentFixture<LiveClassSesionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveClassSesionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveClassSesionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
