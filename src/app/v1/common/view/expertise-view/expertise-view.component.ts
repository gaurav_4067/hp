import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-expertise-view',
  templateUrl: './expertise-view.component.html',
  styleUrls: ['./expertise-view.component.scss']
})
export class ExpertiseViewComponent implements OnInit {
  @Input() showRemove:boolean=false;
  @Input() interest:any;
  @Input() interest_title:string="";
  @Input() description:string="";
  @Input() cover_photo:string="";
  @Input() checked:boolean=false;
  selectedInterest:any;
  @Output() getUpdatedExpertise = new EventEmitter();
  customer: any = {};
  // interestSub: Subscription;
  // interestData = [];

  constructor(public loader:NgxSpinnerService,
    public apiService:ApiService) { }

  ngOnInit(): void {
  }

  selectInterest(interest:any){
    this.selectedInterest=interest;
    console.log(this.selectedInterest);
  }

  deleteInterest() {
    const payload = JSON.parse(localStorage.getItem('logedin_data'));
    payload.interest = payload.interest.filter(item => item !== this.interest.interest);
    this.loader.show();
    this.apiService
      .updateBasicInfo(payload, payload.id)
      .subscribe((response: any) => {
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response)
        );
        debugger;
        //this.customer.interest[0] = response.interest;
        this.getUpdatedExpertise.emit();
        //this.getInterestData();
        this.loader.hide();
      });
  }

  // getCustomerInterestList() {
  //   // const interests = this.customer.interest[0] || [];
  //   let payload = JSON.parse(localStorage.getItem('logedin_data'));
  //   const interests=payload.interest[0];
  //   let param=interests.map(str => `interest_list=${str}`).join('&');
  //   this.apiService.getCustomerInterestList(param).subscribe((data: any) => {
  //     this.interestData = data.interest_list;
  //   });
  // }
  
}
