import { getLocaleDateTimeFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { SubscriptionPlansService } from 'src/app/services/SubscriptionPlansService';
import { ConfirmModalComponent } from 'src/app/v2/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-yesno-modal',
  templateUrl: './yesno-modal.component.html',
  styleUrls: ['./yesno-modal.component.scss']
})
export class YesnoModalComponent implements OnInit {
  noButtontext:string="No";
  yesButtontext:string="Yes";
  message:string="";
  heading:string="";
  yesButtonSelected:boolean=false;
  noButtonSelected:boolean=false;
  subscriptionDetails:any;
  constructor(
    public modalService:NgbModal,
    public authService:AuthService,
    public toaster:ToastrService,
    public generalService:GeneralService,
    public loader:NgxSpinnerService,
    public planService:SubscriptionPlansService,
    public router:Router
  ) { }
  
  ngOnInit(): void {
  }

  yes(){
    this.yesButtonSelected=true;
    this.noButtonSelected=false;
  }
  no(){
    this.yesButtonSelected=false;
    this.noButtonSelected=true;
  }
  confirm(){
    if(this.yesButtonSelected){
      //cancel subscription
      this.cancelSubscription();
    }
    else{
      //close modal
      this.modalService.dismissAll();
    }
  }

  cancelSubscription() {
    this.loader.show();
    
    const payload = {
        offer_id: this.subscriptionDetails?.offer_id,
        plan_id: this.subscriptionDetails?.plan_id,
        unsubscribe_date: new Date(),
        reason: '',
        customer: JSON.parse(localStorage.getItem("logedin_data")).id,
        parent_id: null
    };
    this.planService.removeSubscription(payload, this.subscriptionDetails?.id)
    .subscribe((sub)=>{
        console.log('cancelSubscription', sub);
        this.loader.hide();
        this.toaster.success('Subscription Cancelled Successfully!');
        let logedinData = JSON.parse(localStorage.getItem("logedin_data"));
        logedinData.is_subscribed = false;  

        localStorage.setItem(
            `logedin_data`,
            JSON.stringify(logedinData)
        );
        this.modalService.dismissAll();
        
        this.generalService.getUserSubscription("Unsubscribed");
        this.router.navigate(['home']);
        let modalRef = this.modalService.open(ConfirmModalComponent, { scrollable: true, centered: true, ariaLabelledBy: 'signup-confirm-title', size: 'lg', windowClass: 'confirm-modal' });
        modalRef.componentInstance.closeButtonText = "Back to Dashboard";
        modalRef.componentInstance.showButton=true;
        modalRef.componentInstance.isDefaultBtn=true;
        modalRef.componentInstance.heading = "You canceled your Healplace subscription!";
        modalRef.componentInstance.message = "Your account will be cancelled on "+Date()+", when your current paid period ends. You can use all the features of your subscription until that date.";
        
        
    }, (error)=>{
        console.log('cancelSubscription error', error);
        this.loader.hide();
        this.toaster.error(error.statusText);
    });
}
}
