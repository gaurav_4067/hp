import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-live-class',
  templateUrl: './add-live-class.component.html',
  styleUrls: ['./add-live-class.component.scss']
})
export class AddLiveClassComponent implements OnInit {
  step:number=1;
  selectedExpertise:any;
  loginData:any;
  expertiseList:string[];
  selectedDuration:any;
  minDate: Date = new Date();
  calenderDate: Date = new Date();
  bsValue: Date = new Date();
  durationList=['1','3','3'];
  constructor(public modalService:NgbModal) { }

  ngOnInit(): void {
    this.loginData = JSON.parse(localStorage.getItem("logedin_data"));
    debugger;
    //this.expertiseList=['asas','asasdsd','sdsdd','dsds'];
    this.expertiseList=this.loginData.expertise;
  } 

  onSelectExpertise(event: any): void {
    this.selectedExpertise = event.item;
    //this.loadState(event.item.id);
  }

  typeaheadOnBlurExpertise(event:any):void{
    if (!!event.item.name) {
      this.selectedExpertise=event.item;
      //this.loadState(event.item.id);
    }
    else {
     // this.user.SelectedCountry = new Location();
    }
    
  }

  onValueChange(e){

  }

}
