import { Component, Input, OnInit } from '@angular/core';
enum CardType { "UpcomingLiveClass", 'CompletedLiveClass'};
enum UserType { "Customer", 'Practitioner'};

@Component({
  selector: 'app-live-class-card',
  templateUrl: './live-class-card.component.html',
  styleUrls: ['./live-class-card.component.scss']
})
export class LiveClassCardComponent implements OnInit {
  @Input() cardType:string="";
  @Input() userType:string="";
  constructor() { }

  ngOnInit(): void {
  }

}
