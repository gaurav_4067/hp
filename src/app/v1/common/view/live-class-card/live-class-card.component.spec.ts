import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveClassCardComponent } from './live-class-card.component';

describe('LiveClassCardComponent', () => {
  let component: LiveClassCardComponent;
  let fixture: ComponentFixture<LiveClassCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveClassCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveClassCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
