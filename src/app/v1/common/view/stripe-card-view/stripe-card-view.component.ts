import { Component, Input, OnInit } from '@angular/core';
enum CardType { "UpcomingLiveClass", 'CompletedLiveClass'};
enum UserType { "Customer", 'Practitioner'};
@Component({
  selector: 'app-stripe-card-view',
  templateUrl: './stripe-card-view.component.html',
  styleUrls: ['./stripe-card-view.component.scss']
})
export class StripeCardViewComponent implements OnInit {
  @Input() cardType:string="CompletedLiveClass";
  @Input() userType:string="Customer";
  constructor() { }

  ngOnInit(): void {
  }

}
