import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StripeCardViewComponent } from './stripe-card-view.component';

describe('StripeCardViewComponent', () => {
  let component: StripeCardViewComponent;
  let fixture: ComponentFixture<StripeCardViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StripeCardViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StripeCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
