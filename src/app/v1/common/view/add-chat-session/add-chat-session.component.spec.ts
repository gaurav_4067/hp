import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChatSessionComponent } from './add-chat-session.component';

describe('AddChatSessionComponent', () => {
  let component: AddChatSessionComponent;
  let fixture: ComponentFixture<AddChatSessionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddChatSessionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChatSessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
