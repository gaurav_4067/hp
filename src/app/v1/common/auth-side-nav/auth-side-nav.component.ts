import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SocialAuthService } from 'angularx-social-login';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';

@Component({
  selector: 'app-auth-side-nav',
  templateUrl: './auth-side-nav.component.html',
  styleUrls: ['./auth-side-nav.component.scss']
})
export class AuthSideNavComponent implements OnInit {
  public _token: any;
  hideHeader: any = true;
  model: any;
  isContent: any;
  userData: any;
  isGroup: any;
  logedinData: any;
  
  constructor(
    private router: Router,
    private authService: AuthService,
    public apiService: ApiService,
    private modalService: NgbModal,
    private socialAuthService: SocialAuthService,
    private generalService: GeneralService,
  ) {}

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(["/dashboard"]);
    }
    this.generalService.userSubscription.subscribe((response: any) => {
      if (response == "Subscribed" || response == "Unsubscribed") {
        this.logedinData = this.generalService.setSubscription(response);
      }
    });

    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.isGroup = this.userData?.groups[0];
    if(this.isGroup =="Customer"){
      if (this.logedinData.is_subscribed) {
        this.router.navigateByUrl('/welcome-customer');
      }
      else {
        this.router.navigateByUrl('/home');
      }
    }
    else if(this.isGroup =="Practitioner"){
      //this.router.navigateByUrl('/welcome-practitioner');
    }
    else if (this.isGroup == "Admin") {
      //this.router.navigateByUrl("/welcome-admin");
    }
    // else {
    //   this.router.navigateByUrl("/home");
    // }
  }
  openModal(modalContent:any, isWhere:string) {
    this.isContent = isWhere;
    this.model = this.modalService.open(modalContent, {windowClass: 'modal-holder'});
  }
  closeModal(Event:any){
    this.model.close();
  }
  closeAllModal() {
    this.modalService.dismissAll();
  }
  logout(){
    this.apiService
      .getAppLogout()
      .subscribe((response: any) => {
        console.log(response.detail);
      });
      this.closeAllModal();
      this.socialAuthService.signOut();
      localStorage.removeItem("access_token");
      localStorage.removeItem("user_data");
      localStorage.removeItem("refresh_token");
      localStorage.removeItem("logedin_data");
      window.location.reload();
  }
}
