import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthSideNavComponent } from './auth-side-nav.component';

describe('AuthSideNavComponent', () => {
  let component: AuthSideNavComponent;
  let fixture: ComponentFixture<AuthSideNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthSideNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthSideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
