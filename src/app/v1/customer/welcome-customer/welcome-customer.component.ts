import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import {CustomerInfo, InterestList} from './customer.model';
import { Options } from '@angular-slider/ngx-slider';

@Component({
  selector: 'app-welcome-customer',
  templateUrl: './welcome-customer.component.html',
  styleUrls: ['./welcome-customer.component.scss']
})
export class WelcomeCustomerComponent implements OnInit {
  @ViewChild(DragScrollComponent) ds: DragScrollComponent;
  value: number = 10;
  options: Options = {
    showTicksValues: true,
    floor: 0,
    ceil: 10
  };
  page: any = 1;
  step=1;
  logedinData: any;
  userData: any;
  name:any = localStorage.getItem("nick_name");
  
  interestData = [];
  slideShowData = [];
  interest:any = [];
  isPage:number = 0;
  customer:CustomerInfo;
  interests:Array<InterestList>;
  constructor(
    public apiService: ApiService,
    private http: HttpClient,
    public loader : NgxSpinnerService,
    public toastr : ToastrService,
  ) {
    this.customer=new CustomerInfo();
    this.interests=new Array<InterestList>();
   }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (this.logedinData && this.logedinData.nick_name) {
      this.customer.name = this.logedinData.nick_name;
    }
    if(localStorage.getItem("interest")){
      this.interest = JSON.parse(localStorage.getItem("interest"));
    }
    this.loadData();
  }
  submitStep1(step,isValid){

    if(isValid){
      this.step=step;
    }
  }
  nameChange(){
    this.customer.name=this.customer.name.trim();
    localStorage.setItem("nick_name", this.customer.name);
  }

  submitStep2(){
    this.interestData.forEach(chunk => {
      chunk.forEach(element => {
        if(element.checked){
          this.step=3;
        }
      });
    });
  }

  save(){
    
    let payload = this.logedinData;
    payload["nick_name"]=this.customer.name;
    let flatInterestDAta= [].concat(...this.interestData);

    let selectedInterestData=flatInterestDAta.filter(i=>i.checked==true);
    payload["interest"] = selectedInterestData.map(function (elem) {
      return elem.interest;
    }).join(",");
    console.log(payload);
      payload["is_news_subscribe"] = this.customer.is_news_subscribe;
      payload["is_sms_subscribe"] = this.customer.is_sms_subscribe;
      payload["is_email_subscribe"]=this.customer.is_email_subscribe;
      payload["notification_type"]=this.customer.notification_type;
      payload["reminder_type"]="24" ?this.customer.reminder_type : 1;
      payload["moods"]=this.value;
    

    let user_name = {
      "first_name": this.userData["user"]["first_name"],
      "last_name": this.userData["user"]["last_name"],
      "email": this.userData["user"]["email"],
    };

    let userAddress = {
      "address_line1": null,
      "address_line2": "N/A",
      "address_line3": "N/A",
      "addrees_line4": "N/A",
      "zipcode": null,
      "address_type": null,
      "city": null,
    };
    if(this.userData["city_id"]){
      userAddress.city = this.userData["city_id"];
    }else{
      userAddress.city = null;
    }
    payload["address"] = userAddress;
    payload["user_id"] = user_name;
    delete payload["is_news_subscribe"];
    delete payload["modified"];
    delete payload["created"];
    console.log(payload);
    this.loader.show();
    this.apiService
    .updateProfileInfo(payload, payload["id"])
    .subscribe((response: any) => {
      console.log(response);
      localStorage.setItem(
        `logedin_data`,
        JSON.stringify(response)
      );
      // this.loggedUpdate();
      // this.toastr.success('Basic information updated successfully');
      this.loader.hide();
      this.submit();
    });
    //payload["interest"] =
  }

  moveLeft() {
    this.ds.moveLeft();
  }
  
  moveRight() {
    this.ds.moveRight();
  } 

  // Second Page Code Start
  loadData(){
    this.loader.show();
    this.apiService
    .getIntrest()
    .subscribe((res: any) => {
      //let response=res.interest_list;
      this.interests=res.interest_list;
      this.interests.forEach(element => {
        element.checked=false;
      });
      console.log(this.interests);
      let mapedData = [];
      let iList=Array<Array<InterestList>>();
     // let j,k;
    //   for (j = 0, k = this.interests.length; j < k; j += 4) {
    //     iList.concat(this.interests.slice(j, j + 4));
    //     // do whatever
    //   }
    // console.log(iList);
      for (var i = 0; i < this.interests.length; i += 4) {
        let temp = [];
        temp.push(this.interests[i]);
        if(this.interests[i+1]){
          temp.push(this.interests[i+1]);
        }
        if(this.interests[i+2]){
          temp.push(this.interests[i+2]);
        }
        if(this.interests[i+3]){
          temp.push(this.interests[i+3]);
        }
        mapedData.push(temp);
      }
      console.log(mapedData);
      this.slideShowData = mapedData;
      
      this.interestData = mapedData;
      this.loader.hide();
    });
  }

  submit(){
    let payload = {
      "mood": localStorage.getItem("mood"),
      "customer": this.logedinData["user_id"]["id"],
      description: "",
    };
    console.log(payload);
    //return;
    this.loader.show();
    this.apiService
    .updateMoodInfo(payload, this.logedinData["id"])
    .subscribe((response: any) => {
      console.log(response);
      this.logedinData["moods"] = [{
        mood: response.mood
      }];
      localStorage.setItem(
        `logedin_data`,
        JSON.stringify(this.logedinData)
      );
      // this.toastr.success('Basic information updated successfully');
      this.loader.hide();
      this.loadProfileData();
    });
  }
  loadProfileData(){
    this.loader.show();
    this.apiService
      .pkIdUpdate(this.userData.groups[0].toLowerCase(),"get")
      .subscribe((response: any) => {
        console.log(response);
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response[0])
        );
        this.loader.hide();
        this.loggedUpdate();
      });
  }
  loggedUpdate(){
    this.loader.show();
    let payload = {
      is_previously_logged_in: true
    }
    this.apiService
    .previousLoggedUpdate(payload,this.userData["user"]["pk"])
    .subscribe((response: any) => {
      console.log(response);
      this.userData["is_previously_logged_in"] = true;
      localStorage.setItem(
        `user_data`,
        JSON.stringify(this.userData)
      );
      this.clearStore();
      this.loader.hide();
      this.toastr.success('Basic information updated successfully');
      window.location.reload();
    });
  }
  clearStore(){
    localStorage.removeItem("nick_name");
    localStorage.removeItem("interest");
    localStorage.removeItem("is_news_subscribe");
    localStorage.removeItem("is_sms_subscribe");
    localStorage.removeItem("is_email_subscribe");
    localStorage.removeItem("notification_type");
    localStorage.removeItem("reminder_type");
    localStorage.removeItem("mood");
  }


  // chooseMe(intrestID: any){
  //   let refContent = document.getElementById(`${intrestID}`) as HTMLInputElement;
  //   if(refContent.className == "col-md-8 color-fix fix-shadow active"){
  //     refContent.className = "col-md-8 color-fix";
  //     if(this.interest.find(obj=>obj == intrestID)){
  //       let removed = this.interest.filter(obj=>obj != intrestID);
  //       this.interest = removed;
  //     }
  //     localStorage.removeItem("interest");
  //     localStorage.setItem("interest", JSON.stringify(this.interest));
  //   }else{
  //     refContent.className = "col-md-8 color-fix fix-shadow active";
  //     if(this.interest.length){
  //       if(!this.interest.find(obj=>obj == intrestID)){
  //         this.interest.push(intrestID);
  //       }
  //     }else{
  //       this.interest.push(intrestID);
  //     }
  //     localStorage.removeItem("interest");
  //     localStorage.setItem("interest", JSON.stringify(this.interest));
  //   }
  // }
  // slideForward(){
  //   this.isPage = this.isPage + 1;
  //   setTimeout(() => {
  //     this.interestData = this.slideShowData[this.isPage];
  //   }, 100);
  // }
  // slideBackward(){
  //   this.isPage = this.isPage - 1;
  //   setTimeout(() => {
  //     this.interestData = this.slideShowData[this.isPage];
  //   }, 100);
  // }
  // filterIfThere(list: any){
  //   if(this.interest.find(obj=>obj == list.interest)){
  //     return true;
  //   }else{
  //     return false; 
  //   }
  // }
  // checkIfLetMove(){
  //   if(this.isPage == this.slideShowData.length - 1){
  //     return false;
  //   }else{
  //     return true;
  //   }
  // }

  // // Second Page Code End

  // slideUponMe(isPAGE: any){
  //   if(isPAGE != 1 && isPAGE != 2){
  //     let getData = localStorage.getItem("interest");
  //     let outData = getData ? JSON.parse(getData) : undefined;
  //     if(outData && outData.length){
  //       this.page = isPAGE;
  //     }else{
  //       this.toastr.warning("Choose At Least One Interest");
  //     }
  //   }else{  
  //     this.page = isPAGE;
  //   }
  // }

  // loggedUpdate(){
  //   this.loader.show();
  //   let payload = {
  //     is_previously_logged_in: true
  //   }
  //   this.apiService
  //   .previousLoggedUpdate(payload,this.logedinData["id"])
  //   .subscribe((response: any) => {
  //     console.log(response);
  //     this.userData["is_previously_logged_in"] = true;
  //     localStorage.setItem(
  //       `user_data`,
  //       JSON.stringify(this.userData)
  //     );
  //     localStorage.setItem(
  //       `logedin_data`,
  //       JSON.stringify(response)
  //     );
  //     this.clearStore();
  //     this.loader.hide();
  //     window.location.reload();
  //   });
  // }

  // clearStore(){
  //   localStorage.removeItem("nick_name");
  //   localStorage.removeItem("interest");
  //   localStorage.removeItem("is_news_subscribe");
  //   localStorage.removeItem("is_sms_subscribe");
  //   localStorage.removeItem("is_email_subscribe");
  //   localStorage.removeItem("notification_type");
  //   localStorage.removeItem("reminder_type");
  //   localStorage.removeItem("mood");
  // }

  selectExpertise(expertise:InterestList){
    expertise.checked=!expertise.checked;
    console.log(this.interestData);
    //this.interestData.filter(i=>i.)
  }
}
