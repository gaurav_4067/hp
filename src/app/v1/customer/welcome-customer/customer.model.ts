export class  CustomerInfo{
    public name:string;
    public notification_type:boolean=false;
    public reminder_type:boolean=false;
    public is_email_subscribe:boolean=false;
    public is_sms_subscribe:boolean=false;
    public is_news_subscribe:boolean=false;
    public 
}

export class InterestList{
    public cover_photo: string;
    public description: string;
    public id:string;
    public interest: string;
    public checked:boolean=false;
}