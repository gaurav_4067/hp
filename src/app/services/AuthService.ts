import { EventEmitter, Injectable, Output } from "@angular/core";

@Injectable()
export class AuthService {
  @Output() changeVideo: EventEmitter<any> = new EventEmitter();
  @Output() getLoggedInName: EventEmitter<any> = new EventEmitter();
  @Output() notifiedPurchaseCourseEvent: EventEmitter<any> = new EventEmitter();
  
  public isAuthenticated(): Boolean {
    const token = localStorage.getItem("access_token");
    if(token){
      //this.getLoggedInName.emit(true);
    }
    // Check whether the token is expired and return
    // true or false
    return token ? true : false;
  }
  
  public notifiedChangeVideo(){
    return this.changeVideo.emit(true);
  }

  public notifiedHeader(){
    return this.getLoggedInName.emit(true);
  }

  public notifiedPurchaseCourse(){
    return this.notifiedPurchaseCourseEvent.emit(true);
  }

  public getToken() {
    return localStorage.getItem("access_token");
  }

  public getUserName() {
    let userData = JSON.parse(localStorage.getItem("user_data"));
    return userData["user"]["username"];
  }
  public getUserEmail() {
    let userData = JSON.parse(localStorage.getItem("user_data"));
    return userData["user"]["email"];
  }
  public getUserGroup() {
    let userData = JSON.parse(localStorage.getItem("user_data"));
    return userData["groups"][0];
  }

  public getUser() {
    return JSON.parse(localStorage.getItem("user_data"))["user"];
  }
}
