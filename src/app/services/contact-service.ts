import { Injectable } from "@angular/core";
import {  HttpClient } from '@angular/common/http';
import { Config } from '../config/config';

@Injectable()
export class ContactUsService {
    constructor(
        private http: HttpClient,
      ) { }

    public contact(InputData){
        console.log(InputData);
        return this.http.post<{success: object}>(`${Config.ADMIN_BASE_URL}ContactUs/`, InputData);
      }
}