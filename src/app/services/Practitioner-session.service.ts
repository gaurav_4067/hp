import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Config } from '../config/config';

@Injectable()
export class PractitionerSessionService {
  constructor(
    private http: HttpClient,
  ) { }

  public getpractitionersessions() {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}DashBoard/`);
  }

  public approvecustomersession(InputData, appoinment_id) {
    return this.http.put<{ success: object }>(`${Config.CUSTOMER_BASE_URL}ChangeSessionStatus/${appoinment_id}/`, InputData);
  }

  public postvideosession(params) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}VideoSession/`, params );
  }
}
