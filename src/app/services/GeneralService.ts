import { Injectable, EventEmitter } from "@angular/core";
import { ImageConfig} from '../config/config';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class GeneralService {
  title = null;
  showProfile=false;
  selectedPractitionerDetail:any={};
  selectedPractitioner:any="";
  isPractitionerSwitched = false;
  public planAndDiscountSubject = new Subject<any>();

  emitter: EventEmitter<string> = new EventEmitter();
  public changeModule(type: string) {
    this.emitter.emit(type);
  }

  userSubscription: EventEmitter<string> = new EventEmitter();
  public getUserSubscription(data) {
    this.userSubscription.emit(data);
  }

  public setSubscription(isArg){
    let logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (isArg == "Subscribed"){
      logedinData.is_subscribed = true;
      
    } else {
      logedinData.is_subscribed = false;  
    }
    localStorage.setItem(
      `logedin_data`,
      JSON.stringify(logedinData)
    );
    return logedinData;
  }
  public routeName(isGroup){
    
    switch(isGroup){  
      case 'Admin':  
        return "customer/welcome-admin"; 
      case 'Customer':  
        return "customer/welcome-customer";  
      default:  
        return "";
    }
  }

  public numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;  
  }

  public isCustomer(){
    const userData = JSON.parse(localStorage.getItem("user_data"));
    return userData?.groups[0].toLowerCase() === 'customer' ? true : false;
  }
  public setTitle(title){
    console.log(title)
    this.title = title;
  }

  public setPractitionerSwitch(flag){
    this.isPractitionerSwitched = flag;
  }

  public getAdminImageBaseUrl(url){
    return ImageConfig.ADMIN_BASE_URL + url;
  }

  public getPractiotionerImageBaseUrl(url){
    return ImageConfig.PRACTITIONER_BASE_URL + url;
  }

  public getCustomerImageBaseUrl(url){
    return ImageConfig.CUSTOMER_BASE_URL + url;
  }
}
