import { Config } from '../config/config';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PractitionerScheduleService {

  constructor(
    private http: HttpClient,
  ) { }

  public saveAvailablity(data) {
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}create-practitioner-availability/`, data);
  }

  public duplicateSchedule(data) {
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}CopySchedules/`, data);
  }

  public changeAvailable(payload, pkID) {
    return this.http.patch<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}edit-practitioner/${pkID}/`, payload);
  }

  public getPractitioner(pkID) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}practitioner/${pkID}/`);
  }
}