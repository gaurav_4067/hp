import { Config } from '../config/config';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {

  constructor(
    private http: HttpClient,
  ) { }

  public getFAQ(isFrom) {
    return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}FAQ/?type=${isFrom}`);
  }

  public getCustomerSubscriptionDetails(customerId) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}GetSubscriptionDetails/?customer_id=${customerId}`);
  }

  public getCustomerPlanDetails(plan_id) {
    return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}GetPlanDetails/?plan_id=${plan_id}`);
  }

  public getAdminDashboardDetails() {
    return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}Admindashboard/`);
  }

  public getAdminStats() {
    return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}stats/`);
  }

  public getPractitionerAvailabilityList(pid, from, duration) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}ViewPractitionerAvailability/?practitioner_id=${pid}&start_date=${from}&duration=${duration}`);
  }

  public chooseSessionType(pid, from, to, totalSession, totalBookedSession) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}ChooseSessionType/${pid}/${from}/${to}/${totalSession}/${totalBookedSession}/`);
  }

  public chooseTimeSlot(cid, pid, sid, date, startTime) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}ChooseTimeSlot/${cid}/${pid}/${sid}/${date}/${startTime}/`);
  }

  public bookAppointment(payload) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}BookAppointment/`, payload);
  }

  public customerpaymentdetails() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}customerpaymentdetails/`);
  }

  public stripeCheckout(payload) {
    return this.http.post<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}stripe-checkout/`, payload);
  }

  public confirmingAppointment(payload) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}CustomerPayment/`, payload);
  }
  
  public getAppLogout() {
    return this.http.post<{ success: object }>(`${Config.AUTH_BASE_URL}logout/`, {});
  }
  public login(payload) {
    return this.http.post<{ success: object }>(`${Config.AUTH_BASE_URL}login/`, payload);
  }
  public sociaLogin(payload, isSocial) {
    return this.http.post<{ success: object }>(`${Config.AUTH_BASE_URL}${isSocial}/`, payload);
  }
  public forgotPassword(payload) {
    return this.http.post<{ success: object }>(`${Config.AUTH_BASE_URL}password/reset/`, payload);
  }
  public signup(payload) {
    return this.http.post<{ success: object }>(`${Config.AUTH_BASE_URL}register/`, payload);
  }
  public changeForgotResetPassword(payload, userId, userToken) {
    return this.http.post<{ success: object }>(`${Config.BASE_URL}auth/uid=${userId}&token=${userToken}/`, payload);
    // return this.http.post<{ success: object }>(`https://dev.healplace.co/reset-password/${userId}/${userToken}/`, payload);
  }
  public changePassword(payload) {
    return this.http.post<{ success: object }>(`${Config.AUTH_BASE_URL}password/change/`, payload);
  }

  public getIntrest() {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}customer-interest/?limit=400`);
    // return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}Interest/?limit=400`);
  }

  public getPractitionerDetail(id){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-practitioner-profile-customer/?practitioner_id=${id}`);
  }
  public getMoods(id) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}mood/`);
  }
  public updateBasicInfo(payload, pkID) {
    return this.http.put<{ success: object }>(`${Config.CUSTOMER_BASE_URL}customer/${pkID}/`, payload);
  }
  public updateProfileInfo(payload, pkID) {
    return this.http.put<{ success: object }>(`${Config.CUSTOMER_BASE_URL}update-customer/${pkID}/`, payload);
  }
  public updateCustomerProfileAdmin(payload, pkID) {
    return this.http.put<{ success: object }>(`${Config.CUSTOMER_BASE_URL}edit-customer-profile-admin/${pkID}/`, payload);
  }
  public updatePractitionerProfileAdmin(payload, pkID) {
    return this.http.put<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}edit-practitioner-profile-admin/${pkID}/`, payload);
  }
  public updateMoodInfo(payload, pkID) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}mood/`, payload);
  }

  public AddMoodInfo(payload) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}mood/`, payload);
  }

  public previousLoggedUpdate(payload, id) {
    return this.http.put<{ success: object }>(`${Config.AUTH_BASE_URL}redirect-to-questions/${id}/`, payload);
  }
  public pkIdUpdate(group, method) {
    let userData = JSON.parse(localStorage.getItem("user_data"));
    let address = {
      "address_line1": "N/A",
      "address_line2": "N/A",
      "address_line3": "N/A",
      "addrees_line4": "N/A",
      "address_line5": "N/A",
      "zipcode": null,
      "address_type": null,
      "city": userData["city_id"] ? userData["city_id"] : "",
    };
    let payload = {
      "user_id": {
        // "id": "",
        "first_name": userData.user.first_name,
        "last_name": userData.user.last_name,
        "email": userData.user.email,
      },
      "nick_name": userData.user.first_name,
      "phone_number": userData.user.phone_number,
      "interest": [],
      "news_and_updates": "Weekly",
      "is_sms_subscribe": false,
      "is_email_subscribe": false,
      "notification_type": "Email",
      "reminder_type": "24",
      "blood_group": null,
      "availability": "Available",
      "is_subscribed": false,
      "moods": "",
      "dob": null,
      "gender": null,
      "ssn": null,
      "address": address
    }
    if (group == "customer") {
      
      if (method == "post") {
        return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}${group}/`, payload);
      } else {
        return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}${group}/`);
      }
    } else if (group == "admin") {
      // if (method == "post") {
      //   return this.http.post<{ success: object }>(`${Config.ADMIN_BASE_URL}users/`, payload);
      // } else {
      return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}users/`);
      // }
    } else if (group == "practitioner") {
      // if (method == "post") {
      //   return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}${group}/`, payload);
      // } else {
      // return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}${group}/`);
      return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-practitioner/`);
      // }
    }
  }
  public getPractitionerSessionTypes() {
    return this.http.get(`${Config.PRACTITIONER_BASE_URL}viewpractitionersessiontypes/`);
  }

  public getPractitionerDetails() {
    return this.http.get(`${Config.PRACTITIONER_BASE_URL}practitioner/`);
  }

  public getProfileDetails(practitionerId: string) {
    return this.http.get(`${Config.PRACTITIONER_BASE_URL}profile-details/?practitioner_id=${practitionerId}`);
  }

  public editPractitioner(id: string, payload: any) {
    return this.http.put(`${Config.PRACTITIONER_BASE_URL}edit-practitioner/${id}/`, payload);
  }

  public editPractitionerPatch(id: string, payload: any) {
    return this.http.put(`${Config.PRACTITIONER_BASE_URL}edit-practitioner/${id}/`, payload);
  }

  // List of curated profile pictures
  public getCuratedProfilePictures() {
    return this.http.get(`${Config.ADMIN_BASE_URL}curated-image-list/`);
  }
  public updateProfilePicture(customerId, payload) {
    return this.http.put(`${Config.CUSTOMER_BASE_URL}PhotoVerification/${customerId}/`, payload);
  }
  public updatePractitionerProfilePicture(id, payload) {
    return this.http.put(`${Config.PRACTITIONER_BASE_URL}PhotoVerification/${id}/`, payload);
  }

  public getCustomerInterestList(params) {
    return this.http.get(`${Config.ADMIN_BASE_URL}GetInterestDetails/?${params}`);
  }

  // Expertise data
  public getInterestList(...interest: string[]) {
    const params = interest.map(str => `interest_list=${str}`).join('&');
    return this.http.get(`${Config.ADMIN_BASE_URL}GetInterestDetails/?${params}`);
  }

  // Interest API
  public getAllInterest() {
    return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}Interest/?limit=400`);
  }

  // Customer chat sessions
  public getCustomerChatSessionList() {
    return this.http.get(`${Config.PRACTITIONER_BASE_URL}chatsession-customer-list/`);
  }

  // Practitioner sessions flow
  public getDashboardData(startDate: string, endDate: string) {
    return this.http.get(`${Config.PRACTITIONER_BASE_URL}DashBoard/?start_date=${startDate}&end_date=${endDate}`);
  }


  public getExpertise() {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}expertise/`);
  }

  public getAllExpertise() {
    return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}expertise/`);
  }

  public addPractitioner(payload) {
    return this.http.post<{ success: object }>(`${Config.AUTH_BASE_URL}register/`, payload);
  }

  public zoomCreation(payload) {
    return this.http.post<{ success: object }>(`${Config.ZOOM_BASE_URL}user/custcreate/`, payload);
  }

  public zoomUserCreation(email, fname, lname, type) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}zoom-user-create/?email=${email}&first_name=${fname}&last_name=${lname}&_type=${type}`);
  }

  public addInterest(payload) {
    return this.http.post<{ success: object }>(`${Config.ADMIN_BASE_URL}Interest/`, payload);
  }

  public getInterest() {
    return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}Interest/?limit=400`);
  }

  public updateAdminCustomer(payload, id) {
    return this.http.put<{ success: object }>(`${Config.ADMIN_BASE_URL}Interest/${id}/`, payload);
  }

  public saveInterest(payload, id) {
    return this.http.put<{ success: object }>(`${Config.ADMIN_BASE_URL}Interest/${id}/`, payload);
  }

  public deleteInterest(payload, id) {
    return this.http.delete<{ success: object }>(`${Config.ADMIN_BASE_URL}Interest/${id}/`, payload);
  }

  public getPractitionerSessions() {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}practitionersession/`);
  }

  public addPractitionerSession(payload) {
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}practitionersession/`, payload);
  }

  public editPractitionerSession(payload, practitionerId) {
    return this.http.put<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}practitionersession/${practitionerId}/`, payload);
  }

  public getPractitionerSchedule(start_date, end_date) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-practitioner-availability/?end_date=${end_date}&start_date=${start_date}`);
  }

  public getPractitionerAvailabelSchedule(start_date, end_date,pid) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-practitioner-availability/?end_date=${end_date}&start_date=${start_date}&practitioner=${pid}`);
  }
  public addBankAccount(payload) {
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}practitionerbankdetails/`, payload);
  }

  public externalBankAccount(payload) {
    return this.http.post<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}add-external-bank-account/`, payload);
  }

  public getBankAccount() {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}practitionerbankdetails/`);
  }

  public editBankAccount(payload, practitionerId) {
    return this.http.put<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}practitionerbankdetails/${practitionerId}/`, payload);
  }

  public addConnectAccount(payload) {
    return this.http.post<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}create-connected-account/`, payload);
  }

  public getConnectAccount(connect_id) {
    return this.http.get<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}connected-account/${connect_id}/`);
  }

  public editConnectAccount(payload) {
    return this.http.put<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}edit-connected-account/`, payload);
  }

  public addDocumentVerification(payload) {
    return this.http.post<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}document-verification/`, payload);
  }

  public acceptTerms(connect_id) {
    return this.http.get<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}tos-acceptance/${connect_id}/`);
  }

  public reportPractitioner(payload) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}report-practitioner/`, payload);
  }

  public blockCustomer(payload) {
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}blockcustomer/`, payload);
  }

  public unBlockCustomer(id) {
    return this.http.delete<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}blockcustomer/${id}/`);
  }

  public switchPractitioner(payload) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}SwitchPractitioner/`, payload);
  }

  public followUpForm(payload) {
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}follow-up-form/`, payload);
  }

  // Customer APIs
  public getCustomerDetails() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}DashBoard/`);
  }

  public getCustomerChats() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}chatsession-practitioner-list/`);
  }

  public getPractitionerList(type, page, limit) {
    return this.http.get<{ success: object }>(`${Config.AUTH_BASE_URL}practitioner-list/?_type=${type}&page=${page}&limit=${limit}`);
  }

  public getPractitionerDownloadList(type, page, limit) {
    return this.http.get<{ success: object }>(`${Config.AUTH_BASE_URL}practitioner-list/?_type=${type}&page=${page}&limit=${limit}`);
  }

  public getCustomerList(type, page, limit) {
    return this.http.get<{ success: object }>(`${Config.AUTH_BASE_URL}customer-list/?_type=${type}&page=${page}&limit=${limit}`);
  }

  public getCustomerDownloadList(type, page, limit) {
    return this.http.get<{ success: object }>(`${Config.AUTH_BASE_URL}customer-list/?_type=${type}&page=${page}&limit=${limit}`);
  }

  public getAdminNotification() {
    return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}manage-notification/`);
  }

  public updateAdminNotification(id, data) {
    return this.http.put<{ success: object }>(`${Config.ADMIN_BASE_URL}manage-notification/${id}/`, data);
  }

  public changeUserStatus(auth_id, data) {
    return this.http.put<{ success: object }>(`${Config.AUTH_BASE_URL}change-user-status/${auth_id}/`, data);
  }

  public getAdminCustomerDetails(id) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}get-customers-details/?customer_profile=${id}`);
  }

  public getCustomerAndPractitionerchatSessionDetails(pid, cid) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}get-chat-count/?practitioner_id=${pid}&customer_id=${cid}`);
  }

  public updateProctitionerAvailability(payload, id) {
    return this.http.put<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}edit-practitioner-availability/${id}/`, payload);
  }

  public reviewPractitioner(payload) {
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}ratings/`, payload);
  }

  public stripeTransfers(payload) {
    return this.http.post<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}stripe-transfers/`, payload);
  }

  public getPractitionerListByInterest(id) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}search-practitioner/?customer_id=${id}`);
  }

  public getPractitionerStats() {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}stats/`);
  }

  public getCustomerStats() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}stats/`);
  }

  public getManageNotification(group, customer_id) {
    console.log(group);
    if (group == "Customer") {
      return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}ManageNotification/${customer_id}/`);
    } else {
      return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}managenotifications/${customer_id}/`);
    }
  }

  public updateManageNotification(group, customer_id, payload) {
    if (group == "Customer") {
      return this.http.put<{ success: object }>(`${Config.CUSTOMER_BASE_URL}ManageNotification/${customer_id}/`, payload);
    } else {
      return this.http.put<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}managenotifications/${customer_id}/`, payload);
    }
  }

  public getCardDetails() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}customerpaymentdetails/`);
  }
  public getCardMethods() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}paymentmethods/`);
  }
  public createCardDetails(payload) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}customerpaymentdetails/`, payload);
  }
  public updateCardDetails(id, payload) {
    return this.http.put<{ success: object }>(`${Config.CUSTOMER_BASE_URL}customerpaymentdetails/${id}/`, payload);
  }

  public updateVideoSessionStatus(payload) {
    return this.http.put<{ success: object }>(`${Config.CUSTOMER_BASE_URL}update-video-session/`, payload);
  }
  public getBillingDetails() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}BillingHistory/`);
  }

  // Practitioner - My Earnings
  public getEarnings(id) {
    return this.http.get<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}my-earnings/${id}/`);
  }

  public getWithdrawList(id) {
    return this.http.get<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}payouts/${id}/?limit=1000`);
  }

  public withdrawAmount(payload) {
    return this.http.post<{ success: object }>(`${Config.PAYMENT_CONNECT_URL}create-payout/`, payload);
  }

  public getPractitionerProfileDetails(id) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-practitioner-profile/?practitioner_id=${id}`);
  }

  public getPractitionerSessionList(id) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-practitioner-profile/?session_practitioner=${id}`);
  }

  public searchPractitionerList(id, query) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}search-practitioner/?customer_id=${id}&query=${query}`);
  }

  public sendMsgToCustomer(payload) {
    return this.http.post<{ success: object }>(`${Config.ADMIN_BASE_URL}send-message-to-customer/`, payload);
  }

  public sendMsgToPractitioner(payload) {
    return this.http.post<{ success: object }>(`${Config.ADMIN_BASE_URL}send-message-to-practitioner/`, payload);
  }

  public sendMsgToAdmin(payload) {
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}contact-admin/`, payload);
  }

  public resendActivationMailToPractitioner(payload) {
    return this.http.post<{ success: object }>(`${Config.AUTH_BASE_URL}resend-verification-email/`, payload);
  }
  

  //v2 api
  public getCustomerFavoritePractitioners(pageNo,limit,customerId) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-fav-practitioner/?customer_id=${customerId}&page=${pageNo}&limits=${limit}`);
  }

  public getCustomerFavoriteArticles(pageNo,limit,customerId) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-fav-article/?customer_id=${customerId}&page=${pageNo}&limits=${limit}`);
  }

  public getCustomerFavoriteCourses(pageNo,limit,customerId) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-fav-course/?customer_id=${customerId}&page=${pageNo}&limits=${limit}`);
  }

  public getCustomerFavoriteLiveClasses(pageNo,limit,customerId) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-fav-live-class/?customer_id=${customerId}&page=${pageNo}&limits=${limit}`);
  }

  public getPractitionerProfile(customerId,prac_id) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-practitioner-profile-home/?practitioner_id=${prac_id}&customer_id=${customerId}`);
  }

  public addFavorite(action,payload) {
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}${action}/`, payload);
  }

  public removeFromFavorite(action, id) {
    return this.http.delete<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}${action}/${id}/`);
  }

  public getPractitionerListPublic(pageNo,limit,key) {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}home-page-practitioner/?page=${pageNo}&limits=${limit}&query=${key}`);
  }
  
  public getCourses(page){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-course-list/?page=${page}`);
  }

  public getLiveClasses(limits,category,pageNo,searchKey){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}live-class-search/?query=${category}&limits=${limits}&page=${pageNo}&home_query=${searchKey}`);
  }

  public getClassDetail(customer_id,class_id,customer_email){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-class-detail/?id=${class_id}&customer_id=${customer_id}&email=${customer_email}`);
  }

  public getAllCourse(limits,category,pageNo,searchKey){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}course-search/?query=${category}&limits=${limits}&page=${pageNo}&home_query=${searchKey}`);
  }

  public getCourse(customer_id,course_id){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-selected-course/?course_id=${course_id}&customer_id=${customer_id}`);
  }

  public getAllArticle(limits,category,pageNo,searchKey){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}article-search/?query=${category}&limits=${limits}&page=${pageNo}&home_query=${searchKey}`);
  }

  public getBlogData(page,limits){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-main-blog-page/?page=${page}&limits=${limits}`);
  }

  public getPostBlogData(blogId){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-post-blog-page/?id=${blogId}`);
  }

  public getHomePageSearchData(searchText){
    if(searchText){
      return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}home-page/?query=${searchText}`);
    }
    else{
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}home-page/`);
    }
  }

  public getHomePageData(){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}home-page/`);
  }

  public getCountDetail(id){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}header-fav-count/?customer_id=${id}`);
  }

  public submitContactForm(payload){
    return this.http.post<{ success: object }>(`${Config.ADMIN_BASE_URL}ContactUs/`, payload);
  }

  public getSessionType(cid,pid) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}ChooseSessionType/${cid}/${pid}/`);
  }

  public joinLiveClass(payload){
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}join-live-class/`, payload);
  }

  public purchaseCourse(payload){
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}purchase-course/`, payload);
  }

  public updateCourseViewDetail(payload){
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}course-viewers-detail/`, payload);
  }

  public rateCourse(payload){
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}course-rating/`, payload);
  }

  public getCustomerPurchasedCourse(customer_id){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}view-purchase-course/?customer_id=${customer_id}`);
  }

  public getFAQList(){
    return this.http.get<{ success: object }>(`${Config.ADMIN_BASE_URL}faq-general/`);
  }

  public verifyCustomer(data){
    return this.http.get<{ success: object }>(`https://mservice.healplace.co/auth/account/confirm-email/${data}/`);
  }
  
  public sendEmail(z_id,a_id){
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}appointment-meeting-link-mail/?meeting_id=${z_id}&appointment_id=${a_id}`);
  }

  public donatetoPractitioner(payload){
    return this.http.post<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}donate/`, payload);
  }

  public getPractitionerCourse(p_id){
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}my-course/?practitioner_id=${p_id}`);
  }
}



