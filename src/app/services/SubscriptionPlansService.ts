import { Injectable } from "@angular/core";
import {  HttpClient, HttpParams } from '@angular/common/http';
import { Config } from '../config/config';

@Injectable()
export class SubscriptionPlansService {
    constructor(
        private http: HttpClient,
      ) { }

    public getsubscriptionplans(plan_type){
        return this.http.get<{success: object}>(`${Config.ADMIN_BASE_URL}GetPlanDetails/?plan_type=${plan_type}`);
      }
      public getsubscription(){
        return this.http.get<{success: object}>(`${Config.CUSTOMER_BASE_URL}subscription/`);
      }

      public getSubscriptionDetails(id){
        return this.http.get<{success: object}>(`${Config.CUSTOMER_BASE_URL}subscription/${id}/`);
      }

      public getSubscriptionPlanType(){
        return this.http.get<{success: object}>(`${Config.ADMIN_BASE_URL}plantype/`);
      }

      public getplanDetails(plan_id){
        let params = new HttpParams().set("plan_id",plan_id); //Create new HttpParams
        params.append("plan_id", plan_id);
        return this.http.get<{success: object}>(`${Config.ADMIN_BASE_URL}GetPlanDetails/`, { params: params });
      }

      public addSubscription(payload){
        return this.http.post<{success: object}>(`${Config.CUSTOMER_BASE_URL}subscription/`, payload);
      }

      public addSubscriptionPlan(payload){
        return this.http.post<{success: object}>(`${Config.ADMIN_BASE_URL}plans/`, payload);
      }

      public editSubscriptionPlan(payload, id){
        return this.http.put<{success: object}>(`${Config.ADMIN_BASE_URL}plans/${id}/`, payload);
      }

      public removeSubscription(payload, id){
        return this.http.put<{success: object}>(`${Config.CUSTOMER_BASE_URL}subscription/${id}/`, payload);
      }

      public addSubscriptionAdminCustomer(payload){
        return this.http.post<{success: object}>(`${Config.CUSTOMER_BASE_URL}subscription-admin/`, payload);
      }

      public editSubscriptionAdminCustomer(payload, id){
        return this.http.put<{success: object}>(`${Config.CUSTOMER_BASE_URL}subscription-admin/${id}/`, payload);
      }
}