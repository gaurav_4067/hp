import { Config } from '../config/config';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ChatService {

  constructor(
    private http: HttpClient,
  ) { }

  public getCustomerToken() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}ChatAccessToken/`);
  }

  public getPractitionerToken() {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}ChatAccessToken/`);
  }

  public getPractitionerList() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}chatsession-practitioner-list/ `);
  }

  public getCustomerList() {
    return this.http.get<{ success: object }>(`${Config.PRACTITIONER_BASE_URL}chatsession-customer-list/ `);
  }

  public createChannel(data) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}chat-session/`, data);
  }

  public archiveChat(data) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}archieve-chat/`, data);
  }

  public deleteArchiveChat(id) {
    return this.http.delete<{ success: object }>(`${Config.CUSTOMER_BASE_URL}archieve-chat/${id}/`);
  }

  public getCustomerArchiveChats() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}view-archieve-chat/`);
  }

  public getPractitionerArchiveChats(id) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}view-practitioner-archieve-chat/?id=${id}`);
  }

  public pinMessage(data) {
    return this.http.post<{ success: object }>(`${Config.CUSTOMER_BASE_URL}pin-message/`, data);
  }

  public viewPinMessage() {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}view-pin-message/`);
  }

  public unPinMessage(id) {
    return this.http.delete<{ success: object }>(`${Config.CUSTOMER_BASE_URL}pin-message/${id}/`);
  }
}