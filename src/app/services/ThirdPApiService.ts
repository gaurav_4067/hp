import { Config } from '../config/config';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ThirdPApiService {

  constructor(
    private http: HttpClient,
  ) { }

  public getCountries(field, name) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}country/?fields=${field},name&${name}`);
  }
  public getStates(countryId, field, name) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}state/?country=${countryId}&fields=${field},name&${name}`);
  }
  public getCities(stateId, field, name) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}city/?state=${stateId}&fields=${field},name&${name}`);
  }
  public getZoomMeetingSignature(id, role) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}ZoommeetingSinature/?meeting_id=${id}&role=${role}`);
  }
  public getZoomMeetingList(id) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}GetZoomMeetingId/?id=${id}`);
  }
  public generateMeettingSignature(role, meettingId) {
    return this.http.get<{ success: object }>(`${Config.CUSTOMER_BASE_URL}ZoommeetingSinature/?meeting_id=${meettingId}&role=${role}`);
  }
}

