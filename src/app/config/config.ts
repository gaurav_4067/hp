import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

export class Config {
  static BASE_URL = "https://localhost:5000/";
  static AUTH_BASE_URL = "https://localhost:5000/auth/api/v1/";
  static PRACTITIONER_BASE_URL = "https://localhost:5000/practitioner/api/v1/";
  static PAYMENT_CONNECT_URL = "https://localhost:5000/payment/api/v1/";
  static ADMIN_BASE_URL = "https://localhost:5000/admin/api/v1/";
  static CUSTOMER_BASE_URL = "https://localhost:5000/customer/api/v1/";
  static ZOOM_BASE_URL = "https://api.test.us/v2/";
}

export class ImageConfig {
  static PRACTITIONER_BASE_URL = "/";
  static ADMIN_BASE_URL = "";
  static CUSTOMER_BASE_URL = "";
}

export class ScoailLoginConfig {
  static CREDENTIALS = [
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(
        // '687059919219-br63raufd60a3gmrtelmpgnbdhe97me3.apps.googleusercontent.com'
        '28219092747-19gnucp7ov2kr5giak3ig8p5o5dh9lbb.apps.googleusercontent.com'
      )
    },
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      // provider: new FacebookLoginProvider('4070606993029772')
      provider: new FacebookLoginProvider('306302267815872')
    }
  ]
}
export class ZoomMeetingConfig {
  static API_KEY = '';
  static SECRET_KEY = '';
  static ZOOM_SESSION_APP_URL = 'https://test.hp.co/';
}
