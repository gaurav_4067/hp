import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import {FAQWrapper, Question} from './support.model';
@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {
  search:string;
  faqList:FAQWrapper;
 flatFAQList:Array<Question>;
 filterFAQList:Array<Question>;
 filteredSearch:boolean=false;
  constructor(public apiService:ApiService,
    public loader:NgxSpinnerService) {
      this.faqList=new FAQWrapper;
      this.flatFAQList=new Array<Question>();
      this.filterFAQList=new Array<Question>();

     }

  ngOnInit(): void {
    this.getFAQList();
  }

  getFAQList(){
    this.loader.show();
    this.apiService.getFAQList()
      .subscribe((faqRes: any) => {
        console.log('faqRes', faqRes);
        this.faqList=faqRes;
        this.buildFlatFAQ();
        this.loader.hide();
         
      }, (error) => {
        console.log('faqRes error', error);
        this.loader.hide();
      });
  }

  buildFlatFAQ(){
    this.faqList.context.forEach(contextQuestions => {
      contextQuestions.questions.forEach(question => {
        question.category=contextQuestions.category;
        this.flatFAQList.push(question);
      });
    });

    console.log(this.flatFAQList);
  }

  searchFAQ(){
    if(this.search.length>=3){
    this.filterFAQList= this.flatFAQList.filter(i=>i.question.toLowerCase().includes(this.search.toLowerCase()));
    console.log(this.filterFAQList);
    this.filteredSearch=true;
    }
    if(this.search.length==0){
      this.filteredSearch=false;
    }
  }
}
