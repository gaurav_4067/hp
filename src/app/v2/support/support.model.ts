
    export class Question {
        public  created: Date;
        public  modified: Date;
        public  user_created: string;
        public  user_modified: string;
        public  hostname_created: string;
        public  hostname_modified: string;
        public  device_created: string;
        public  device_modified: string;
        public  id: string;
        public  question: string;
        public  answer: string;
        public  _type: string;
        public  is_published: boolean;
        public category:string;
    }

    export class Context {
        constructor(){
            this.questions=new Array<Question>();
        }
        public category: string;
        public questions: Array<Question>;
    }

    export class FAQWrapper {
        constructor(){
            this.context=new  Array<Context>();
        }
        public context: Array<Context>;
    }
 
