import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoPlayerCarouselComponent } from './video-player-carousel.component';

describe('VideoPlayerCarouselComponent', () => {
  let component: VideoPlayerCarouselComponent;
  let fixture: ComponentFixture<VideoPlayerCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoPlayerCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPlayerCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
