import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { MainCourseInfo } from '../../all-course/detail/detail.model';

@Component({
  selector: 'app-video-player-carousel',
  templateUrl: './video-player-carousel.component.html',
  styleUrls: ['./video-player-carousel.component.scss']
})
export class VideoPlayerCarouselComponent implements OnInit {
  @ViewChild(DragScrollComponent) ds: DragScrollComponent;
  @Input() course_details:MainCourseInfo;
  @Input() course_id:string='';
  // @Input() videoSources: Plyr.Source[] = [];
  // videoSources: Array<Plyr.Source>;
  constructor(public generalService: GeneralService,
    public authService:AuthService) { 
    this.authService.changeVideo.subscribe(call => {
      if (call) {
        this.ds.moveRight();
      }
    });
  }

  ngOnInit(): void {
    
    //this.videoSources=new Array<Plyr.Source>();
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }

  // getVideoSourse(lesson){
  //   let activeLesson=this.course_details.lessons.filter(i=>i.id==lesson.id)
  //   this.videoSources.push({"src":this.generalService.getPractiotionerImageBaseUrl(activeLesson[0].video_file),"type": 'video/mp4'});
  //   return this.videoSources;
  // }

}
