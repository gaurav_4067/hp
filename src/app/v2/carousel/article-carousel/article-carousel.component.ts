import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { ArticleDetails, ArticleList } from '../../my-favorites/my-favorites.model';

@Component({
  selector: 'app-article-carousel',
  templateUrl: './article-carousel.component.html',
  styleUrls: ['./article-carousel.component.scss']
})
export class ArticleCarouselComponent implements OnInit {
  @ViewChild(DragScrollComponent) ds: DragScrollComponent;
  @Input() articles:Array<ArticleList>;
  constructor() { }

  ngOnInit(): void {
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }
  // ngAfterViewInit() {
  //   
  //   // Starting ngx-drag-scroll from specified index(3)
  //   setTimeout(() => {
  //     this.ds.moveLeft();
  //   }, 5000);
  // }
}
