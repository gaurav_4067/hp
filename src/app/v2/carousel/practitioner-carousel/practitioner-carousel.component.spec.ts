import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PractitionerCarouselComponent } from './practitioner-carousel.component';

describe('PractitionerCarouselComponent', () => {
  let component: PractitionerCarouselComponent;
  let fixture: ComponentFixture<PractitionerCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PractitionerCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PractitionerCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
