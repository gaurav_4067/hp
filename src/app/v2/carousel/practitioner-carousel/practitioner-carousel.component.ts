import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { RelatedPractitioner } from '../../practitioner/practitioner-profile/practitioner-profile-model';

@Component({
  selector: 'app-practitioner-carousel',
  templateUrl: './practitioner-carousel.component.html',
  styleUrls: ['./practitioner-carousel.component.scss']
})
export class PractitionerCarouselComponent implements OnInit {
  @ViewChild(DragScrollComponent) ds: DragScrollComponent;
  @Input() related_practitioner:Array<RelatedPractitioner>;
  constructor() { }

  ngOnInit(): void {
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }
  // ngAfterViewInit() {
  //   // Starting ngx-drag-scroll from specified index(3)
  //   setTimeout(() => {
  //     this.ds.moveTo(3);
  //   }, 0);
  // }

}
