import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { LiveClass } from '../../practitioner/practitioner-profile/practitioner-profile-model';

@Component({
  selector: 'app-class-carousel',
  templateUrl: './class-carousel.component.html',
  styleUrls: ['./class-carousel.component.scss']
})
export class ClassCarouselComponent implements OnInit {
  @ViewChild(DragScrollComponent) ds: DragScrollComponent;
  @Input() live_classes:Array<LiveClass>;
  constructor() { }

  ngOnInit(): void {
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }
  // ngAfterViewInit() {
  //   // Starting ngx-drag-scroll from specified index(3)
  //   setTimeout(() => {
  //     this.ds.moveTo(3);
  //   }, 0);
  // }
}
