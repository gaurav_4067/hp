import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manage-subscription',
  templateUrl: './manage-subscription.component.html',
  styleUrls: ['./manage-subscription.component.scss']
})
export class ManageSubscriptionComponent implements OnInit {
  logedinData:any;
  constructor(public router:Router) { }

  ngOnInit(): void {
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if(this.logedinData && this.logedinData["id"]){

    }
    else{
      //this.router.navigate(["/dashboard"]);
    }
  }
}
