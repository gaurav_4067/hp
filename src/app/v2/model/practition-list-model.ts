export class Practitioner {
    public practitioner_firstname: string;
    public practitioner_lastname: string;
    public practitioner_expertise: string[];
    public practitioner_rating: number;
    public practitioner_rate_count: number;
    public practitioner_id: string;
    public starting_price: number;
    public practitioner_profile_image: string;
}

export class PractitionerList {
    constructor(){
        this.data= new  Array<Practitioner>();
    }
    public total: number;
    public previous_page?: any;
    public next_page: number;
    public data: Array<Practitioner>;
}

export class PractitionerWrapper {
    constructor(){
        this.practitioner_list=new PractitionerList();
    }
    public practitioner_list: PractitionerList;
    public practitioner_count: number;
}

