
export class ArticleDetail {
    public id: string;
    public title: string;
    public image: string;
    public description: string;
    public author__first_name: string;
    public author__last_name: string;
    public is_published: boolean;
    public category: string[];
    public created: Date;
    public modified: Date;
}

export class ArticleInfo {
    constructor(){
        this.data=new Array<ArticleDetail>();
    }
    public total: number;
    public previous_page?: any;
    public next_page: number;
    public data: Array<ArticleDetail>;
}

export class ArticleWrapper {
    constructor(){
        this.article_list=new ArticleInfo();
    }
   public article_list: ArticleInfo;
   public article_count: number;
}

