export class StripePaymentResponse {
    public has_paid: boolean;
    public transaction_id: string;
}