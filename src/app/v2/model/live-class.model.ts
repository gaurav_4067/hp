
    export class LiveClassDetail {
        public id: string;
        public title: string;
        public practitioner__user_id__first_name: string;
        public practitioner__user_id__last_name: string;
        public practitioner__expertise: string[];
        public practitioner__profile_image: string;
        public start_time: Date;
        public end_time: Date;
        public class_date: string;
        public duration: number;
        public image: string;
        public practitioner: string;
    }

    export class LiveClass {
        constructor(){
            this.data=new Array<LiveClassDetail>();
        }
        public total: number;
        public previous_page?: any;
        public next_page?: any;
        public data: Array<LiveClassDetail>;
    }

    export class LiveClassWrapper {
        constructor(){
            this.live_class_list=new LiveClass();
        }
        public live_class_list: LiveClass;
        public live_class_count: number;
    }

