export class BlogDetails {
    public id: string;
    public title: string;
    public image: string;
    public description: string;
    public author__first_name: string;
    public author__last_name: string;
    public is_published: boolean;
    public category: string[];
}

export class RecommendedArticle {
    public id: string;
    public title: string;
    public image: string;
    public description: string;
    public author__first_name: string;
    public author__last_name: string;
    public is_published: boolean;
    public category: string[];
}

export class Article {
    public id: string;
    public title: string;
    public image: string;
    public description: string;
    public author__first_name: string;
    public author__last_name: string;
    public is_published: boolean;
    public category: string[];
}

export class AllArticles {
    constructor() {
        this.data = new Array<Article>();
    }
    public total: number;
    public previous_page?: any;
    public next_page: number;
    public data: Array<Article>;
}

export class BlogData {
    constructor() {
        this.blog_details = new BlogDetails();
        this.recommended_article = new Array<RecommendedArticle>();
        this.all_articles = new AllArticles();

    }
    public blog_details: BlogDetails;
    public recommended_article: Array<RecommendedArticle>;
    public all_articles: AllArticles;
    public all_articles_count:number;
}
