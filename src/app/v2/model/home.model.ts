
    export class PractitionerList {
        constructor(){
            this.practitioner_expertise = new Array<string>();
        }
        public practitioner_firstname: string;
        public practitioner_lastname: string;
        public practitioner_expertise: Array<string>;
        public practitioner_rating: number;
        public practitioner_rate_count: number;
        public practitioner_id: string;
        public starting_price: number;
        public practitioner_profile_image: string;
    }

    export class Lesson {
        public id: string;
        public lesson_title: string;
        public lesson_duration: number;
        public video_file: string;
        public lesson_description: string;
        public cover_photo:string="./../../assets/images/Rectangle 92.png";
    }

    export class CoursesList {
        constructor(){
            this.practitioner_expertise=new Array<string>();
            this.lessons=new Array<Lesson>();
            this.viewers_detail=new Array<Viewer>();
        }
        public practitioner_firstname: string;
        public practitioner_lastname: string;
        public practitioner_expertise: Array<string>;
        public course_id: string;
        public course_title: string;
        public related_course_duration_in_sec: number;
        public related_course_rating: number;
        public related_course_rated_count: number;
        public viewers_count: number;
        public viewers_detail: Array<Viewer>;
        public lesson_count: number;
        public lessons: Array<Lesson>;
    }


    export class Viewer {
        constructor(){
            this.interest=new Array<string>();
        }
        public id: string;
        public user_id__first_name: string;
        public user_id__last_name: string;
        public user_id__email: string;
        public interest: Array<string>;
        public nick_name: string;
        public profile_image: string;
        public is_profile_pic_approved: boolean;
        public is_curated_images: boolean;
    }

    export class LiveClassList {
        constructor(){
            this.practitioner__expertise=new Array<string>();
        }
        public id: string;
        public title: string;
        public practitioner__user_id__first_name: string;
        public practitioner__user_id__last_name: string;
        public practitioner__expertise: Array<string>;
        public practitioner__profile_image: string;
        public start_time: Date;
        public end_time: Date;
        public class_date: string;
        public duration: number;
        public image: string;
        public practitioner: string;
        public is_completed: boolean;
        public description: string;
    }

    export class ArticleList {
        constructor(){
            this.category=new Array<string>();
        }
        public id: string;
        public title: string;
        public image: string;
        public description: string;
        public author__first_name: string;
        public author__last_name: string;
        public is_published: boolean;
        public category: Array<string>;
    }

    export class Testimonial {
        public review: string;
        public overall_rating: number;
        public customer_id: string;
        public id: string;
        public user_id__first_name: string;
        public user_id__last_name: string;
        public user_id__email: string;
        public interest: string[];
        public nick_name: string;
        public profile_image: string;
        public is_profile_pic_approved: boolean;
        public is_curated_images: boolean;
    }

    export class HomeDetail {
        constructor(){
            this.practitioner_list=new Array<PractitionerList>();
            this.courses_list=new Array<CoursesList>();
            this.live_class_list=new Array<LiveClassList>();
            this.highlighted_class=new Array<LiveClassList>();
            this.article_list=new Array<ArticleList>();
            this.testimonials=new Array<Testimonial>();
        }
        public practitioner_list: Array<PractitionerList>;
        public courses_list: Array<CoursesList>;
        public live_class_list: Array<LiveClassList>;
        public highlighted_class: Array<LiveClassList>;
        public article_list: Array<ArticleList>;
        public testimonials: Array<Testimonial>;
    }
