export class PostBlogDetails {
    constructor(){
        this.category=new Array<string>();
    }
    public id: string;
    public title: string;
    public image: string;
    public description: string;
    public first_name: string;
    public last_name: string;
    public is_published: boolean;
    public category: Array<string>;
    public created_date:Date;
}

export class RelatedArticle {
    constructor(){
        this.category=new Array<string>();
    }
    public id: string;
    public title: string;
    public image: string;
    public description: string;
    public author__first_name: string;
    public author__last_name: string;
    public is_published: boolean;
    public category: Array<string>;
}

export class PostBlogData {
    constructor(){
        this.blog_details=new PostBlogDetails();
        this.related_articles=new Array<RelatedArticle>();
    }
    public blog_details: PostBlogDetails;
    public related_articles: Array<RelatedArticle>;
}