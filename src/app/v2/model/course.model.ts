
export class Rating {
    public overallrating: number;
    public rated_person_count: number;
}

export class Lesson {
    public id: string;
    public created: Date;
    public modified: Date;
    public lesson_title: string;
    public lesson_duration: number;
    public video_file: string;
    public lesson_description: string;
}

export class PractitionerUserInfo {
    public id: string;
    public first_name: string;
    public last_name: string;
    public email: string;
}

export class Address {
    public id: string;
    public created: Date;
    public modified: Date;
    public address_line1?: any;
    public address_line2: string;
    public address_line3: string;
    public addrees_line4?: any;
    public address_line5: string;
    public zipcode: number;
    public lattitude?: any;
    public longitude?: any;
    public location?: any;
}

export class Practitioner {
    constructor() {
        this.user_id = new PractitionerUserInfo();
        this.address = new Address();
    }
    public id: string;
    public user_id: PractitionerUserInfo;
    public name?: any;
    public description: string;
    public profile_image?: any;
    public address: Address;
    public expertise: string[];
    public phone_number: string;
}

export class CourseDetail {
    constructor(){
        this.practitioner = new Practitioner();
    }
    public id: string;
    public lesson_count: number;
    public course_duration_in_sec: number;
    public rating: Rating;
    public lessons: Lesson[];
    public practitioner: Practitioner;
    public created: Date;
    public modified: Date;
    public course_title: string;
    public description: string;
    public plays_count: number;
}

export class Courses {
    constructor(){
        this.results=new Array<CourseDetail>();
    }
    public count: number;
    public next: string;
    public previous?: any;
    public results: Array<CourseDetail>;
}