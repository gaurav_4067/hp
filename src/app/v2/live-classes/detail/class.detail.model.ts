
export class PractitionerReview {
    constructor() {
        this.interest = new Array<string>();
    }
    public review: string;
    public overall_rating: number;
    public customer_id: string;
    public id: string;
    public user_id__first_name: string;
    public user_id__last_name: string;
    public user_id__email: string;
    public interest: Array<string>;
    public nick_name: string;
    public profile_image: string;
    public is_profile_pic_approved: boolean;
    public is_curated_images: boolean;
}

export class UpcomingClass {
    constructor() {
        this.practitioner__expertise = new Array<string>();
    }
    public id: string;
    public title: string;
    public practitioner__user_id__first_name: string;
    public practitioner__user_id__last_name: string;
    public practitioner__expertise: Array<string>;
    public practitioner__profile_image: string;
    public start_time: Date;
    public end_time: Date;
    public class_date: string;
    public duration: number;
    public image: string;
    public practitioner: string;
    public is_completed: boolean;
    public description: string;
}

export class ClassDetail {
    constructor() {
        this.practitioner_expertise = new Array<string>();
        this.practitioner_reviews = new Array<PractitionerReview>();
        this.upcoming_classes = new Array<UpcomingClass>();
    }
    public live_class_id: string;
    public live_class_title: string;
    public live_class_description: string;
    public live_class_start_time: Date;
    public live_class_end_time: Date;
    public live_class_date: string;
    public live_class_duration: number;
    public live_class_price: number;
    public is_joined_class:boolean;
    public practitioner_id: string;
    public practitioner_firstname: string;
    public practitioner_lastname: string;
    public practitioner_expertise: Array<string>;
    public practitioner_description: string;
    public practitioner_address_line1: string;
    public practitioner_address_line2: string;
    public practitioner_address_line3?: any;
    public practitioner_address_line4: string;
    public practitioner_zipcode: number;
    public practitioner_overall_rating: number;
    public practitioner_rating_count: number;
    public practitioner_reviews: Array<PractitionerReview>;
    public practitioner_profile_image: string;
    public live_class_image: string;
    public is_favorite_practitioner: boolean;
    public favorite_practitioner_id: string;
    public favorite_liveclass_id: string;
    public is_favorite_class: boolean;
    public upcoming_classes: Array<UpcomingClass>;
    public z_id:string;
}

export class JoinClass{
    public username:string;
    public useremail:string;
    public cvc:string;
    public cc_number:string;
    public card_full_name:string;
    public session_amount:string;
    public description:string;
    public country:string;
    public exp_month:number;
    public exp_year:number;
    public expiry:string;
    public zip_code:string;
}


export class JoinClassResponse {
    public id: string;
    public created: Date;
    public modified: Date;
    public customer?: any;
    public name: string;
    public email: string;
    public is_joined: boolean;
    public join_url: string;
    public live_class: string;
    public has_paid: boolean;
    public transaction_id: string;
}