import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import {ClassDetail, JoinClass} from './class.detail.model';
import {JoinPaidLiveClassComponent} from '../modal/join-paid-live-class/join-paid-live-class.component';
import { JoinFreeLiveClassComponent } from '../modal/join-free-live-class/join-free-live-class.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-class-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class ClassDetailComponent implements OnInit {
  @ViewChild('myModal') joinclassmodal;
  @ViewChild(DragScrollComponent) reviewScroll: DragScrollComponent;
  modalRef?: BsModalRef;
  classDetail:ClassDetail;
  showLoadScreen:boolean=false;
  customer_id:string="";
  class_id:string;
  customer_email:string="";
  joinClass:JoinClass;
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public route:ActivatedRoute,
    public authService:AuthService,
    public modalService:NgbModal,
    public toast:ToastrService) {
      this.classDetail=new ClassDetail();
      this.joinClass=new JoinClass();
    }

  ngOnInit(): void {
    let logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (logedinData) {
      this.customer_id = logedinData["id"];
      let userInfo = logedinData["user_id"];
      this.customer_email = userInfo["email"];
    }
    //this.is_subscribed=logedinData["is_subscribed"];
    this.route.params.subscribe((param)=>{
      this.class_id=param["cid"];
      this.getClassDetail();
    });
    
  }


  getClassDetail() {
    this.loader.show();
    this.showLoadScreen=true;
    this.apiService.getClassDetail(this.customer_id ,this.class_id,this.customer_email)
      .subscribe((classRes: any) => {
        console.log('classRes', classRes);
        this.classDetail = classRes;
        this.showLoadScreen=false;
        this.loader.hide();
      }, (error) => {
        console.log('class detail error', error);
        this.loader.hide();
        this.showLoadScreen=false;
      });
  }

  moveLeft(){
    this.reviewScroll.moveLeft();
  }

  moveRight(){
    this.reviewScroll.moveRight();
  }

  openModal(template: TemplateRef<any>) {
    this.modalService.open(template,{windowClass:'share-modal'});
  }

  initiateJoinClassModal() {
    
    let modalRef;
    //modalRef =this.modalService.open(JoinFreeLiveClassComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'modal-basic-title',  size: 'md', windowClass: 'join-class-modal' });
    //modalRef =this.modalService.open(JoinPaidLiveClassComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'modal-basic-title',  size: 'md', windowClass: 'join-class-modal' });
    if (this.classDetail.is_joined_class) {
      this.toast.info("Looks like you have already booked this class!");
    }
    else {
      if (this.classDetail.live_class_price == 0) {
        modalRef = this.modalService.open(JoinFreeLiveClassComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'md', windowClass: 'join-class-modal' });
      } else {
        modalRef = this.modalService.open(JoinPaidLiveClassComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'md', windowClass: 'join-class-modal' });
      }

      modalRef.componentInstance.selectedClass = this.classDetail;
    }
  }



  getTime(min:any){
    min = Math.floor(min);
    if (min < 60) {
      return min + " min";
    }
    else if (min == 60) {
      return "1 hour";
    }
    else {
      if ((min % 60) > 0) {
        return Math.floor(min / 60) + " hours " + (min % 60) + " min";
      }
      else {
        return Math.floor(min / 60) + " hours";
      }
    }
  }

  addRemoveFavPractitioner(){
    this.loader.show();
    let action='favorite-practitioner';
    
    let payload = {
      "customer_id": this.customer_id,
      "practitioner": this.classDetail.practitioner_id
    };
    if(this.classDetail.is_favorite_practitioner){
      this.removeFavorite(action,this.classDetail.favorite_practitioner_id);
    }
    else{
      this.addFavorite(action,payload);
    }
  }

  addFavorite(action,payload) {
    this.apiService.addFavorite(action,payload)
      .subscribe((addFavoritePractitionResponse: any) => {
        
        console.log('addFavoritePractitionResponse', addFavoritePractitionResponse);
        let response = addFavoritePractitionResponse;
        if (action == 'favorite-practitioner') {
          this.classDetail.is_favorite_practitioner = response.is_favorite;
          this.classDetail.favorite_practitioner_id = response.id;
        }
        else if (action == 'favorite-live-class') {
          this.classDetail.is_favorite_class = response.is_favorite;
          this.classDetail.favorite_liveclass_id = response.id;
        }
        this.loader.hide();
        //this.isAdded=true;
        //this.modalService.show(template);
      }, (error) => {
        console.log('addFavRes error', error);
        this.loader.hide();
      });
  }

  removeFavorite(action,id){
    this.loader.show();
    this.apiService.removeFromFavorite(action,id)
      .subscribe((res: any) => {
        
        console.log('Remove '+action, res);  
        if(action=='favorite-practitioner'){
          this.classDetail.is_favorite_practitioner=false;  
        } 
        else if(action=='favorite-live-class') {
          this.classDetail.is_favorite_class=false;  
        }  
        this.loader.hide();
        //this.isAdded=false;
        //this.modalService.show(template);
      }, (error) => {
        console.log('Remove '+action, error);
        this.loader.hide();
      });
  }

  addRemoveFavClass(){
    this.loader.show();
    
    let action='favorite-live-class'
    let payload = {
      "customer_id": this.customer_id,
      "live_class": this.classDetail.live_class_id
    };
    if(this.classDetail.is_favorite_class){
      this.removeFavorite(action,this.classDetail.favorite_liveclass_id);
    }
    else{
      this.addFavorite(action,payload);
    }
  }

}
