import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinFreeLiveClassComponent } from './join-free-live-class.component';

describe('JoinFreeLiveClassComponent', () => {
  let component: JoinFreeLiveClassComponent;
  let fixture: ComponentFixture<JoinFreeLiveClassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoinFreeLiveClassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinFreeLiveClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
