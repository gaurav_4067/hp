import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ZoomMeetingConfig } from 'src/app/config/config';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { ConfirmModalComponent } from 'src/app/v2/confirm-modal/confirm-modal.component';
import { ClassDetail, JoinClass, JoinClassResponse } from '../../detail/class.detail.model';

@Component({
  selector: 'app-join-free-live-class',
  templateUrl: './join-free-live-class.component.html',
  styleUrls: ['./join-free-live-class.component.scss']
})
export class JoinFreeLiveClassComponent implements OnInit {
  joinClass:JoinClass;
  selectedClass:ClassDetail;
  joinClassResponse:JoinClassResponse;
  step:number=1;
  logedinData:any;
  userData:any;
  isGroup:any;
  @Input() class_price:number;
  constructor(
    public authService:AuthService,
    public modalService:NgbModal,
    public apiService:ApiService,
    private loader: NgxSpinnerService,
  ) { 
    this.joinClass=new JoinClass();
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.isGroup = this.userData?.groups[0];
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (this.logedinData) {
      let userInfo = this.logedinData["user_id"];
      this.joinClass.username = userInfo["first_name"] + " " + userInfo["last_name"];
      this.joinClass.useremail = userInfo["email"];
    }
  }

  initiateJoinClass(e){
    if(e){
      this.loader.show();
      let payload={
        "customer": this.authService.isAuthenticated() ? this.logedinData.id : null,
        "name": this.joinClass.username,
        "email": this.joinClass.useremail,
        "join_url": '',
        "live_class": this.selectedClass.live_class_id
      }
      this.apiService.joinLiveClass(payload)
      .subscribe((joinClassResponse: any) => {
        
        console.log('joinClassResponse', joinClassResponse);
        this.joinClassResponse = joinClassResponse;
        
        if(this.joinClassResponse.is_joined){
          this.modalService.dismissAll();
            let modalRef =this.modalService.open(ConfirmModalComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'signup-confirm-title',  size: 'lg', windowClass: 'confirm-modal' });
            modalRef.componentInstance.showButton = true;
            modalRef.componentInstance.closeButtonText = "Got it";
            modalRef.componentInstance.isDefaultBtn=true;
            modalRef.componentInstance.message = "Check you email to find the link for this Live Class. You will be notified on email again with 30 minutes before this Live Class will start.";
            modalRef.componentInstance.heading = "Thank your for booking a seat in this Live Class!";
          // this.step=2;
        }

        this.loader.hide();
        //this.isAdded=true;
        //this.modalService.show(template);
      }, (error) => {
        console.log('joinClassResponse error', error);
        this.loader.hide();
      });
      
    }
  }
}
