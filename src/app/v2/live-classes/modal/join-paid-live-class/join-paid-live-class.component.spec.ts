import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinPaidLiveClassComponent } from './join-paid-live-class.component';

describe('JoinPaidLiveClassComponent', () => {
  let component: JoinPaidLiveClassComponent;
  let fixture: ComponentFixture<JoinPaidLiveClassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoinPaidLiveClassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinPaidLiveClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
