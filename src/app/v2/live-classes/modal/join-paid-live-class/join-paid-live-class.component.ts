import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { ConfirmModalComponent } from 'src/app/v2/confirm-modal/confirm-modal.component';
import { CardFormComponent } from 'src/app/v2/payment/card-form/card-form.component';
import { LoginViewComponent } from 'src/app/v2/view/login-view/login-view.component';
import { ClassDetail, JoinClass, JoinClassResponse } from '../../detail/class.detail.model';
enum PaymentType { "CreditCard", 'Paypal', 'Other', 'None' };

@Component({
  selector: 'app-join-paid-live-class',
  templateUrl: './join-paid-live-class.component.html',
  styleUrls: ['./join-paid-live-class.component.scss']
})
export class JoinPaidLiveClassComponent implements OnInit {
  @ViewChild('validateChildComponentForm') public ChildComponent: CardFormComponent;
  private childFormValidCheck: boolean;
  joinClass: JoinClass;
  selectedClass: ClassDetail;
  joinClassResponse: JoinClassResponse;
  step: number = 1;
  payment_types = PaymentType;

  payment_type: PaymentType = PaymentType.None;

  @Input() class_price: number;
  //@Input() selectedClass:ClassDetail;
  constructor(
    public authService: AuthService,
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public modalService: NgbModal,
    public toastr:ToastrService
  ) {
    this.joinClass = new JoinClass();
    //this.selectedClass=new ClassDetail();
  }

  ngOnInit(): void {
    let logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (logedinData) {
      let userInfo = logedinData["user_id"];
      this.joinClass.username = userInfo["first_name"] + " " + userInfo["last_name"];
      this.joinClass.useremail = userInfo["email"];
    }
  }

  public isChildFormValid(formValid: any) {
    this.childFormValidCheck = formValid;
  }

  // initiateJoinClass(e) {
  //   if (e) {

  //     this.loader.show();
      
  //     let payload = {
  //       "customer": null,
  //       "name": this.joinClass.username,
  //       "email": this.joinClass.useremail,
  //       "join_url": "https://stackoverflow.com/",
  //       "live_class": this.selectedClass.live_class_id,
  //       "cc_number": this.joinClass.cc_number.replace(/\s+/g, ""),
  //       "exp_month": this.joinClass.expiry.split('/')[0],
  //       "exp_year": this.joinClass.expiry.split('/')[1],
  //       "cvc": this.joinClass.cvc,
  //       "session_amount": 5,
  //       "description": "",
  //       "country": "US"
  //     }

  //     this.apiService.joinLiveClass(payload)
  //       .subscribe((joinClassResponse: any) => {
          
  //         console.log('joinClassResponse', joinClassResponse);
  //         this.joinClassResponse = joinClassResponse;

  //         if (this.joinClassResponse.is_joined) {
  //           this.step = 3;
  //         }

  //         this.loader.hide();
  //         //this.isAdded=true;
  //         //this.modalService.show(template);
  //       }, (error) => {
  //         console.log('joinClassResponse error', error);
  //         this.loader.hide();
  //       });
  //   }
  // }

  enterForm(parentForm) {
    //this.submitted = true;
    this.ChildComponent.validateChildForm(true);
    if (!parentForm.valid || !this.childFormValidCheck || this.payment_type==PaymentType.None) {
      return;
    } else {
      this.loader.show();
      
      let payload = {
        "customer": null,
        "name": this.joinClass.username,
        "email": this.joinClass.useremail,
        "join_url": "",
        "live_class": this.selectedClass.live_class_id,
        "cc_number": this.joinClass.cc_number.replace(/\s+/g, ""),
        "exp_month": this.joinClass.expiry.split('/')[0],
        "exp_year": this.joinClass.expiry.split('/')[1],
        "cvc": this.joinClass.cvc,
        "session_amount": this.selectedClass.live_class_price,
        "description":this.selectedClass.live_class_title,
        "country": "US"
      }

      this.apiService.joinLiveClass(payload)
        .subscribe((joinClassResponse: any) => {
          
          console.log('joinClassResponse', joinClassResponse);
          this.joinClassResponse = joinClassResponse;

          if (this.joinClassResponse.has_paid) {
            this.updateCustomerPayment();
            this.modalService.dismissAll();
            let modalRef =this.modalService.open(ConfirmModalComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'signup-confirm-title',  size: 'lg', windowClass: 'confirm-modal' });
            modalRef.componentInstance.showButton = true;
            modalRef.componentInstance.closeButtonText = "Got it";
            modalRef.componentInstance.isDefaultBtn=true;
            modalRef.componentInstance.message = "Check you email to find the link for this Live Class. You will be notified on email again with 30 minutes before this Live Class will start.";
            modalRef.componentInstance.heading = "Thank your for booking a seat in this Live Class!";
            //this.step = 3;
          }

          this.loader.hide();
          //this.isAdded=true;
          //this.modalService.show(template);
        }, (error) => {
          console.log('joinClassResponse error', error);
          this.loader.hide();
        });
    }
  }

  updateCustomerPayment() {
    const payload = {
      'customer':null,
      'transactionid': this.joinClassResponse.transaction_id,
      'haspaid': this.joinClassResponse.has_paid,
      'paymentinfo': '',
      'name':this.joinClass.username,
      'email':this.joinClass.useremail,
      'live_class': this.selectedClass.live_class_id
    };
    this.loader.show();
    this.apiService.confirmingAppointment(payload)
      .subscribe((confirmingAppointment) => {
        console.log('confirmingCustomerPayment', confirmingAppointment);
        this.loader.hide();
      }, (error) => {
        console.log('confirmingCustomerPayment error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  selectPaymentType(targetType: PaymentType) {

    // If the checkbox was already checked, clear the currentlyChecked variable
    if (this.payment_type === targetType) {
      this.payment_type = PaymentType.None;
      return;
    }

    this.payment_type = targetType;
  }

  openLoginForm(){
    let modalRef=this.modalService.open(LoginViewComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'login-view-title',  size: 'md', windowClass: 'login-view-class-modal' });
    modalRef.componentInstance.loginFromScreen=false;
    modalRef.componentInstance.customLogin=true;
    modalRef.componentInstance.modalRef=modalRef;
  }
}
