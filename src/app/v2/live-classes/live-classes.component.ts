import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { LiveClassDetail, LiveClassWrapper } from '../model/live-class.model';
@Component({
  selector: 'app-live-classes',
  templateUrl: './live-classes.component.html',
  styleUrls: ['./live-classes.component.scss']
})
export class LiveClassesComponent implements OnInit {
  //liveClass:LiveClassWrapper;
  nextFourClasses: Array<LiveClassDetail>;
  liveClass: Array<LiveClassDetail>;
  dataLimit: number = 12;
  dataCategory: string = "all";
  live_class_count: number = 0;
  page:number=1;
  showLoadScreen:boolean=false;
  searchKey:string="";
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public route:ActivatedRoute) {
    this.liveClass = Array<LiveClassDetail>();
    this.nextFourClasses = new Array<LiveClassDetail>();
  }
  ngOnInit(): void {
    this.route.params.subscribe((param) => {
      if (param["key"]) {
        this.searchKey = param["key"];
      }
      this.getLiveClasses('all', false);
    })
   
  }

  getLiveClasses(category, isMore = false) {
    
    this.dataCategory = category;
    if (isMore) {
      if (this.liveClass.length >= this.live_class_count) {
        //No call further API
      }
      else {
        this.page=this.page+1;
        this.showLoadScreen=true;
        //this.dataLimit = this.dataLimit + 10;
        this.callAPI(isMore);
      }
    }
    else {
      this.callAPI(false);
    }
  }

  callAPI(isMore) {
    this.loader.show();
    this.apiService.getLiveClasses(this.dataLimit, this.dataCategory,this.page,this.searchKey)
      .subscribe((liveClassesResponse: any) => {
        console.log('liveClasses', liveClassesResponse);
        this.live_class_count = liveClassesResponse.live_class_count;
        if (!isMore) {
          this.liveClass = [];
          //this.dataLimit=this.dataLimit+10;
        }
         
        if (liveClassesResponse.live_class_list.data.length > 0) {
          if (this.dataCategory == 'all' && this.page==1) {
            this.nextFourClasses = liveClassesResponse.live_class_list.data.slice(0, 4);
          }
        }

        liveClassesResponse.live_class_list.data.forEach(element => {
          this.liveClass.push(element);
        });
        this.showLoadScreen=false;
        this.loader.hide();
      }, (error) => {
        console.log('liveClasses error', error);
        this.loader.hide();
        this.showLoadScreen=false;
      });
  }
}
