import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { HomeDetail, Testimonial } from '../model/home.model';
import { PractitionerSignupComponent } from '../view/practitioner-signup/practitioner-signup.component';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  @ViewChild(DragScrollComponent) ds1: DragScrollComponent;
  logedinData:any;
  homeDetail: HomeDetail;
  searchText: string = "";
  selectedIndex: number;
  selectedTestimonial: Testimonial;
  verification_data: string = '';
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public route: ActivatedRoute,
    public modalService: NgbModal,
    public authService: AuthService) {
    this.homeDetail = new HomeDetail();
    this.selectedTestimonial = new Testimonial();
  }

  ngOnInit(): void {
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.route.params.subscribe((param) => {
      this.verification_data = param["data"];
      if (this.verification_data) {
        this.verifyCustomer();
      }
    });

    this.getHomePageData();
  }

  verifyCustomer() {
    this.loader.show();
    this.showConfirmModal();
    this.apiService.verifyCustomer(this.verification_data)
      .subscribe((res: any) => {
        console.log('verifyCustomer', res);
        //this.showConfirmModal();
        this.loader.hide();

      }, (error) => {
        console.log('verifyCustomer error', error);
        this.loader.hide();
      });
  }

  showConfirmModal() {
    let modalRef = this.modalService.open(ConfirmModalComponent, { scrollable: true, centered: true, ariaLabelledBy: 'signup-confirm-title', size: 'md', windowClass: 'confirm-modal' });
    modalRef.componentInstance.showButton = true;
    modalRef.componentInstance.isPrimary=true;
    modalRef.componentInstance.heading = "Congratulation! Your account is now confirmed!";
  }

  getHomePageData() {
    this.loader.show();
    this.apiService.getHomePageData()
      .subscribe((homeDetail: any) => {
        console.log('homeDetail', homeDetail);
        this.homeDetail = homeDetail;
        if (this.homeDetail.testimonials.length > 0) {
          this.selectedIndex = 0;
          this.selectedTestimonial = this.homeDetail.testimonials[0]
        }
        this.loader.hide();

      }, (error) => {
        console.log('homeDetail error', error);
        this.loader.hide();
      });
  }

  moveLeft() {

    if (this.selectedIndex > 0) {
      this.selectedIndex = this.selectedIndex - 1;
      this.selectedTestimonial = this.homeDetail.testimonials[this.selectedIndex];
    }
    this.ds1.moveLeft();
  }

  moveRight() {

    if (this.selectedIndex < this.homeDetail.testimonials.length - 1) {
      this.selectedIndex = this.selectedIndex + 1;
      this.selectedTestimonial = this.homeDetail.testimonials[this.selectedIndex];
    }
    this.ds1.moveRight();
  }

  bindSelectedTestimonial(i) {
    this.selectedIndex = i;
    this.selectedTestimonial = this.homeDetail.testimonials[i];
  }

  signupPractitionerModal() {
    let logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (logedinData) {
      let modalRef =this.modalService.open(ConfirmModalComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'signup-confirm-title',  size: 'md', windowClass: 'confirm-modal' });
      modalRef.componentInstance.showConfirmButton = true;
      modalRef.componentInstance.isPrimary=true;
      modalRef.componentInstance.message = "";
      modalRef.componentInstance.heading = "Are you sure to join healplace as a practitioner?";
    }
    else {
      localStorage.removeItem("access_token");
      localStorage.removeItem("user_data");
      localStorage.removeItem("refresh_token");
      localStorage.removeItem("logedin_data");
      this.authService.notifiedHeader();
      let modalRef = this.modalService.open(PractitionerSignupComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'signup-class-modal', backdrop: false });
    }
  }
}
