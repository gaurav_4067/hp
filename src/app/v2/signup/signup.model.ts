export class SignupUser {
    constructor(){
        this.SelectedCity=new Location();
        this.SelectedCountry=new Location();
        this.SelectedState=new Location();
    }
    public first_name: string;
    public email: string;
    public username: string;
    public password1: string;
    public password2: string;
    public city_id: string;
    public zipcode: string;
    public is_newsletter: boolean=false;
    public SelectedCountry: Location;
    public SelectedState: Location;
    public SelectedCity: Location;
    
}

export class Location{
    public id:string;
    public name:string;
    public phone_code:string;
}