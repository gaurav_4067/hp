import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import {SignupUser,Location} from './signup.model';
import { GeneralService } from 'src/app/services/GeneralService';
import { timeStamp } from 'console';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupType:string;
  countries:Array<any>;
  states:any;
  cities:any;
  user:SignupUser;
  selectedCountry?:string;
  selectedState?:string;
  registrationRes:any;
  constructor(public route:ActivatedRoute,
    public loader:NgxSpinnerService,
    public apiService:ApiService,
    public thirpApiService:ThirdPApiService,
    public generalService:GeneralService,
    public modalService:NgbModal) {
     }

  ngOnInit(): void {
  }
 
}
