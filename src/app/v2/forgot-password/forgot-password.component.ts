import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { HttpErrorResponse } from "@angular/common/http";
import { ActivatedRoute, Router } from '@angular/router';
import { ForgotUserDetail } from './forgot-password-model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  user_email: string;
  userId: string;
  user:ForgotUserDetail;
  
  userToken: string;
  resetMode: boolean = false;
  constructor(public toastr: ToastrService,
    public loader: NgxSpinnerService,
    public apiService: ApiService,
    public route: ActivatedRoute,
    public router: Router) { 
      this.user=new ForgotUserDetail();
    }

  ngOnInit(): void {
    let tempURL = this.route.snapshot.queryParamMap;
    this.userId = tempURL.get('/uid');
    this.userToken = tempURL.get('token').slice(0, -1);
    
    if (this.userId) {
      this.resetMode = true;
    }
  }

  forgotPassword(isValid) {
    if (isValid) {
      this.loader.show();
      let payload = {
        email: this.user_email,
      }
      console.log(payload);
      //return;
      this.apiService
        .forgotPassword(payload)
        .subscribe((response: any) => {
          if (response) {
            this.loader.hide();
            this.toastr.success(response.detail);
          }
        },
          (errResponse: HttpErrorResponse) => {
            if (errResponse['error']['non_field_errors']) {
              errResponse['error']['non_field_errors'].forEach(element => {
                this.toastr.error(element);
              });
            } else {
              this.toastr.error('Something went wrong please try again');
            }
            this.loader.hide();
          }
        );
    }
  }

  resetPassword(isValid) {
    if (isValid) {
      this.loader.show();
      let payload = {
        new_password1: this.user.password1,
        new_password2: this.user.password2,
        uid: this.userId,
        token: this.userToken,
      }
      console.log(payload);
      this.apiService
        .changeForgotResetPassword(payload, this.userId, this.userToken)
        .subscribe((response: any) => {
          console.log(response);
          if (response.email) {
            this.toastr.warning(response.email);
          } else if (response.detail) {
            this.toastr.success(response.detail);
            this.router.navigate(["/home"]);
          }
          this.loader.hide();
        },
          (errResponse: HttpErrorResponse) => {
            if (errResponse['error']['non_field_errors']) {
              errResponse['error']['non_field_errors'].forEach(element => {
                this.toastr.error(element);
              });
            } else if (errResponse['error']['password1']) {
              errResponse['error']['password1'].forEach(element => {
                this.toastr.error(element);
              });
            } else if (errResponse['error']['new_password2']) {
              errResponse['error']['new_password2'].forEach(element => {
                this.toastr.error(element);
              });
            } else {
              this.toastr.error('Something went wrong please try again');
            }
            this.loader.hide();
          }
        );
    }
  }
}
