import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PractitionerProfileComponent } from './practitioner-profile.component';

describe('PractitionerProfileComponent', () => {
  let component: PractitionerProfileComponent;
  let fixture: ComponentFixture<PractitionerProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PractitionerProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PractitionerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
