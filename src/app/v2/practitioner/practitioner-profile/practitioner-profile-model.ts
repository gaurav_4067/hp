
    export class Review {
        constructor(){
            this.interest=new Array<String>();
        }
        public review: string;
        public overall_rating: number;
        public customer_id: string;
        public id: string;
        public user_id__first_name: string;
        public user_id__last_name: string;
        public user_id__email: string;
        public interest: Array<String>;
        public nick_name: string;
        public profile_image: string;
        public is_profile_pic_approved: boolean;
        public is_curated_images: boolean;
    }

    export class PractitionerDetails {
        constructor(){
            this.expertise=new  Array<string>();
            this.reviews=new Array<Review>();
        }
        public firstname: string;
        public lastname: string;
        public expertise: Array<string>;
        public overall_rating: number;
        public rating_count: number;
        public description: string;
        public practitioner_id: string;
        public address_line1: string;
        public address_line2: string;
        public address_line3?: any;
        public address_line4: string;
        public zipcode: number;
        public reviews: Array<Review>;
        public course_count: number;
        public live_class_count: number;
        public is_favorite_practitioner: boolean;
        public favorite_practitioner_id:string;
        public practitioner_profile_image: string;
        public starting_price: number;
    }

    export class ViewersDetail {
        constructor(){
            this.interest=new Array<string>();
        }
        public id: string;
        public user_id__first_name: string;
        public user_id__last_name: string;
        public user_id__email: string;
        public interest: Array<string>;
        public nick_name: string;
        public profile_image: string;
        public is_profile_pic_approved: boolean;
        public is_curated_images: boolean;
    }

    export class Lesson {
        public id: string;
        public lesson_title: string;
        public lesson_duration: number;
        public video_file: string;
        public lesson_description: string;
        public cover_photo:string;
    }

    export class MyCourse {
        constructor(){
            this.viewers_detail=new Array<ViewersDetail>();
            this.lessons=new Array<Lesson>();
            this.practitioner_expertise=new Array<string>();
        }
       public practitioner_firstname: string;
       public practitioner_lastname: string;
       public practitioner_expertise: Array<string>;
       public course_id: string;
       public course_title: string;
       public related_course_duration_in_sec: number;
       public related_course_rating: number;
       public related_course_rated_count: number;
       public viewers_count: number;
       public viewers_detail: Array<ViewersDetail>;
       public lesson_count: number;
       public lessons: Array<Lesson>;
    }


    export class RelatedCourse {
        constructor(){
            this.practitioner_expertise=new Array<string>();
            this.lessons=new Array<Lesson>();
            this.viewers_detail=new Array<any>();
        }
       public practitioner_firstname: string;
       public practitioner_lastname: string;
       public practitioner_expertise: Array<string>;
       public course_id: string;
       public course_title: string;
       public related_course_duration_in_sec: number;
       public related_course_rating: number;
       public related_course_rated_count: number;
       public viewers_count: number;
       public viewers_detail: Array<any>;
       public lesson_count: number;
       public lessons: Array<Lesson>;
    }

    export class RelatedPractitioner {
        constructor(){
            this.practitioner_expertise = new Array<string>();
        }
       
        public practitioner_firstname: string;
        public practitioner_lastname: string;
        public practitioner_expertise: Array<string>;
        public practitioner_rating: number;
        public practitioner_rate_count: number;
        public practitioner_id: string;
        public practitioner_profile_image: string;
        public starting_price: number;
    }

    export class LiveClass {
        constructor(){
            this.practitioner__expertise = new Array<string>();
        }
       public id: string;
       public title: string;
       public practitioner__user_id__first_name: string;
       public practitioner__user_id__last_name: string;
       public practitioner__expertise: Array<string>;
       public practitioner__profile_image: string;
       public start_time: Date;
       public end_time?: Date;
       public class_date: string;
       public duration: number;
       public image: string;
       public practitioner: string;
       public is_completed: boolean;
       public description: string;
    }

    export class ProfileDetail {
        constructor(){
            this.practitioner_details=new PractitionerDetails();
            this.my_courses=new Array<MyCourse>()
            this.related_practitioner=new Array<RelatedPractitioner>();
            this.live_classes=new Array<LiveClass>();
            this.related_courses=new Array<RelatedCourse>();
        }
        public practitioner_details: PractitionerDetails;
        public my_courses: Array<MyCourse>;
        public related_courses: Array<RelatedCourse>;
        public related_practitioner: Array<RelatedPractitioner>;
        public live_classes: Array<LiveClass>;
    }


    export class AddFavoritePractitionerResponse {
        public id: string;
        public created: Date;
        public modified: Date;
        public customer_id: string;
        public is_favorite: boolean;
        public practitioner: string;
    }

