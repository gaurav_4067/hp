import { templateSourceUrl } from '@angular/compiler';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import {AddFavoritePractitionerResponse, ProfileDetail} from './practitioner-profile-model';

@Component({
  selector: 'app-practitioner-profile',
  templateUrl: './practitioner-profile.component.html',
  styleUrls: ['./practitioner-profile.component.scss']
})
export class PractitionerProfileComponent implements OnInit {
  @ViewChild(DragScrollComponent) reviewScroll: DragScrollComponent;
  modalRef?: BsModalRef;
  isAdded:boolean=false;
  profileDetail:ProfileDetail;
  addFavoritePractitionResponse:AddFavoritePractitionerResponse;
  itemsPerSlide = 4;
  singleSlideOffset = true;
  noWrap = true;
  expertise="";
  rating_count=5;
  practitioner_id="";
  customer_id="";
  is_subscribed:boolean;
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService:GeneralService,
    public route:ActivatedRoute,
    public modalService: NgbModal,
    ) {
    this.profileDetail=new ProfileDetail();
    this.addFavoritePractitionResponse=new AddFavoritePractitionerResponse();
   }

  ngOnInit(): void {
    let logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.customer_id=logedinData["id"]
    this.is_subscribed=logedinData["is_subscribed"];
    this.route.params.subscribe((param)=>{
      this.practitioner_id=param["pid"];
      this.getPractitionerProfile();
    })
    
  }

  getPractitionerProfile() {
    this.loader.show();
    this.apiService.getPractitionerProfile(this.customer_id,this.practitioner_id)
      .subscribe((profileDetail: any) => {
        console.log('profileDetail', profileDetail);
        this.profileDetail=profileDetail;
        this.expertise=this.profileDetail.practitioner_details.expertise[0];
        this.loader.hide();
         
      }, (error) => {
        console.log('favoriteArticles error', error);
        this.loader.hide();
      });
  }

  addRemoveFavPractitioner(template: TemplateRef<any>){
    this.loader.show();
    let payload = {
      "customer_id": this.customer_id,
      "practitioner": this.practitioner_id
    };
    if(this.profileDetail.practitioner_details.is_favorite_practitioner){
      this.removeFromFavorite(this.profileDetail.practitioner_details.favorite_practitioner_id,template);
    }
    else{
      this.addFavoritePractitioner(payload,template);
    }
  }

  addFavoritePractitioner(payload,template) {
    this.apiService.addFavorite('favorite-practitioner',payload)
      .subscribe((addFavoritePractitionResponse: any) => {
        console.log('addFavoritePractitionResponse', addFavoritePractitionResponse);
        this.addFavoritePractitionResponse = addFavoritePractitionResponse;
        this.profileDetail.practitioner_details.is_favorite_practitioner = this.addFavoritePractitionResponse.is_favorite;
        this.profileDetail.practitioner_details.favorite_practitioner_id=this.addFavoritePractitionResponse.id;
        this.loader.hide();
        this.isAdded=true;
        this.modalService.open(template,{windowClass:'favorite-modal'}) ;
      }, (error) => {
        console.log('addFavRes error', error);
        this.loader.hide();
      });
  }

  removeFromFavorite(id,template){
    this.loader.show();
    this.apiService.removeFromFavorite('favorite-practitioner',id)
      .subscribe((res: any) => {
        
        console.log('Remove favorite-practitioner', res);  
        this.profileDetail.practitioner_details.is_favorite_practitioner=false;      
        this.loader.hide();
        this.isAdded=false;
        this.modalService.open(template,{windowClass:'favorite-modal'}) ;
        // this.modalRef=this.modalService.show(template,{class:'favorite-modal'});
      }, (error) => {
        console.log('Remove favorite-practitioner', error);
        this.loader.hide();
      });
  }
 
  moveLeft(){
    this.reviewScroll.moveLeft();
  }

  moveRight(){
    this.reviewScroll.moveRight();
  }

  
  openModal(template: TemplateRef<any>) {
    this.modalService.open(template,{windowClass:'share-modal'});
  }

  closeModal(template: TemplateRef<any>) {
      this.modalService.dismissAll();
  }
}
