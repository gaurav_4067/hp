import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { JoinClass } from '../../live-classes/detail/class.detail.model';
import { CardFormComponent } from '../../payment/card-form/card-form.component';
import { LoginViewComponent } from '../../view/login-view/login-view.component';
enum PaymentType { "CreditCard", 'Paypal', 'Other', 'None' };

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.scss']
})
export class DonateComponent implements OnInit {
  data: any;
  id:string;
  step: number = 1;
  valueIndex: number = 0
  donateAmount: string;
  logedinData: any;
  @ViewChild('validateChildComponentForm') public ChildComponent: CardFormComponent;
  private childFormValidCheck: boolean;
  joinClass: JoinClass;
  payment_types = PaymentType;

  payment_type: PaymentType = PaymentType.None;

  constructor(
    public authService: AuthService,
    public modalService: NgbModal,
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService
  ) {
    this.joinClass = new JoinClass();
  }

  ngOnInit(): void {
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (this.logedinData) {
      let userInfo = this.logedinData["user_id"];
      // this.joinClass.username = userInfo["first_name"] + " " + userInfo["last_name"];
      // this.joinClass.useremail = userInfo["email"];
    }
  }

  donateToPractitioner(amount, valueIndex) {
    
    this.donateAmount = amount;
    this.valueIndex = valueIndex;
    if (this.authService.isAuthenticated()) {
      this.step = 2;
    }
    else {
      let modalRef = this.modalService.open(LoginViewComponent, { scrollable: true, centered: true, ariaLabelledBy: 'login-view-title', size: 'md', windowClass: 'login-view-class-modal' });
      modalRef.componentInstance.loginFromScreen = false;
      modalRef.componentInstance.customLogin = true;
      modalRef.componentInstance.modalRef = modalRef;
    }
  }

  public isChildFormValid(formValid: any) {
    this.childFormValidCheck = formValid;
  }

  enterForm(parentForm) {
    
    //this.submitted = true;
    this.ChildComponent.validateChildForm(true);
    if (!parentForm.valid || !this.childFormValidCheck || this.payment_type==this.payment_types.None) {
      return;
    } else {
      this.loader.show();
      
      let payload = {
        
        "customer_id": this.logedinData["id"],
        "donated_amount": this.donateAmount,
        "cc_number": this.joinClass.cc_number.replace(/\s+/g, ""),
        "exp_month": this.joinClass.expiry.split('/')[0],
        "exp_year": this.joinClass.expiry.split('/')[1],
        "cvc": this.joinClass.cvc,
        "session_amount": this.donateAmount,
        "description": "donation",
        "country": "US"
      }

      if(this.data.donateType=="live-class"){
        payload["live_class"]=this.data.id
      }
      else if(this.data.donateType=="course"){
        payload["course"]=this.data.id
      }
    

      this.apiService.donatetoPractitioner(payload)
        .subscribe((response: any) => {
          console.log('donatetoPractitioner response', response);
          let payment_payload = {
            "customer": this.logedinData["id"],
            "transactionid": response.transaction_id,
            "haspaid": response.has_paid,
            "paymentinfo": "",
            "donate": response.id
          }
          this.confirmPayment(payment_payload);
          //this.loader.hide();
        }, (errorResponse) => {
          console.log('donatetoPractitioner errorResponse', errorResponse);
          //this.loader.hide();
        });
    }
  }

  confirmPayment(payload){
    this.apiService.confirmingAppointment(payload)
        .subscribe((response: any) => {
          console.log('donatetoPractitioner confirm payment response', response);
          this.loader.hide();
          this.modalService.dismissAll();
          let modalRef =this.modalService.open(ConfirmModalComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'signup-confirm-title',  size: 'md', windowClass: 'confirm-modal' });
          modalRef.componentInstance.showButton=true;
          modalRef.componentInstance.isPrimary=true;


          modalRef.componentInstance.heading="Thank you for donation!";
        }, (errorResponse) => {
          console.log('donatetoPractitioner confirm payment errorResponse', errorResponse);
          this.loader.hide();
        });
  }

  selectPaymentType(targetType: PaymentType) {

    // If the checkbox was already checked, clear the currentlyChecked variable
    if (this.payment_type === targetType) {
      this.payment_type = this.payment_types.None;
      return;
    }

    this.payment_type = targetType;
  }


  // initiateJoinClass(e){
  //   if(e){
  //     this.loader.show();
  //     let payload={
  //       "customer": this.authService.isAuthenticated() ? this.logedinData.id : null,
  //       "name": this.joinClass.username,
  //       "email": this.joinClass.useremail,
  //       "join_url": "https://stackoverflow.com/",
  //       "live_class": this.selectedClass.live_class_id
  //     }
  //     this.apiService.joinLiveClass(payload)
  //     .subscribe((joinClassResponse: any) => {

  //       console.log('joinClassResponse', joinClassResponse);
  //       this.joinClassResponse = joinClassResponse;

  //       if(this.joinClassResponse.is_joined){
  //         this.step=2;
  //       }

  //       this.loader.hide();
  //       //this.isAdded=true;
  //       //this.modalService.show(template);
  //     }, (error) => {
  //       console.log('joinClassResponse error', error);
  //       this.loader.hide();
  //     });

  //   }
  // }
}
