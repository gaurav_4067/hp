import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPractitionerComponent } from './search-practitioner.component';

describe('SearchPractitionerComponent', () => {
  let component: SearchPractitionerComponent;
  let fixture: ComponentFixture<SearchPractitionerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchPractitionerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPractitionerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
