import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import {PractitionerWrapper} from 'src/app/v2/model/practition-list-model'
@Component({
  selector: 'app-search-practitioner',
  templateUrl: './search-practitioner.component.html',
  styleUrls: ['./search-practitioner.component.scss']
})
export class SearchPractitionerComponent implements OnInit {
  practitioner:PractitionerWrapper;
  dataLimit:number=15;
  itemsPerPage:number=15;
  maxSize:number=4;
  searchKey:string='';

  conditions = [{ ID: 1, "Name": "First Condition", "Checked": false },
  { ID: 2, "Name": "Second Condition", "Checked": false },
  { ID: 3, "Name": "Third Condition", "Checked": false },
  { ID: 4, "Name": "Fourth Condition", "Checked": false },
  { ID: 5, "Name": "Fifth Condition", "Checked": false }
  ];
  specialties = [{ ID: 1, "Name": "First Specialties", "Checked": false },
  { ID: 2, "Name": "Second Specialties", "Checked": false },
  { ID: 3, "Name": "Third Specialties", "Checked": false },
  { ID: 4, "Name": "Fourth Specialties", "Checked": false },
  { ID: 5, "Name": "Fifth Specialties", "Checked": false }
  ];
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService:GeneralService,
    public route:ActivatedRoute) { 
      //this.dataLimit=this.generalService.dataLimit;
      this.practitioner=new PractitionerWrapper();
      
    } 

  ngOnInit(): void {
    this.route.params.subscribe((param) => {
      if (param["key"]) {
        this.searchKey = param["key"];
      }
      this.getPractitionerList(1);
    })
    
  }

  getPractitionerList(pageNo){
    this.loader.show();
    this.apiService.getPractitionerListPublic(pageNo,this.dataLimit,this.searchKey)
      .subscribe((practitioner: any) => {
        console.log('practitioner', practitioner);
        this.practitioner=practitioner;
        //this.practitionerPage=pageNo;
        this.loader.hide();
      }, (error) => {
        console.log('favoritePractitioners error', error);
        this.loader.hide();
      });
  }

}
