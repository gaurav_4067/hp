import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { PlyrComponent } from 'ngx-plyr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { DonateComponent } from '../../practitioner/donate/donate.component';
import { LoginViewComponent } from '../../view/login-view/login-view.component';
import { CourseRatingComponent } from '../course-rating/course-rating.component';
import { PurchaseCourseComponent } from '../purchase-course/purchase-course.component';
import {CourseDetailWrapper} from './detail.model';
 
@Component({
  selector: 'course-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  //@ViewChild(PlyrComponent, { static: true });
  // @ViewChild(DragScrollComponent) courseReviewScroll: DragScrollComponent;
  @ViewChild('courseReviewScroll', {read: DragScrollComponent}) courseReviewScroll: DragScrollComponent
  plyr: PlyrComponent;
  videoSources: Plyr.Source[] = [];
  course_wrapper:CourseDetailWrapper;
  showLoadScreen:boolean=true;
  customer_id:string="";
  course_id:string;
  player: Plyr;
  showNextLessonText:boolean=false;
  
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public route:ActivatedRoute,
    public authService:AuthService,
    private modalService:NgbModal) {
      this.course_wrapper=new CourseDetailWrapper();
      this.authService.notifiedPurchaseCourseEvent.subscribe(call => {
        if (call) {
          
          this.onInit();
        }
      });
      
    }

  ngOnInit(): void {
    this.onInit();
  }

  onInit(){
    let logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (logedinData) {
      this.customer_id = logedinData["id"];
    }
    //this.is_subscribed=logedinData["is_subscribed"];
    this.route.params.subscribe((param)=>{
      this.course_id=param["cid"];
      this.getCourse();
    });
  }

  getCourse() {
    this.loader.show();
    this.showLoadScreen=true;
    this.apiService.getCourse(this.customer_id,this.course_id)
      .subscribe((courseRes: any) => {
        console.log('courseRes', courseRes);
        this.course_wrapper = courseRes;
        this.course_wrapper.course_details.lessons.forEach(element => {
          this.videoSources.push({"src":this.generalService.getPractiotionerImageBaseUrl(element.video_file),"type": 'video/mp4'});
        });
        this.showLoadScreen=false;
        this.loader.hide();
      }, (error) => {
        console.log('courseList error', error);
        this.loader.hide();
        this.showLoadScreen=false;
      });
  }

  
  // playVideo(params: any){
  //   //params.detail.plyr.pause();
  //   if(this.authService.isAuthenticated()){
  //     //user is logged in
      
  //     
  //     if(this.course_wrapper.course_details.is_purchase_course || this.course_wrapper.course_details.course_price ==0){ 

       
  //       // if(this.course_wrapper.course_details.viewers_detail.findIndex(i=>i.id==this.customer_id)==-1){
  //       //   //user playing video first time, update video viewr detail
  //       //   this.updateViewerDetail();
  //       // }
  //       params.detail.plyr.play();
  //       console.log('played', event);
  //     }
  //     else{
  //       params.detail.plyr.stop();
  //       let modalRef =this.modalService.open(PurchaseCourseComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'modal-basic-title',  size: 'lg', windowClass: 'purchase-course-modal' });
  //       modalRef.componentInstance.selectedCourse=this.course_wrapper.course_details;
  //       modalRef.componentInstance.course_id=this.course_id;
  //     }
  //   }
  //   else{
  //     params.detail.plyr.stop()
  //     //User is not logged in 
  //     let modalRef=this.modalService.open(LoginViewComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'login-view-title',  size: 'md', windowClass: 'login-view-class-modal' });
  //     modalRef.componentInstance.loginFromScreen=false;
  //     modalRef.componentInstance.customLogin=true;
  //     modalRef.componentInstance.modalRef=modalRef;
  //   }
    
    
  // }

  // pauseVideo(params:any){
  //   params.pause();
  // }

  // updateViewerDetail(){
  //   let that =this;
  //   let payload = {
  //     "customer": this.customer_id,
  //     "course": this.course_id
  //   }
  //   that.apiService.updateCourseViewDetail(payload)
  //     .subscribe((updateViewerDetailResponse: any) => {
        
  //       console.log('updateViewerDetailResponse', updateViewerDetailResponse);
  //       let response = updateViewerDetailResponse;
         
  //       this.loader.hide();
  //       //this.isAdded=true;
  //       //this.modalService.show(template);
  //     }, (error) => {
  //       console.log('addFavRes error', error);
  //       this.loader.hide();
  //     });
  // }

  // initiateDonateToPractitioner(){
  //   
  //   let  modalRef = this.modalService.open(DonateComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'donate-class-modal',backdrop:false });
  //   let course_detail= this.course_wrapper.course_details;
  //   modalRef.componentInstance.data = {"first_name":course_detail.practitioner_firstname,"last_name":course_detail.practitioner_lastname,"practitioner_id":course_detail.practitioner_id,"practitioner_image":course_detail.practitioner_profile_image};
  // }

  // videoEnded(params: any){
  //   
    
  //   if(this.course_wrapper.course_details.lessons.length==1){
  //     let  modalRef = this.modalService.open(CourseRatingComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'donate-class-modal',backdrop:false });
  //     let course_detail= this.course_wrapper.course_details;
  //     modalRef.componentInstance.data =course_detail;
  //     modalRef.componentInstance.course_id =this.course_id;
  //   }
  //   else if(this.course_wrapper.course_details.lessons.length>1){
  //     this.showNextLessonText=true;
  //   }

  // }


  addRemoveFavPractitioner(){
    this.loader.show();
    let action='favorite-practitioner';
    let payload = {
      "customer_id": this.customer_id,
      "practitioner": this.course_wrapper.course_details.practitioner_id
    };
    if(this.course_wrapper.course_details.is_favorite_practitioner){
      this.removeFavorite(action,this.course_wrapper.course_details.favorite_practitioner_id);
    }
    else{
      this.addFavorite(action,payload);
    }
  }

  addFavorite(action,payload) {
    this.apiService.addFavorite(action,payload)
      .subscribe((addFavoritePractitionResponse: any) => {
        
        console.log('addFavoritePractitionResponse', addFavoritePractitionResponse);
        let response = addFavoritePractitionResponse;
        if (action == 'favorite-practitioner') {
          this.course_wrapper.course_details.is_favorite_practitioner = response.is_favorite;
          this.course_wrapper.course_details.favorite_practitioner_id = response.id;
        }
        else if (action == 'favorite-course') {
          this.course_wrapper.course_details.is_favorite_course = response.is_favorite;
          //this.classDetail.fav = response.id;
        }
        this.loader.hide();
        //this.isAdded=true;
        //this.modalService.show(template);
      }, (error) => {
        console.log('addFavRes error', error);
        this.loader.hide();
      });
  }

  removeFavorite(action,id){
    this.loader.show();
    this.apiService.removeFromFavorite(action,id)
      .subscribe((res: any) => {
        
        console.log('Remove '+action, res);  
        if(action=='favorite-practitioner'){
          this.course_wrapper.course_details.is_favorite_practitioner=false;  
        } 
        else if(action=='favorite-course') {
          this.course_wrapper.course_details.is_favorite_course=false;  
        }  
        this.loader.hide();
        //this.isAdded=false;
        //this.modalService.show(template);
      }, (error) => {
        console.log('Remove '+action, error);
        this.loader.hide();
      });
  }

  addRemoveFavCourse(){
    this.loader.show();
    
    let action='favorite-course'
    let payload = {
      "customer_id": this.customer_id,
      "course": this.course_id
    };
    if(this.course_wrapper.course_details.is_favorite_course){
      this.removeFavorite(action,this.course_wrapper.course_details.favorite_course_id);
    }
    else{
      this.addFavorite(action,payload);
    }
  }

  getTime(min:any){
    min = Math.floor(min);
    if (min < 60) {
      return min + " min";
    }
    else if (min == 60) {
      return "1 hour";
    }
    else {
      if ((min % 60) > 0) {
        return Math.floor(min / 60) + " hours " + (min % 60) + " min";
      }
      else {
        return Math.floor(min / 60) + " hours";
      }
    }
  }

  
}
