import {Viewer,Lesson,CourseDetail} from '../all-course-model'
export class MainCourseInfo {
    constructor(){
        this.viewers_detail=new Array<Viewer>();
        this.reviews=new Array<Review>();
        this.lessons=new Array<Lesson>();
        this.practitioner_expertise=new Array<string>();
    }
    public practitioner_firstname: string;
    public practitioner_lastname: string;
    public practitioner_expertise: Array<string>;
    public practitioner_overall_rating: number;
    public practitioner_rating_count: number;
    public practitioner_description: string;
    public practitioner_id: string;
    public practitioner_profile_image: string;
    public practitioner_address_line1: string;
    public practitioner_address_line2: string;
    public practitioner_address_line3?: any;
    public practitioner_address_line4: string;
    public practitioner_zipcode: number;
    public course_title: string;
    public course_duration_in_sec: number;
    public course_price: number;
    public course_overall_rating: number;
    public rated_person_count: number;
    public viewers_count: number;
    public viewers_detail: Array<Viewer>;
    public is_favorite_practitioner: boolean;
    public favorite_practitioner_id:string;
    public favorite_course_id:string;
    public is_favorite_course: boolean;
    public is_purchase_course: boolean;
    public reviews: Array<Review>;
    public lesson_count: number;
    public lessons: Array<Lesson>;
}

export class Review {
    constructor(){
        this.interest=new Array<string>();
    }
    public review: string;
    public overall_rating: number;
    public customer_id: string;
    public id: string;
    public user_id__first_name: string;
    public user_id__last_name: string;
    public user_id__email: string;
    public interest: Array<string>;
    public nick_name: string;
    public profile_image: string;
    public is_profile_pic_approved: boolean;
    public is_curated_images: boolean;
}
export class RelatedPractitioner {
    constructor(){
        this.practitioner_expertise=new Array<string>();
    }
    public practitioner_firstname: string;
    public practitioner_lastname: string;
    public practitioner_expertise: Array<string>;
    public practitioner_rating: number;
    public practitioner_rate_count: number;
    public practitioner_id: string;
    public starting_price: number;
    public practitioner_profile_image: string;
}

export class CourseDetailWrapper {
    constructor(){
        this.course_details=new MainCourseInfo();
        this.other_courses=new Array<CourseDetail>();
        this.related_courses=new Array<CourseDetail>();
        this.related_practitioner=new Array<RelatedPractitioner>();
    }
    public course_details: MainCourseInfo;
    public other_courses: Array<CourseDetail>;
    public related_courses: Array<CourseDetail>;
    public related_practitioner: Array<RelatedPractitioner>;
}