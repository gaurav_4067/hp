import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { CardFormComponent } from 'src/app/v2/payment/card-form/card-form.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { ClassDetail, JoinClass, JoinClassResponse } from '../../live-classes/detail/class.detail.model';
import { StripePaymentResponse } from '../../model/payment.model';
import { MainCourseInfo } from '../detail/detail.model';
enum PaymentType { "CreditCard", 'Paypal', 'Other', 'None' };

@Component({
  selector: 'app-purchase-course',
  templateUrl: './purchase-course.component.html',
  styleUrls: ['./purchase-course.component.scss']
})
export class PurchaseCourseComponent implements OnInit {
  @ViewChild('validateCardComponentForm') public cardComponent: CardFormComponent;
  @ViewChild('PurchaseCourseForm') parentForm;
  private childFormValidCheck: boolean;
  joinClass: JoinClass;
  selectedCourse: MainCourseInfo;
  course_id:string;
  joinClassResponse: JoinClassResponse;
  stripeCheckOutDetails:StripePaymentResponse;
  step: number = 1;
  payment_types = PaymentType;
  payment_mode:PaymentType =PaymentType.CreditCard;
  payment_type: PaymentType = PaymentType.CreditCard;
  logedinData:any;
  @Input() class_price: number;
  //@Input() selectedClass:ClassDetail;
  constructor(
    public authService: AuthService,
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public modalService: NgbModal,
    public toastr:ToastrService
  ) {
    this.joinClass = new JoinClass();
    this.stripeCheckOutDetails=new StripePaymentResponse();
    //this.selectedClass=new ClassDetail();
  }

  ngOnInit(): void {
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (this.logedinData) {
      let userInfo = this.logedinData["user_id"];
    }
  }

  public isChildFormValid(formValid: any) {
    this.childFormValidCheck = formValid;
  }

  submitCourseForm(e:NgForm){
    if(e.valid){
      this.enterForm(e);
    }
  }
  
  enterForm(parentForm) {
    // let that=this;
    //this.submitted = true;
    this.cardComponent.validateChildForm(true);
    if (!parentForm.valid || !this.childFormValidCheck) {
      return;
    } else {
      this.stripeCheckout();
    }
  }

  stripeCheckout() {
    
    const payload = {
      'cc_number': this.joinClass.cc_number,
      'exp_month': this.joinClass.expiry.split('/')[0],
      'exp_year': this.joinClass.expiry.split('/')[1],
      'cvc': this.joinClass.cvc,
      'session_amount': this.selectedCourse.course_price,
      'description': this.selectedCourse.course_title,
      'country': 'US'
    };
    this.loader.show();
    this.apiService.stripeCheckout(payload)
      .subscribe((stripe: any) => {
        //this.loader.hide();
        
        console.log('stripe', stripe);
        this.stripeCheckOutDetails = stripe;
        if (stripe && stripe.has_paid) {
          this.updateCustomerPayment();
          this.purchaseCourse();
        } else {
          this.loader.hide();
        }
      }, (error) => {
        console.log('stripeCheckout error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  updateCustomerPayment() {
    const payload = {
      'customer':this.logedinData.id,
      'transactionid': this.stripeCheckOutDetails.transaction_id,
      'haspaid': this.stripeCheckOutDetails.has_paid,
      'paymentinfo': '',
      'appointment': null,
      'subscription': null,
      'course': this.course_id
    };
    this.loader.show();
    this.apiService.confirmingAppointment(payload)
      .subscribe((payment) => {
        console.log('confirmingCustomerPayment', payment);
        this.modalService.dismissAll();
            let modalRef =this.modalService.open(ConfirmModalComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'signup-confirm-title',  size: 'lg', windowClass: 'confirm-modal' });
            modalRef.componentInstance.showButton = true;
            modalRef.componentInstance.closeButtonText = "Got it";
            modalRef.componentInstance.isDefaultBtn=true;
            modalRef.componentInstance.message = "";
            modalRef.componentInstance.refresh = true;
            modalRef.componentInstance.heading = "Thank you for purchasing this Course!";
        this.loader.hide();
      }, (error) => {
        console.log('confirmingCustomerPayment error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  purchaseCourse(){
    let payload = {
      "customer_id": this.logedinData.id,
      "course": this.course_id
    }
    this.loader.show();
    this.apiService.purchaseCourse(payload)
      .subscribe((purchaseCourse) => {
        console.log('purchaseCourseResponse', purchaseCourse);
        this.loader.hide();
      }, (error) => {
        console.log('purchaseCourseResponse error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  selectPaymentType(targetType: PaymentType,event) {
    
    // If the checkbox was already checked, clear the currentlyChecked variable
    if (this.payment_type === targetType) {
      this.payment_type = targetType;
      return;
    }

    this.payment_type = targetType;
  }
}