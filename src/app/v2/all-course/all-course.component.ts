import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { CourseDetail, CourseWrapper } from './all-course-model';

@Component({
  selector: 'app-all-course',
  templateUrl: './all-course.component.html',
  styleUrls: ['./all-course.component.scss']
})
export class AllCourseComponent implements OnInit {
  nextFourCourse: Array<CourseDetail>;
  courseWrapper: CourseWrapper;
  courseList:Array<CourseDetail>;
  dataLimit: number = 10;
  dataCategory: string = "popular";
  course_count: number = 0;
  page:number=1;
  showLoadScreen:boolean=false;
  searchKey:string="";
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public route:ActivatedRoute) {
    // this.courseWrapper =new  CourseWrapper();
    this.courseList=new Array<CourseDetail>();
    this.nextFourCourse=new Array<CourseDetail>();
    //this.nextFourCourses = new Array<CourseDetails>();
  }
  ngOnInit(): void {
    this.route.params.subscribe((param) => {
      if (param["key"]) {
        this.searchKey = param["key"];
      }
      this.getAllCourse(this.dataCategory, false);
    })
    
  }

  getAllCourse(category, isMore = false) {
    
    this.dataCategory = category;
    if (isMore) {
      if (this.courseList.length >= this.course_count) {
        //No call further API
      }
      else {
        this.page=this.page+1;
        this.showLoadScreen=true;
        // this.dataLimit = this.dataLimit + 10;
        this.callAPI(isMore);
      }
    }
    else {
      this.callAPI(false);
    }
  }

  callAPI(isMore) {
    this.loader.show();
    this.apiService.getAllCourse(this.dataLimit, this.dataCategory,this.page,this.searchKey)
      .subscribe((courseRes: any) => {
        console.log('courseRes', courseRes);
        this.course_count = courseRes.course_count;
        if (!isMore) {
          //this.liveClass = [];
          //this.dataLimit=this.dataLimit+10;
        }
        if (courseRes.courses_list.data.length > 0) {
          if (this.dataCategory == 'popular' && this.page==1) {
            this.nextFourCourse = courseRes.courses_list.data.slice(0, 4);
          }
        }

        courseRes.courses_list.data.forEach(element => {
          this.courseList.push(element);
        });
        this.showLoadScreen=false;
        this.loader.hide();
      }, (error) => {
        console.log('courseList error', error);
        this.loader.hide();
        this.showLoadScreen=false;
      });
  }
}
