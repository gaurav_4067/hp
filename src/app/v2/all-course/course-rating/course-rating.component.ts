import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RatingConfig } from 'ngx-bootstrap/rating';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { DonateComponent } from '../../practitioner/donate/donate.component';

export function getRatingConfig(): RatingConfig {
  return Object.assign(new RatingConfig(), { ariaLabel: 'Course Rating' });
}

@Component({
  selector: 'app-course-rating',
  templateUrl: './course-rating.component.html',
  styleUrls: ['./course-rating.component.scss'],
  providers: [{ provide: RatingConfig, useFactory: getRatingConfig }]
})
export class CourseRatingComponent implements OnInit {
  data:any;
  rate:number=0;
  isRatingDone:boolean=false;
  selectedRating:number=0;
  customer_id:string='';
  course_id:string='';
  constructor(public modalService:NgbModal,
    public loader:NgxSpinnerService,
    public apiService:ApiService,
    public route:ActivatedRoute) { }

  ngOnInit(): void {
    let logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (logedinData) {
      this.customer_id = logedinData["id"];
    }
  }

  initiateDonateToPractitioner(){
    
    let  modalRef = this.modalService.open(DonateComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'donate-class-modal',backdrop:false });
    let course_detail=this.data;
    modalRef.componentInstance.data = {"first_name":course_detail.practitioner_firstname,"last_name":course_detail.practitioner_lastname,"practitioner_id":course_detail.practitioner_id,"practitioner_image":course_detail.practitioner_profile_image,"id":this.course_id,"donateType":"course"};
  }

  ratingCourse(ratingCount){
    this.loader.show();
    let payload = {
      "customer": this.customer_id,
      "rating": ratingCount,
      "course_id": this.course_id
    }
   
    this.apiService.rateCourse(payload)
      .subscribe((res: any) => {
        this.selectedRating=ratingCount;
        this.isRatingDone=true;
        this.loader.hide();
      }, (error) => {
        console.log('rateCourse '+payload, error);
        this.loader.hide();
      });
    
  }
}
