import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { CourseDetail, CourseWrapper, PurchaseCourseWrapper } from '../all-course-model';

@Component({
  selector: 'app-my-course',
  templateUrl: './my-course.component.html',
  styleUrls: ['./my-course.component.scss']
})
export class MyCourseComponent implements OnInit {
  // nextFourCourse: Array<CourseDetail>;
  courseWrapper: PurchaseCourseWrapper;
  all_course_data : Array<CourseDetail>;
  in_progress_course_data : Array<CourseDetail>;
  completed_course_data : Array<CourseDetail>;
  course_count: number = 0;
  showLoadScreen: boolean = true;
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService) {
    this.courseWrapper = new PurchaseCourseWrapper();
    this.all_course_data =new Array<CourseDetail>();
    this.in_progress_course_data =new Array<CourseDetail>();
    this.completed_course_data =new Array<CourseDetail>();
    
  }
  ngOnInit(): void {
    this.getAllCourse();
  }

  getAllCourse() {
    this.loader.show();
    this.all_course_data=[];
    this.apiService.getCustomerPurchasedCourse("4f5cbd13-4863-4584-8d59-b7b50b18ffec")
      .subscribe((courseRes: any) => {
        console.log('courseRes', courseRes);
        this.courseWrapper = courseRes;
        this.courseWrapper.purchase_course_list.data.forEach(course => {
          this.all_course_data.push(course);
          if(course.completed_lesson_count==course.lesson_count){
            this.completed_course_data.push(course);
          }
          else{
            this.in_progress_course_data.push(course);
          }
        });
        
        this.showLoadScreen = false;
        this.loader.hide();
      }, (error) => {
        console.log('courseList error', error);
        this.loader.hide();
        this.showLoadScreen = false;
      });
  }
  getInProgressCourse(){
    this.all_course_data=[];
    this.all_course_data=this.in_progress_course_data;
  }

  getCompletedCourse(){
    this.all_course_data=[];
    this.all_course_data=this.completed_course_data;
  }
}
