
export class Viewer {
    constructor(){
        this.interest=new Array<string>();
    }
    public id: string;
    public user_id__first_name: string;
    public user_id__last_name: string;
    public user_id__email: string;
    public interest: Array<string>;
    public nick_name: string;
    public profile_image: string;
    public is_profile_pic_approved: boolean;
    public is_curated_images: boolean;
}

export class Lesson {
    public id: string;
    public lesson_title: string;
    public lesson_duration: number;
    public video_file: string;
    public lesson_description: string;
    public cover_photo: string;
}

export class CourseDetail {
    constructor() {
        this.practitioner_expertise = new Array<string>();
        this.lessons = new Array<Lesson>();
        this.viewers_detail = new Array<Viewer>();
    }
    public practitioner_firstname: string;
    public practitioner_lastname: string;
    public practitioner_expertise: Array<String>;
    public course_id: string;
    public course_title: string;
    public related_course_duration_in_sec: number;
    public related_course_rating: number;
    public related_course_rated_count: number;
    public viewers_count: number;
    public viewers_detail: Array<Viewer>;
    public lesson_count: number;
    public completed_lesson_count: number;
    public lessons: Array<Lesson>;
}

export class CourseInfo {
    constructor() {
        this.data = new Array<CourseDetail>();
    }
    public total: number;
    public previous_page?: any;
    public next_page: number;
    public data: Array<CourseDetail>;
}

export class CourseWrapper {
    constructor() {
        this.courses_list = new CourseInfo();
    }
    public courses_list: CourseInfo;
    public course_count: number;
}


export class PurchaseCourseWrapper {
    constructor() {
        this.purchase_course_list = new CourseInfo();
    }
    public purchase_course_list: CourseInfo;
    public purchase_course_count: number;
}