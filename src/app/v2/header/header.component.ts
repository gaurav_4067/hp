import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { PractitionerSignupComponent } from '../view/practitioner-signup/practitioner-signup.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  // @ViewChild("searchtext") searchTextField: ElementRef;
  showProfileSection: boolean = false;
  userData: any;
  logedinData: any;
  isGroup: any;
  showSearch: boolean = false;
  searchText: string = "";
  my_fav_count: number;
  my_course_count: number;
  constructor(private authService: AuthService,
    private router: Router,
    public generalService: GeneralService,
    public apiService: ApiService,
    private modalService: NgbModal) { 
    this.authService.getLoggedInName.subscribe(call => {
      if (call) {
        
        this.onInit()
      }
    });
  }

  ngOnInit(): void {
    this.onInit();    
  }

  onInit(){
    if (this.authService.isAuthenticated()) {
      this.showProfileSection = true;
      this.userData = JSON.parse(localStorage.getItem("user_data"));
      this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
      if (this.logedinData) {
        
        this.getCountDetail();
        this.isGroup = this.userData?.groups[0];
        
        if (this.isGroup == "Admin") {
          this.router.navigateByUrl("/welcome-admin");
        }
        else if (this.isGroup == "Customer") {
          if (this.logedinData.is_subscribed) {
            //this.router.navigateByUrl("/welcome-customer");
          }
          else {

          }
        }
        else if (this.isGroup == "Practitioner") {
          //alert("2");
          this.router.navigateByUrl("/welcome-practitioner");
        }
        else {
          //alert("3");
          this.router.navigateByUrl("/home");
        }
      }
    }
    else{
      this.showProfileSection = false;
    }
  }

  getCountDetail() {
    // this.loader.show();
    this.apiService.getCountDetail(this.logedinData["id"])
      .subscribe((countDetail: any) => {
        console.log('countDetail', countDetail);
        this.my_fav_count = countDetail.my_fav_count;
        this.my_course_count = countDetail.my_course_count;
        //this.loader.hide();

      }, (error) => {
        console.log('getCountDetail error', error);
        //this.loader.hide();
      });
  }

  showField() {
    this.showSearch = !this.showSearch;
    // this.searchTextField.nativeElement.focus();
  }

  onFocusOutEvent(e) {
    if (this.searchText == "" || this.searchText == undefined) {
      this.showSearch = false;
    }
    else {
      this.router.navigate(['search', this.searchText]);
    }
  }
  transform(value: string): string {
    let first = value.substr(0, 1).toUpperCase();
    return first + value.substr(1);
  }

  searchFromAll(e) {

    if (this.searchText) {
      this.router.navigate(['search', this.searchText]);
    }
  }

  logout() {
    this.generalService.showProfile = false;
    this.apiService
      .getAppLogout()
      .subscribe((response: any) => {
        console.log(response.detail);
      });
    this.modalService.dismissAll();
    //this.socialAuthService.signOut();
    localStorage.removeItem("access_token");
    localStorage.removeItem("user_data");
    localStorage.removeItem("refresh_token");
    localStorage.removeItem("logedin_data");
    
    if (window.location.pathname == '/home') {
      window.location.reload();
    }
    else {
      this.router.navigateByUrl("/home");
    }
    //window.location.reload();
  }

  openSignupModal(){
    let  modalRef = this.modalService.open(PractitionerSignupComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'signup-class-modal',backdrop:false });
  }
}
