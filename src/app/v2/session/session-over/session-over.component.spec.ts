import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionOverComponent } from './session-over.component';

describe('SessionOverComponent', () => {
  let component: SessionOverComponent;
  let fixture: ComponentFixture<SessionOverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionOverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionOverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
