import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { DonateComponent } from '../../practitioner/donate/donate.component';
import {  PractitionerWrapper} from './session-over.model';

@Component({
  selector: 'app-session-over',
  templateUrl: './session-over.component.html',
  styleUrls: ['./session-over.component.scss']
})
export class SessionOverComponent implements OnInit {
  p_id:string;
  user:string;
  practitionerWrapper:PractitionerWrapper;
  constructor(public route:ActivatedRoute,
    public apiService:ApiService,
    public loader:NgxSpinnerService,
    public modalService:NgbModal ) { 
      this.practitionerWrapper=new PractitionerWrapper();
    }

  ngOnInit(): void {
    this.route.params.subscribe((param)=>{
      this.p_id=param["pid"];
      this.user=param["user"];
      this.getPractitionerCourse();
    });
  }

  getPractitionerCourse() {
    this.loader.show();
    this.apiService.getPractitionerCourse(this.p_id)
      .subscribe((pCourses: any) => {
        console.log('getPractitionerCourse', pCourses);
        this.practitionerWrapper=pCourses;
        this.loader.hide();
         
      }, (error) => {
        console.log('getPractitionerCourse error', error);
        this.loader.hide();
      });
  }

  donateToPractitioner(){
    
    let practitioner_detail=this.practitionerWrapper.course.data[0];
    let  modalRef = this.modalService.open(DonateComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'donate-class-modal',backdrop:false });
    modalRef.componentInstance.data = {"first_name":practitioner_detail.practitioner_firstname,"last_name":practitioner_detail.practitioner_lastname,"practitioner_id":this.p_id,"practitioner_image":"","id":"","donateType":"course"};
  }
}
