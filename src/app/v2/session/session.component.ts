import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { JoinClass } from '../live-classes/detail/class.detail.model';
import { CardFormComponent } from '../payment/card-form/card-form.component';
import { OtherSession, PaymentInfo, Plan, SessionForm, SessionTypeResponse, SubscriptionDetail } from './session.modal';
import {StripePaymentResponse} from '../model/payment.model';
enum PaymentType { "CreditCard", 'Paypal', 'Other', 'None' };

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {
  @ViewChild('validateChildComponentForm') public ChildComponent: CardFormComponent;
  @ViewChild('step4Form') parentForm;
  public childFormValidCheck: boolean;
  logedinData: any;
  selectedSession: OtherSession;
  sessionForm: SessionForm;
  step: number = 1;
  joinClass: JoinClass;
  stripeCheckOutDetails:StripePaymentResponse;
  appointment:any;
  isIncluded:boolean=false;

  payment_types = PaymentType;

  payment_type: PaymentType = PaymentType.None;

  subscriptionDetails: SubscriptionDetail;
  planDetails: Array<Plan>;
  sessiontTypeDetails: SessionTypeResponse;
  paymentCardDetails: Array<PaymentInfo>;



  timeslotPractitioner: any;
  prac_id: string;
  minDate: Date = new Date();
  calenderDate: Date = new Date();
  bsValue: Date = new Date();
  selectionTimeslot: any = {};
  constructor(public authService: AuthService,
    public apiService: ApiService,
    public toastr: ToastrService,
    private loader: NgxSpinnerService,
    public router: Router,
    public route: ActivatedRoute) {
    this.subscriptionDetails = new SubscriptionDetail();
    this.planDetails = new Array<Plan>();
    this.sessiontTypeDetails = new SessionTypeResponse();
    this.paymentCardDetails = new Array<PaymentInfo>();
    this.sessionForm = new SessionForm();
    this.joinClass = new JoinClass();
    this.stripeCheckOutDetails=new StripePaymentResponse();
  }

  ngOnInit(): void {
    //this.cid = JSON.parse(localStorage.getItem("logedin_data")).id;
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.route.params.subscribe((param) => {
      this.prac_id = param["pid"];
      if (!!this.logedinData && this.logedinData.id) {
        this.getCustomerSubscriptionDetails();
        this.customerpaymentdetails();
      }
      else {
        const payload = {
          pid: this.prac_id,
          from: "NA",
          to: "NA",
          totSession: "NA",
          bookedSession: "NA"
        };
        this.chooseSessionType(payload);
      }
    });
  }

  getCustomerSubscriptionDetails() {
    this.loader.show();

    this.apiService.getCustomerSubscriptionDetails(this.logedinData.id)
      .subscribe((subscription: any) => {
        this.loader.hide();
        console.log('subscription', subscription);
        this.subscriptionDetails = subscription;
        if (subscription && subscription.plan_id && subscription.plan_id !== "") {
          this.getCustomerPlanDetails(subscription.plan_id);
        } else {
          const payload = {
            pid: this.prac_id,
            from: "NA",
            to: "NA",
            totSession: "NA",
            bookedSession: "NA"
          };
          this.chooseSessionType(payload);
          this.loader.hide();
        }
      }, (error) => {
        console.log('subscription error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  getCustomerPlanDetails(planId) {
    this.loader.show();
    this.apiService.getCustomerPlanDetails(planId)
      .subscribe((plan: any) => {
        console.log('plan', plan);
        this.loader.hide();
        this.planDetails = plan;
        const payload = {
          pid: this.prac_id,
          from: this.subscriptionDetails.subscribed_on,
          to: this.subscriptionDetails.subscription_ends_on,
          totSession: plan.plan[0].number_of_session,
          bookedSession: this.subscriptionDetails.total_session_booked
        };
        this.chooseSessionType(payload);
      }, (error) => {
        console.log('plan error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  chooseSessionType(payload) {
    this.loader.show();
    this.apiService.chooseSessionType(payload.pid, payload.from, payload.to, payload.totSession, payload.bookedSession)
      .subscribe((sessionType: any) => {
        console.log('sessionType', sessionType);
        this.sessiontTypeDetails = sessionType;
        this.loader.hide();
      }, (error) => {
        console.log('sessionType error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  getSessionType() {
    this.loader.show();
    this.apiService.getSessionType(this.logedinData.id, this.prac_id)
      .subscribe((sessionType: any) => {
        console.log('sessionType', sessionType);
        this.sessiontTypeDetails = sessionType;
        this.loader.hide();
      }, (error) => {
        console.log('sessionType error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  customerpaymentdetails() {
    this.loader.show();
    this.apiService.customerpaymentdetails()
      .subscribe((paymentCardDetails: any) => {
        console.log('paymentCardDetails', paymentCardDetails);
        this.loader.hide();
        paymentCardDetails.forEach(element => {
          if (element && element.is_default) {
            this.paymentCardDetails = element;
          }
        });
      }, (error) => {
        console.log('paymentCardDetails error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  onValueChange(date) {
    console.log("date", date);
    this.bsValue = new Date(date);
    this.loader.show();
    this.getPractitionerAvailabilityList(new Date(date).toISOString().slice(0, -14));
  }

  getPractitionerAvailabilityList(from) {
    this.loader.show();
    this.apiService.getPractitionerAvailabilityList(this.prac_id, from, this.selectedSession.duration_min)
      .subscribe((pList: any) => {
        console.log('pList', pList);
        this.timeslotPractitioner = pList.response;
        this.selectionTimeslot = {};
        this.loader.hide();
        if (this.timeslotPractitioner && this.timeslotPractitioner.length === 0) {
          this.toastr.error('No schedule available for selected Date');
        }
      }, (error) => {
        console.log('pList error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }
  getAvailabilityTime(time) {
    if (!time) {
      return '';
    }

    const [h, m] = time.split(':');

    if (Number(h) > 12) {
      return `${h - 12}:${m} pm`;
    }
    return `${h}:${m} ${Number(h) === 12 ? 'pm' : 'am'}`;
  }


  getTime(min: any) {
    min = Math.floor(min);
    if (min < 60) {
      return min + " min";
    }
    else if (min == 60) {
      return "1 hour";
    }
    else {
      if ((min % 60) > 0) {
        return Math.floor(min / 60) + " hours " + (min % 60) + " min";
      }
      else {
        return Math.floor(min / 60) + " hours";
      }
    }
  }

  timeslotPractitionerSelect(value) {
    console.log(value)
    this.selectionTimeslot = value;
    //this.showSelected = true;
    //this.confirmDoctorNameDisabled = false;
  }

  bindSelectedSession(session) {

    this.selectedSession = session;
  }

  submitSession(step, isValid) {
    
    if (isValid) {
      if (step == 1) { this.step = 2; }
      else if (step == 2) { this.step = 3; }
      else if (step == 3) { this.step = 4; }
      else if (step == 4) { this.step = 5; }
      else if (step == 5) {

        this.enterForm(this.parentForm)
      }
      window.scrollTo({ top: 0 });
    }
  }

  public isChildFormValid(formValid: any) {
    this.childFormValidCheck = formValid;
  }

  selectPaymentType(targetType: PaymentType) {

    // If the checkbox was already checked, clear the currentlyChecked variable
    if (this.payment_type === targetType) {
      this.payment_type = PaymentType.None;
      return;
    }

    this.payment_type = targetType;
  }

  enterForm(parentForm) {
    //this.submitted = true;
    this.ChildComponent.validateChildForm(true);
    if (!parentForm.valid || !this.childFormValidCheck) {
      return;
    } else {
      //success
        if (!this.isIncluded) {
          this.stripeCheckout();
        } else {
          this.addAppointment();
        }
      //this.step = 6;
      
    }
  }

  stripeCheckout() {
    
    const payload = {
      'cc_number': this.joinClass.cc_number.replace(/\s+/g, ""),
      'exp_month': this.joinClass.expiry.split('/')[0],
      'exp_year': this.joinClass.expiry.split('/')[1],
      'cvc': this.joinClass.cvc,
      'session_amount': this.selectedSession.price,
      'description': this.sessionForm.title,
      'country': 'US'
    };
    this.loader.show();
    this.apiService.stripeCheckout(payload)
      .subscribe((stripe: any) => {
        //this.loader.hide();
        
        console.log('stripe', stripe);
        this.stripeCheckOutDetails = stripe;
        if (stripe && stripe.has_paid) {
          this.addAppointment();
        } else {
          this.loader.hide();
        }
      }, (error) => {
        console.log('stripeCheckout error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  addAppointment() {
    
    const payload = {
      'practitioner_session': this.selectedSession.id,
      'start_time': this.selectionTimeslot.start_time__time,
      'appointment_date': new Date(this.bsValue).toISOString().slice(0, -14),
      'customer': this.logedinData.id,
      'customerpaymentdetail': null,
      'subscription': this.subscriptionDetails.id
    };
    this.loader.show();
    this.apiService.bookAppointment(payload)
      .subscribe((appointment) => {
        //this.loader.hide();
        
        console.log('appointment', appointment);
        this.appointment = appointment;
        this.updatePractitionerAvailabilitySchedule();
        if (!this.isIncluded) {
          this.confirmingAppointment();
        }
        else{
          this.step=6;
        }
      }, (error) => {
        console.log('appointment error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  confirmingAppointment() {
    const payload = {
      'transactionid': this.stripeCheckOutDetails.transaction_id,
      'haspaid': this.stripeCheckOutDetails.has_paid,
      'paymentinfo': '',
      'subscription': null,
      'customer': this.logedinData.id,
      'appointment': this.appointment.id
    };
    this.loader.show();
    this.apiService.confirmingAppointment(payload)
      .subscribe((confirmingAppointment) => {
        console.log('confirmingAppointment', confirmingAppointment);
        
        this.loader.hide();
        this.step=6;
      }, (error) => {
        console.log('confirmingAppointment error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

  updatePractitionerAvailabilitySchedule() {
    this.loader.show();
    
    this.apiService.updateProctitionerAvailability({ is_from_appointment: true }, this.selectionTimeslot.id)
      .subscribe((updatedRes) => {
        this.loader.hide();
        
        console.log('updatedRes', updatedRes);
      }, (error) => {
        console.log('updatedRes error', error);
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }
}
