export class SubscriptionDetail {
    public id: string;
    public offer_id: string;
    public plan_id: string;
    public subscribed_on: Date;
    public subscription_ends_on: Date;
    public total_session_booked: number;
}

export class Plan {
    constructor() {
        this.plan_description = new Array<string>();
    }
    public created: Date;
    public modified: Date;
    public plan_type__plan_type: string;
    public id: string;
    public plan_name: string;
    public plan_description: Array<string>;
    public sale_price: number;
    public regular_price: number;
    public number_of_session: number;
    public start_date?: any;
    public end_date?: any;
}

export class OtherSession {
    constructor() {
        this.practitioner__expertise = new Array<string>();
    }
    public modified: Date;
    public id: string;
    public practitioner_id: string;
    public title: string;
    public description: string;
    public duration_min: number;
    public price: number;
    public commission?: any;
    public is_active: boolean;
    public practitioner__name?: any;
    public practitioner__expertise: Array<string>;
}

export class included_package {
    public modified: Date;
    public id: string;
    public practitioner_id: string;
    public title: string;
    public description: string;
    public duration_min: number;
    public price: number;
    public commission?: any;
    public is_active: boolean;
}

export class SessionTypeResponse {
    constructor() {
        this.others = new Array<OtherSession>();
    }
    public included_packages: string;
    public others: Array<OtherSession>;
    public total_session_message: string;
    public message: string;
    public exception: string;
    public status: number;
}


export class PaymentInfo {
    public id: string;
    public created: Date;
    public modified: Date;
    public cc_number: string;
    public cc_expiry: string;
    public is_default: boolean;
    public account_holder_name: string;
    public account_holder_type: string;
    public routing_number?: any;
    public account_number?: any;
    public billing_address: string;
    public city: string;
    public zip_code: number;
    public customer: string;
    public payment_methods: string;
}

export class SessionForm{
    public title:string;
    public description:string;
    // public username:string;
    // public useremail:string;
    public cvc:string;
    public cc_number:string;
    public card_full_name:string;
    public session_amount:string;
    // public description:string;
    public country:string;
    public exp_month:number;
    public exp_year:number;
    public expiry:string;
    public zip_code:string;
}
