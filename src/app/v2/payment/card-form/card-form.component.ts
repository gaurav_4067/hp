import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { JoinClass } from '../../live-classes/detail/class.detail.model';

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.scss']
})
export class CardFormComponent implements OnInit {
  @ViewChild('childForm') childForm;
  @Output() isChildFormValid:EventEmitter<any>=new EventEmitter<any>();
  //@Input() form:NgForm;
  @Input() joinClass:any;
  constructor() {
  }

  ngOnInit(): void {
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;  
  }
  transform() {
    this.joinClass.cc_number= this.joinClass.cc_number.toString().replace(/\s+/g, '').replace(/(\d{4})/g, '$1 ').trim();
  }

  formatCardDate() {
    if (RegExp('/^0[1-9]|1[0-2]/[0-9]+$/').test(this.joinClass.expiry)) {
      return this.joinClass.expiry;
    }
  
    let regexpExpiry = /^(?<month>0[1-9]|1[0-2])(?<year>[0-9]+)$/
    let match = regexpExpiry.exec(this.joinClass.expiry);
    this.joinClass.expiry= match.groups.month+'/'+match.groups.year;
  };
   
  public validateChildForm(data: any) {
    if (data === true) {
      this.childForm.submitted=true;
        if(this.childForm.valid === true) {
            this.isChildFormValid.emit(true);
        } else {
            this.isChildFormValid.emit(false);
        }
    }
}
}
