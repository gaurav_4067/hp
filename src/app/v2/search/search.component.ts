import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { HomeDetail } from '../model/home.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  homeDetail:HomeDetail;
  searchText="";
  totalCount:number;
  isLoading:boolean=true;
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService:GeneralService,
    public route:ActivatedRoute) {
      this.homeDetail=new HomeDetail();
   }

  ngOnInit(): void {
    
    this.route.params.subscribe(param=>{
      this.searchText = param['text'];
      console.log(this.searchText);  
      this.getHomePageData();
    });
    
  }


  getHomePageData() {
    this.loader.show();
    this.apiService.getHomePageSearchData(this.searchText)
      .subscribe((homeDetail: any) => {
        console.log('homeDetail', homeDetail);
        this.homeDetail=homeDetail;
        this.totalCount=Object.values(this.homeDetail).reduce((sum, val) => sum + (Array.isArray(val) ? val.length : 0), 0);
        this.loader.hide();
        this.isLoading=false;
         
      }, (error) => {
        console.log('homeDetail error', error);
        this.loader.hide();
        this.isLoading=false;
      });
  }
}
