import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  logedinData:any;
  constructor() { }

  ngOnInit(): void {
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
  }

}
