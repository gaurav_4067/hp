import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/services/AuthService';
import { PractitionerSignupComponent } from '../view/practitioner-signup/practitioner-signup.component';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {
  closeButtonText: string = "GO TO WEBSITE";
  showCurrentPlanSection:boolean=false;
  message: string = "";
  heading: string = "";
  showButton: boolean = false;
  showConfirmButton: boolean = false;
  isPrimary: boolean = false;
  isDefaultBtn: boolean = false;
  isHighlight: boolean = false;
  refresh: boolean = false;
  constructor(
    public modalService: NgbModal,
    public authService: AuthService,
  ) { }

  ngOnInit(): void {
  }

  signupPractitionerModal() {
    localStorage.removeItem("access_token");
    localStorage.removeItem("user_data");
    localStorage.removeItem("refresh_token");
    localStorage.removeItem("logedin_data");
    this.authService.notifiedHeader();
    let modalRef = this.modalService.open(PractitionerSignupComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'signup-class-modal', backdrop: false });
  }

  closeModal() {
    if (!this.refresh) {
      this.modalService.dismissAll()
    }
    else {
      window.location.reload();
    }
  }
}
