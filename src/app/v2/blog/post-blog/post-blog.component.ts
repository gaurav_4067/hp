import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import {PostBlogData} from '../../model/post-blog.model';
@Component({
  selector: 'app-post-blog',
  templateUrl: './post-blog.component.html',
  styleUrls: ['./post-blog.component.scss']
})
export class PostBlogComponent implements OnInit {
  postBlogData: PostBlogData;
  article_id:string="";
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public route:ActivatedRoute) {
    this.postBlogData = new PostBlogData();
  }

  ngOnInit(): void {
    
    this.route.params.subscribe((param)=>{
      this.article_id=param["id"];
      this.getPostBlogData();
    })
    
  }

  getPostBlogData() {
    this.loader.show();
    this.apiService.getPostBlogData(this.article_id)
      .subscribe((blogData: any) => {
        console.log('blogData', blogData);
        this.postBlogData = blogData;

        this.loader.hide();
      }, (error) => {
        console.log('postBlogData error', error);
        this.loader.hide();
      });
  }
}
