import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { BlogData } from '../../model/main-blog.model';
@Component({
  selector: 'app-main-blog',
  templateUrl: './main-blog.component.html',
  styleUrls: ['./main-blog.component.scss']
})
export class MainBlogComponent implements OnInit {
  blogData: BlogData;
  //dataLimit:number=15;
  itemsPerPage:number=9;
  maxSize:number=4;
  currentPage:number=1;
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService) {
    this.blogData = new BlogData();
  }

  ngOnInit(): void {
    this.getBlogData(1,9);
  }

  getBlogData(page,limits) {
    this.currentPage=page;
    // if(page==1){
    //   limits=6;
    // }
    this.loader.show();
    this.apiService.getBlogData(page,limits)
      .subscribe((blogData: any) => {
        console.log('blogData', blogData);
        this.blogData = blogData;

        this.loader.hide();
      }, (error) => {
        console.log('blogData error', error);
        this.loader.hide();
      });
  }
}

