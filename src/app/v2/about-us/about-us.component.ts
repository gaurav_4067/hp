import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { FAQWrapper, Question } from '../support/support.model';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  search:string;
  faqList:FAQWrapper;
  flatFAQList:Array<Question>;
  selectedQuestion:Question;
  filteredSearch:boolean=false;
  qid:string;
  category:string;
  constructor(public apiService:ApiService,
    public loader:NgxSpinnerService,
    public route:ActivatedRoute) {
      this.faqList=new FAQWrapper;
      this.flatFAQList=new Array<Question>();
      this.selectedQuestion=new Question();

    }

  ngOnInit(): void {
    this.route.params.subscribe((param)=>{
      this.qid=param["id"];
      this.category=param["category"];
    });

    this.getFAQList();
  }

  getFAQList(){
    this.loader.show();
    this.apiService.getFAQList()
      .subscribe((faqRes: any) => {
        console.log('faqRes', faqRes);
        this.faqList=faqRes;
        
        this.faqList.context.forEach(context => {
          if(context.category==this.category){
            context.questions.forEach(question => {
              if(question.id==this.qid){
                this.selectedQuestion=question;
                return;
              }
            });
          }
        });
        this.loader.hide();
      }, (error) => {
        console.log('faqRes error', error);
        this.loader.hide();
      });
  }
}
