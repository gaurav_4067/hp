import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { SocialAuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/services/AuthService';
import { LoginUser } from '../../login/login.model';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-social-media-signup',
  templateUrl: './social-media-signup.component.html',
  styleUrls: ['./social-media-signup.component.scss']
})
export class SocialMediaSignupComponent implements OnInit {
  public _accesstoken: any;
  public _refreshtoken: any;
  @Output() changeBack = new EventEmitter<any>();
  loginFromScreen: boolean = true;
  customLogin:boolean=false;
  user: LoginUser;
  modalRef:any
  constructor(public route: ActivatedRoute,
    public apiService: ApiService,
    public loader: NgxSpinnerService,
    public router: Router,
    private socialAuthService: SocialAuthService,
    public toastr: ToastrService,
    public modalService: NgbModal,
    private authService:AuthService) { 
      this.user = new LoginUser();
    }

  ngOnInit(): void {
  }

  signInWithFB(): void {
    this.socialAuthService
      .signIn(FacebookLoginProvider.PROVIDER_ID)
      .then(getUser => {
        console.log(getUser);
        if (getUser) {
          this.sociaLogin(getUser, "facebook");
        }
      });
  }
  sociaLogin(User, isUser) {
    this.loader.show();
    let payload;
    console.log(User)
    if (isUser == "google") {
      payload = {
        access_token: User.access_token ? User.access_token : User.authToken,
        code: User.first_issued_at ? User.first_issued_at : User.id,
        id_token: User.id_token ? User.id_token : User.idToken,
      }
    } else {
      payload = {
        access_token: User.authToken,
        code: User.id,
        id_token: User.id,
      }
    }
    console.log(payload);
    this.apiService
      .sociaLogin(payload, isUser)
      .subscribe((response: any) => {
        // console.log(response);
        if (response) {
          this._accesstoken = response.access_token;
          this._refreshtoken = response.access_token;
          localStorage.setItem(
            "access_token",
            response.access_token
          );
          this.setCookie('access_token', 'test', 7);
          localStorage.setItem(
            "refresh_token",
            response.refresh_token
          );
          delete response.access_token;
          delete response.refresh_token;
          localStorage.setItem(
            "user_data",
            JSON.stringify(response)
          );
          if (this.user.remember) {
            localStorage.setItem("is_remember", "true");
            const remember = {
              email: this.user.email,
              password: this.user.password,
            };
            localStorage.setItem("remember_data", JSON.stringify(remember));
          } else {
            localStorage.setItem("is_remember", "false");
          }
          this.loader.hide();
          this.changeBack.emit(true);
          if (response.is_user_object_created == false) {
            this.loader.show();
            this.apiService
              .pkIdUpdate(response.groups[0].toLowerCase(), "post")
              .subscribe((response: any) => {
                console.log(response);

                localStorage.setItem(
                  `logedin_data`,
                  JSON.stringify(response[0] ? response[0] : response["results"][0])
                );
                // this.toastr.success('Login Successfully');
                this.loggedUpdate({
                  is_user_object_created: true
                });
              });
          } else {
            this.PKCallInit(response.groups[0].toLowerCase(), "get");
          }
        }
      },
        (errResponse: HttpErrorResponse) => {
          if (errResponse['error']['non_field_errors']) {
            errResponse['error']['non_field_errors'].forEach(element => {
              this.toastr.error(element);
            });
          } else {
            this.toastr.error('Something went wrong please try again');
          }
          this.loader.hide();
        }
      );
  }


  loggedUpdate(payload) {
    this.loader.show();
    let userData = JSON.parse(localStorage.getItem("user_data"));
    this.loader.show();
    this.apiService
      .previousLoggedUpdate(payload, userData["user"]["pk"])
      .subscribe((response: any) => {
        console.log(response);
        userData["is_user_object_created"] = true;
        localStorage.setItem(
          `user_data`,
          JSON.stringify(userData)
        );
        window.location.reload();
        this.loader.hide();
      });
  }
  PKCallInit(group, method) {
    this.loader.show();
    this.apiService
      .pkIdUpdate(group, method)
      .subscribe((response: any) => {
        console.log(response);
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response[0] ? response[0] : response["results"][0])
        );
        // this.toastr.success('Login Successfully');
        this.loader.hide();
        const userData = JSON.parse(localStorage.getItem('user_data'));
        const logedinData = JSON.parse(localStorage.getItem('logedin_data'));
        const isGroup = userData?.groups[0];
        let userRoute: string;

        if (this.loginFromScreen && !this.customLogin) {
          if (isGroup == "Admin") {
            //alert("admin");
            //window.location.reload();
            this.router.navigateByUrl("/welcome-admin");
          }
          else if (isGroup == "Customer") {
            this.router.navigateByUrl("/home");
            // if (logedinData.is_subscribed) {
            //   this.router.navigateByUrl("/welcome-customer");
            //   //window.location.reload();
            // } else {
            //   this.router.navigateByUrl("/home");
            // }
          }
          else if (isGroup == "Practitioner") {
            this.router.navigateByUrl("/welcome-practitioner");
            //window.location.reload();
            //this.router.navigateByUrl("/welcome-practitioner");
          }
          else {
            this.router.navigateByUrl("/home");
          }
        }
        else if(!this.loginFromScreen && this.customLogin){
          this.authService.notifiedHeader();
          this.authService.notifiedPurchaseCourse();
          this.modalRef.close();
        }
        else {
          this.modalService.dismissAll();
          window.location.reload();
        }
        //window.location.reload();
      });
  }

  signInWithGoogle(): void {
    this.socialAuthService
      .signIn(GoogleLoginProvider.PROVIDER_ID)
      .then(getUser => {
        if (getUser) {
          console.log(getUser);
          this.sociaLogin(getUser, "google");
        }
      });
  }


  setCookie(name, value, days) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
  }
}
