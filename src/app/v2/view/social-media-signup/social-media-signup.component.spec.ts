import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialMediaSignupComponent } from './social-media-signup.component';

describe('SocialMediaSignupComponent', () => {
  let component: SocialMediaSignupComponent;
  let fixture: ComponentFixture<SocialMediaSignupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SocialMediaSignupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialMediaSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
