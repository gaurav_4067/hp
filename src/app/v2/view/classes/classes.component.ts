import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { LiveClassDetail } from '../../my-favorites/my-favorites.model';
 
@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.scss']
})
export class ClassesComponent implements OnInit {
  @Output() getUpdatedClassData = new EventEmitter();
  @Input() id:string="";
  @Input() showRemove:boolean=false;
  @Input() title: string;
  @Input() firstname: string;
  @Input() lastname: string;
  @Input() expertise: string;
  @Input() image: string;
  @Input() classdate: string;
  @Input() starttime: string;
  @Input() duration: string;
  constructor(public generalService:GeneralService,
    public apiService: ApiService,
    private loader: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  removeFromFavorite(id){
    this.loader.show();
    this.apiService.removeFromFavorite('favorite-live-class',id)
      .subscribe((res: any) => {
        
        console.log('Remove favorite-live-class', res);
        this.getUpdatedClassData.emit();
        this.loader.hide();
      }, (error) => {
        console.log('Remove favorite-live-class', error);
        this.loader.hide();
      });
  }

  getTime(min:any){
    min = Math.floor(min);
    if (min < 60) {
      return min + " min";
    }
    else if (min == 60) {
      return "1 hour";
    }
    else {
      if ((min % 60) > 0) {
        return Math.floor(min / 60) + " hours " + (min % 60) + " min";
      }
      else {
        return Math.floor(min / 60) + " hours";
      }
    }
  }

  transform(value:string): string {
    let first = value.substr(0,1).toUpperCase();
    return first + value.substr(1); 
  }
}
