import { Component, OnInit,Input, EventEmitter, Output } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { Viewer } from '../../model/home.model';
import { CourseDetails } from '../../my-favorites/my-favorites.model';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  //@Input() data : Array<CourseDetails>;
  @Output() getUpdatedCourseData = new EventEmitter();
  @Input() id:string="";
  @Input() cover_photo:string="";
  @Input() showRemove:boolean=false;
  @Input() rating : string;
  @Input() video : string;
  @Input() ratecount : string;
  @Input() title : string;
  @Input() firstname : string;
  @Input() lastname : string;
  @Input() lessoncount : string;
  @Input() completed_lesson_count : string='';
  @Input() myCourse : boolean=false;
  
  @Input() duration : number;
  @Input() viewers_detail:Array<Viewer>=[];
   
  constructor(public generalService:GeneralService,
    public apiService: ApiService,
    private loader: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  removeFromFavorite(id){
    this.loader.show();
    this.apiService.removeFromFavorite('favorite-course',id)
      .subscribe((res: any) => {
        
        console.log('Remove favorite-course', res);
        this.getUpdatedCourseData.emit();
        this.loader.hide();
      }, (error) => {
        console.log('Remove favorite-course', error);
        this.loader.hide();
      });
  }

  getTime(min:any){
    min = Math.floor(min);
    if (min < 60) {
      return min + " min";
    }
    else if (min == 60) {
      return "1 hour";
    }
    else {
      if ((min % 60) > 0) {
        return Math.floor(min / 60) + " hours " + (min % 60) + " min";
      }
      else {
        return Math.floor(min / 60) + " hours";
      }
    }
  }

  transform(value:string): string {
    let first = value.substr(0,1).toUpperCase();
    return first + value.substr(1); 
  }

  getProgressText(){
    if(this.completed_lesson_count===this.lessoncount){
      return "Completed";
    }
    else if(this.lessoncount=="1")
    {
      return "In progress (lesson 1)";
    }
    
    return "In progress (lesson "+ this.completed_lesson_count +" of "+this.lessoncount+")";
  }
}
