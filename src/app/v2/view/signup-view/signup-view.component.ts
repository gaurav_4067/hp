import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { SignupUser,Location } from '../../signup/signup.model';
import { LoginViewComponent } from '../login-view/login-view.component';

@Component({
  selector: 'app-signup-view',
  templateUrl: './signup-view.component.html',
  styleUrls: ['./signup-view.component.scss']
})
export class SignupViewComponent implements OnInit {
  countries:Array<any>;
  states:any;
  cities:any;
  user:SignupUser;
  selectedCountry?:string;
  selectedState?:string;
  registrationRes:any;
  signupFromScreen:boolean=true;
  errorMessage:any;
  constructor(public route:ActivatedRoute,
    public loader:NgxSpinnerService,
    public apiService:ApiService,
    public thirpApiService:ThirdPApiService,
    public generalService:GeneralService,
    public modalService:NgbModal,
    public router:Router,
    public toastr:ToastrService) {
      this.countries=new Array<any>();
      this.user=new SignupUser();
     }

  ngOnInit(): void {
      // this.route.params.subscribe(param=>{
      //   this.signupType = param['type'];
      //   console.log(this.signupType);  
      // });
      this.loadCountry();
  }

  loadCountry(){
    this.loader.show();
    this.thirpApiService
    .getCountries("id,name","no_page")
    .subscribe((response: any) => {
      console.log(response);
      this.countries = response;
      this.loader.hide();
    });
  }
  loadState(countryId){
    this.loader.show();
    this.thirpApiService
    .getStates(countryId,"id,country,name","no_page")
    .subscribe((response: any) => {
      console.log(response);
      this.states = response;
      this.loader.hide();
    });
  }
  loadCity(stateId){
    this.loader.show();
    this.thirpApiService
    .getCities(stateId,"id,state,name","no_page")
    .subscribe((response: any) => {
      console.log(response);
      this.cities = response;
      this.loader.hide();
    });
  }

  onSelectCountry(event: any): void {
    this.user.SelectedCountry = event.item;
    this.loadState(event.item.id);
  }

  typeaheadOnBlurCountry(event:any):void{
    if (!!event.item.name) {
      this.user.SelectedCountry=event.item;
      this.loadState(event.item.id);
    }
    else {
      this.user.SelectedCountry = new Location();
    }
    
  }

  onSelectState(event: any): void {
    
    this.user.SelectedState= event.item;
    this.loadCity(event.item.id);
  }

  typeaheadOnBlurState(event:any):void{
    
    if (!!event.item.name) {
      this.user.SelectedState=event.item;
      this.loadCity(event.item.id);
    }
    else {
      this.user.SelectedState = new Location();
    }
    
  }

  onSelectCity(event: any): void {
    this.user.SelectedCity= event.item;
  }

  typeaheadOnBlurCity(event:any):void{
    if (!!event.item.name) {
      this.user.SelectedCity= event.item;
    }
    else {
      this.user.SelectedCity = new Location();
    }
  }

  signup(SignupForm:NgForm) {
    if (SignupForm.valid) {
      let payload = {
        "username": this.user.email,
        "email": this.user.email,
        "password1": this.user.password1,
        "password2": this.user.password2,
        "first_name": this.user.first_name,
        "city_id": this.user.SelectedCity.id,
        "zipcode": this.user.zipcode,
        "is_newsletter": this.user.is_newsletter
      }
      console.log(payload);
      this.loader.show();
      this.apiService.signup(payload)
      .subscribe((res: any) => {
        console.log('Registration res', res);
        this.registrationRes=res;
        if(!this.signupFromScreen){
          this.modalService.dismissAll();
        }
        let modalRef =this.modalService.open(ConfirmModalComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'signup-confirm-title',  size: 'md', windowClass: 'confirm-modal' });
        modalRef.componentInstance.message=this.registrationRes.detail;
        modalRef.componentInstance.heading="Account created successfully";
        modalRef.componentInstance.isPrimary=true;
        //this.user=new SignupUser();
        SignupForm.resetForm();
        this.loader.hide();
      }, (errorRes) => {
        console.log('Registration res', errorRes);
        
        if(errorRes['error']['email']){
          this.toastr.error(errorRes['error']['email']);
        }else if(errorRes['error']['username']){
          this.toastr.error(errorRes['error']['username']);
        }
        else if (errorRes['error']['non_field_errors']) {
          errorRes['error']['non_field_errors'].forEach(element => {
            this.errorMessage = element;
            this.toastr.error(this.errorMessage);
          });
        }
        //this.toastr.error(this.errorMessage);
        this.loader.hide();
      });
    }
  }

  openLoginModal() {
    if (this.signupFromScreen) {
      this.router.navigateByUrl("/login/client");
    }
    else {
      this.modalService.dismissAll();
      let modalRef = this.modalService.open(LoginViewComponent, { scrollable: true, centered: true, ariaLabelledBy: 'login-view-title', size: 'md', windowClass: 'login-view-class-modal' });
      modalRef.componentInstance.loginFromScreen = false;
    }
  }
}
