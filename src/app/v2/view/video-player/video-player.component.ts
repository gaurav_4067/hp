import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlyrComponent } from 'ngx-plyr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { Lesson } from '../../all-course/all-course-model';
import { CourseRatingComponent } from '../../all-course/course-rating/course-rating.component';
import { MainCourseInfo } from '../../all-course/detail/detail.model';
import { PurchaseCourseComponent } from '../../all-course/purchase-course/purchase-course.component';
import { DonateComponent } from '../../practitioner/donate/donate.component';
import { LoginViewComponent } from '../login-view/login-view.component';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {
  @Input() course_details:MainCourseInfo;
  @Input() course_id:string='';
  @Input() lesson:Lesson;
  @Input() lesson_index:number;
  //@Input() videoSources: Plyr.Source[] = [];
  
  customer_id:string='';
  showNextLessonText:boolean=false;
  plyr: PlyrComponent;
  player: Plyr;

  videoSources: Array<Plyr.Source>;
  logedinData:any;
   
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public route:ActivatedRoute,
    public authService:AuthService,
    private modalService:NgbModal) {
      this.videoSources=new Array<Plyr.Source>();
    }

  ngOnInit(): void {
    
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if (this.logedinData) {
      this.customer_id = this.logedinData["id"];
    }
    
    this.videoSources=new Array<Plyr.Source>(); 
    //let activeLesson=this.course_details.lessons.filter(i=>i.id==this.lesson.id)
    this.videoSources.push({"src":this.generalService.getPractiotionerImageBaseUrl(this.lesson.video_file),"type": 'video/mp4'});
  }

  
  
  playVideo(params: any){
    //params.detail.plyr.pause();
    if(this.authService.isAuthenticated()){
      //user is logged in
      
      
      if(this.course_details.is_purchase_course || this.course_details.course_price ==0){ 

       
        // if(this.course_details.viewers_detail.findIndex(i=>i.id==this.customer_id)==-1){
        //   //user playing video first time, update video viewr detail
        //   this.updateViewerDetail();
        // }
        params.detail.plyr.play();
        this.showNextLessonText=false;
        console.log('played', event);
      }
      else{
        params.detail.plyr.stop();
        let modalRef =this.modalService.open(PurchaseCourseComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'modal-basic-title',  size: 'lg', windowClass: 'purchase-course-modal' });
        modalRef.componentInstance.selectedCourse=this.course_details;
        modalRef.componentInstance.course_id=this.course_id;
      }
    }
    else{
      params.detail.plyr.stop()
      //User is not logged in 
      let modalRef=this.modalService.open(LoginViewComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'login-view-title',  size: 'md', windowClass: 'login-view-class-modal' });
      modalRef.componentInstance.loginFromScreen=false;
      modalRef.componentInstance.customLogin=true;
      modalRef.componentInstance.modalRef=modalRef;
    }
  }

  pauseVideo(params:any){
    params.pause();
  }

  updateViewerDetail(){
    let that =this;
    let payload = {
      "customer": this.customer_id,
      "course": this.course_id,
      "lesson":this.lesson.id,
      "is_recommended":true
   
    }
    that.apiService.updateCourseViewDetail(payload)
      .subscribe((updateViewerDetailResponse: any) => {
        console.log('updateViewerDetailResponse', updateViewerDetailResponse);
        let response = updateViewerDetailResponse;
      }, (error) => {
        console.log('addFavRes error', error);
      });
  }

  initiateDonateToPractitioner(){
    
    let  modalRef = this.modalService.open(DonateComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'donate-class-modal',backdrop:false });
    let course_detail= this.course_details;
    modalRef.componentInstance.data = {"first_name":course_detail.practitioner_firstname,"last_name":course_detail.practitioner_lastname,"practitioner_id":course_detail.practitioner_id,"practitioner_image":course_detail.practitioner_profile_image,"id":this.course_id,"donateType":"course"};
  }

  videoEnded(params: any){
    
    
    if(this.course_details.lessons.length==1){
      let  modalRef = this.modalService.open(CourseRatingComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'donate-class-modal',backdrop:false });
      let course_detail= this.course_details;
      modalRef.componentInstance.data =course_detail;
      modalRef.componentInstance.course_id =this.course_id;
    }
    else if (this.course_details.lessons.length > 1) {
     
      if (this.lesson_index == this.course_details.lessons.length - 1) {
        let modalRef = this.modalService.open(CourseRatingComponent, { scrollable: true, centered: true, ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'donate-class-modal', backdrop: false });
        let course_detail = this.course_details;
        modalRef.componentInstance.data = course_detail;
        modalRef.componentInstance.course_id = this.course_id;
      }
      else {
        this.showNextLessonText = true;
        setTimeout(() => {
          this.moveToRight();
          this.showNextLessonText = false;
        }, 10000);
      }
    }

    this.updateViewerDetail();
  }

  moveToRight(){
    this.authService.notifiedChangeVideo();
  }

  openModal(template: TemplateRef<any>) {
    this.modalService.open(template,{windowClass:'share-modal'});
  }

  addRemoveFavPractitioner(){
    this.loader.show();
    let action='favorite-practitioner';
    let payload = {
      "customer_id": this.customer_id,
      "practitioner": this.course_details.practitioner_id
    };
    if(this.course_details.is_favorite_practitioner){
      this.removeFavorite(action,this.course_details.favorite_practitioner_id);
    }
    else{
      this.addFavorite(action,payload);
    }
  }

  addFavorite(action,payload) {
    this.apiService.addFavorite(action,payload)
      .subscribe((addFavoritePractitionResponse: any) => {
        
        console.log('addFavoritePractitionResponse', addFavoritePractitionResponse);
        let response = addFavoritePractitionResponse;
        if (action == 'favorite-practitioner') {
          this.course_details.is_favorite_practitioner = response.is_favorite;
          this.course_details.favorite_practitioner_id = response.id;
        }
        else if (action == 'favorite-course') {
          this.course_details.is_favorite_course = response.is_favorite;
          //this.classDetail.fav = response.id;
        }
        this.loader.hide();
        //this.isAdded=true;
        //this.modalService.show(template);
      }, (error) => {
        console.log('addFavRes error', error);
        this.loader.hide();
      });
  }

  removeFavorite(action,id){
    this.loader.show();
    this.apiService.removeFromFavorite(action,id)
      .subscribe((res: any) => {
        
        console.log('Remove '+action, res);  
        if(action=='favorite-practitioner'){
          this.course_details.is_favorite_practitioner=false;  
        } 
        else if(action=='favorite-course') {
          this.course_details.is_favorite_course=false;  
        }  
        this.loader.hide();
        //this.isAdded=false;
        //this.modalService.show(template);
      }, (error) => {
        console.log('Remove '+action, error);
        this.loader.hide();
      });
  }

  addRemoveFavCourse(){
    this.loader.show();
    
    let action='favorite-course'
    let payload = {
      "customer_id": this.customer_id,
      "course": this.course_id
    };
    if(this.course_details.is_favorite_course){
      this.removeFavorite(action,this.course_details.favorite_course_id);
    }
    else{
      this.addFavorite(action,payload);
    }
  }
}
