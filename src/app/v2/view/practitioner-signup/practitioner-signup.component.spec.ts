import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PractitionerSignupComponent } from './practitioner-signup.component';

describe('PractitionerSignupComponent', () => {
  let component: PractitionerSignupComponent;
  let fixture: ComponentFixture<PractitionerSignupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PractitionerSignupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PractitionerSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
