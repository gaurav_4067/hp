export class PractitionerSignup {
    constructor(){
        this.expertise=new Array<string>();
        this.certitifcations=new Array<any>();
        this.documents=new Array<any>();
    }

    public username:string;
    public business_name:string;
    public email:string;
    public description:string;    
    public password1:string;
    public password2:string;
    public first_name:string;
    public last_name:string;
    public phonenumber:string;
    public zipcode:string;
    public city_id:number;
    public is_practitioner_registration:boolean=true;
    public expertise:Array<string>;
    public certifications_type:string;
    public documents_type:string;
    public documents: Array<any>;
    public certitifcations:Array<any>;
} 