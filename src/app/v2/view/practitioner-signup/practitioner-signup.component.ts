import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { Location } from '../../signup/signup.model';
import { PractitionerSignup } from './practitioner-signup-modal';
 
import {DomSanitizer} from '@angular/platform-browser';
import { HttpErrorResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-practitioner-signup',
  templateUrl: './practitioner-signup.component.html',
  styleUrls: ['./practitioner-signup.component.scss']
})
export class PractitionerSignupComponent implements OnInit {
  step:number=1;
  countries:Array<any>;
  selectedCountry: Location;
  selectedState: Location;
  selectedCity: Location;
  states:any;
  cities:any;
  selectedExpertise:Array<any>;
  prac:PractitionerSignup;
  expertiseConfig = {
    displayKey: 'interest',
    search: true,
    height: 'auto',
    placeholder: 'Select Expertise *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'interest',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  documentSrc:any;
  documents:any;
  certifications:any;
  phoneCode:string;
  expertiseData: any = [];

  constructor(public toastr:ToastrService,
    public generalService:GeneralService,
    public thirpApiService:ThirdPApiService,
    public loader:NgxSpinnerService,
    public apiService:ApiService,
    public sanitizer :DomSanitizer,
    public modalService:NgbModal) { 
      this.countries=new Array<any>();
      this.selectedCountry= new Location();
      this.selectedState= new Location();
      this.selectedCity= new Location();
      this.prac=new PractitionerSignup();
      this.documents=new Array<any>();
      this.certifications=new Array<any>();
      this.selectedExpertise=new Array<any>();
    }

  ngOnInit(): void {
    this.loadCountry();
    this.apiService.getInterest()
      .subscribe((expertise: any) => {
        this.expertiseData = [...expertise];
      });
  }

  onDocFileSelected(event) {
    
    this.documents=new Array<any>();
    const file = event.target.files[0];
    this.documentSrc = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));
    this.documents.push(file);
  }

  onCertFileSelected(event){
    
    const file = event.target.files[0];
    this.certifications.push(file);
  }

  loadCountry(){
    this.loader.show();
    this.thirpApiService
    .getCountries("id,name,phone_code","no_page")
    .subscribe((response: any) => {
      console.log(response);
      this.countries = response;
      this.loader.hide();
    });
  }

  onSelectCountry(event: any): void {
    this.selectedCountry = event.item;
    if (this.selectedCountry.phone_code.slice(0, 1) == "+") {
      this.phoneCode=this.selectedCountry.phone_code;
       
    } else {
      this.phoneCode= '+'+this.selectedCountry.phone_code
    }
    // this.phoneCode=this.selectedCountry.phone_code;
    this.loadState(event.item.id);
  }

  typeaheadOnBlurCountry(event:any):void{
    if (!!event.item.name) {
      this.selectedCountry=event.item;
      this.loadState(event.item.id);
    }
    else {
      this.selectedCountry = new Location();
    }    
  }

  loadState(countryId){
    this.loader.show();
    this.thirpApiService
    .getStates(countryId,"id,country,name","no_page")
    .subscribe((response: any) => {
      console.log(response);
      this.states = response;
      this.loader.hide();
    });
  }

  loadCity(stateId){
    this.loader.show();
    this.thirpApiService
    .getCities(stateId,"id,state,name","no_page")
    .subscribe((response: any) => {
      console.log(response);
      this.cities = response;
      this.loader.hide();
    });
  }
  
  onSelectState(event: any): void {
    
    this.selectedState= event.item;
    this.loadCity(event.item.id);
  }

  typeaheadOnBlurState(event:any):void{
    
    if (!!event.item.name) {
      this.selectedState= event.item;
      this.loadCity(event.item.id);
    }
    else {
      this.selectedState = new Location();
    }
    
  }

  onSelectCity(event: any): void {
    this.selectedCity= event.item;
  }

  typeaheadOnBlurCity(event:any):void{
    if (!!event.item.name) {
      this.selectedCity= event.item;
    }
    else {
      this.selectedCity = new Location();
    }
  }

  submitStep1(isValid) {
    if (isValid) {
      this.step=2;
    }
  }

  submitStep2(isValid) {
    if (this.documents.length==0) {
      this.toastr.warning("Atleast one Id Document needed");
    }
    else if (this.certifications.length==0) {
      this.toastr.warning("Atleast one Certification needed");
    }
    else if(isValid) {
        this.step=3;
    }
  }

  createPractitioner(isValid) {
    if(isValid){

    }

    // if (this.documents.length==0) {
    //   this.toastr.warning("Atleast one Id Document needed");
    // }
    // else if (this.certifications.length==0) {
    //   this.toastr.warning("Atleast one Certification needed");
    // }
    // else if(isValid) {
    //     this.step=3;
    // }
  }

  expertiseChanged(data) {
    this.selectedExpertise=data;
    console.log(data);
  }

  addPractioner(isValid) {
    // console.log('this.AddPractitionerForm ', this.);
    //this.formSubmitted = true;
    if (isValid) {
      if (!this.documents) {
        this.toastr.warning("Atleast one Id Document needed");
      }
      if (this.certifications.length==0) {
        this.toastr.warning("Atleast one Certification needed");
      }
      if (this.documents && this.certifications) {
        let formData = new FormData();
        formData.append("username", this.prac.email);
        formData.append("email", this.prac.email);
        formData.append("password1", this.prac.password1);
        formData.append("password2", this.prac.password1);
        formData.append("first_name", this.prac.first_name);
        formData.append("last_name", this.prac.last_name);
        // formData.append("phonenumber", this.AddPractitionerForm.value.phoneCode + this.prac.phonenumber);
        formData.append("phonenumber",  this.phoneCode+this.prac.phonenumber);
        formData.append("city_id", this.selectedCity.id);
        formData.append("city", this.selectedCity.name);
        formData.append("state", this.selectedState.name);
        formData.append("country", this.selectedCity.name);
        formData.append("zipcode", this.prac.zipcode);
        formData.append("description", this.prac.description);        
        formData.append("business_name", this.prac.business_name);        
        formData.append("is_add_from_admin", 'false');
        formData.append("is_practitioner_registration", 'true');
        formData.append("documents", this.documents);
        formData.append("certitifcations", this.certifications);
        let expertiseData: any = '';
        if (this.selectedExpertise.length>0) {
          this.selectedExpertise.forEach(element => {
            if (element) {
              expertiseData =  expertiseData === '' ? element.interest :  expertiseData + ',' +  element.interest;
            }
          });
        } else {
          expertiseData = '';
        }
        formData.append("expertise", expertiseData);
        console.log(formData);
        this.loader.show();
        this.apiService.addPractitioner(formData)
          .subscribe((res: any) => {
            this.zoomUserCreation(this.prac.email, this.prac.first_name, this.prac.last_name);
            this.toastr.success(res?.detail);
            this.loader.hide();
            this.modalService.dismissAll();
          }, (error) => {
            if (error['error']['non_field_errors']) {
              error['error']['non_field_errors'].forEach(element => {
                this.toastr.error(element);
              });
            } else if (error['error']['phonenumber']) {
              error['error']['phonenumber'].forEach(element => {
                this.toastr.error(element);
              });
            } else if (error['error']['email']) {
              error['error']['email'].forEach(element => {
                this.toastr.error(element);
              });
            } else if (error['error']['zipcode']) {
              error['error']['zipcode'].forEach(element => {
                this.toastr.error(element);
              });
            } else {
              this.toastr.error('Something went wrong please try again');
            }
            this.loader.hide();
          });
      }
    }
  }

  zoomUserCreation(resEmail, fname, lname){
    this.loader.show();
    this.apiService
      .zoomUserCreation(resEmail, fname, lname, 1)
      .subscribe((response: any) => {
        console.log('zoomUserCreation', response);
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.loader.hide();
      });
  }
}
