import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { PractitionerDetail } from '../../my-favorites/my-favorites.model';
import { LoginViewComponent } from '../login-view/login-view.component';

@Component({
  selector: 'app-practitioners',
  templateUrl: './practitioners.component.html',
  styleUrls: ['./practitioners.component.scss']
})
export class PractitionersComponent implements OnInit {
  // @Input() data : Array<PractitionerDetail>;
  @Input() id:string="";
  @Input() fav_id:string="";  
  @Input() showRemove:boolean=false;
  @Input() image : string;
  @Input() firstname : string;
  @Input() lastname : string;
  @Input() rating : string;
  @Input() ratecount : string;
  @Input() expertise : string;
  @Input() startingprice : number; 
  @Output() getUpdatedPractitionerData = new EventEmitter();
  // @Input() isSearchBox : boolean=false;
  isOpen = false;
  constructor(public generalService:GeneralService,
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public authService:AuthService,
    public router:Router,
    public modalService:NgbModal) { }

  ngOnInit(): void {
  }

  removeFromFavorite(id){
    this.loader.show();
    this.apiService.removeFromFavorite('favorite-practitioner',id)
      .subscribe((res: any) => {
        
        console.log('Remove favorite-practitioner', res);   
        this.getUpdatedPractitionerData.emit();   
        this.loader.hide();
      }, (error) => {
        console.log('Remove favorite-practitioner', error);
        this.loader.hide();
      });
  }

  validateCustomer(){
    if(this.authService.isAuthenticated()){
      this.router.navigateByUrl("/practitioner/profile/"+this.id);
    }
    else{
      
      let modalRef=this.modalService.open(LoginViewComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'login-view-title',  size: 'md', windowClass: 'login-view-class-modal' });
      modalRef.componentInstance.loginFromScreen=false;
      // alert("please login first");
    }
  }
}
