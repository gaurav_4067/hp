import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ArticleDetails } from '../../my-favorites/my-favorites.model';
import { LoginViewComponent } from '../login-view/login-view.component';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {
  // @Input() data : Array<ArticleDetails>;
  @Output() getUpdatedArticleData = new EventEmitter();
  @Input() id:string="";
  @Input() showRemove:boolean=false;
  @Input() firstname : string;
  @Input() lastname : string;
  @Input() image : string;
  @Input() category : string;
  @Input() title : string;
  @Input() description : string;
  constructor(public generalService:GeneralService,
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public router:Router,
    public authService:AuthService,
    public modalService:NgbModal) { }

  ngOnInit(): void {
  }

  removeFromFavorite(id){
    this.loader.show();
    this.apiService.removeFromFavorite('favorite-article',id)
      .subscribe((res: any) => {
        
        console.log('Remove favorite-article', res);
        this.getUpdatedArticleData.emit();
        this.loader.hide();
      }, (error) => {
        console.log('Remove favorite-article', error);
        this.loader.hide();
      });
  }
  validateCustomer(){
    if(this.authService.isAuthenticated()){
      
      this.router.navigateByUrl("/blog/post-blog/"+this.id);
    }
    else{
      let modalRef=this.modalService.open(LoginViewComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'login-view-title',  size: 'md', windowClass: 'login-view-class-modal' });
      modalRef.componentInstance.loginFromScreen=false;
    }
  }
}
