import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SocialAuthService,GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { LoginUser } from '../login/login.model';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginType: string;
  user: LoginUser;
  errorMessage: any;
  @Input() resource: any;
  @Output() changeBack = new EventEmitter<any>();
  public _accesstoken: any;
  public _refreshtoken: any;
  constructor(public route: ActivatedRoute,
    public apiService: ApiService,
    public loader: NgxSpinnerService,
    public router: Router,
    private socialAuthService: SocialAuthService,
    public toastr: ToastrService) {
    this.user = new LoginUser();
  }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.loginType = param['type'];
      console.log(this.loginType);
    });
  }

  login(e) {
    if (e) {
      console.log(this.user);
      if (this.user) {
        this.loader.show();
        let payload = {
          username: "",
          email: this.user.email,
          password: this.user.password,
          // phone_number: "",
        }
        console.log(payload);
        this.apiService
          .login(payload)
          .subscribe((response: any) => {
            console.log(response);
            if (response) {
              this._accesstoken = response.access_token;
              this._refreshtoken = response.access_token;
              localStorage.setItem(
                "access_token",
                response.access_token
              );
              localStorage.setItem(
                "refresh_token",
                response.refresh_token
              );
              delete response.access_token;
              delete response.refresh_token;
              localStorage.setItem(
                "user_data",
                JSON.stringify(response)
              );
              if (this.user.remember) {
                localStorage.setItem("is_remember", "true");
                const remember = {
                  email: this.user.email,
                  password: this.user.password,
                };
                localStorage.setItem("remember_data", JSON.stringify(remember));
              } else {
                localStorage.setItem("is_remember", "false");
              }
              this.loader.hide();
              this.changeBack.emit(true);
              if (response.is_user_object_created == false) {
                this.loader.show();
                this.apiService
                  .pkIdUpdate(response.groups[0].toLowerCase(), "post")
                  .subscribe((response: any) => {
                    console.log(response);
                    localStorage.setItem(
                      `logedin_data`,
                      JSON.stringify(response[0] ? response[0] : response["results"][0])
                    );
                    this.loggedUpdate({
                      is_user_object_created: true
                    });
                  });
              } else {
                this.PKCallInit(response.groups[0].toLowerCase(), "get");
              }
            }
          },
            (errResponse: HttpErrorResponse) => {
              if (errResponse['error']['non_field_errors']) {
                errResponse['error']['non_field_errors'].forEach(element => {
                  this.errorMessage = element;
                });
              } else {
                this.errorMessage = 'Something went wrong please try again';
              }
              this.loader.hide();
            }
          );
      }
    }
  }

  loggedUpdate(payload) {
    this.loader.show();
    let userData = JSON.parse(localStorage.getItem("user_data"));
    this.loader.show();
    this.apiService
      .previousLoggedUpdate(payload, userData["user"]["pk"])
      .subscribe((response: any) => {
        console.log(response);
        userData["is_user_object_created"] = true;
        localStorage.setItem(
          `user_data`,
          JSON.stringify(userData)
        );
        window.location.reload();
        this.loader.hide();
      });
  }
  PKCallInit(group, method) {
    this.loader.show();
    this.apiService
      .pkIdUpdate(group, method)
      .subscribe((response: any) => {
        console.log(response);
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response[0] ? response[0] : response["results"][0])
        );
        // this.toastr.success('Login Successfully');
        this.loader.hide();
        const userData = JSON.parse(localStorage.getItem('user_data'));
        const logedinData = JSON.parse(localStorage.getItem('logedin_data'));
        const isGroup = userData?.groups[0];
        let userRoute: string;
        
        if(isGroup=="Admin"){
          //window.location.reload();
          this.router.navigateByUrl("/welcome-admin");
        }
        else if(isGroup=="Customer"){
          this.router.navigateByUrl("/home");
          // if (logedinData.is_subscribed) {
          //   this.router.navigateByUrl("/welcome-customer");
          //   window.location.reload();
          // } else {
          //   this.router.navigateByUrl("/home");
          // }
        }
        else if(isGroup=="Practitioner"){
          //window.location.reload();
          this.router.navigateByUrl("/welcome-practitioner");
        }
        else{
          this.router.navigateByUrl("/home");
        }
        //window.location.reload();
      });
  }

  signInWithGoogle(): void {
    this.socialAuthService
    .signIn(GoogleLoginProvider.PROVIDER_ID)
    .then(getUser => {
      if(getUser){
        console.log(getUser);
        this.sociaLogin(getUser, "google");
      }
    });
  }
  signInWithFB(): void {
    this.socialAuthService
    .signIn(FacebookLoginProvider.PROVIDER_ID)
    .then(getUser => {
      console.log(getUser);
      if(getUser){
        this.sociaLogin(getUser, "facebook");
      }
    });
  }
  sociaLogin(User, isUser){
    this.loader.show();
    let payload;
    console.log(User)
    if(isUser == "google"){
      payload = {
        access_token:	User.access_token ? User.access_token : User.authToken,
        code:	User.first_issued_at ? User.first_issued_at : User.id,
        id_token:	User.id_token ? User.id_token : User.idToken,
      }
    }else{
      payload = {
        access_token:	User.authToken,
        code:	User.id,
        id_token:	User.id,
      }
    }
    console.log(payload);
    this.apiService
    .sociaLogin(payload,isUser)
    .subscribe((response: any) => {
      // console.log(response);
      if(response){
        this._accesstoken = response.access_token;
        this._refreshtoken = response.access_token;
        localStorage.setItem(
          "access_token",
          response.access_token
        );
        this.setCookie('access_token','test',7);
        localStorage.setItem(
          "refresh_token",
          response.refresh_token
        );
        delete response.access_token;
        delete response.refresh_token;
        localStorage.setItem(
          "user_data",
          JSON.stringify(response)
        );
        if(this.user.remember){
          localStorage.setItem("is_remember", "true");
          const remember = {
            email: this.user.email,
            password: this.user.password,
          };
          localStorage.setItem("remember_data", JSON.stringify(remember));
        }else{
          localStorage.setItem("is_remember", "false");
        }
        this.loader.hide();
        this.changeBack.emit(true);
        if(response.is_user_object_created == false){
          this.loader.show();
          this.apiService
          .pkIdUpdate(response.groups[0].toLowerCase(),"post")
          .subscribe((response: any) => {
            console.log(response);
            
            localStorage.setItem(
              `logedin_data`,
              JSON.stringify(response[0] ? response[0] : response["results"][0])
            );
            // this.toastr.success('Login Successfully');
            this.loggedUpdate({
              is_user_object_created: true
            });
          });
        }else{
          this.PKCallInit(response.groups[0].toLowerCase(),"get");
        }
      }
    },
    (errResponse: HttpErrorResponse) => {
      if(errResponse['error']['non_field_errors']){
        errResponse['error']['non_field_errors'].forEach(element => {
          this.toastr.error(element);
        });
      }else{
        this.toastr.error('Something went wrong please try again');
      }
      this.loader.hide();
    }
  );
  }

  setCookie(name,value,days) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days*24*60*60*1000));
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
  }
}
