import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import {ArticleDetail, ArticleWrapper} from '../model/article.model'
@Component({
  selector: 'app-all-article',
  templateUrl: './all-article.component.html',
  styleUrls: ['./all-article.component.scss']
})
export class AllArticleComponent implements OnInit {
  nextFourArticle: Array<ArticleDetail>;
  articleWrapper: ArticleWrapper;
  articleList:Array<ArticleDetail>;
  dataLimit: number = 10;
  dataCategory: string = "latest";
  article_count: number = 0;
  page:number=1;
  showLoadScreen:boolean=false;
  searchKey:string="";
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public route:ActivatedRoute) {
    // this.courseWrapper =new  CourseWrapper();
    this.articleList=new Array<ArticleDetail>();
    this.nextFourArticle=new Array<ArticleDetail>();
  }
  ngOnInit(): void {
    this.route.params.subscribe((param) => {
      if (param["key"]) {
        this.searchKey = param["key"];
      }
      this.getAllArticle(this.dataCategory, false);
    })
    
  };

  getAllArticle(category, isMore = false) {
    
    this.dataCategory = category;
    if (isMore) {
      if (this.articleList.length >= this.article_count) {
        //No call further API
      }
      else {
        this.page=this.page+1;
        this.showLoadScreen=true;
        // this.dataLimit = this.dataLimit + 10;
        this.callAPI(isMore);
      }
    }
    else {
      this.callAPI(false);
    }
  }

  callAPI(isMore) {
    this.loader.show();
    this.apiService.getAllArticle(this.dataLimit, this.dataCategory,this.page,this.searchKey)
      .subscribe((articleRes: any) => {
        console.log('articleRes', articleRes);
        this.article_count = articleRes.article_count;
        if (!isMore) {
          //this.liveClass = [];
          //this.dataLimit=this.dataLimit+10;
        }
        if (articleRes.article_list.data.length > 0) {
          if (this.dataCategory == 'latest' && this.page==1) {
            this.nextFourArticle = articleRes.article_list.data.slice(0, 3);
          }
        }

        articleRes.article_list.data.forEach(element => {
          this.articleList.push(element);
        });
        this.showLoadScreen=false;
        this.loader.hide();
      }, (error) => {
        console.log('articleList error', error);
        this.loader.hide();
        this.showLoadScreen=false;
      });
  }
}
