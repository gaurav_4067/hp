import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ContactFormModel, ContactUsResponse } from './contact-us-model';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  contact: ContactFormModel;
  res: ContactUsResponse;
  showSuccess: boolean = false;
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService) {
    this.contact = new ContactFormModel();
    this.res = new ContactUsResponse();
  }

  ngOnInit(): void {
  }

  submit(ContactForm:NgForm) {
    if (ContactForm.valid) {
      console.log(this.contact);
      this.loader.show();

      this.apiService
        .submitContactForm(this.contact)
        .subscribe((response: any) => {
          console.log(response);
          this.res = response;
          if (this.res.id != "") {
            this.showSuccess = true;
            this.contact = new ContactFormModel();
          }
          ContactForm.resetForm();
          this.loader.hide();
        });
    }
  }
}
