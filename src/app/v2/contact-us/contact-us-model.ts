export class ContactFormModel {
    public first_name: string;
    public last_name: string;
    public email: string;
    public phone_number: string;
    public message: string;
}

export class ContactUsResponse {
    public id: string;
    public created: Date;
    public modified: Date;
    public first_name: string;
    public last_name: string;
    public email: string;
    public phone_number: string;
    public message: string;
}