
    export class PractitionerDetail {
        public practitioner_firstname: string;
        public practitioner_lastname: string;
        public practitioner_expertise: string[];
        public practitioner_rating: number;
        public practitioner_rate_count: number;
        public practitioner_id: string;
        public fav_practitioner_id: string;
        public practitioner_profile_image: string;
    }

    export class PractitionerList {
        constructor(){
            this.data=new Array<PractitionerDetail>();
        }
        public total: number;
        public previous_page?: any;
        public next_page?: any;
        public data: Array<PractitionerDetail>;
    }
    export class ArticleList {
        constructor(){
            this.data=new Array<ArticleDetails>();
        }
        public total: number;
        public previous_page?: any;
        public next_page?: any;
        public data: Array<ArticleDetails>;
    }

    export class ArticleDetails {
        public id: string;
        public article__title: string;
        public article__image: string;
        public article__description: string;
        public article__author__first_name: string;
        public article__author__last_name: string;
        public article__is_published: boolean;
        public article__category: string[];
    }

    export class Lesson {
        public id: string;
        public lesson_title: string;
        public lesson_duration: number;
        public video_file: string;
        public lesson_description: string;
    }

    export class CourseDetails {
        constructor(){
            this.lessons = new Array<Lesson>();
            this.practitioner_expertise=new Array<string>();
        }
        public id: string;
        public practitioner_firstname: string;
        public practitioner_lastname: string;
        public practitioner_expertise: Array<string>;
        public course_id: string;
        public course_title: string;
        public related_course_duration_in_sec: number;
        public related_course_rating: number;
        public related_course_rated_count: number;
        public lesson_count: number;
        public lessons: Array<Lesson>;
    }

    export class CoursesList {
        constructor(){
            this.data=new Array<CourseDetails>();
        }
        public total: number;
        public previous_page?: any;
        public next_page?: any;
        public data: Array<CourseDetails>;
    }

    export class LiveClassDetail {
        constructor(){
            this.live_class__practitioner__expertise=new Array<string>();
        }
        public id: string;
        public live_class__title: string;
        public live_class__image: string;
        public live_class__description: string;
        public live_class__practitioner__user_id__first_name: string;
        public live_class__practitioner__user_id__last_name: string;
        public live_class__start_time: Date;
        public live_class__end_time?: Date;
        public live_class__duration: number;
        public live_class__class_date: string;
        public live_class__is_completed: boolean;
        public live_class__practitioner__expertise: Array<string>;
    }

    export class LiveClassList {
        constructor(){
            this.data=new Array<LiveClassDetail>();
        }
        public total: number;
        public previous_page?: any;
        public next_page?: any;
        public data: Array<LiveClassDetail>;
    }

    export class CustomerFavorites {
        constructor(){
            this.practitioner_list=new PractitionerList();
            this.article_list=new ArticleList();
            this.fav_courses_list=new CoursesList();
            this.live_class_list=new LiveClassList();
        }
        public practitioner_list: PractitionerList;
        public article_list: ArticleList;
        public fav_courses_list: CoursesList;
        public live_class_list:LiveClassList;
        public fav_practitioner_count: number;
        public fav_article_count: number;
        public fav_courses_count: number;
        public fav_live_class_count: number;
    }