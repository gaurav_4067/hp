import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { CustomerFavorites } from './my-favorites.model';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-favorites',
  templateUrl: './my-favorites.component.html',
  styleUrls: ['./my-favorites.component.scss']
})
export class MyFavoritesComponent implements OnInit {
  customerFavorites: CustomerFavorites;
  dataLimit: number = 15;
  itemsPerPage: number = 15;

  maxSize: number = 4;

  practitionerPage: number = 1;
  classPage: number = 1;
  coursePage: number = 1;
  articlePage: number = 1;
  userData: any;
  logedinData: any;
  isGroup: any;
  constructor(public apiService: ApiService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public router:Router) {
    this.customerFavorites = new CustomerFavorites();
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.isGroup = this.userData?.groups[0];
    if(this.logedinData && this.logedinData["id"]){
    this.getCustomerFavoritePractitioners(1);
    }
    else{
      this.router.navigate(["/dashboard"]);
    }
  }

  scrollToTop(){
    let scrollToTop = window.setInterval(() => {
      let pos = window.pageYOffset;
      if (pos > 0) {
          window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
          window.clearInterval(scrollToTop);
      }
  }, 16);
  }
  getCustomerFavoritePractitioners(pageNo) {
    this.loader.show();
    this.apiService.getCustomerFavoritePractitioners(pageNo, this.dataLimit, this.logedinData["id"])
      .subscribe((favoritePractitioners: any) => {
        console.log('favoritePractitioners', favoritePractitioners);
        this.customerFavorites = favoritePractitioners;
        this.practitionerPage = pageNo;
        this.scrollToTop();
        this.loader.hide();
      }, (error) => {
        console.log('favoritePractitioners error', error);
        this.loader.hide();
      });
  }

  getCustomerFavoriteArticles(pageNo) {
    this.loader.show();
    this.apiService.getCustomerFavoriteArticles(pageNo, this.dataLimit, this.logedinData["id"])
      .subscribe((favoriteArticles: any) => {
        console.log('favoriteArticles', favoriteArticles);
        this.customerFavorites = favoriteArticles;
        this.articlePage = pageNo;
        this.loader.hide();

      }, (error) => {
        console.log('favoriteArticles error', error);
        this.loader.hide();
      });
  }

  getCustomerFavoriteCourses(pageNo) {
    this.loader.show();
    this.apiService.getCustomerFavoriteCourses(pageNo, this.dataLimit, this.logedinData["id"])
      .subscribe((favoriteCourse: any) => {
        console.log('favoriteCourse', favoriteCourse);
        this.customerFavorites = favoriteCourse;
        this.coursePage = pageNo;
        this.loader.hide();
      }, (error) => {
        console.log('favoriteCourse error', error);
        this.loader.hide();
      });
  }

  getCustomerFavoriteLiveClasses(pageNo) {
    this.loader.show();
    this.apiService.getCustomerFavoriteLiveClasses(pageNo, this.dataLimit, this.logedinData["id"])
      .subscribe((liveClasses: any) => {
        console.log('liveClasses', liveClasses);
        this.customerFavorites = liveClasses;
        this.classPage = pageNo;
        this.loader.hide();
      }, (error) => {
        console.log('favoriteCourse error', error);
        this.loader.hide();
      });
  }

  // removeFromFavorite(action, id) {
  //   this.loader.show();
  //   this.apiService.removeFromFavorite(action, id)
  //     .subscribe((res: any) => {
  //       console.log('Remove' + action, res);

  //       this.loader.hide();
  //     }, (error) => {
  //       console.log('Remove' + action, error);
  //       this.loader.hide();
  //     });
  // }

  getTime(min: any) {
    min = Math.floor(min);
    if (min < 60) {
      return min + " min";
    }
    else if (min == 60) {
      return "1 hour";
    }
    else {
      if ((min % 60) > 0) {
        return Math.floor(min / 60) + " hours " + (min % 60) + " min";
      }
      else {
        return Math.floor(min / 60) + " hours";
      }
    }
  }

  transform(value: string): string {
    let first = value.substr(0, 1).toUpperCase();
    return first + value.substr(1);
  }
}
