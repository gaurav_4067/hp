import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicAboutusComponent } from './public-pages/about-us/about-us.component';
import { PublicContactusComponent } from './public-pages/contact-us/contact-us.component';
import { PublicFaqComponent } from './public-pages/faq/faq.component';
import { PublicHomeComponent } from './public-pages/home/home.component';
import { PublicBecomePractitionerComponent } from './public-pages/become-practitioner/become-practitioner.component';
import { PublicHowitworksComponent } from './public-pages/howitworks/howitworks.component';
import { PublicChangePasswordComponent } from './public-pages/change-password/change-password.component';

import { AuthGuardService as AuthGuard } from './services/AuthGuardService';
import { AuthHomeComponent } from './auth-layouts/home/auth-home.component';
import { ChangePasswordComponent } from './auth-pages/common-pages/change-password/change-password.component';
import { DashboardComponent } from './auth-pages/dashboard/dashboard.component';
import { EditProfileComponent } from './auth-pages/common-pages/edit-profile/edit-profile.component';
import { SessiononlyComponent } from './auth-pages/common-pages/sessiononly/sessiononly.component';
import { GeneralDashboardComponent } from './auth-pages/common-pages/general-dashboard/general-dashboard.component';
import { AllInterestsComponent } from './auth-pages/common-pages/all-interests/all-interests.component';
import { AdminDashboardComponent } from './auth-pages/admin-pages/admin-dashboard/admin-dashboard.component';
import { CustomerDashboardComponent } from './auth-pages/customer-pages/customer-dashboard/customer-dashboard.component';
import { PractitionerDashboardComponent } from './auth-pages/practitioner-pages/practitioner-dashboard/practitioner-dashboard.component';
import { PractitionerChatComponent } from './auth-pages/practitioner-pages/practitioner-chat/practitioner-chat.component';
import { CustomerChatComponent } from './auth-pages/customer-pages/customer-chat/customer-chat.component';
import { CustomerChatArchiveComponent } from './auth-pages/customer-pages/customer-chat-archive/customer-chat-archive.component';
import { PractitionerChatArchiveComponent } from './auth-pages/practitioner-pages/practitioner-chat-archive/practitioner-chat-archive.component';
import { CustomerSessionComponent } from './auth-pages/customer-pages/customer-session/customer-session.component';
import { NotificationsComponent } from './auth-pages/practitioner-pages/notifications/notifications.component';
import { BasicInfoComponent } from './auth-pages/customer-pages/basic-info/basic-info.component';
import { AddExpertiseComponent } from './auth-pages/add-expertise/add-expertise.component';
import { AdminStatsComponent } from './auth-pages/admin-pages/admin-stats/admin-stats.component';

import { MySubscriptionComponent } from './auth-pages/customer-pages/my-subscription/my-subscription.component';
import { CustomerPaymentComponent } from './auth-pages/customer-pages/customer-payment/customer-payment-component';
import { ViewInterestsComponent } from './auth-pages/customer-pages/view-interests/view-interests.component';
import { PractitionerSessionsComponent } from './auth-pages/practitioner-pages/practitioner-sessions/practitioner-sessions.component';
import { PractitionerScheduleComponent } from './auth-pages/practitioner-pages/practitioner-schedule/practitioner-schedule.component';
import { EditScheduleComponent } from './auth-pages/practitioner-pages/practitioner-schedule/edit-schedule/edit-schedule.component';
import { AddPractionerComponent } from './auth-pages/add-practioner-pages/admin-add-practioner-component';
import { PractitionerSessionComponent } from './auth-pages/practioner-dashboard-pages/practitioner-session.component';
import { PaymentInfoComponent } from './auth-pages/payment-info/payment-info.component';
import { BankDetailsComponent } from './auth-pages/payment-info/bank-details/bank-details.component';
import { WithdrawHistoryComponent } from './auth-pages/payment-info/withdraw-history/withdraw-history.component';
import { ConnectAccountComponent } from './auth-pages/payment-info/connect-account/connect-account.component';
import { DocumentVerificationComponent } from './auth-pages/payment-info/document-verification/document-verification.component';
import { CustomerProfileComponent } from './auth-pages/customer-pages/customer-profile/customer-profile.component';
import { CustomerBookingComponent } from './auth-pages/customer-pages/customer-booking/customer-booking.component';
import { CustomerStatsComponent } from './auth-pages/customer-pages/customer-stats/customer-stats.component';

import { StatsComponent } from './auth-pages/practitioner-pages/stats/stats.component';
import { BillingInfoComponent } from './auth-pages/customer-pages/billing-info/billing-info.component';
import { AddSubscriptionPlanComponent } from './auth-pages/admin-pages/add-subscription/add-subscription-plan/add-subscription-plan.component';
import { ManageNotificationComponent } from './auth-pages/admin-pages/manage-notification/manage-notification.component';
import { InterestListComponent } from './auth-pages/admin-pages/interest-list/interest-list.component';
import { CustomerAboutusComponent } from './auth-pages/customer-pages/customer-aboutus/customer-aboutus.component';

import { PrivacyPolicyComponent } from './public-pages/privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './public-pages/terms-conditions/terms-conditions.component';

import { BillingHistoryComponent } from './auth-pages/customer-pages/billing-info/billing-history/billing-history.component';
import { PaymentUpdateComponent } from './auth-pages/customer-pages/billing-info/payment-update/payment-update.component';
import { PaymentcreateComponent } from './auth-pages/customer-pages/billing-info/payment-create/payment-create.component';

import { CustomerPractitionerProfileComponent } from './auth-pages/customer-pages/customer-practitioner-profile/customer-practitioner-profile.component';
import { AdminPractitionerListComponent } from './auth-pages/admin-pages/admin-practitioner-list/admin-practitioner-list.component';
import { AdminCustomerListComponent } from './auth-pages/admin-pages/admin-customer-list/admin-customer-list.component';
import { AddCustomerComponent } from './auth-pages/add-customer-pages/admin-add-customer-component';
import { ViewExpertiseComponent } from './auth-pages/view-expertise/view-expertise.component';
import { HelpFaqComponent } from './auth-pages/common-pages/help-faq/help-faq.component';
import { TermsConditionComponent } from './auth-pages/common-pages/terms-condition/terms-condition.component';
import { ContactUsComponent } from './v2/contact-us/contact-us.component';
import { MyFavoritesComponent } from './v2/my-favorites/my-favorites.component';
import { HomepageComponent } from './v2/homepage/homepage.component';
import { SearchPractitionerComponent } from './v2/practitioner/search-practitioner/search-practitioner.component';
import { PractitionerProfileComponent } from './v2/practitioner/practitioner-profile/practitioner-profile.component';
import { SignupComponent } from './v2/signup/signup.component';
import { LoginComponent } from './v2/login/login.component';
import { LiveClassesComponent } from './v2/live-classes/live-classes.component';
import { MainBlogComponent } from './v2/blog/main-blog/main-blog.component';
import { PostBlogComponent } from './v2/blog/post-blog/post-blog.component';
import { SupportComponent } from './v2/support/support.component';
import { AboutUsComponent } from './v2/about-us/about-us.component';
import { AllCourseComponent } from './v2/all-course/all-course.component';
import { AllArticleComponent } from './v2/all-article/all-article.component';
import { SearchComponent } from './v2/search/search.component';
import { DetailComponent } from './v2/all-course/detail/detail.component';
import { ClassDetailComponent } from './v2/live-classes/detail/detail.component';
import { SessionComponent } from './v2/session/session.component';
import { MyCourseComponent } from './v2/all-course/my-course/my-course.component';
import { SessionOverComponent } from './v2/session/session-over/session-over.component';
import { ForgotPasswordComponent } from './v2/forgot-password/forgot-password.component';
import { ManageSubscriptionComponent } from './v2/manage-subscription/manage-subscription.component';
import { ManageMySubscriptionComponent } from './v1/customer/manage-subscription/manage-subscription.component';
import { WelcomeCustomerComponent } from './v1/customer/welcome-customer/welcome-customer.component';
import { TestComponent } from './v1/customer/test/test.component';
import { LiveClassComponent } from './v1/customer/live-class/live-class.component';
import { BookSessionComponent } from './v2/session/book-session/book-session.component';
import { PractitionerCoursesComponent} from './v1/practitioner/courses/courses.component';
import { TncComponent } from './v1/practitioner/tnc/tnc.component';
import { ScheduleSessionComponent } from './v1/practitioner/schedule-session/schedule-session.component';

const userData = JSON.parse(localStorage.getItem('user_data'));
const logedinData = JSON.parse(localStorage.getItem('logedin_data'));
const isGroup = userData?.groups[0];
let userRoute: string;

// switch (isGroup) {
//   case 'Admin':
//     userRoute = 'welcome-admin';
//     break;
//   case 'Customer':
//     if(!!logedinData && logedinData.is_subscribed){
//       userRoute = 'welcome-customer';
//     }else{
//       userRoute = 'home';
//     }
//     break;
//   case 'Practitioner':
//     userRoute = 'welcome-practitioner';
//     break;
//   default:
//     userRoute = 'home';
// }

const routes: Routes = [
  // {
  //   path: '',
  //   component: AuthHomeComponent,
  //   canActivate: [AuthGuard],
  //   children: [
  //     { path: '', redirectTo: userRoute, pathMatch: 'full' },
  //     { path: 'change-password', component: ChangePasswordComponent, data: { title: 'Change Password' } },
  //     { path: 'practitioner-profile', component: EditProfileComponent, data: { title: 'My Profile' } },
  //     { path: 'add-expertise', component: AddExpertiseComponent, data: { title: 'Add Expertise' } },
  //     { path: 'view-expertise', component: ViewExpertiseComponent, data: { title: 'My Expertises' } },
  //     { path: 'welcome-admin', component: AdminDashboardComponent },
  //     { path: 'welcome-customer', component: CustomerDashboardComponent,data: { title: "Welcome "+ JSON.parse(localStorage.getItem("user_data"))?.user?.first_name.substr(0,1).toUpperCase() +JSON.parse(localStorage.getItem("user_data"))?.user?.first_name.substr(1) } },
  //     { path: 'customer-basic-info', component: BasicInfoComponent },
  //     { path: 'customer-profile', component: CustomerProfileComponent, data: { title: 'My Profile' } },
  //     { path: 'billing-info', component: BillingInfoComponent },
  //     { path: 'billing-history', component: BillingHistoryComponent,data: { title: 'Account History' } },
  //     { path: 'payment-create', component: PaymentcreateComponent, data: { title: 'Add Payment Information' } },
  //     { path: 'payment-update', component: PaymentUpdateComponent, data: { title: 'Update Payment Information' } },
  //     { path: 'welcome-practitioner', component: DashboardComponent,data: { title: 'Hi Dr.'+ JSON.parse(localStorage.getItem("logedin_data"))?.user_id?.first_name } },
  //     // { path: 'practitionersessions', component: PractitionerSessionComponent },
  //     { path: 'session-dashboard', component: PractitionerSessionComponent },
  //     { path: 'courses', component: CoursesComponent },
  //     { path: 'addpractitioner', component: AddPractionerComponent },
  //     { path: 'addcustomer', component: AddCustomerComponent },
  //     { path: 'mysubscription', component: MySubscriptionComponent },
  //     { path: 'customerpayment', component: CustomerPaymentComponent },
  //     { path: 'cutomer/notifications', component: NotificationsComponent, data: { title: "Manage Notifications" } },
  //     { path: 'view-interests', component: ViewInterestsComponent, data: { title: 'Interest' } },
  //     { path: 'practitioner/stats', component: StatsComponent, data: { title: "My Stats" } },
  //     { path: 'practitioner/schedule', component: PractitionerScheduleComponent, data: { title: "My Schedule" } },
  //     { path: 'practitioner/sessions', component: PractitionerSessionsComponent, data: { title: "My Sessions" } },
  //     { path: 'payment-info', component: PaymentInfoComponent, data: { title: "Payment Info" } },
  //     { path: 'bank-details', component: BankDetailsComponent, data: { title: "Bank Account" } },
  //     { path: 'withdraw-history', component: WithdrawHistoryComponent, data: { title: "Withdraw History" } },
  //     { path: 'connect-account', component: ConnectAccountComponent, data: { title: "Connect Account" } },
  //     { path: 'document-verification', component: DocumentVerificationComponent, data: { title: "Document Verification" } },
  //     { path: 'practitioner/notifications', component: NotificationsComponent, data: { title: "Manage Notifications" } },
  //     { path: 'practitioner/edit-schedule', component: EditScheduleComponent, data: { title: "My Schedule" } },
  //     { path: 'sessiononly', component: SessiononlyComponent },
  //     { path: 'customer/general-dashboard', component: GeneralDashboardComponent },
  //     { path: 'customer/all-interests', component: AllInterestsComponent },
  //     { path: 'chat', component: CustomerChatComponent },
  //     { path: 'archive-chat', component: CustomerChatArchiveComponent, data: { title: 'My Chats Archive' } },
  //     { path: 'practitioner-archive', component: PractitionerChatArchiveComponent, data: { title: 'Chat Archive' } },
  //     { path: 'customer-session', component: CustomerSessionComponent },
  //     { path: 'practitioner/chat', component: PractitionerChatComponent },
  //     { path: 'customer-booking', component: CustomerBookingComponent },
  //     { path: 'add-subscription-plan', component: AddSubscriptionPlanComponent },
  //     { path: 'manage-notification', component: ManageNotificationComponent, data: { title: 'Edit Notifications' }},
  //     { path: 'customer-stat', component: CustomerStatsComponent, data: { title: 'Stats' } },
  //     { path: 'interest-list', component: InterestListComponent },
  //     { path: 'aboutus', component: CustomerAboutusComponent, data: { title: 'About Healplace' } },
  //     { path: 'help-faq', component: HelpFaqComponent, data: { title: 'Help and Faq' } },
  //     // { path: 'terms-conditions', component: TermsConditionComponent, data: { title: 'Terms and Conditions' } },
  //     { path: 'customer/practitioner-profile', component: CustomerPractitionerProfileComponent},
  //     { path: 'admin/practitioner-list', component: AdminPractitionerListComponent, data: { title: 'Practitioners' } },
  //     { path: 'admin/customer-list', component: AdminCustomerListComponent, data: { title: 'Users / Clients' } },
  //     { path: 'admin/stats', component: AdminStatsComponent, data: { title: 'Users / Clients' } },
  //     { path: 'customer/live-class', component: LiveClassComponent, data: { title: 'Live Classes' } },
  //     { path: 'session/book-session/:pid', component:  BookSessionComponent},
  //   ]
  // },
  { path: '', component: HomepageComponent },
  { path: 'home', component: HomepageComponent },
  //Customer Routing Start
  { path: 'welcome-customer', component: CustomerDashboardComponent,data: { title: "Welcome "+ JSON.parse(localStorage.getItem("user_data"))?.user?.first_name.substr(0,1).toUpperCase() +JSON.parse(localStorage.getItem("user_data"))?.user?.first_name.substr(1) } },
  { path: 'customer-profile', component: CustomerProfileComponent, data: { title: 'My Profile' } },
  { path: 'customer/notifications', component: NotificationsComponent, data: { title: "Manage Notifications" } },
  { path: 'billing-info', component: BillingInfoComponent },
  { path: 'mysubscription', component: MySubscriptionComponent },
  { path: 'change-password', component: ChangePasswordComponent, data: { title: 'Change Password' } },
  { path: 'help-faq', component: HelpFaqComponent, data: { title: 'Help and Faq' } },
  { path: 'aboutus', component: CustomerAboutusComponent, data: { title: 'About Healplace' } },
  { path: 'live-class', component: LiveClassComponent, data: { title: 'Live Classes' } },
  { path: 'customer-stat', component: CustomerStatsComponent, data: { title: 'Stats' } },
  { path: 'customer-session', component: CustomerSessionComponent },
  { path: 'chat', component: CustomerChatComponent },
  { path: 'view-interests', component: ViewInterestsComponent, data: { title: 'Interest' } },
  { path: 'customer/practitioner-profile', component: CustomerPractitionerProfileComponent},
  { path: 'archive-chat', component: CustomerChatArchiveComponent, data: { title: 'My Chats Archive' } },
  { path: 'session/book-session/:pid', component:  BookSessionComponent},
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'my-favorites', component: MyFavoritesComponent },
  { path: 'home', component: HomepageComponent },
  { path: 'home/:data', component: HomepageComponent },
  { path: 'practitioner/search', component: SearchPractitionerComponent },
  { path: 'practitioner/search/:key', component: SearchPractitionerComponent },
  { path: 'practitioner/profile/:pid', component:  PractitionerProfileComponent},
  { path: 'session/book/:pid', component:  SessionComponent},  
  { path: 'session/over/:pid/:user', component:  SessionOverComponent},
  { path: 'live-classes', component: LiveClassesComponent },
  { path: 'live-classes/:key', component: LiveClassesComponent },
  { path: 'class/detail/:cid', component: ClassDetailComponent },
  { path: 'all-course', component: AllCourseComponent },
  { path: 'all-course/:key', component: AllCourseComponent },
  { path: 'my-course', component: MyCourseComponent },
  { path: 'course/detail/:cid', component: DetailComponent },
  { path: 'all-article', component: AllArticleComponent },
  { path: 'all-article/:key', component: AllArticleComponent },
  { path: 'search', component: SearchComponent },
  { path: 'search/:text', component: SearchComponent },
  { path: 'blog/main-blog', component: MainBlogComponent },
  { path: 'blog/post-blog/:id', component: PostBlogComponent },
  { path: 'signup/:type', component: SignupComponent },
  { path: 'login/:type', component: LoginComponent },
  { path: 'reset-password', component: ForgotPasswordComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'support', component: SupportComponent },
  { path: 'support/faq/:id/:category', component: AboutUsComponent },
  { path: 'terms-conditions', component: TermsConditionsComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'v2/welcome-customer', component: WelcomeCustomerComponent },
  { path: 'manage-subscription', component: ManageSubscriptionComponent },
  { path: 'manage-my-subscription', component: ManageMySubscriptionComponent },
  //Customer Routing end
  //Practitioner Routing Start
  { path: 'welcome-practitioner', component: DashboardComponent,data: { title: 'Hi Dr.'+ JSON.parse(localStorage.getItem("logedin_data"))?.user_id?.first_name } },
  { path: 'session-dashboard', component: PractitionerSessionComponent },
  { path: 'practitioner-profile', component: EditProfileComponent, data: { title: 'My Profile' } },
  { path: 'document-verification', component: DocumentVerificationComponent, data: { title: "Document Verification" } },
  { path: 'practitioner/notifications', component: NotificationsComponent, data: { title: "Manage Notifications" } },
  { path: 'courses', component: PractitionerCoursesComponent },
  { path: 'practitioner/stats', component: StatsComponent, data: { title: "My Stats" } },
  { path: 'practitioner/schedule', component: PractitionerScheduleComponent, data: { title: "My Schedule" } },
  { path: 'session/schedule', component: ScheduleSessionComponent, data: { title: "My Schedule" } },
  { path: 'practitioner/sessions', component: PractitionerSessionsComponent, data: { title: "My Sessions" } },
  { path: 'payment-info', component: PaymentInfoComponent, data: { title: "Payment Info" } },
  { path: 'connect-account', component: ConnectAccountComponent, data: { title: "Connect Account" } },
  { path: 'add-expertise', component: AddExpertiseComponent, data: { title: 'Add Expertise' } },
  { path: 'bank-details', component: BankDetailsComponent, data: { title: "Bank Account" } },
  { path: 'withdraw-history', component: WithdrawHistoryComponent, data: { title: "Withdraw History" } },
  { path: 'tnc', component: TncComponent, data: { title: 'Terms and Conditions' } },
  //Practitioner Routing End

  //Admin Routing Start
  { path: 'welcome-admin', component: AdminDashboardComponent },
  { path: 'practitioner-list', component: AdminPractitionerListComponent, data: { title: 'Practitioners' } },
  { path: 'customer-list', component: AdminCustomerListComponent, data: { title: 'Users / Clients' } },
  { path: 'admin/stats', component: AdminStatsComponent, data: { title: 'Users / Clients' } },
  { path: 'interest-list', component: InterestListComponent },
  { path: 'addpractitioner', component: AddPractionerComponent },
  { path: 'manage-notification', component: ManageNotificationComponent, data: { title: 'Edit Notifications' }},
  { path: 'add-subscription-plan', component: AddSubscriptionPlanComponent },
  //Admin Routing end
  { path: 'test', component: TestComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
