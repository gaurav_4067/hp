import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-admin-stats',
  templateUrl: './admin-stats.component.html',
  styleUrls: ['./admin-stats.component.scss']
})
export class AdminStatsComponent implements OnInit {

  chatsData = [
    {
      data: [65, 59, 80, 81, 56, 55, 88],
      label: 'Series B',
      stack: 'a',
      barThickness: 18, barPercentage: 0.1,
      categoryPercentage: 0.1
    },
    {

      data: [30, 20, 0, 0, 0, 0, 0],
      label: 'Series A',
      stack: 'a',
      barThickness: 18, barPercentage: 0.1,
      categoryPercentage: 0.1
    },

  ];

  lineChartData = [];

  usersData = [];
  practitioners = [];
  stats: any;
  selectedSubs: any = 'This Week';
  isSubs: boolean = true;
  isSession: boolean = false;

  constructor(private apiService: ApiService, private loader: NgxSpinnerService,) { }

  ngOnInit(): void {
    this.getAdminStats();
  }

  getAdminStats() {
    this.loader.show();
    this.apiService.getAdminStats().subscribe((success: any) => {
      console.log('success', success);
      this.stats = success;
      let data = [];
      success.customer_data.users.last_month.forEach((item, index) => {
        data.push({
          earnings: item.users,
          month: 'week ' + (index + 1),
        });
      });
      success.practitioner_data.practitioners.last_month.forEach((item, index) => {
        this.practitioners.push({
          earnings: item.practitioner,
          month: 'week ' + (index + 1),
        });
      });
      console.log('data', data);
      this.usersData = data;
      // this.lineChartData.
      this.loader.hide();
    }, err => {
      console.log('err', err);
      this.loader.hide();
    })
  }

}
