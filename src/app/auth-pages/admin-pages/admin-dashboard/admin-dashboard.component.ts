import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { ApiService } from 'src/app/services/ApiService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddInterestComponent } from '../add-interest/add-interest.component';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AdminDashboardComponent implements OnInit {

  details: any = {};
  chatsData = [
    {
      data: [65, 59, 80, 81, 56, 55, 88],
      label: 'Series B',
      stack: 'a',
      barThickness: 18, barPercentage: 0.1,
      categoryPercentage: 0.1
    },
    {

      data: [30, 20, 0, 0, 0, 0, 0],
      label: 'Series A',
      stack: 'a',
      barThickness: 18, barPercentage: 0.1,
      categoryPercentage: 0.1
    },

  ];

  constructor(
    public apiService : ApiService,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    console.log('Admin Dashboard');
    this.getDashboardDetails();
  }

  getDashboardDetails() {
    this.apiService.getAdminDashboardDetails().subscribe((success) => {
      console.log('success', success);
      this.details = success;
    }, (err) => {
      console.log('err', err);
    })
  }

  math(val) {
    return Math.floor(val);
  }

  addnewInterest(){
    console.log('clicked');
    const modalRef = this.modalService.open(AddInterestComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modalRef.componentInstance.isEdit = false;
  }
}