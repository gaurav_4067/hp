import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-interest',
  templateUrl: './add-interest.component.html',
  styleUrls: ['./add-interest.component.scss']
})
export class AddInterestComponent implements OnInit {
  isEnableBtn = false;
  fileData;
  tags;
  title;
  currentStep = 'add';
  interestDetails;
  selectedImage;
  @Input() isEdit;
  @Input() selectedList;

  constructor(
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    public router: Router,
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    if(this.isEdit){
      this.currentStep = 'edit_confirmation';
      this.interestDetails = this.selectedList;
      this.title = this.interestDetails.interest;
      this.tags = this.interestDetails.description;
    }
  }

  addInterest() {
    const formData = new FormData();
    formData.append("interest", this.title);
    formData.append("description", this.tags);
    formData.append("cover_photo", this.fileData);
    if(this.fileData){
      this.loader.show();
      this.apiService.addInterest(formData)
      .subscribe((interest: any) => {
        console.log('interest', interest);
        this.loader.hide();
        this.interestDetails = interest;
        this.currentStep = 'add_success';
      }, (error) => {
        this.toastr.error(error['error']['interest']);
        this.loader.hide();
      });
    } else {
      this.toastr.error('Please add Interest Image');
    }
  }
  closeModal() {
    this.activeModal.close();
  }

  onFileSelected(event) {
    let file = event.target.files[0];
    this.fileData = file;
    if(event.target.files && event.target.files[0]){
      let reader = new FileReader();

      reader.onload = (e)=> {
        this.selectedImage = e.target.result;
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  onTags(val) {
    if (val) {
      this.isEnableBtn = true;
    } else {
      this.isEnableBtn = false;
    }
  }
  onTitle(val) {
    if (val) {
      this.isEnableBtn = true;
    } else {
      this.isEnableBtn = false;
    }
  }
  backToPlatform() {
    this.activeModal.close();
    this.router.navigate(['welcome-admin']);
  }
  saveInterest(){
    let formData;
    if(this.fileData){
      formData = new FormData();
      formData.append("interest", this.title);
      formData.append("description", this.tags);
      formData.append("cover_photo", this.fileData);
    } else{
      formData = {
        interest: this.title,
        description: this.tags
      }
    }
    this.loader.show();
    this.apiService.saveInterest(formData, this.interestDetails.id)
      .subscribe((interest: any) => {
        console.log('interest', interest);
        this.loader.hide();
        this.interestDetails = interest;
        this.currentStep = 'edit_success';
      }, (error) => {
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }
  editInterest(){
    this.currentStep = 'edit';
  }
}
