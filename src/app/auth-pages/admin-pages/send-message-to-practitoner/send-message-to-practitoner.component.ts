import { Component, OnInit, Input } from '@angular/core';
import { GeneralService } from '../../../services/GeneralService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ApiService } from '../../../services/ApiService';

@Component({
  selector: 'app-send-message-to-practitoner',
  templateUrl: './send-message-to-practitoner.component.html',
  styleUrls: ['./send-message-to-practitoner.component.scss']
})
export class SendMessageToPractitonerComponent implements OnInit {

  @Input() selectedList;
  title;
  message;
  isSubmitted = false;
  constructor(
    public generalService: GeneralService,
    public modalService: NgbModal,
    private loader: NgxSpinnerService,
    private toaster: ToastrService,
    private router: Router,
    public apiService: ApiService,
  ) { }

  ngOnInit(): void {
    console.log('selectedList', this.selectedList);

  }

  close() {
    this.modalService.dismissAll();
  }

  sendMessage() {
    this.isSubmitted = true;
    if (this.message && this.title) {
      this.loader.show();
      const payLoad = {
        title: this.title,
        message: this.message,
        name: this.selectedList?.user_id?.first_name + ' ' + this.selectedList?.user_id?.last_name,
        email: this.selectedList?.user_id.email,
        practitioner: this.selectedList?.user_id?.id
      };
      this.apiService.sendMsgToPractitioner(payLoad)
        .subscribe((msg:any) => {
          this.isSubmitted = false;
          this.loader.hide();
          this.toaster.success('Message send Successfully');
          this.modalService.dismissAll();
        }, (error) => {
          this.loader.hide();
          this.toaster.error(error.statusText);
        });
    }

  }

}
