import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendMessageToPractitonerComponent } from './send-message-to-practitoner.component';

describe('SendMessageToPractitonerComponent', () => {
  let component: SendMessageToPractitonerComponent;
  let fixture: ComponentFixture<SendMessageToPractitonerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendMessageToPractitonerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessageToPractitonerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
