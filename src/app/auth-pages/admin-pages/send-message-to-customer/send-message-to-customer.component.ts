import { Component, OnInit, ViewChild, TemplateRef, Input } from '@angular/core';
import { GeneralService } from '../../../services/GeneralService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ApiService } from '../../../services/ApiService';

@Component({
  selector: 'app-send-message-to-customer',
  templateUrl: './send-message-to-customer.component.html',
  styleUrls: ['./send-message-to-customer.component.scss']
})
export class SendMessageToCustomerComponent implements OnInit {

  @Input() selectedList;
  title;
  message;
  isSubmitted = false;
  constructor(
    public generalService: GeneralService,
    public modalService: NgbModal,
    private loader: NgxSpinnerService,
    private toaster: ToastrService,
    private router: Router,
    public apiService: ApiService,
  ) { }

  ngOnInit(): void {
    console.log('selectedList', this.selectedList);

  }

  close() {
    this.modalService.dismissAll();
  }

  sendMessage() {
    this.isSubmitted = true;
    if (this.message && this.title) {
      this.loader.show();
      const payLoad = {
        title: this.title,
        message: this.message,
        name: this.selectedList?.user_id__first_name + ' ' + this.selectedList?.user_id__last_name,
        email: this.selectedList?.user_id__email
      };
      this.apiService.sendMsgToCustomer(payLoad)
        .subscribe((msg:any) => {
          this.isSubmitted = false;
          this.loader.hide();
          this.toaster.success(msg?.response);
          this.modalService.dismissAll();
        }, (error) => {
          this.loader.hide();
          this.toaster.error(error.statusText);
        });
    }

  }

}
