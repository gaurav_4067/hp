import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendMessageToCustomerComponent } from './send-message-to-customer.component';

describe('SendMessageToCustomerComponent', () => {
  let component: SendMessageToCustomerComponent;
  let fixture: ComponentFixture<SendMessageToCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendMessageToCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessageToCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
