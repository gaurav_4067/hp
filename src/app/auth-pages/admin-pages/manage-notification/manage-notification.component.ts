import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/ApiService';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-notification',
  templateUrl: './manage-notification.component.html',
  styleUrls: ['./manage-notification.component.scss']
})
export class ManageNotificationComponent implements OnInit {

  notification = {
    title1: '',
    title2: '',
    title3: '',
    description1: '',
    description2: '',
    description3: '',
    frequency1: '',
    frequency2: '',
    frequency3: ''
  }

  frequencyData1 = ['Daily', 'Weekly', 'Off']
  frequencyData2 = ['Email', 'Phone', 'In Platform']
  frequencyData3 = ['24 hours', '1 hour', '15 min']


  frequencyConfig1 = {
    displayKey: 'name',
    search: false,
    height: 'auto',
    placeholder: 'Select Frequency',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }

  frequencyConfig2 = {
    displayKey: 'name',
    search: false,
    height: 'auto',
    placeholder: 'Select Frequency',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }

  frequencyConfig3 = {
    displayKey: 'name',
    search: false,
    height: 'auto',
    placeholder: 'Select Frequency',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }

  id: any;

  constructor(private apiService: ApiService, private toaster: ToastrService) { }

  ngOnInit(): void {
    this.getNotifications();
  }

  getNotifications() {
    this.apiService.getAdminNotification().subscribe((success: any) => {
      if (success) {
        this.id = success[0].id;
        success[0].notification.forEach((item, index) => {
          if (item.reminder_type) {
            this.notification[`title3`] = item.title;
            this.notification[`description3`] = item.description;
            this.notification.frequency3 = item.reminder_type;
          } else if (item.notification_type) {
            this.notification[`title2`] = item.title;
            this.notification[`description2`] = item.description;
            this.notification[`frequency2`] = item.notification_type;
          } else if (item.news_and_updates) {
            this.notification[`title1`] = item.title;
            this.notification[`description1`] = item.description;
            this.notification[`frequency1`] = item.news_and_updates;
          }
        })
      }
    }, (err) => {
      console.log(err);
    })
  }

  frequency1Changed(data: any) {

  }
  frequency2Changed(data: any) {

  }
  frequency3Changed(data: any) {

  }

  saveSettings() {
    const payload = {
      is_flag: false,
      notification: [{
        title: this.notification.title1,
        description: this.notification.description1,
        news_and_updates: this.notification.frequency1
      },
      {
        title: this.notification.title2,
        description: this.notification.description2,
        notification_type: this.notification.frequency2
      },
      {
        title: this.notification.title3,
        description: this.notification.description3,
        reminder_type: this.notification.frequency3
      }]
    }

    this.apiService.updateAdminNotification(this.id, payload).subscribe((success) => {
      console.log(success);
      this.toaster.success('Notification updated successfully!');
    }, err => {
      console.log('err');
      this.toaster.error(err.statusText);
    });
  }

}
