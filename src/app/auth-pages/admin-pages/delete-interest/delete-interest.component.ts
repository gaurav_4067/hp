import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete-interest',
  templateUrl: './delete-interest.component.html',
  styleUrls: ['./delete-interest.component.scss']
})
export class DeleteInterestComponent implements OnInit {

  isEnableBtn = false;
  fileData;
  tags;
  title;
  currentStep = 'delete_confirm';
  interestDetails;

  @Input() isEdit;
  @Input() selectedList;

  constructor(
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    public router: Router,
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    this.interestDetails = this.selectedList;
  }

 
  closeModal() {
    this.activeModal.close();
  }

 
  backToPlatform() {
    this.activeModal.close();
    this.router.navigate(['welcome-admin']);
  }
  deleteInterest(){
    this.loader.show();
    this.apiService.deleteInterest(this.interestDetails, this.interestDetails.id)
      .subscribe((interest: any) => {
        console.log('delete interest', interest);
        this.loader.hide();
        //this.interestDetails = interest;
        this.currentStep = 'delete_success';
      }, (error) => {
        this.loader.hide();
        this.toastr.error(error.statusText);
      });
  }

}
