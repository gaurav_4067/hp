import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApiService } from '../../../services/ApiService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeleteInterestComponent } from '../delete-interest/delete-interest.component';
import { AddInterestComponent } from '../add-interest/add-interest.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-interest-list',
  templateUrl: './interest-list.component.html',
  styleUrls: ['./interest-list.component.scss']
})
export class InterestListComponent implements OnInit, OnDestroy {
  expertises = [];
  selectedExpertises = [{ id: null }];
  selectedInterest;
  sub: Subscription;

  constructor(
    private service: ApiService,
    private router: Router,
    private ref: ElementRef,
    private modalService: NgbModal,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
  ) {
  }

  ngOnInit(): void {
    this.loader.show();
    this.sub = this.service.getInterest().subscribe((data: any) => {
      this.expertises = [...data];
      this.loader.hide();
      console.log({
        selectedExpertises: this.selectedExpertises,
        allExpertises: this.expertises
      });
    }, (error) => {
      this.toastr.error(error.statusText);
      this.loader.hide();
    });
  }

  isSelected(expId: string) {
    return this.selectedExpertises.some(exp => exp.id === expId);
  }

  toggleItem(expertise: any) {
    if (this.isSelected(expertise.id)) {
      this.selectedExpertises = this.selectedExpertises.filter(exp => exp.id !== expertise.id);
      return;
    }
    this.selectedExpertises.push(expertise);
  }

  toggleCls(id: any, display: boolean) {
    const card = this.ref.nativeElement.querySelector(`[id="chat-${id}"]`);

    card.classList[display ? 'add' : 'remove']('active');
  }

  showOptions(exp) {
    this.selectedInterest = exp;
  }
  edit() {
    let cols = document.getElementsByClassName('popover') as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < cols.length; i++) {
      cols[i].style.display = 'none';
    }
    const modalRef = this.modalService.open(AddInterestComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modalRef.componentInstance.selectedList = this.selectedInterest;
    modalRef.componentInstance.isEdit = true;
  }

  delete() {
    let cols = document.getElementsByClassName('popover') as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < cols.length; i++) {
      cols[i].style.display = 'none';
    }
    const modalRef = this.modalService.open(DeleteInterestComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modalRef.componentInstance.selectedList = this.selectedInterest;
  }

  addInterest() {
    const modalRef = this.modalService.open(AddInterestComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modalRef.componentInstance.isEdit = false;
  }

  ngOnDestroy() {
    this.sub?.unsubscribe();
  }

}
