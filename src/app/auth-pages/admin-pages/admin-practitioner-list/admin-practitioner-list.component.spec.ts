import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPractitionerListComponent } from './admin-practitioner-list.component';

describe('AdminPractitionerListComponent', () => {
  let component: AdminPractitionerListComponent;
  let fixture: ComponentFixture<AdminPractitionerListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminPractitionerListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPractitionerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
