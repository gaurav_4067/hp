import { Component, OnInit, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { GeneralService } from '../../../services/GeneralService';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SignupModalComponent } from 'src/app/public-pages/popup-modals/sign-up-modal/sign-up-modal.component';
import { Router } from '@angular/router';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { SendMessageToPractitonerComponent } from '../send-message-to-practitoner/send-message-to-practitoner.component';
import { FormGroup } from '@angular/forms';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';

@Component({
  selector: 'app-admin-practitioner-list',
  templateUrl: './admin-practitioner-list.component.html',
  styleUrls: ['./admin-practitioner-list.component.scss']
})

export class AdminPractitionerListComponent implements OnInit {
  @ViewChild('editModal') editModal: TemplateRef<any>;
  currentPage = 'profile';
  practitionerSessions;
  expertises:any = [];
  selectedExpertises = [];
  practitionerList;
  currentStatus = 'Active';
  selectedPractitoner;
  selectedPractitionerProfile;
  model: any;
  practitionerDownloadList: any = [];
  selectedPractitioner: any = [];
  selectedExpertise: any = [];
  userProfile: any;
  profileForm: FormGroup;
  userData: any;
  logedinData: any;
  isPractitionerList: any;
  countryConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select Country *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  stateConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select State *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  cityConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select City *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  countryData: any;
  stateData: any;
  cityData: any;
  isCityData: any;
  totalPage = [];
  currentPageNumber = 1;
  pageSize = 10;
  isDropup = true;

  constructor(
    private modalService: NgbModal,
    config: NgbCarouselConfig,
    ratingConfig: NgbRatingConfig,
    public generalService: GeneralService,
    private ref: ElementRef,
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    private toaster: ToastrService,
    private router : Router,
    private thirpApiService: ThirdPApiService
  ) {
    // customize default values of carousels used by this component tree
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;
    // customize default values of ratings used by this component tree
    ratingConfig.max = 5;
    ratingConfig.readonly = true;
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.getPractitionerList(this.currentStatus, this.currentPageNumber);
  }

  sendMessage(list){
    let modalRef = this.modalService.open(SendMessageToPractitonerComponent, { backdrop: false, windowClass: 'modal-send-msg', size: 'lg' });
    modalRef.componentInstance.selectedList = list;
  }

  pageChange(page){
    this.currentPageNumber = page;
    this.getPractitionerList(this.currentStatus, this.currentPageNumber);
  }

  pageSizeChange(size){
    this.pageSize = size;
    this.getPractitionerList(this.currentStatus, this.currentPageNumber);
  }

  getPractitionerList(type, currentPage) {
    this.loader.show();
    this.currentStatus = type === 'deactive' ? 'Deactivated' : (type === 'Blocked' ? 'Pending' : type);
    this.apiService.getPractitionerList(type.toLowerCase(), currentPage, this.pageSize)
      .subscribe((plist: any) => {
        this.practitionerList = plist?.response?.data;
        console.log('plist', plist);
        this.loader.hide();
      }, (error) => {
        this.loader.hide();
        this.toaster.error(error.statusText);
        console.log('plist error', error);
      });
  }

  getAddress(addr) {
    let address = '';
    if (addr.address_line3) {
      address = address + ' ' + addr.address_line3;
    }
    if (addr.address_line4) {
      address = address + ' ' + addr.address_line4;
    }
    if (addr.address_line5) {
      address = address + ' ' + addr.address_line5;
    }
    return address.trim();
  }
  getExpertise(expertise){
    expertise.forEach(element => {
      element
    });
  }

  addPractitioner() {
    this.router.navigate(["/addpractitioner"]);
  }
  openModal(list) {
    this.selectedPractitoner = list;
    console.log('called', list);
    this.loader.show();
    this.apiService.getPractitionerProfileDetails(list.id)
    .subscribe((details:any)=>{
      console.log('details', details);
      this.selectedPractitionerProfile = details;
      this.isPractitionerList = details.profile[0];
      this.userProfile = {
        firstname: `${this.isPractitionerList.user_id__first_name ? this.isPractitionerList.user_id__first_name : ""}`,
        lastname: `${this.isPractitionerList.user_id__last_name ? this.isPractitionerList.user_id__last_name : ""}`,
        businessname: this.isPractitionerList.name ? this.isPractitionerList.name : "",
        email: this.isPractitionerList.user_id__email ? this.isPractitionerList.user_id__email : "",
        phonenumber: this.isPractitionerList.phone_number ? this.isPractitionerList.phone_number : "",
        country: this.isPractitionerList.address__address_line5 ? this.isPractitionerList.address__address_line5 : "",
        state: this.isPractitionerList.address__addrees_line4 ? this.isPractitionerList.address__addrees_line4 : "",
        city: this.isPractitionerList.address__address_line3 ? this.isPractitionerList.address__address_line3 : "",
        postalcode: this.isPractitionerList.address__zipcode ? this.isPractitionerList.address__zipcode : "",
      };
      this.loadCountry();
      this.loader.hide();
      this.modalService.open(this.editModal, { windowClass: 'right edit-practitioner', scrollable:true });
    }, (error)=>{
      this.toaster.error(error.statusText);
      this.loader.hide();
    });
  }
  loadCountry() {
    this.loader.show();
    this.thirpApiService
      .getCountries("id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.countryData = response;
        if (this.isPractitionerList.address__address_line5) {
          let result = this.countryData.find(obj => obj.name === this.isPractitionerList.address__address_line5)
          this.ifloadState(result["id"]);
        }
        this.loader.hide();
      });
  }
  ifloadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.stateData = response;
        if (this.isPractitionerList.address__addrees_line4) {
          let result = this.stateData.find(obj => obj.name === this.isPractitionerList.address__addrees_line4)
          this.ifloadCity(result["id"]);
        }
        this.loader.hide();
      });
  }
  ifloadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.cityData = response;
        response.forEach(element => {
          if (element.name === this.isPractitionerList.address__address_line3) {
            this.isCityData = element;
          }
        });
        this.loader.hide();
      });
  }
  loadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.stateData = response;
        this.loader.hide();
      });
  }
  loadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.cityData = response;
        this.loader.hide();
      });
  }
  //on changes events
  countryChanged(data: any) {
    console.log(data);
    if (data) {
      this.stateData = [];
      this.cityData = [];
      // this.user.state = [];
      // this.user.city = [];
      this.loadState(data.id);
    } else {
      this.stateData = [];
      this.cityData = [];
      // this.user.state = [];
      // this.user.city = [];
    }
  }
  stateChanged(data: any) {
    console.log(data);
    if (data) {
      this.cityData = [];
      // this.user.city = [];
      this.loadCity(data.id);
    } else {
      this.cityData = [];
      // this.user.city = [];
    }
  }
  cityChanged(data: any) {
    console.log(data);
    this.isCityData = data;
  }
  showListPage(page) {
    this.currentPage = page;
    this.loader.show();
    if (page === 'interest-list') {
      this.apiService.getInterest()
        .subscribe((interest: any) => {
          this.expertises = [...interest];
          let tempData = [];
          this.isPractitionerList["expertise"].forEach(element => {
            if (element) {
              let result = this.expertises.filter(obj => obj.interest === element);
              if (result.length) {
                tempData.push(result[0]);
              }
            }
          });
          this.isPractitionerList["expertise"] = tempData;
          this.selectedExpertise = tempData;
          this.loader.hide();
        }, (error)=>{
          this.toaster.error(error.statusText);
          this.loader.hide();
        });
    } else {
      this.apiService.getPractitionerSessionList(this.selectedPractitoner.id)
      .subscribe((session: any) => {
        this.practitionerSessions = session.session;
        this.loader.hide();
      }, (error)=>{
        this.toaster.error(error.statusText);
        this.loader.hide();
      });
    }
  }
  goBack() {
    this.currentPage = 'profile';
  }
  isSelected(expId: string) {
    return this.selectedExpertises.some(exp => exp.id === expId);
  }

  toggleItem(expertise: any) {
    if (this.isSelected(expertise.id)) {
      this.selectedExpertises = this.selectedExpertises.filter(exp => exp.id !== expertise.id);
      return;
    }
    this.selectedExpertises.push(expertise);
  }

  toggleCls(id: any, display: boolean) {
    const card = this.ref.nativeElement.querySelector(`[id="chat-${id}"]`);

    card.classList[display ? 'add' : 'remove']('active');
  }

  changeUserStatus(list, type) {
    console.log('type', type);
    this.loader.show();
    const data = {
      id: list.id,
      "is_block": type === 'block',
      "is_activate": type === 'active',
      "is_delete": type === 'delete'
    }
    console.log('data', data);
    this.apiService.changeUserStatus(list.auth_id, data).subscribe((success) => {
      console.log('success', success);
      this.toaster.success('Practitioner Status changed successfully!');
      this.loader.hide();
      this.getPractitionerList(this.currentStatus, this.currentPageNumber);
    }, (err) => {
      this.toaster.error(err.statusText);
      this.loader.hide();
      console.log('err', err);
    })
  }
  closeModal(){
    this.modalService.dismissAll();
  }
  filterIfThere(list: any) {
    if (this.isPractitionerList["expertise"].find(obj => obj.interest == list.interest)) {
      return true;
    } else {
      return false;
    }
  }
  chooseMe(data: any) {
    //
    let refContent = document.getElementById(`${data.id}`) as HTMLInputElement;
    if (refContent.className == "card expertise-card active" || refContent.className == "card expertise-card active ng-star-inserted") {
      refContent.className = "card expertise-card";
      let removed;
      if (this.selectedExpertise.find(obj => obj.interest == data.interest)) {
        removed = this.selectedExpertise.filter(obj => obj.interest != data.interest);
        this.selectedExpertise = removed;
      }
    } else {
      refContent.className = "card expertise-card active";
      if (this.selectedExpertise.length) {
        if (!this.selectedExpertise.find(obj => obj.interest == data.interest)) {
          this.selectedExpertise.push(data);
        }
      } else {
        this.selectedExpertise.push(data);
      }
      console.log(this.selectedExpertise);
    }
  }
  practSubmit() {
    let payload = {
      "id": this.isPractitionerList["id"],
      "user_id":{
        "id": this.isPractitionerList["user_id"],
        "first_name": this.userProfile["firstname"],
        "last_name": this.userProfile["lastname"],
        "email": this.userProfile["email"],
      },
      "address": {
        "address_line1": null,
        "address_line2": "N/A",
        "address_line3": this.isPractitionerList["address__address_line5"],
        "addrees_line4": this.isPractitionerList["address__address_line4"],
        "address_line5": this.isPractitionerList["address__address_line3"],
        "zipcode": this.userProfile["postalcode"],
        "lattitude": null,
        "longitude": null,
        "location": null
      },
      "name": this.userProfile["businessname"],
      "phone_number": this.userProfile["phonenumber"],
      "expertise": this.isPractitionerList["expertise"],
    };
    if (this.userProfile.country && this.userProfile.country["name"]) {
      payload["address"]["address_line5"] = this.userProfile.country["name"];
    } else {
      payload["address"]["address_line5"] = this.userProfile.country;
    }
    if (this.userProfile.state && this.userProfile.state["name"]) {
      payload["address"]["addrees_line4"] = this.userProfile.state["name"];
    } else {
      payload["address"]["addrees_line4"] = this.userProfile.state;
    }
    if (this.userProfile.city && this.userProfile.city["name"]) {
      payload["address"]["address_line3"] = this.userProfile.city["name"];
    } else {
      payload["address"]["address_line3"] = this.userProfile.city;
    }
    console.log(payload);
    //return;
    this.loader.show();
    this.apiService
      .updatePractitionerProfileAdmin(payload, payload["id"])
      .subscribe((response: any) => {
        this.toaster.success("Practitioner updated successfully.");
        this.loader.hide();
      }, (error) => {
        this.toaster.error(error.statusText);
        this.loader.hide();
      });
  }
  expertiseSubmit() {
    let payload = {
      "id": this.isPractitionerList["id"],
      "user_id":{
        "id": this.isPractitionerList["user_id"],
        "first_name": this.isPractitionerList["user_id__first_name"],
        "last_name": this.isPractitionerList["user_id__last_name"],
        "email": this.isPractitionerList["user_id__email"],
      },
      "address": {
        "address_line1": null,
        "address_line2": "N/A",
        "address_line3": this.isPractitionerList["address__address_line5"],
        "addrees_line4": this.isPractitionerList["address__address_line4"],
        "address_line5": this.isPractitionerList["address__address_line3"],
        "zipcode": this.userProfile["postalcode"],
        "lattitude": null,
        "longitude": null,
        "location": null
      },
      "name": this.userProfile["name"],
      "phone_number": this.userProfile["phonenumber"],
      "expertise": this.isPractitionerList["expertise"],
    };
    if (this.selectedExpertise.length) {
      let expertiseFormat = [];
      this.selectedExpertise.forEach(element => {
        expertiseFormat.push(element.interest);
      });
      payload["expertise"] = expertiseFormat;
    }
    console.log(payload);
    //return;
    this.loader.show();
    this.apiService
      .updatePractitionerProfileAdmin(payload, payload["id"])
      .subscribe((response: any) => {
        this.toaster.success("Expertise updated successfully.");
        this.loader.hide();
      }, (error) => {
        this.toaster.error(error.statusText);
        this.loader.hide();
      });
  }
  downloadCSV() {
    if(this.practitionerList.length){
      this.loader.show();
      this.apiService.getPractitionerDownloadList(this.currentStatus.toLowerCase(), 1, "all")
      .subscribe((plist: any) => {
        this.practitionerDownloadList = plist?.response;
        if(this.practitionerDownloadList.length){
          var bodyContent: any = [];
          var fileNanme;
          var currentdate = new Date(); 
          var datetime = "Last Sync: " + currentdate.getDate() + "/"
            + (currentdate.getMonth()+1)  + "/" 
            + currentdate.getFullYear() + " @ "  
            + currentdate.getHours() + ":"  
            + currentdate.getMinutes() + ":" 
            + currentdate.getSeconds();
          if(this.currentStatus == "Active"){
            fileNanme = `Practitioner_Confirmed_Accounts ${datetime}`;
          }else if(this.currentStatus == "Pending"){
            fileNanme = `Practitioner_Waiting_Approval ${datetime}`;
          }else if(this.currentStatus == "Deactivated"){
            fileNanme = `Practitioner_Deactivated ${datetime}`;
          }else{
            fileNanme = `PractitionerList ${datetime}`;
          }
          for (var i = 0; i < this.practitionerDownloadList.length; i++) {
            let firstname = this.practitionerDownloadList[i].user_id.first_name ? this.practitionerDownloadList[i].user_id.first_name : "";
            let lastname = this.practitionerDownloadList[i].user_id.last_name ? this.practitionerDownloadList[i].user_id.last_name : "";
            bodyContent.push({
              name: `${firstname} ${lastname}`,
              location: this.practitionerDownloadList[i].address ? this.getAddress(this.practitionerDownloadList[i].address) : "",
              expertise: this.practitionerDownloadList[i].expertise.length ? this.practitionerDownloadList[i].expertise.join() : "",
              billing: "",
              certificationName: this.practitionerDownloadList[i].certitifcations.length ? this.practitionerDownloadList[i].certitifcations[0].certitifcation_title ? this.practitionerDownloadList[i].certitifcations[0].certitifcation_title : "" : "",
              certificationFile: this.practitionerDownloadList[i].certitifcations.length ? this.practitionerDownloadList[i].certitifcations[0].certitifcation : "",
              status: this.currentStatus,
            });
          }
          var titleContent = {
            title: "Practitioner Report",
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: true,
            useBom: true,
            headers: [
              'Practitioner Name',
              'Location',
              'Practitioner’s Expertise',
              'Percentage Billing',
              'Certification Name',
              'Certificate File',
              'Status'
            ],
            nullToEmptyString: true,
          };
          new Angular5Csv(bodyContent, fileNanme, titleContent);
        }
        console.log('plist', plist);
        this.loader.hide();
      }, (error) => {
        this.loader.hide();
        this.toaster.error(error.statusText);
        console.log('plist error', error);
      });
    }
  }

  resendActivationLink(list){
    let payload = {
      "email": list.user_id.email
      }
    this.loader.show();
    this.apiService
      .resendActivationMailToPractitioner(payload)
      .subscribe((response: any) => {
        this.toaster.success(response.detail);
        this.loader.hide();
      }, (error) => {
        this.toaster.error(error.statusText);
        this.loader.hide();
      });
  }
}
