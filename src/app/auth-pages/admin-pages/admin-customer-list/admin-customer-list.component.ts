
import { Component, OnInit, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { ApiService } from '../../../services/ApiService';
import { GeneralService } from '../../../services/GeneralService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { SubscriptionPlansService } from '../../../services/SubscriptionPlansService';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { FormGroup } from '@angular/forms';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { SendMessageToCustomerComponent } from '../send-message-to-customer/send-message-to-customer.component';

@Component({
  selector: 'app-admin-customer-list',
  templateUrl: './admin-customer-list.component.html',
  styleUrls: ['./admin-customer-list.component.scss']
})
export class AdminCustomerListComponent implements OnInit {

  @ViewChild('editModal') editModal: TemplateRef<any>;

  currentStatus = 'Active';
  customerList: any = [];
  selectedCustomer: any;
  interests: any;
  subscriptionDetails: any;
  selectedExpertises = [];
  currentPage = 'profile';
  selectedCustomerProfile;
  subscriptionId;
  selectedItem: string = '0';
  showPackageItems: string = 'Monthly Pricing';
  PlanData: any;
  plan_input = "Monthly Pricing";
  customerName = JSON.parse(localStorage.getItem("user_data"));
  MonthlyData = [];
  ThreeMonthData = [];
  SixMonthData = [];
  AnnualData = [];
  selectedPlan;
  planDetails;

  userProfile = {
    first_name: "",
    last_name: "",
    email: "",
    phone_number: "",
    country_name: "",
    state_name: "",
    city_name: "",
    zipcode: "",
  };
  userData: any;
  logedinData: any;
  profileForm: FormGroup;
  countryConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select Country *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  stateConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select State *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  cityConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select City *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  countryData: any;
  stateData: any;
  cityData: any;
  selectedInterest: any = [];
  isCityData: any;
  customerDownloadList: any = [];
  totalPage = [];
  currentPageNumber = 1;
  pageSize = 10;
  isDropup = true;

  constructor(private apiService: ApiService,
    public generalService: GeneralService,
    public modalService: NgbModal,
    private loader: NgxSpinnerService,
    private ref: ElementRef,
    private plansservice: SubscriptionPlansService,
    private toaster: ToastrService,
    private router: Router,
    private thirpApiService: ThirdPApiService,
    private elementRef: ElementRef
  ) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.getCustomerList(this.currentStatus, this.currentPageNumber);
  }

  sendMessage(list){
    let modalRef = this.modalService.open(SendMessageToCustomerComponent, { backdrop: false, windowClass: 'modal-send-msg', size: 'lg' });
    modalRef.componentInstance.selectedList = list;
  }

  pageChange(page){
    this.currentPageNumber = page;
    this.getCustomerList(this.currentStatus, this.currentPageNumber);
  }

  pageSizeChange(size){
    this.pageSize = size;
    this.getCustomerList(this.currentStatus, this.currentPageNumber);
  }

  getCustomerList(type, currentPage) {
    this.currentStatus = type;
    this.loader.show();
    this.apiService.getCustomerList(type.toLowerCase(), currentPage, this.pageSize)
      .subscribe((clist: any) => {
        this.customerList = clist?.response?.data;
        console.log('clist', clist);
        this.totalPage = [];
        for(let page=1;page<= clist?.response?.total;page++){
          this.totalPage.push(page);
        }
        this.loader.hide();
      }, (error) => {
        console.log('clist error', error);
        this.loader.hide();
        this.toaster.error(error.statusText);
      });
  }

  changeUserStatus(list, type) {
    console.log('type', type);
    const data = {
      id: list.id,
      "is_block": type === 'block',
      "is_activate": type === 'active',
      "is_delete": type === 'delete'
    }
    console.log('data', data);
    this.loader.show();
    this.apiService.changeUserStatus(list.auth_id, data).subscribe((success) => {
      console.log('success', success);
      this.toaster.success('Customer Status changed successfully!');
      this.loader.hide();
      this.getCustomerList(this.currentStatus, this.currentPageNumber);
    }, (err) => {
      this.loader.hide();
      console.log('err', err);
      this.toaster.error(err.statusText);
    })
  }
  getIntetests() {
    this.loader.show();
    this.apiService.getInterest()
      .subscribe((interest: any) => {
        this.interests = [...interest];
        let tempData = [];
        this.selectedCustomer["interest"].forEach(element => {
          if (element) {
            let result = this.interests.filter(obj => obj.interest === element);
            if (result.length) {
              tempData.push(result[0]);
            }
          }
        });
        this.selectedCustomer["interest"] = tempData;
        this.selectedInterest = tempData;
        this.loader.hide();
      }, (error) => {
        this.toaster.error(error.statusText);
        this.loader.hide();
      });
  }
  filterIfThere(list: any) {
    if (this.selectedCustomer["interest"].find(obj => obj.interest == list.interest)) {
      return true;
    } else {
      return false;
    }
  }
  chooseMe(data: any) {
    //
    let refContent = document.getElementById(`${data.id}`) as HTMLInputElement;
    if (refContent.className == "card expertise-card active" || refContent.className == "card expertise-card active ng-star-inserted") {
      refContent.className = "card expertise-card";
      let removed;
      if (this.selectedInterest.find(obj => obj.interest == data.interest)) {
        removed = this.selectedInterest.filter(obj => obj.interest != data.interest);
        this.selectedInterest = removed;
      }
    } else {
      refContent.className = "card expertise-card active";
      if (this.selectedInterest.length) {
        if (!this.selectedInterest.find(obj => obj.interest == data.interest)) {
          this.selectedInterest.push(data);
        }
      } else {
        this.selectedInterest.push(data);
      }
      console.log(this.selectedInterest);
    }
  }
  interestSubmit() {
    let payload = this.selectedCustomer;
    payload["user_id__first_name"] = this.userProfile.first_name;
    payload["user_id__last_name"] = this.userProfile.last_name;
    payload["user_id__email"] = this.userProfile.email;
    payload["phone_number"] = this.userProfile.phone_number;
    if (this.userProfile.country_name && this.userProfile.country_name["name"]) {
      payload["address__city__country__name "] = this.userProfile.country_name["name"];
    } else {
      payload["address__city__country__name "] = this.userProfile.country_name;
    }
    if (this.userProfile.state_name && this.userProfile.state_name["name"]) {
      payload["address__city__state__name"] = this.userProfile.state_name["name"];
    } else {
      payload["address__city__state__name"] = this.userProfile.state_name;
    }
    if (this.userProfile.city_name && this.userProfile.city_name["name"]) {
      payload["address__city__name"] = this.userProfile.city_name["name"];
    } else {
      payload["address__city__name"] = this.userProfile.city_name;
    }
    payload["address__zipcode"] = this.userProfile.zipcode;
    let request = {
      "id": this.selectedCustomer["id"],
      "user_id": {
        "id": this.logedinData["id"],
        "first_name": payload["user_id__first_name"],
        "last_name": payload["user_id__last_name"],
        "email": payload["user_id__email"],
      },
      "address": {
        "address_line1": null,
        "address_line2": "N/A",
        "address_line3": payload["address__city__country__name"],
        "addrees_line4": payload["address__city__state__name"],
        "address_line5": payload["address__city__name"],
        "zipcode": payload["address__zipcode"],
        "address_type": null,
        "city": this.isCityData ? this.isCityData["id"] : null,
      },
      "interest": payload["interest"],
    };
    if (this.selectedInterest.length) {
      let interestFormat = [];
      this.selectedInterest.forEach(element => {
        interestFormat.push(element.interest);
      });
      request["interest"] = interestFormat;
    } else {
      request["interest"] = [];
    }
    console.log(request);
    //return;
    this.loader.show();
    this.apiService
      .updateCustomerProfileAdmin(request, request["id"])
      .subscribe((response: any) => {
        this.toaster.success("Interest updated successfully.");
        this.loader.hide();
      }, (error) => {
        this.toaster.error(error.statusText);
        this.loader.hide();
      });
  }
  profileSubmit() {
    let payload = this.selectedCustomer;
    payload["user_id__first_name"] = this.userProfile.first_name;
    payload["user_id__last_name"] = this.userProfile.last_name;
    payload["user_id__email"] = this.userProfile.email;
    payload["phone_number"] = this.userProfile.phone_number;
    if (this.userProfile.country_name && this.userProfile.country_name["name"]) {
      payload["address__city__country__name "] = this.userProfile.country_name["name"];
    } else {
      payload["address__city__country__name "] = this.userProfile.country_name;
    }
    if (this.userProfile.state_name && this.userProfile.state_name["name"]) {
      payload["address__city__state__name"] = this.userProfile.state_name["name"];
    } else {
      payload["address__city__state__name"] = this.userProfile.state_name;
    }
    if (this.userProfile.city_name && this.userProfile.city_name["name"]) {
      payload["address__city__name"] = this.userProfile.city_name["name"];
    } else {
      payload["address__city__name"] = this.userProfile.city_name;
    }
    payload["address__zipcode"] = this.userProfile.zipcode;
    if (this.selectedInterest.length) {
      let interestFormat = [];
      this.selectedInterest.forEach(element => {
        interestFormat.push(element.interest);
      });
      payload["interest"] = interestFormat;
    } else {
      payload["interest"] = [];
    }
    let request = {
      "id": this.selectedCustomer["id"],
      "user_id": {
        "id": this.logedinData["id"],
        "first_name": payload["user_id__first_name"],
        "last_name": payload["user_id__last_name"],
        "email": payload["user_id__email"],
      },
      "address": {
        "address_line1": null,
        "address_line2": "N/A",
        "address_line3": payload["address__city__country__name"],
        "addrees_line4": payload["address__city__state__name"],
        "address_line5": payload["address__city__name"],
        "zipcode": payload["address__zipcode"],
        "address_type": null,
        "city": this.isCityData ? this.isCityData["id"] : null,
      },
      "interest": payload["interest"],
      "phone_number": payload["phone_number"],
    };
    console.log(request);
    //return;
    this.loader.show();
    this.apiService
      .updateCustomerProfileAdmin(request, request["id"])
      .subscribe((response: any) => {
        this.toaster.success("Customer updated successfully.");
        this.loader.hide();
      }, (error) => {
        this.toaster.error(error.statusText);
        this.loader.hide();
      });
  }
  openModal(list) {
    this.currentPage = 'profile';
    this.apiService.getAdminCustomerDetails(list.id).subscribe((success: any) => {
      console.log('success', success);
      this.selectedCustomer = success.customer_details[0];
      this.loadCountry();
      this.userProfile = {
        first_name: this.selectedCustomer.user_id__first_name ? this.selectedCustomer.user_id__first_name : "",
        last_name: this.selectedCustomer.user_id__last_name ? this.selectedCustomer.user_id__last_name : "",
        email: this.selectedCustomer.user_id__email ? this.selectedCustomer.user_id__email : "",
        phone_number: this.selectedCustomer.phone_number ? this.selectedCustomer.phone_number : "",
        country_name: this.selectedCustomer.address__city__country__name ? this.selectedCustomer.address__city__country__name : "",
        state_name: this.selectedCustomer.address__city__state__name ? this.selectedCustomer.address__city__state__name : "",
        city_name: this.selectedCustomer.address__city__name ? this.selectedCustomer.address__city__name : "",
        zipcode: this.selectedCustomer.address__zipcode ? this.selectedCustomer.address__zipcode : "",
      };
      this.getIntetests();
      if (success.subscriptions) {
        this.subscriptionDetails = success.subscriptions && success.subscriptions.plan && success.subscriptions.plan[0];
      }
      if(success && success.subscription_id){
        this.subscriptionId = success.subscription_id;
      }
      console.log('subscriptionDetails', this.subscriptionDetails);
      this.modalService.open(this.editModal, { windowClass: 'right' });
      this.loader.hide();
    }, err => {
      console.log('err', err);
      this.loader.hide();
      this.toaster.error(err.statusText);
    });
    console.log('called', list);
  }
  addCustomer() {
    console.log("addcustomer")
    this.router.navigate(["/addcustomer"]);
  }
  showListPage(page) {
    this.currentPage = page;
    console.log('subscriptionDetails', this.subscriptionDetails);
  }
  goBack() {
    this.currentPage = 'profile';
  }
  isSelected(expId: string) {
    return this.selectedExpertises.some(exp => exp.id === expId);
  }

  toggleItem(expertise: any) {
    if (this.isSelected(expertise.id)) {
      this.selectedExpertises = this.selectedExpertises.filter(exp => exp.id !== expertise.id);
      return;
    }
    this.selectedExpertises.push(expertise);
  }

  toggleCls(id: any, display: boolean) {
    const card = this.ref.nativeElement.querySelector(`[id="chat-${id}"]`);

    card.classList[display ? 'add' : 'remove']('active');
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  getPlanData(plan_input) {
    this.loader.show();
    this.plansservice.getsubscriptionplans(plan_input).subscribe(
      response => {
        this.PlanData = response;
        if (plan_input == '3 Months') {
          this.ThreeMonthData = this.PlanData.plan;
        }
        if (plan_input == '6 Months') {
          this.SixMonthData = this.PlanData.plan;
        }
        if (plan_input == 'Monthly Pricing') {
          this.MonthlyData = this.PlanData.plan;
        }
        if (plan_input == 'Annually') {
          this.AnnualData = this.PlanData.plan;
        }
        this.loader.hide();
      },
      error => {
        console.log(error);
        this.loader.hide();
        this.toaster.error(error.statusText);
      }
    );
  }

  ChooseClick(data, planid) {
    this.selectedItem = data;
    this.selectedPlan = planid;
    const payLoad = {
      offer_id: null,
      plan_id: planid.id,
      unsubscribe_date: null,
      reason: '',
      customer: this.selectedCustomer.id,
      parent_id: null
    };
    this.loader.show();
    if (this.subscriptionDetails) {
      this.plansservice.editSubscriptionAdminCustomer(payLoad, this.subscriptionId)
        .subscribe((sub) => {
          this.loader.hide();
          this.toaster.success('Subscription Changed Successfully');
        }, (error) => {
          this.loader.hide();
          this.toaster.error(error.statusText);
        });
    } else {
      this.plansservice.addSubscriptionAdminCustomer(payLoad)
        .subscribe((sub) => {
          this.toaster.success('Subscription Added Successfully');
          this.loader.hide();
        }, (error) => {
          this.loader.hide();
          this.toaster.error(error.statusText);
        });
    }
    console.log('planid', planid, data);
  }

  ShowPackage(data, apiInput) {
    this.selectedItem = '0';
    this.showPackageItems = data;
    this.getPlanData(apiInput);

  }

  chooseNewPlan() {
    this.getPlanData(this.plan_input);
    this.currentPage = 'plan-list';
  }

  getAddress(addr) {
    let address = '';
    if (addr.address__city__name) {
      address = address + ' ' + addr.address__city__name;
    }
    if (addr.address__city__state__name) {
      address = address + ' ' + addr.address__city__state__name;
    }
    if (addr.address__city__country__name) {
      address = address + ' ' + addr.address__city__country__name;
    }
    return address.trim();
  }
  loadCountry() {
    this.loader.show();
    this.thirpApiService
      .getCountries("id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.countryData = response;
        if (this.selectedCustomer.address__city__country__name) {
          let result = this.countryData.find(obj => obj.name === this.selectedCustomer.address__city__country__name)
          this.ifloadState(result["id"]);
        }
        this.loader.hide();
      });
  }
  ifloadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.stateData = response;
        if (this.selectedCustomer.address__city__state__name) {
          let result = this.stateData.find(obj => obj.name === this.selectedCustomer.address__city__state__name)
          this.ifloadCity(result["id"]);
        }
        this.loader.hide();
      });
  }
  ifloadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.cityData = response;
        response.forEach(element => {
          if (element.name === this.selectedCustomer.address__city__name) {
            this.isCityData = element;
          }
        });
        this.loader.hide();
      });
  }
  loadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.stateData = response;
        this.loader.hide();
      });
  }
  loadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.cityData = response;
        this.loader.hide();
      });
  }
  //on changes events
  countryChanged(data: any) {
    console.log(data);
    if (data) {
      this.stateData = [];
      this.cityData = [];
      // this.user.state = [];
      // this.user.city = [];
      this.loadState(data.id);
    } else {
      this.stateData = [];
      this.cityData = [];
      // this.user.state = [];
      // this.user.city = [];
    }
  }
  stateChanged(data: any) {
    console.log(data);
    if (data) {
      this.cityData = [];
      // this.user.city = [];
      this.loadCity(data.id);
    } else {
      this.cityData = [];
      // this.user.city = [];
    }
  }
  cityChanged(data: any) {
    console.log(data);
    this.isCityData = data;
  }
  downloadCSV() {
    if(this.customerList.length){
      this.loader.show();
      this.apiService.getCustomerDownloadList(this.currentStatus.toLowerCase(), 1, "all")
      .subscribe((clist: any) => {
        this.customerDownloadList = clist?.response;
        if(this.customerDownloadList.length){
          var bodyContent: any = [];
          var fileNanme;
          var currentdate = new Date(); 
          var datetime = "Last Sync: " + currentdate.getDate() + "/"
            + (currentdate.getMonth()+1)  + "/" 
            + currentdate.getFullYear() + " @ "  
            + currentdate.getHours() + ":"  
            + currentdate.getMinutes() + ":" 
            + currentdate.getSeconds();
          if(this.currentStatus == "Active"){
            fileNanme = `Customer_Active_Accounts ${datetime}`;
          }else if(this.currentStatus == "Blocked"){
            fileNanme = `Customer_Blocked_Accounts ${datetime}`;
          }else if(this.currentStatus == "Deleted"){
            fileNanme = `Customer_Deleted_Accounts ${datetime}`;
          }else{
            fileNanme = `customerList ${datetime}`;
          }
          for (var i = 0; i < this.customerDownloadList.length; i++) {
            let firstname = this.customerDownloadList[i].user_id__first_name ? this.customerDownloadList[i].user_id__first_name : "";
            let lastname = this.customerDownloadList[i].user_id__last_name ? this.customerDownloadList[i].user_id__last_name : "";
            var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            bodyContent.push({
              name: `${firstname} ${lastname}`,
              chatsessioncount: `228/${this.customerDownloadList[i].session_count}`,
              location: this.customerDownloadList[i].address__city__country__name ? this.getAddress(this.customerDownloadList[i]) : "",
              interest: this.customerDownloadList[i].interest.length ? this.customerDownloadList[i].interest  .join() : "",
              signupdate: this.customerDownloadList[i].created.toLocaleString("en-US", options),
              email: this.customerDownloadList[i].user_id__email ? this.customerDownloadList[i].user_id__email : "",
              status: this.currentStatus,
            });
          }
          var titleContent = {
            title: "Customer Report",
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: true,
            useBom: true,
            headers: [
              'Customer Name',
              'Chat Count/Session Count',
              'Location',
              'Customer’s Interest',
              'Sign Up Date',
              'E-mail Address',
              'Status'
            ],
            nullToEmptyString: true,
          };
          new Angular5Csv(bodyContent, fileNanme, titleContent);
        }
        this.loader.hide();
      }, (error) => {
        console.log('clist error', error);
        this.loader.hide();
        this.toaster.error(error.statusText);
      });
    }
  }

  ngOndestroy() {
    this.elementRef.nativeElement.remove();
  }
}
