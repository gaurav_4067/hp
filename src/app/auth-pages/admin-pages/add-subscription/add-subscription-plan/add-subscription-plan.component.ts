import { Component, OnInit } from '@angular/core';
import { SubscriptionPlansService } from '../../../../services/SubscriptionPlansService';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { GeneralService } from '../../../../services/GeneralService';
@Component({
  selector: 'app-add-subscription-plan',
  templateUrl: './add-subscription-plan.component.html',
  styleUrls: ['./add-subscription-plan.component.scss']
})
export class AddSubscriptionPlanComponent implements OnInit {

  showPlanList = false;
  showPackageItems: string = 'Monthly Pricing';
  MonthlyData = [];
  ThreeMonthData = [];
  SixMonthData = [];
  AnnualData = [];
  PlanData: any;
  plan_input = "Monthly Pricing";
  selectedItem: string = 'null';
  selectedPlan;
  planDetails;
  isPlanEdit = false;
  showPlanCreatePage = false;
  includedService = [1];
  selectedBillingPeriod = 'Billing';
  defaultBillingPeriods;
  plan = {
    "plan_name": "",

    "sale_price": "",

    "regular_price": "",

    "number_of_session": 0,

    "currency": "dollars",

    "display_regular_price": false,

    "start_date": null,

    "end_date": null,

    "plan_description": [''],

    "user": "",

    "plan_type": "",

    "offer": null
  };
  selectedPlanDes = null;
  selectedPlanDescIndex = null;

  constructor(
    private plansservice: SubscriptionPlansService,
    private toaster: ToastrService,
    private router: Router,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
  ) {
    this.generalService.planAndDiscountSubject.subscribe((data)=>{
      if(data.canReload){
        this.showPlanList = false;
        this.isPlanEdit = false;
        this.showPlanCreatePage = false;
        this.showPackageItems = 'Monthly Pricing';
        this.MonthlyData = [];
        this.ThreeMonthData = [];
        this.SixMonthData = [];
        this.AnnualData = [];
        this.PlanData = null;
        this.plan_input = "Monthly Pricing";
        this.selectedItem = 'null';
        this.selectedPlan = null;
        this.planDetails = null;
        this.includedService = [1];
        this.selectedBillingPeriod = 'Billing';
        this.plan = {
        "plan_name": "",

        "sale_price": "",

        "regular_price": "",

        "number_of_session": 0,

        "currency": "dollars",

        "display_regular_price": false,

        "start_date": null,

        "end_date": null,

        "plan_description": [''],

        "user": "",

        "plan_type": "",

        "offer": null
      };
      this.selectedPlanDes = null;
      this.selectedPlanDescIndex = null;
      }
    });
  }

  ngOnInit(): void {
    this.loader.show();
    this.plansservice.getSubscriptionPlanType()
      .subscribe((planType) => {
        console.log('planType', planType);
        this.loader.hide();
        this.defaultBillingPeriods = planType;
      }, (error) => {
        this.toaster.error(error.statusText);
        this.loader.hide();
      });
  }

  editPlan() {
    this.showPlanList = true;
    this.isPlanEdit = true;
    this.getPlanData(this.plan_input);
  }

  descriptionChange(value, index) {
    this.selectedPlanDescIndex = index;
    this.selectedPlanDes = value;
  }

  saveSubscription() {
    this.loader.show();
    if (this.selectedPlanDescIndex !== null && this.selectedPlanDes !== null) {
      this.plan.plan_description[this.selectedPlanDescIndex] = this.selectedPlanDes;
    }
    this.selectedPlanDescIndex = null;
    this.selectedPlanDes = null;
    this.plan.user = JSON.parse(localStorage.getItem("logedin_data")).id;
    let pDesc = [];
      this.plan.plan_description.forEach((ele)=>{
        if(ele && ele !== ""){
          pDesc.push(ele);
        }
      });
      this.plan.plan_description = pDesc;
      console.log('req', this.plan);
    if (this.isPlanEdit) {
      this.plansservice.editSubscriptionPlan(this.plan, this.selectedPlan.id)
        .subscribe((plan) => {
          console.log('plan', plan);
          this.toaster.success('Plan Updated Successfully');
          this.loader.hide();
        }, (error) => {
          this.loader.hide();
          this.toaster.error(error.statusText);
        });
    } else {
      this.plansservice.addSubscriptionPlan(this.plan)
        .subscribe((plan) => {
          console.log('plan', plan);
          this.toaster.success('Plan Added Successfully');
          this.loader.hide();
        }, (error) => {
          this.loader.hide();
          this.toaster.error(error.statusText);
        });
    }
  }

  addNewDescriptio() {
    let desc = this.plan.plan_description;
    if (this.selectedPlanDescIndex !== null && this.selectedPlanDes !== null) {
      desc[this.selectedPlanDescIndex] = this.selectedPlanDes;
      this.selectedPlanDescIndex = null;
      this.selectedPlanDes = null;
      this.plan.plan_description = desc;
      setTimeout(() => {
        this.plan.plan_description.push("");
      }, 100);
    }
  }

  createPlan() {
    this.showPlanCreatePage = true;
    this.isPlanEdit = false;
    this.showPlanList = false;
  }

  backToDashboard() {
    this.showPlanList = false;
  }

  ChooseClick(data, plan) {
    this.selectedItem = data;
    this.selectedPlan = plan;
    this.showPlanCreatePage = true;
    this.showPlanList = false;
    this.plan.end_date = plan.end_date;
    this.plan.number_of_session = plan.number_of_session;
    this.plan.plan_description = plan.plan_description;
    this.plan.plan_name = plan.plan_name;
    this.plan.plan_type = this.defaultBillingPeriods.filter((obj) => { return obj.plan_type === plan.plan_type__plan_type })[0].id;
    this.plan.regular_price = plan.regular_price;
    this.plan.sale_price = plan.sale_price;
    this.plan.start_date = plan.start_date;
    this.selectedBillingPeriod = plan.plan_type__plan_type;
    this.selectedPlanDescIndex = plan.plan_description.length -1;
    this.selectedPlanDes = plan.plan_description[this.selectedPlanDescIndex];
    console.log("data", data, "plan", plan, "this.plan", this.plan);
  }

  ShowPackage(data, apiInput) {
    this.selectedItem = 'null';
    this.showPackageItems = data;
    this.getPlanData(apiInput);

  }

  getPlanData(plan_input) {
    this.loader.show();
    this.plansservice.getsubscriptionplans(plan_input).subscribe(
      response => {
        this.PlanData = response;
        if (plan_input == '3 Months') {
          this.ThreeMonthData = this.PlanData.plan;
        }
        if (plan_input == '6 Months') {
          this.SixMonthData = this.PlanData.plan;
        }
        if (plan_input == 'Monthly Pricing') {
          this.MonthlyData = this.PlanData.plan;
        }
        if (plan_input == 'Annually') {
          this.AnnualData = this.PlanData.plan;
        }
        this.loader.hide();
      },
      error => {
        console.log(error);
        this.loader.hide();
        this.toaster.error(error.statusText);
      }
    );
  }

}
