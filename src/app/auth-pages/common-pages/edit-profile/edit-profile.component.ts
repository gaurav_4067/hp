import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/services/AuthService';
import { ApiService } from 'src/app/services/ApiService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProfilePictureModalComponent } from '../../popup-modals/profile-picture-modal/profile-picture-modal.component';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit, OnDestroy {
  public _token: any;
  hideHeader: any = true;
  model: any;
  isContent: any;
  profilePictureModal = ProfilePictureModalComponent;
  sessionCount = 0;
  expertiseCount = 0;
  expertises = [];
  practitionerDetails: any = {};
  userData: any = {};
  profileData: any = {};
  formObject: any = {};
  commonConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  };
  cityConfig: any = {};
  stateConfig: any = {};
  countryConfig: any = {};
  sessionSubscription: Subscription;
  expertiseSubscription: Subscription;
  practitionerSubscription: Subscription;
  profileDataSubscription: Subscription;
  submission: Subscription;
  countrySubscription: Subscription;
  stateSubscription: Subscription;
  citySubscription: Subscription;
  countryData: any[] = [];
  stateData: any[] = [];
  cityData: any[] = [];
  dataSubscription: Subscription;
  data: any;
  logedinData: any;
  month_name: any;
  profileForm: FormGroup;
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private apiService: ApiService,
    public thirpApiService: ThirdPApiService,
    private modalService: NgbModal,
    private loader: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    const [startDate, endDate] = this.createDates(13);

    this.dataSubscription = this.apiService.getPractitionerSchedule(startDate, endDate).subscribe((data: any) => {
      const extractedDates = this.extractDates([...data]);
      this.createCalendar(extractedDates);
    });

    this.sessionSubscription = this.apiService.getPractitionerSessionTypes().subscribe((result: any) => {
      this.sessionCount = result.count;
    });
    this.expertiseSubscription = this.apiService.getExpertise().subscribe((result: any) => {
      this.expertiseCount = result.count;
      this.expertises = [...result.results];
    });
    this.practitionerSubscription = this.apiService.pkIdUpdate('practitioner', null).subscribe((result: any) => {
      if (result.count > 0 && result.results[0]) {
        this.practitionerDetails = { ...result.results[0] };
        const userId = this.practitionerDetails.user_id;
        const address = this.practitionerDetails.address || {};
        this.formObject = {
          first_name: userId.first_name,
          last_name: userId.last_name,
          fullName: [userId.first_name, userId.last_name].join(' '),
          name: this.practitionerDetails.name,
          email: userId.email,
          phone: this.practitionerDetails.phone_number,
          country: address.address_line5,
          state: address.addrees_line4,
          city: address.address_line3,
          postalCode: address.zipcode
        };
        this.cityConfig = { placeholder: 'Select a city *', ...this.commonConfig };
        this.stateConfig = { placeholder: 'Select a state *', ...this.commonConfig };
        this.countryConfig = { placeholder: 'Select a country *', ...this.commonConfig };
      }
    });

    this.userData = this.authService.getUser();
    this.profileDataSubscription = this.apiService.getProfileDetails(this.userData.pk).subscribe((result: any) => {
      this.profileData = result;
    });
    this.countrySubscription = this.thirpApiService.getCountries('id', 'no_page').subscribe((response: any) => {
      this.countryData = [...response];
      if(this.logedinData.address){
        if(this.logedinData.address.address_line3){
          let result = this.countryData.find(obj=>obj.name === this.logedinData.address.address_line5)
          this.ifloadState(result["id"]);
        }
      }
    });

    const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const d = new Date();
this.month_name = (monthNames[d.getMonth()]);

  }
  ifloadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        
        this.stateData = [...response];
        //
        if(this.logedinData.address.addrees_line4){
          let result = this.stateData.find(obj=>obj.name === this.logedinData.address.addrees_line4)
          this.ifloadCity(result["id"]);
        }
      });
  }
  ifloadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.cityData = [...response];
        this.loader.hide();
      });
  }
  createDates(count: number): [any, any] {
    let yourDate = new Date();
    const offset = yourDate.getTimezoneOffset();
    yourDate = new Date(yourDate.getTime() - offset * 60 * 1000);
    const startDate = yourDate.toISOString().split('T')[0];

    const lastDate = new Date(yourDate);
    lastDate.setDate(lastDate.getDate() + count);
    const endDate = lastDate.toISOString().split('T')[0];

    return [startDate, endDate];
  }

  extractDates(datesArray: any[]) {
    const dates = new Set();
    for (const item of datesArray) {
      const [dt] = item.start_time.split('T');
      const [y, m, d] = dt.split('-');
      dates.add(d);
    }
    return dates;
  }

  createCalendar(setOfDates: Set<any>) {
    const yourDate = new Date();
    this.data = [];

    for (let i = 0; i < 14; i++) {
      const lastDate = new Date(yourDate);
      lastDate.setDate(lastDate.getDate() + i);

      const date = lastDate.getDate();
      const day = this.shortDay(lastDate.getDay());
      const active = ['Su', 'Sa'].indexOf(day) < 0;
      const hasSession = setOfDates.has(date < 10 ? `0${date}` : `${date}`);
      this.data.push({
        date, day, active, hasSession
      });
    }
  }

  shortDay(num: number) {
    return [
      'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa',
    ][num];
  }

  openModal(modalContent: any, isWhere: string) {
    this.isContent = isWhere;
    this.model = this.modalService.open(modalContent, { windowClass: 'modal-holder', centered: true, size: 'lg' });
    this.model.componentInstance.isPractitioner = true;
    this.model.componentInstance.uploadDone = this.closeModal;
  }
  closeModal(Event?: any) {
    this.model.close();
  }
  goToExpertises() {
    this.router.navigate(['/add-expertise'], { state: { expertisesSelected: [...this.expertises] } });
    window.scrollTo(0, 0);
  }

  countryChanged(data: any) {
    if (data) {
      this.stateData = [];
      this.cityData = [];
      this.loadState(data.id);
      this.formObject.state="";
      this.formObject.city="";
    } else {
      this.stateData = [];
      this.cityData = [];
    }
  }
  loadState(id: any) {
    this.loader.show();
    this.stateSubscription?.unsubscribe();
    this.stateSubscription = this.thirpApiService
      .getStates(id, 'id', 'no_page')
      .subscribe((response: any) => {
        this.stateData = [...response];
        this.loader.hide();
      });
  }
  stateChanged(data: any) {
    if (data) {
      this.cityData = [];
      this.loadCity(data.id);
      this.formObject.city="";
    } else {
      this.cityData = [];
    }
  }
  loadCity(id: any) {
    this.loader.show();
    this.citySubscription?.unsubscribe();
    this.citySubscription = this.thirpApiService
      .getCities(id, 'id', 'no_page')
      .subscribe((response: any) => {
        this.cityData = response;
        this.loader.hide();
      });
  }

  submitForm() {
    this.loader.show();
    var payload = { ...this.practitionerDetails };
    var userAddress = {
      "address_line1": null,
      "address_line2": "N/A",
      "address_line3": payload.address ? payload.address.address_line3 : "N/A",
      "addrees_line4": payload.address ? payload.address.addrees_line4 : "N/A",
      "address_line5": payload.address ? payload.address.address_line5 : "N/A",
      "zipcode": payload.address ? payload.address.zipcode : null,
      // "address_type": null,
      // "city": null,
    };
    if(this.formObject.city && this.formObject.city["name"]){
      userAddress.address_line3 = this.formObject.city["name"];
    }
    if(this.formObject.state && this.formObject.state["name"]){
      userAddress.addrees_line4 = this.formObject.state["name"];
    }
    if(this.formObject.country && this.formObject.country["name"]){
      userAddress.address_line5 = this.formObject.country["name"];
    }
    userAddress.zipcode = this.formObject.postalCode;
    console.log(this.formObject);
    payload["address"] = userAddress;
    payload["user_id"]["first_name"] = this.formObject.first_name;
    payload["user_id"]["last_name"] = this.formObject.last_name;
    payload["user_id"]["email"] = this.formObject.email;
    payload["phone_number"] = this.formObject.phone;
    payload["name"] = this.formObject.name;
    console.log(payload);
    this.submission = this.apiService.editPractitioner(payload.id, payload).subscribe((response:any) => {
      localStorage.setItem(
        `logedin_data`,
        JSON.stringify(response)
      );
      this.loader.hide();
      this.router.navigate(['welcome-practitioner']);
    }, err => {
      this.loader.hide();
    });
  }

  ngOnDestroy() {
    this.sessionSubscription?.unsubscribe();
    this.expertiseSubscription?.unsubscribe();
    this.practitionerSubscription?.unsubscribe();
    this.profileDataSubscription?.unsubscribe();
    this.submission?.unsubscribe();
    this.countrySubscription?.unsubscribe();
    this.stateSubscription?.unsubscribe();
    this.citySubscription?.unsubscribe();
    this.dataSubscription?.unsubscribe();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;  
  }
}
