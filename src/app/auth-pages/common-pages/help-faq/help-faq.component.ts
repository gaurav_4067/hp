import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';

@Component({
  selector: 'app-help-faq',
  templateUrl: './help-faq.component.html',
  styleUrls: ['./help-faq.component.scss'],
})
export class HelpFaqComponent implements OnInit {

  faqList: any;
  userData: any;

  constructor(
    public apiService: ApiService,
    public toastr: ToastrService,
    public generalService:GeneralService
  ) { }

  ngOnInit() {
    this.generalService.showProfile=false;
    this.userData = JSON.parse(localStorage.getItem("user_data"))
    this.loadData(this.userData['groups'][0]);
  }
  loadData(arg){
    this.apiService
      .getFAQ(arg)
      .subscribe((response: any) => {
        this.faqList = response;
      },(error) => {
        this.toastr.error(error.statusText);
      });
  }
  collapseIcon(event) {
    if (event.currentTarget.className.indexOf("fas fa-arrow-down") > -1) {
      event.currentTarget.className = "fas fa-arrow-up";
    } else if (event.currentTarget.className.indexOf("fas fa-arrow-up") > -1) {
      event.currentTarget.className = "fas fa-arrow-down"
    }
  }
}