import { Component, OnInit, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../../services/ApiService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from '../../customer-pages/customer-booking/ngbdmodalcontent.component';
import { ToastrService } from 'ngx-toastr';
import { elementAt } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sessiononly',
  templateUrl: './sessiononly.component.html',
  styleUrls: ['./sessiononly.component.scss'],
  providers: [NgbCarouselConfig]
})
export class SessiononlyComponent implements OnInit {

  gamesFormatted: any = [
    [1, 2, 3, 4],
    [1, 2, 2, 3]
  ]
  currentRate = 4;
  list: any = [];
  selectedPract: any;
  itemsPerSlide = 4;
  singleSlideOffset = false;
  noWrap = true;
  mockDesc = "N/A";
  msg:string="Please wait..."
  subMsg:boolean=false;
  @Input() query = '';
  logedinData: any;

  constructor(
    config: NgbCarouselConfig,
    ratingConfig: NgbRatingConfig,
    private apiService: ApiService,
    private modalService: NgbModal,
    private toaster: ToastrService,
    private router: Router
  ) {
    // customize default values of carousels used by this component tree
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;

    // customize default values of ratings used by this component tree
    ratingConfig.max = 5;
    ratingConfig.readonly = true;
  }

  ngOnInit(): void {
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.getPractitionerListByInterest();
  }

  getPractitionerListByInterest() {
    const cid = JSON.parse(localStorage.getItem("logedin_data")).id;
    this.apiService.searchPractitionerList(cid, this.query).subscribe((success: any) => {
      let results = [];
      
      success.results.forEach((result) => {
        result.practitionersession.forEach((session) => {
          if( session && session.id && session.duration_min){
            if(session.expertise.length && this.logedinData.interest.length){
              let outSet = this.logedinData.interest.find(element=> element == session.expertise[0]);
              if(outSet){
                results.push({
                  description: result.description,
                  id: result.id,
                  sessionId: session.id,
                  isSelected: false,
                  profile_image: result.profile_image,
                  ratings: result.ratings,
                  user_id: result.user_id,
                  name: result.name,
                  chat_availability: result.chat_availability,
                  duration_min: session.duration_min,
                  price: session.price,
                  expertise: session.expertise,
                  title: session.title,
                });
              }
            }
          }
        });
      });
      this.list = results;
     
      if(this.list.length==0){
        this.msg="Unfortunately, We haven't found any practitioner matches to your interest.";
        this.subMsg=true;
      }
      console.log('this.list', this.list);
    }, (err) => {
      console.log('err', err);
      this.msg="Unfortunately, We haven't found any practitioner matches to your interest.";
      this.subMsg=true;
    })
  }

  selectPractitioner(item, index) {
    
    this.selectedPract = item;
    this.list.forEach(element => {
      if (element.sessionId === item.sessionId) {
        element.isSelected = !element.isSelected;
        // element.className="active-card";
      } else {
        element.isSelected = false;
        // element.isSelected
      } 
    });
    this.list.forEach(element => {
      if(element.isSelected){
        element.className="highlight";
      }
      else{
        element.className="dimMe";
      }
    });

    let result = this.list.find(obj=>obj.isSelected === true);
    if(!result){
      this.list.forEach(element => {
        element.className="highlight";
      });
      // this.list[index].className="default-active";
      this.selectedPract = undefined;
    }

    
  }

  choosePract() {
    if(this.selectedPract){
      const selectedSession = {
        session: {
          practitioner__name: this.selectedPract.name,
          practitioner__expertise: this.selectedPract.expertise,
          price: this.selectedPract.price,
          title: this.selectedPract.title,
          duration_min: this.selectedPract.duration_min,
          id: this.selectedPract.sessionId
        },
        pid: this.selectedPract.id
      };
      const modalRef = this.modalService.open(NgbdModalContent, { ariaLabelledBy: 'modal-basic-title', backdrop: 'static', size: 'lg', centered: true, scrollable: true, windowClass: 'customer-booking-modal' });
      modalRef.componentInstance.selectedSession = selectedSession;
      console.log(this.selectedPract);
    } else {
      this.toaster.error('Please Choose any one Practitioner');
    }
  }
 redirectToInterest(){
   this.modalService.dismissAll();
   this.router.navigate(["/view-interests"]);
 }
}
