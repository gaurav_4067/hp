import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from '../../customer-pages/customer-booking/ngbdmodalcontent.component';

@Component({
    selector: 'practitioner-modal-content',
    templateUrl: './practitionermodalcontent.component.html',
    styleUrls: ['./practitionermodalcontent.component.scss'],
})
export class PractitionerModalContent {
    list: any = [];
    mockDesc = "N/A";
    itemsPerSlide = 3;
    singleSlideOffset = false;
    noWrap = true;
    selectedPract: any;

    constructor(public activeModal: NgbActiveModal,
        public apiService: ApiService,
        ratingConfig: NgbRatingConfig,
        private loader: NgxSpinnerService,
        public toastr: ToastrService,
        private modalService: NgbModal,
        public router: Router,
    ) {
        // this.loader.show();

        // customize default values of ratings used by this component tree
        ratingConfig.max = 5;
        ratingConfig.readonly = true;
    }

    ngOnInit() {
        this.getPractitionerListByInterest();
    }
    getPractitionerListByInterest() {
        const cid = JSON.parse(localStorage.getItem("logedin_data")).id;
        this.apiService.getPractitionerListByInterest(cid).subscribe((success: any) => {
            let results = [];
            
            success.results.forEach((result) => {
                result.practitionersession.forEach((session) => {
                    results.push({
                        description: result.description,
                        id: result.id,
                        sessionId: session.id,
                        isSelected: false,
                        profile_image: result.profile_image,
                        ratings: result.ratings,
                        user_id: result.user_id,
                        name: result.name,
                        chat_availability: result.chat_availability,
                        duration_min: session.duration_min,
                        price: session.price,
                        expertise: session.expertise,
                        title: session.title,
                    })
                })
            });

            this.list = results;
        }, (err) => {
            console.log('err', err);
        })
    }

    confirmPractitioner() {
        const selectedSession = {
            session: {
                practitioner__name: this.selectedPract.name,
                practitioner__expertise: this.selectedPract.expertise,
                price: this.selectedPract.price,
                title: this.selectedPract.title,
                duration_min: this.selectedPract.duration_min,
                id: this.selectedPract.sessionId
            },
            pid: this.selectedPract.id
        };
        const modalRef = this.modalService.open(NgbdModalContent, {centered:true,scrollable:true, ariaLabelledBy: 'modal-basic-title', backdrop: 'static', size: 'lg', windowClass: 'customer-booking-modal' });
        modalRef.componentInstance.selectedSession = selectedSession;
    }

    selectPractitioner(item, index) {
        this.selectedPract = item;
        this.list.forEach((s) => {
            if (s.sessionId === item.sessionId) {
                s.isSelected = !s.isSelected;
            } else {
                s.isSelected = false;
            }
        });
    }
}