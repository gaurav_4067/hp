import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets, Chart } from 'chart.js';
import "../../../../services/charts/doughnut-chart-rounded-corners";

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.scss']
})
export class DoughnutChartComponent implements OnInit {

  public chartType: string = 'doughnut' as ChartType;
  public chartLabels: Array<string> = ['January'];
  public chartData: Array<any> = [50, 40];
  public chartColors = [
    {
      backgroundColor: [
        '#00bad5',
        '#cddbe0',
      ]
    }
  ];

  public chartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    pieceLabel: {
      render: function (args) {
        const label = args.label,
          value = args.value;
        return label + ': ' + value;
      }
    },
    // Here is where we enable the 'radiusBackground'
    radiusBackground: {
      color: '#d1d1d1' // Set your color per instance if you like
    },
    cutoutPercentage: 70,
    animation: {
      animationRotate: true,
      duration: 2000
    },
    legend: {
      display: false
    },
    tooltips: {
      enabled: false
    }
  }

  @Input() averageRatings;
  constructor() {

  }

  ngOnInit(): void {
    this.chartData = this.averageRatings ? [this.averageRatings, 100 - this.averageRatings] : [0,100];
  }

}

