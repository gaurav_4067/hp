import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
export interface ChartColor {
  backgroundColor?: string | string[] | CanvasGradient;
  borderWidth?: number | number[];
  borderColor?: string | string[] | CanvasGradient;
  borderCapStyle?: string;
  borderDash?: number[];
  borderDashOffset?: number;
  borderJoinStyle?: string;
  pointBorderColor?: string | string[] | CanvasGradient;
  pointBackgroundColor?: string | string[] | CanvasGradient;
  pointBorderWidth?: number | number[];
  pointRadius?: number | number[];
  pointHoverRadius?: number | number[];
  pointHitRadius?: number | number[];
  pointHoverBackgroundColor?: string | string[] | CanvasGradient;
  pointHoverBorderColor?: string | string[] | CanvasGradient;
  pointHoverBorderWidth?: number | number[];
  pointStyle?: string | string[] | CanvasGradient;
  hoverBackgroundColor?: string | string[] | CanvasGradient;
  hoverBorderColor?: string | string[] | CanvasGradient;
  hoverBorderWidth?: number;
}
@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {
  @ViewChild('myCanvas') myChart: ElementRef;
  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  @Input() earnings: any;
  @Input() bgColor: any;
  context: CanvasRenderingContext2D;
  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    elements: {
      point:
      {
        radius: 5,
        hitRadius: 5,
        hoverRadius: 10,
        hoverBorderWidth: 3
      }
    },
    legend: {
      display: true,
      labels: {
        fontColor: '#E9E9EA'
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        display: true,
        gridLines: {
          display: false,
          drawBorder: false
        }


      }],
      yAxes: [
        {
          display: true,
          gridLines: {
            borderDash: [6, 4],
            color: "#E9E9EA",
            drawTicks: false,
            drawBorder: false,
          },
          ticks: {
            beginAtZero: true,
            callback: function (value, index, values) {
              return '' + value;
            },
            autoSkip: true,
            maxTicksLimit: 5,
            stepSize: .5
          }
        }],

    },

    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: ChartColor[] = [
    { // grey
      backgroundColor: "#FBD036",
      borderColor: '#FBD036',
      pointBackgroundColor: '#FAD038',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#FAD038',
      pointHoverBorderColor: '#fff',
    },
  ];
  public lineChartLegend = false;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];



  constructor() { }

  ngOnInit(): void {
    let earning = [];
    let months = [];
    this.earnings.forEach((data) => {
      earning.push(data.earnings);
      months.push(data.month);
    })
    this.lineChartData = [
      { data: earning.reverse() }
    ];
    this.lineChartLabels = months.reverse();
    this.lineChartColors[0].borderColor = this.bgColor || '#FBD036';
    this.lineChartColors[0].backgroundColor = this.bgColor || '#FBD036';
    this.lineChartColors[0].pointHoverBackgroundColor = this.bgColor || '#FBD036';
    this.lineChartColors[0].pointBackgroundColor = this.bgColor || '#FBD036';
  }
  ngAfterViewInit() {
    const ctx = (<HTMLCanvasElement>this.myChart.nativeElement).getContext("2d");
    const gradient = ctx.createLinearGradient(0, 0, 0, 100);
    gradient.addColorStop(0, '#FBEEBB');
    gradient.addColorStop(1, '#FFFFFF');
    this.lineChartColors[0].backgroundColor = gradient;
  }



  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}

