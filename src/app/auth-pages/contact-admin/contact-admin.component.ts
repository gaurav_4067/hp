import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ApiService } from '../../services/ApiService';

@Component({
  selector: 'app-contact-admin',
  templateUrl: './contact-admin.component.html',
  styleUrls: ['./contact-admin.component.scss']
})
export class ContactAdminComponent implements OnInit {

  title;
  message;
  isSubmitted = false;

  constructor(
    public modalService: NgbModal,
    private loader: NgxSpinnerService,
    private toaster: ToastrService,
    private router: Router,
    public apiService: ApiService,
  ) { }

  ngOnInit(): void {
  }

  close() {
    this.modalService.dismissAll();
  }

  sendMessage() {
    this.isSubmitted = true;
    if (this.message && this.title) {
      this.loader.show();
      const payLoad = {
        subject: this.title,
        text: this.message,
        practitioners: JSON.parse(localStorage.getItem("logedin_data")).id
      };
      this.apiService.sendMsgToAdmin(payLoad)
        .subscribe((msg:any) => {
          this.isSubmitted = false;
          this.loader.hide();
          this.toaster.success('Message send Successfully');
          this.modalService.dismissAll();
        }, (error) => {
          this.loader.hide();
          this.toaster.error(error.statusText);
        });
    }

  }

}
