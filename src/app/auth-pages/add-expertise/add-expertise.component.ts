
import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { ApiService } from '../../services/ApiService';

@Component({
  selector: 'app-add-expertise',
  templateUrl: './add-expertise.component.html',
  styleUrls: ['./add-expertise.component.scss']
})
export class AddExpertiseComponent implements OnInit, OnDestroy {
  expertises = [];
  selectedExpertises: any = [];
  sub: Subscription;
  chatSessions = [];
  chatSubscription: Subscription;
  userData: any;
  logedinData: any;

  constructor(
    public apiService: ApiService,
    public router: Router,
    public thirdPApiService: ThirdPApiService,
    public auth: AuthService,
    public generalService: GeneralService,
    private loader: NgxSpinnerService,
    public toastr : ToastrService,
    private ref: ElementRef
  ) { }
  
  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if(this.logedinData){
      localStorage.setItem("expertise", JSON.stringify(this.logedinData["expertise"]));
      this.logedinData["expertise"].forEach(element => {
        this.selectedExpertises.push(element);
      });
    }
    this.fetchInterests();
    this.chatSubscription = this.apiService.getCustomerChatSessionList().subscribe((data: any) => {
      const { count, response } = data.data;
      this.chatSessions = [...response];
    });
  }
  fetchInterests() {
    this.loader.show();
    this.sub = this.apiService.getAllInterest().subscribe((data: any) => {
      this.expertises = [...data];
      this.loader.hide();
    });
  }
  filterIfThere(list:any){
    if(this.logedinData["expertise"].find(obj=>obj == list.interest)){
      return true;
    }else{
      return false;
    }
  }
  chooseMe(data:any){
    //
    let refContent = document.getElementById(`${data.id}`) as HTMLInputElement;
    if(refContent.className == "card expertise-card active" || refContent.className == "card expertise-card active ng-star-inserted"){
      refContent.className = "card expertise-card";
      let removed;
      if(this.selectedExpertises.find(obj=>obj == data.interest)){
        removed = this.selectedExpertises.filter(obj=>obj != data.interest);
        this.selectedExpertises = removed;
      }
      localStorage.removeItem("expertise");
      localStorage.setItem("expertise", JSON.stringify(this.selectedExpertises));
    }else{
      refContent.className = "card expertise-card active";
      if(this.selectedExpertises.length){
        if(!this.selectedExpertises.find(obj=>obj == data.interest)){
          this.selectedExpertises.push(data.interest);
        }
      }else{  
        this.selectedExpertises.push(data.interest);
      }
      console.log(this.selectedExpertises);
      localStorage.removeItem("expertise");
      localStorage.setItem("expertise", JSON.stringify(this.selectedExpertises));
    }
  }
  profileSubmit(){
    let payload = this.logedinData;
    if(localStorage.getItem("expertise")){
      payload["expertise"] = JSON.parse(localStorage.getItem("expertise"));
    }
    // var userAddress = {
    //   "address_line1": null,
    //   "address_line2": "N/A",
    //   "address_line3": payload.address ? payload.address.city_name["name"] : "N/A",
    //   "addrees_line4": payload.address ? payload.address.state_name["name"] : "N/A",
    //   "address_line5": payload.address ? payload.address.country_name["name"] : "N/A",
    //   "zipcode": payload.address ? payload.address.zipcode : null,
    // };
    // payload["address"] = userAddress;
    console.log(payload);
    //return;
    this.loader.show();
    this.apiService
    this.apiService.editPractitioner(payload["id"], payload)
    .subscribe((response: any) => {
      console.log(response);
      localStorage.setItem(
        `logedin_data`,
        JSON.stringify(response)
      );
      this.toastr.success('Updated successfully');
      this.loader.hide();
      this.router.navigate(["practitioner-profile"]);
    });
  }
  isSelected(expId: string) {
    return this.selectedExpertises.some(exp => exp.id === expId);
  }
  toggleItem(expertise: any) {
    if (this.isSelected(expertise.id)) {
      this.selectedExpertises = this.selectedExpertises.filter(exp => exp.id !== expertise.id);
      return;
    }
    this.selectedExpertises.push(expertise);
  }
  toggleCls(id: any, display: boolean) {
    const card = this.ref.nativeElement.querySelector(`[id="chat-${id}"]`);
    card.classList[display ? 'add' : 'remove']('active');
  }
  ngOnDestroy() {
    this.sub?.unsubscribe();
    this.chatSubscription?.unsubscribe();
  }
}
