import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { ApiService } from 'src/app/services/ApiService';
import { Router } from '@angular/router';
import { ThirdPApiService } from '../../services/ThirdPApiService';
import { ZoomMeetingConfig } from '../../config/config';
import { AuthService } from '../../services/AuthService';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { GeneralService } from '../../services/GeneralService';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit, OnDestroy {
  data: any = [];
  dataSubscription: Subscription;
  expertises = [];
  expertisesSubscription: Subscription;
  sessionArray: any[] = [];
  dateData: any[] = [];
  sessionStats: any = {};
  earningStats: any = {};
  upDownIcons = {
    down: 'assets/images/arrow_button_down.png',
    up: 'assets/images/arrow_button-up.png',
  };
  chatStat = [];
  logedinData: any;
  selectedExpertise:any;
  isLoaded:boolean=false;
  constructor(
    public apiService: ApiService,
    private router: Router,
    public thirdPApiService: ThirdPApiService,
    public auth: AuthService,
    private ref: ElementRef,
    private loader: NgxSpinnerService,
    private toaster: ToastrService,
    public generalService: GeneralService,
  ) { }

  ngOnInit() {
    console.log('Dashboard');
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    
    // this.generalService.setTitle(null);
    // this.generalService.setTitle("Hi Dr. "+ this.logedinData?.user_id.first_name);
    const [startDate, endDate] = this.createDates();
    this.dataSubscription = this.apiService.getDashboardData(startDate, endDate).subscribe((data: any) => {
      this.data = data;
      
      this.isLoaded=true;
      this.sessionStats = data.session_count;
      this.earningStats = data.earnings_amount;
      console.log(data);
      this.fetchExpertiseData();
      this.mergeAndSetChatStat();
      const extractedDates = this.extractDates(data.this_week_session);
      this.createCalendar(extractedDates);
      this.createSessionArray(data.this_week_session);
    });
  }
  mergeAndSetChatStat() {
    const chat = this.data.stats_value.chat;
    const form = this.data.stats_value.followupform;

    this.chatStat = [];

    for (let i = 0; i < chat.length; i++) {
      this.chatStat.push({ ...chat[i], ...form[i] });
    }
  }

  expertiseData() {
    //
    console.log(this.data);
    const arr = this.data.expertise;
    if (arr && arr[0]) {
      return [...arr[0]];
    }
    return [];
  }

  fetchExpertiseData() {
    const expertiseData = this.expertiseData();
    console.log(expertiseData);
    if (expertiseData.length) {
      this.expertisesSubscription = this.apiService.getInterestList(...expertiseData).subscribe((data: any) => {
        this.expertises = [...data.interest_list];
        console.log(data);
      });
    }
  }

  upcomingSession() {
    const arr = this.data?.upcoming_sessions?.data;
    if (arr && arr[0]) {
      return arr[0];
    }
    return {};
  }

  createDates(): Array<string> {
    let yourDate = new Date();
    const offset = yourDate.getTimezoneOffset();
    yourDate = new Date(yourDate.getTime() - offset * 60 * 1000);
    const todayDate = yourDate.toISOString().split('T')[0];
    // const lastDate = new Date(yourDate);
    // lastDate.setDate(lastDate.getDate() + 6);
    // const endDate = lastDate.toISOString().split('T')[0];
    let firstday = new Date(yourDate.setDate(yourDate.getDate() - yourDate.getDay()));
    let lastday = new Date(yourDate.setDate(yourDate.getDate() - yourDate.getDay()+6));
    const startDate = firstday.toISOString().split('T')[0];
    const endDate = lastday.toISOString().split('T')[0];
    return [todayDate, todayDate];
  }

  goToAddExpertise() {
    this.router.navigate(['/add-expertise'], { state: { expertisesSelected: [...this.expertises] } });
    window.scrollTo(0, 0);
  }

  selectExpertise(expertise:any){
    this.selectedExpertise=expertise;
    console.log(this.selectedExpertise);
  }

  goToViewExpertise() {
    if(this.logedinData["expertise"].length){
      this.router.navigate(['/view-expertise'], { state: { expertisesSelected: [...this.expertises] } });
      window.scrollTo(0, 0);
    }
  }
  convertToHrs(min: number | null) {
    if (min == null) { return ''; }
    const hrs = Math.floor(min / 60);
    const rem = min % 60;
    let res = '';

    if (hrs > 0) {
      res += `${hrs}h`;
    }
    if (rem > 0) {
      res += ` ${rem}min`;
    }
    return res;
  }

  getPrice() {
    const session = this.upcomingSession();

    if (!session.start_time) {
      return 'Free';
    }

    const startTime = session.start_time;
    const endTime = session.end_time;

    const [h1, m1] = startTime.split(':').map(Number);
    const [h2, m2] = endTime.split(':').map(Number);

    const h2Final = h2 < h1 ? h2 + 24 : h2;

    const totalS1 = m1 + 60 * h1;
    const totalS2 = m2 + 60 * h2Final;

    if (session.subscription) {
      if (totalS2 - totalS1 > 30) {
        return `$${session.practitioner_session.price}`;
      }
      return 'Free';
    }

    return `$${session.practitioner_session.price}`;
  }

  getSessionPrice(session: any) {
    if (!session.start_time) {
      return 'Free';
    }

    const startTime = session.start_time;
    const endTime = session.end_time;

    const [h1, m1] = startTime.split(':').map(Number);
    const [h2, m2] = endTime.split(':').map(Number);

    const h2Final = h2 < h1 ? h2 + 24 : h2;

    const totalS1 = m1 + 60 * h1;
    const totalS2 = m2 + 60 * h2Final;

    if (session.subscription) {
      if (totalS2 - totalS1 > 30) {
        return `$${session.price}`;
      }
      return 'Free';
    }

    return `$${session.price}`;
  }

  getDuration() {
    const session = this.upcomingSession();

    if (!session.start_time) {
      return '30 min';
    }

    const startTime = session.start_time;
    const endTime = session.end_time;

    const [h1, m1] = startTime.split(':').map(Number);
    const [h2, m2] = endTime.split(':').map(Number);

    const h2Final = h2 < h1 ? h2 + 24 : h2;

    const totalS1 = m1 + 60 * h1;
    const totalS2 = m2 + 60 * h2Final;

    return this.convertToHrs(totalS2 - totalS1);
  }

  getAppointmentDate() {
    const appDate = this.upcomingSession().appointment_date;

    if (!appDate) {
      return '';
    }

    const [y, m, d] = appDate.split('-');
    const month = months[Number(m) - 1];

    return `${Number(d)} ${month}`;
  }

  getAppointmentTime() {
    const time = this.upcomingSession().start_time;

    if (!time) {
      return '';
    }

    const [h, m] = time.split(':');

    if (Number(h) > 12) {
      return `${h - 12}:${m} pm`;
    }
    return `${h}:${m} ${Number(h) === 12 ? 'pm' : 'am'}`;
  }

  getSessionTime(time: string) {
    if (!time) {
      return '';
    }

    const [h, m] = time.split(':');

    if (Number(h) > 12) {
      return `${Number(h) - 12}:${m} pm`;
    }
    return `${h}:${m} ${Number(h) === 12 ? 'pm' : 'am'}`;
  }

  shortDay(num: number) {
    return [
      'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa',
    ][num];
  }

  extractDates(datesArray: any[]) {
    const dates = new Set();
    for (const item of datesArray) {
      const [y, m, d] = item.appointment_date.split('-');
      dates.add(d);
    }
    return dates;
  }

  createCalendar(setOfDates: Set<any>) {
    var yourDate = new Date;
    var firstday = new Date(yourDate.setDate(yourDate.getDate() - yourDate.getDay()));
    var lastday = new Date(yourDate.setDate(yourDate.getDate() - yourDate.getDay()+6));
    // console.log(firstday, lastday);
    this.dateData = [];
    for (let i = 0; i < 7; i++) {
      const lastDate = new Date(firstday);
      lastDate.setDate(lastDate.getDate() + i);
      const date = lastDate.getDate();
      const day = this.shortDay(lastDate.getDay());
      const active = ['Su', 'Sa'].indexOf(day) < 0;
      const hasSession = setOfDates.has(date < 10 ? `0${date}` : `${date}`);

      const todayDate = new Date();
      lastDate.setDate(lastDate.getDate());
      //
      console.log(todayDate,lastDate);
      let choosenDay;
      if(todayDate.toISOString().split('T')[0] === lastDate.toISOString().split('T')[0]){
        choosenDay = true;
      }else{
        choosenDay = false;
      }
      this.dateData.push({
        date, day, active, hasSession, lastDate, choosenDay
      });
      console.log(this.dateData);
    }
  }
  loadDateBasedSessions(choosenDate: any){
    console.log(choosenDate);
    if(choosenDate){
      this.dateData.forEach(element => {
        if(element.date == choosenDate.date){
          element.choosenDay = true;
        }else{
          element.choosenDay = false;
        }
      })
    }
    let yourDate = choosenDate.lastDate;
    const offset = yourDate.getTimezoneOffset();
    yourDate = new Date(yourDate.getTime() - offset * 60 * 1000);
    const startDate = yourDate.toISOString().split('T')[0];
    this.loadDateBased(startDate,startDate);
  }
  loadDateBased(startDate: any, endDate: any){
    this.loader.show();
    this.dataSubscription = this.apiService.getDashboardData(startDate, endDate).subscribe((data: any) => {
      this.data = data;
      this.sessionStats = data.session_count;
      this.earningStats = data.earnings_amount;
      console.log(data);
      this.fetchExpertiseData();
      this.mergeAndSetChatStat();
      const extractedDates = this.extractDates(data.this_week_session);
      // this.createCalendar(extractedDates, true);
      this.createSessionArray(data.this_week_session);
      this.loader.hide();
    });
  }
  createSessionArray(arr: any[]) {
    this.sessionArray = [];
    for (const item of arr) {
      const time = this.getSessionTime(item.start_time);
      const price = this.getSessionPrice(item);
      const image = item.customer__profile_image ? this.generalService.getCustomerImageBaseUrl(item.customer__profile_image) : 'assets/images/users/avator-2.jpg';
      const name = item.customer__nick_name;

      this.sessionArray.push({ time, price, image, name });
    }
  }

  navigateToSessionPage(session) {
    const userData = JSON.parse(localStorage.getItem('user_data'));
    const isGroup = userData?.groups[0].toLowerCase();
    const userName = userData?.user.username;
    const email = userData?.user.email;
    this.loader.show();
    this.thirdPApiService.getZoomMeetingList(session.id)
      .subscribe((response: any) => {
        console.log('response', response);
        this.loader.hide();
        if (response && response.length > 0) {
          window.open(`${ZoomMeetingConfig.ZOOM_SESSION_APP_URL}?user=${userName}&id=${response[0].meeting_id}&auth=${this.auth.getToken()}&email=${email}&group=${isGroup}`, '_blank');
        } else {
          this.toaster.error('No meetings id available');
        }
      }, (error) => {
        this.loader.hide();
        console.error(error);
      });
  }

  showStats(day) {
    const bar = this.ref.nativeElement.querySelector(`[id="${day}-bar"]`);
    const formBar = this.ref.nativeElement.querySelector(`[id="${day}-form-bar"]`);
    const cloud = this.ref.nativeElement.querySelector(`[id="${day}-cloud"]`);

    const data = this.chatStat.find(d => d.day === day);

    bar.style['background-color'] = '#00BAD5';
    bar.style['border-radius'] = '8px';
    formBar.style['border-radius'] = '8px';
    formBar.style['background-color'] = '#FBD036';
    formBar.style.display = 'block';
    formBar.style.height = `${88 * data.form_count / Math.max(data.chat_count, data.form_count)}px`;

    cloud.style.display = 'block';
    cloud.style.top = '-' + (88 * this.percentage(data) - 49) + 'px';
  }

  hideStats(day) {
    const bar = this.ref.nativeElement.querySelector(`[id="${day}-bar"]`);
    const formBar = this.ref.nativeElement.querySelector(`[id="${day}-form-bar"]`);
    const cloud = this.ref.nativeElement.querySelector(`[id="${day}-cloud"]`);

    bar.style['background-color'] = 'transparent';
    formBar.style['background-color'] = 'transparent';
    formBar.style.display = 'none';
    formBar.style.height = '0px';

    cloud.style.display = 'none';
    cloud.style.top = '0px';
  }

  percentage(data) {
    const maxVal = Math.max(...this.chatStat.map(c => c.chat_count), ...this.chatStat.map(c => c.form_count));
    return Math.max(data.chat_count, data.form_count) / maxVal;
  }

  toggleCls(id, display) {
    const card = this.ref.nativeElement.querySelector(`[id="chat-${id}"]`);

    card.classList[display ? 'add' : 'remove']('active');
  }

  navigateToChatPage(session) {
    this.router.navigate(['chat'], { queryParams: { selected: session.customer_id, redirect: true } });
  }

  deleteInterest(expertise: any) {
    const payload = JSON.parse(localStorage.getItem('logedin_data'));
    payload.expertise = payload.expertise.filter(item => item !== expertise.interest);
    this.loader.show();
    this.apiService
      .editPractitioner(payload.id, payload)
      .subscribe((response: any) => {
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response)
        );
        if(response.expertise && response.expertise){
          this.data.expertise = [response.expertise];
        }
        this.expertises = response.expertise;
        this.fetchExpertiseData();
        this.loader.hide();
      });
  }

  ngOnDestroy() {
    this.dataSubscription?.unsubscribe();
    this.expertisesSubscription?.unsubscribe();
  }
}
