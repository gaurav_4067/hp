import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/AuthService';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import { ApiService } from 'src/app/services/ApiService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'addpractitioner',
  templateUrl: './admin-add-practioner-component.html',
  styleUrls: ['./admin-add-practioner-component.scss'],
  providers: [AuthService]

})

export class AddPractionerComponent implements OnInit {
  AddPractitionerForm: FormGroup;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  countryData: any = [];
  stateData: any = [];
  cityData: any = [];
  practitionerDetails = {};
  expertiseData: any = [];
  documents;
  certitifcations;
  separateDialCode = false;
  formSubmitted = false;
  countryConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select Country *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  stateConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select State *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  cityConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select City *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }

  expertiseConfig = {
    displayKey: 'interest',
    search: true,
    height: 'auto',
    placeholder: 'Select Expertise *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'interest',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  userData: any;

  constructor(
    public apiService: ApiService,
    public thirpApiService: ThirdPApiService,
    public modalService: NgbModal,
    public toastr: ToastrService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.AddPractitionerForm = this.fb.group({
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      bname: ['', Validators.required],
      description: ['', Validators.required],
      email: ['', Validators.required],
      city: ['', Validators.required],
      postalCode: ['', Validators.required],
      phoneCode: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      expertise: ['', Validators.required],
    });
    this.apiService.getInterest()
      .subscribe((expertise: any) => {
        this.expertiseData = [...expertise];
      });
    this.loadCountry();
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  AddPractioner() {
    console.log('this.AddPractitionerForm ', this.AddPractitionerForm);
    this.formSubmitted = true;
    if (this.AddPractitionerForm.valid) {
      if (!this.documents) {
        this.toastr.warning("Atleast one Id Document needed");
      }
      if (!this.certitifcations) {
        this.toastr.warning("Atleast one Certification needed");
      }
      if (this.documents && this.certitifcations) {
        let formData = new FormData();
        formData.append("username", this.AddPractitionerForm.value.email);
        formData.append("email", this.AddPractitionerForm.value.email);
        formData.append("password1", 'welcome@HEALPLACE123#!');
        formData.append("password2", 'welcome@HEALPLACE123#!');
        formData.append("first_name", this.AddPractitionerForm.value.fname);
        formData.append("last_name", this.AddPractitionerForm.value.lname);
        formData.append("phonenumber", this.AddPractitionerForm.value.phoneCode + this.AddPractitionerForm.value.phoneNumber);
        formData.append("city_id", this.AddPractitionerForm.value.city.id);
        formData.append("city", this.AddPractitionerForm.value.city.name);
        formData.append("state", this.AddPractitionerForm.value.state.name);
        formData.append("country", this.AddPractitionerForm.value.country.name);
        formData.append("zipcode", this.AddPractitionerForm.value.postalCode);
        formData.append("description", this.AddPractitionerForm.value.description);        
        formData.append("business_name", this.AddPractitionerForm.value.bname);  
        formData.append("is_add_from_admin", 'true');
        formData.append("is_practitioner_registration", 'true');
        formData.append("documents", this.documents);
        formData.append("certitifcations", this.certitifcations);
        let expertiseData: any = '';
        if (this.AddPractitionerForm.value.expertise.length) {
          this.AddPractitionerForm.value.expertise.forEach(element => {
            if (element) {
              expertiseData =  expertiseData === '' ? element.interest :  expertiseData + ',' +  element.interest;
            }
          });
        } else {
          expertiseData = '';
        }
        formData.append("expertise", expertiseData);
        console.log(formData);
        this.loader.show();
        this.apiService.addPractitioner(formData)
          .subscribe((res: any) => {
            this.zoomUserCreation(this.AddPractitionerForm.value.email, this.AddPractitionerForm.value.fname, this.AddPractitionerForm.value.lname);
            this.toastr.success(res?.detail);
            this.loader.hide();
          }, (error) => {
            if (error['error']['non_field_errors']) {
              error['error']['non_field_errors'].forEach(element => {
                this.toastr.error(element);
              });
            } else if (error['error']['phonenumber']) {
              error['error']['phonenumber'].forEach(element => {
                this.toastr.error(element);
              });
            } else if (error['error']['email']) {
              error['error']['email'].forEach(element => {
                this.toastr.error(element);
              });
            } else if (error['error']['zipcode']) {
              error['error']['zipcode'].forEach(element => {
                this.toastr.error(element);
              });
            } else {
              this.toastr.error('Something went wrong please try again');
            }
            this.loader.hide();
          });
      }
    }
  }
  zoomUserCreation(resEmail, fname, lname){
    this.loader.show();
    this.apiService
      .zoomUserCreation(resEmail, fname, lname, 1)
      .subscribe((response: any) => {
        console.log('zoomUserCreation', response);
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.loader.hide();
      });
  }
  onDocFileSelected(event) {
    const file = event.target.files[0];
    this.documents = file;
  }

  onCetFileSelected(event) {
    
    const file = event.target.files[0];
    this.certitifcations = file;
  }

  loadCountry() {
    this.loader.show();
    this.thirpApiService
      .getCountries("id,phone_code", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.countryData = response;
        this.loader.hide();
      });
  }
  loadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.stateData = response;
        this.loader.hide();
      });
  }
  loadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.cityData = response;
        this.loader.hide();
      });
  }
  //on changes events
  countryChanged(data: any) {
    console.log(data);
    if (data) {
      if (data.phone_code.slice(0, 1) == "+") {
        this.AddPractitionerForm.patchValue({
          phoneCode: `${data.phone_code}`
        });
      } else {
        this.AddPractitionerForm.patchValue({
          phoneCode: `+${data.phone_code}`
        });
      }
      this.stateData = [];
      this.cityData = [];
      this.loadState(data.id);
    } else {
      this.stateData = [];
      this.cityData = [];
    }
  }
  stateChanged(data: any) {
    console.log(data);
    if (data) {
      this.cityData = [];
      this.loadCity(data.id);
    } else {
      this.cityData = [];
    }
  }
  cityChanged(data: any) {
    console.log(data);
  }

  expertiseChanged(data) {
    console.log(data);
  }

}