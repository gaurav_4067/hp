import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-add-new-customer-payment',
  templateUrl: './add-new-customer-payment.component.html',
  styleUrls: ['./add-new-customer-payment.component.scss']
})
export class AddNewCustomerPaymentComponent implements OnInit {
  @ViewChild('input2') input2: ElementRef;
  @ViewChild('input3') input3: ElementRef;
  @ViewChild('input4') input4: ElementRef;
  
  paymentForm: FormGroup;
  month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', 10, 11, 12];
  year = [2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030];
  selectedYear = 'Year';
  selectedMonth = 'Month';
  formSubmitted = false;
  isCardNumberFocused = false;

  @Input() isShowBackButton: boolean;
  @Input() buttonText: string;
  @Output() saveCardDetails = new EventEmitter<any>();
  @Output() goBack = new EventEmitter<any>();
  cardMethods: any = [];

  constructor(public apiService: ApiService,
    private fb: FormBuilder,
    private loader: NgxSpinnerService,
    public toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.paymentForm = this.fb.group({
      fullName: ['', Validators.required],
      cardHolderName: ['', Validators.required],
      billingAddr: ['', Validators.required],
      cardNumPart1: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      cardNumPart2: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?([0-9]\d*)?$/)]],
      cardNumPart3: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?([0-9]\d*)?$/)]],
      cardNumPart4: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?([0-9]\d*)?$/)]],
      city: ['', Validators.required],
      zipCode: ['', Validators.required],
      cvvNumber: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(3), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
    });
    this.getAllCardInfo();
  }
  onGoBackBtnClick() {
    this.goBack.emit(true);
  }

  getAllCardInfo(){
    this.loader.show();
    this.apiService
      .getCardMethods()
      .subscribe((response: any) => {
        // console.log(response);
        this.cardMethods = response;
        // console.log(this.cardMethods[3]["id"])
        this.loader.hide();
      });
  }

  addNewPaymentDetails(paymentDetails) {
    console.log('paymentDetails', paymentDetails)
    this.formSubmitted = true;
    if (this.paymentForm.valid && this.selectedMonth !== 'Month' && this.selectedYear !== 'Year') {
      const payload = {
        "cc_number": paymentDetails.value.cardNumPart1 + paymentDetails.value.cardNumPart2 + paymentDetails.value.cardNumPart3 + paymentDetails.value.cardNumPart4,
        "cc_expiry": this.selectedMonth + '/' + this.selectedYear,
        "is_default": true,
        "billing_address": paymentDetails.value.billingAddr,
        "city": paymentDetails.value.city,
        "zip_code": paymentDetails.value.zipCode,
        "account_holder_name": paymentDetails.value.fullName,
        "account_holder_type": null,
        "routing_number": null,
        "account_number": null,
        "customer": JSON.parse(localStorage.getItem("logedin_data")).id,
        "payment_methods": this.cardMethods[3]["id"],
        "cvc": paymentDetails.value.cvvNumber
      };
      this.saveNewCardDetails(payload);
      this.saveCardDetails.emit(payload);
    }
  }

  saveNewCardDetails(payload){
    this.apiService.createCardDetails(payload)
    .subscribe((card)=>{
      console.log('saveNewCardDetails', card);
    }, error =>{
      console.log('saveNewCardDetails error', error);
    });
  }

  cardNumberFocused(){
    this.isCardNumberFocused = true;
  }

  cardNumberFocuseOut(){
    this.isCardNumberFocused = false;
  }

  onInputEntry(event, feild) {
    let input = event.target;
    let length = input.value.length;
    let maxLength = input.attributes.maxlength.value;
  
    if (length >= maxLength) {
      if(feild==2)
        this.input2.nativeElement.focus();
      else if(feild==3)
        this.input3.nativeElement.focus();
      else if(feild==4)
        this.input4.nativeElement.focus();
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  
  }

  selectMonth(m) {
    this.selectedMonth = m;
  }

  validateExpiry(){
    
    let curYear = new Date().getFullYear();
    if (this.selectedYear !='Year' && this.selectedMonth !='Month') {
      if (parseInt(this.selectedYear, 10) > curYear) {

      } else {
        let curMonth = new Date().getMonth() + 1;
        if (parseInt(this.selectedMonth, 10) < curMonth) {
          this.toastr.error(`Please provide valid expiry.`);
          this.selectedMonth="Month";
        }
      }
    }
  }
}
