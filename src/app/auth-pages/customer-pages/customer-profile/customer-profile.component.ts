import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { ProfilePictureModalComponent } from '../../popup-modals/profile-picture-modal/profile-picture-modal.component';

@Component({
  selector: 'app-customer-profile',
  templateUrl: './customer-profile.component.html',
  styleUrls: ['./customer-profile.component.scss']
})
export class CustomerProfileComponent implements OnInit {
  userData: any;
  logedinData: any;
  isContent: string;
  model: any;
  selectedInterest:any;
  profilePictureModal = ProfilePictureModalComponent;

  countryData: any = [];
  stateData: any = [];
  cityData: any = [];
  msgPopId: any;
  countryConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select Country *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  stateConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select State *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  cityConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select City *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  interests: Array<Object> = [{}];
  userProfile = {
    first_name: "",
    last_name: "",
    email: "",
    phone_number: "",
    country_name: "",
    state_name: "",
    city_name: "",
    zipcode: "",
  };
  profileForm: FormGroup;
  
  constructor(
    public apiService: ApiService,
    public router: Router,
    public thirpApiService: ThirdPApiService,
    public auth: AuthService,
    public generalService: GeneralService,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.generalService.showProfile=false;
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.userProfile = {
      first_name: this.logedinData.user_id.first_name ? this.logedinData.user_id.first_name : "",
      last_name: this.logedinData.user_id.last_name ? this.logedinData.user_id.last_name : "",
      email: this.logedinData.user_id.email ? this.logedinData.user_id.email : "",
      phone_number: this.logedinData.phone_number ? this.logedinData.phone_number : "",
      country_name: this.logedinData?.address?.city?.country?.name ? this.logedinData.address.city.country.name : "",
      state_name: this.logedinData?.address?.city?.state?.name ? this.logedinData.address.city.state.name : "",
      city_name: this.logedinData?.address?.city?.name ? this.logedinData.address.city.name : "",
      zipcode: this.logedinData?.address?.zipcode ? this.logedinData.address.zipcode : "",
    };
    this.fetchInterests();
    this.loadCountry();
  }
  fetchInterests() {
    this.apiService
      .getIntrest()
      .subscribe((res: any) => {
        if (res) {
          let tempData = [];
          this.logedinData["interest"].forEach(element => {
            //
            if (element) {
              let result = res.filter(obj => obj.interest === element);
              if (result.length) {
                tempData.push(result[0]);
              }
            }
          });
          this.interests = tempData;
          console.log(this.interests);
        }
        this.loader.hide();
      },
        (errResponse: HttpErrorResponse) => {
          this.loader.hide();
        });
  }
  loadCountry() {
    this.loader.show();
    this.thirpApiService
      .getCountries("id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.countryData = response;
        if(this.logedinData.address){
          if(this.logedinData.address.city && this.logedinData.address.city.country){
            this.ifloadState(this.logedinData.address.city.country.id);
          }
        }
        this.loader.hide();
      });
  }
  ifloadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.stateData = response;
        if(this.logedinData.address.city.state){
          this.ifloadCity(this.logedinData.address.city.state.id);
        }
        this.loader.hide();
      });
  }
  ifloadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.cityData = response;
        this.loader.hide();
      });
  }
  loadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.stateData = response;
        this.loader.hide();
      });
  }
  loadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.cityData = response;
        this.loader.hide();
      });
  }
  //on changes events
  countryChanged(data: any) {
    console.log(data);
    if (data) {
      this.stateData = [];
      this.cityData = [];
      this.userProfile.state_name="";
      this.userProfile.city_name="";
      // this.user.state = [];
      // this.user.city = [];
      this.loadState(data.id);
    } else {
      this.stateData = [];
      this.cityData = [];
      // this.user.state = [];
      // this.user.city = [];
    }
  }
  stateChanged(data: any) {
    console.log(data);
    if (data) {
      this.cityData = [];
      this.userProfile.city_name="";
      // this.user.city = [];
      this.loadCity(data.id);
    } else {
      this.cityData = [];
      // this.user.city = [];
    }
  }
  cityChanged(data: any) {
    console.log(data);
  }
  selectMsg(message: any) {
    console.log(message);
  }
  profileSubmit() {
    //
    // console.log(this.userProfile.city_name);
    let payload = this.logedinData;
    var userAddress = {
      "address_line1": null,
      "address_line2": "N/A",
      "address_line3": this.userProfile.city_name.length ? this.userProfile.city_name["name"] : "N/A",
      "addrees_line4": this.userProfile.state_name.length ? this.userProfile.state_name["name"] : "N/A",
      "address_line5": this.userProfile.country_name.length ? this.userProfile.country_name["name"] : "N/A",
      "zipcode": this.userProfile.zipcode ? this.userProfile.zipcode : null,
      "address_type": null,
      "city": null,
    };
    if(this.userProfile.city_name["id"]){
      userAddress.city = this.userProfile.city_name["id"];
    }else if(this.userProfile.city_name && this.userProfile.city_name.length){
      var result = this.cityData.find(obj=>obj.name === this.userProfile.city_name);
      console.log(result);
      if(result){
        userAddress.city = result["id"];
      }
    }else{
      userAddress.city = null;
    }
    payload["address"] = userAddress;
    payload["user_id"]["first_name"] = this.userProfile.first_name;
    payload["user_id"]["last_name"] = this.userProfile.last_name;
    payload["user_id"]["email"] = this.userProfile.email;
    payload["phone_number"] = "+"+this.userProfile.phone_number;
    payload["user_id"]["first_name"] = this.userProfile.first_name;
    console.log(payload);
    this.loader.show();
    this.apiService
      .updateProfileInfo(payload, payload["id"])
      .subscribe((response: any) => {
        this.userData["city_id"] = response["address"]["city"];
        localStorage.setItem(
          `user_data`,
          JSON.stringify(this.userData)
        );
        this.loadProfileData();
        this.toastr.success('Profile information updated successfully');
        this.loader.hide();
      });
  }
  loadProfileData(){
    this.loader.show();
    this.apiService
      .pkIdUpdate(this.userData.groups[0].toLowerCase(),"get")
      .subscribe((response: any) => {
        console.log(response);
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response[0])
        );
        this.loader.hide();
      });
  }

  selectInterest(interest:any){
    this.selectedInterest=interest;
    console.log(this.selectedInterest);
  }

  deleteInterest(interest: any) {
    const payload = JSON.parse(localStorage.getItem('logedin_data'));
    payload.interest = payload.interest.filter(item => item !== interest.interest);
    this.loader.show();
    this.apiService
      .updateBasicInfo(payload, payload.id)
      .subscribe((response: any) => {
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response)
        );
        this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
        this.fetchInterests();
        this.loader.hide();
      });
  }
  openModal(modalContent: any, isWhere: string) {
    this.isContent = isWhere;
    this.model = this.modalService.open(modalContent, { windowClass: 'modal-remove-chat', centered: true, scrollable:true, size: 'lg' });
  }
  closeModal(Event: any) {
    this.model.close();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;  
  }
}
