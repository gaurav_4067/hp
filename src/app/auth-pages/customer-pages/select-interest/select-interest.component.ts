import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/ApiService';
import { PractitionerModalContent } from '../../common-pages/sessiononly/practitionermodalcontent.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-select-interest',
  templateUrl: './select-interest.component.html',
  styleUrls: ['./select-interest.component.scss']
})
export class SelectInterestComponent implements OnInit {

  interests: any = [];
  mappedInterest:any=[];
  customer: any;
  selectedInterest: any = [];
  isPage:number = 0;
  constructor(private apiService: ApiService, private modalService: NgbModal, private loader: NgxSpinnerService) {
    this.customer = JSON.parse(localStorage.getItem("logedin_data"));
    if (this.customer) {
      localStorage.setItem("interest", JSON.stringify(this.customer["interest"]));
      this.customer["interest"].forEach(element => {
        this.selectedInterest.push(element);
      });
    }
  }

  ngOnInit(): void {
    this.fetchInterests();
  }

  fetchInterests() {
    this.apiService
      .getIntrest()
      .subscribe((res: any) => {
        if (res) {
          
          this.interests = res.interest_list;
          
          this.interests = res.interest_list.reduce((resultArray, item, index) => { 
            const chunkIndex = Math.floor(index/9)
            if(!resultArray[chunkIndex]) {
              resultArray[chunkIndex] = [] // start a new chunk
            }
          
            resultArray[chunkIndex].push(item);
          
            return resultArray;
          }, []);

          this.mappedInterest=this.interests[0];
        }
        this.loader.hide();
      },
        (err) => {
          this.loader.hide();
        });
  }

  selectInterest(interest) {
    let removed;
    if (this.selectedInterest.find(obj => obj == interest.interest)) {
      removed = this.selectedInterest.filter(obj => obj != interest.interest);
      this.selectedInterest = removed;
    } else {
      this.selectedInterest.push(interest.interest);
    }
    let payload = this.customer;
    payload["interest"] = this.selectedInterest;
    console.log(payload);

    this.apiService
      .updateBasicInfo(payload, payload["id"])
      .subscribe((response: any) => {
        console.log(response);
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response)
        );
        this.openModal();
      });
  }

  slideForward(){
    
    if (this.isPage < this.interests.length-1) {
      this.isPage = this.isPage + 1;
      setTimeout(() => {
        this.mappedInterest = this.interests[this.isPage];
      }, 100);
    }
  }
  slideBackward(){
    
    if (this.isPage >0) {
    this.isPage = this.isPage - 1;
    setTimeout(() => {
      this.mappedInterest = this.interests[this.isPage];
    }, 100);
    }
  }
  checkIfLetMove(){
    
    if(this.isPage == this.interests.length - 1){
      return false;
    }else{
      return true;
    }
  }
  openModal() {
    this.modalService.dismissAll();
    this.modalService.open(PractitionerModalContent, { centered:true, scrollable:true, ariaLabelledBy: 'modal-basic-title', backdrop: 'static', size: 'lg', windowClass: 'practitioner-modal' });
  }

}
