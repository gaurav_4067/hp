import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../services/ApiService';
import { ThirdPApiService } from '../../../services/ThirdPApiService';
import { ZoomMeetingConfig } from '../../../config/config';
import { AuthService } from '../../../services/AuthService';
import { GeneralService } from '../../../services/GeneralService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['./customer-dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CustomerDashboardComponent implements OnInit, OnDestroy {
  userData: any;
  logedinData:any;
  chats: Array<any> = [];
  interests: Array<any> = [];
  name: string;
  customer: any = {};
  interestData = [];
  interestSub: Subscription;
  chartData = [{
    data: Array(7).fill(0),
    label: 'Check ins',
    barThickness: 18,
    barPercentage: 0.1,
    categoryPercentage: 0.1
  }];

  selectedInterest:any;
  isLoaded:boolean=false;
  constructor(
    public apiService: ApiService,
    public router: Router,
    public thirdPApiService: ThirdPApiService,
    public auth: AuthService,
    public generalService: GeneralService,
    private loader: NgxSpinnerService,
    private toaster: ToastrService,
  ) { }

  ngOnInit() {
    console.log('Customer Dashboard');    
    this.userData = JSON.parse(localStorage.getItem('user_data'));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    // this.generalService.setTitle(this.userData?.user.first_name);
    this.name = this.userData?.user.first_name + ' ' + this.userData?.user.last_name;
    this.fetchDetails();
    this.fetchChats();
  }
  fetchDetails() {
    this.apiService
      .getCustomerDetails()
      .subscribe((response: any) => {
        if (response) {
          
          this.customer = response.stats_value;
          //this.customer?.upcoming_session?.data.length>0
          this.isLoaded=true;
          const arr = Array(7).fill(0);
          arr[new Date().getDay()] = this.customer.check_ins;
          this.chartData[0].data = arr;
          this.getCustomerInterestList();
        }
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.loader.hide();
      });
  }
  fetchChats() {
    this.apiService
      .getCustomerChats()
      .subscribe((res: any) => {
        if (res) {
          // this.chats = res.data;
          this.chats = this.filterUnwanted(res.data);
        }
        this.loader.hide();
      },
        (errResponse: HttpErrorResponse) => {
          this.loader.hide();
        });
  }


filterUnwanted = (arr) => {
   const required = arr.filter(el => {
      return el.channelname;
   });
   return required;
};


  getCustomerInterestList() {
    debugger;
    // const interests = this.customer.interest[0] || [];
    let payload = JSON.parse(localStorage.getItem('logedin_data'));
    //const interests = payload.interest[0];
    let param = payload.interest.map(str => `interest_list=${str}`).join('&');
    this.apiService.getCustomerInterestList(param).subscribe((data: any) => {
      this.interestData = data.interest_list;
    });
  }
  // getInterestData() {
  //   const interests = this.customer.interest[0] || [];
  //   this.interestSub = this.apiService.getInterestList(...interests).subscribe((data: any) => {
  //     this.interestData = [...data.interest_list];
  //   });
  // }
  upcomingSession() {
    const session = this.customer?.upcoming_session?.data;

    if (!session || !session.length) {
      return {};
    }
    return session[0];
  }
  navigateToSessionPage(session) {
    const userData = JSON.parse(localStorage.getItem('user_data'));
    const isGroup = userData?.groups[0].toLowerCase();
    const userName = userData?.user.username;
    const email = userData?.user.email;
    this.loader.show();
    this.thirdPApiService.getZoomMeetingList(session.id)
      .subscribe((response: any) => {
        console.log('response', response);
        this.loader.hide();
        if (response && response.length > 0) {
          window.open(`${ZoomMeetingConfig.ZOOM_SESSION_APP_URL}?user=${userName}&id=${response[0].meeting_id}&auth=${this.auth.getToken()}&email=${email}&group=${isGroup}&pid=${session.practitioner_id}&aid=${session.id}`, '_blank');
        } else {
          this.toaster.error('No meetings id available');
        }
      }, (error) => {
        console.error(error);
        this.loader.hide();
      });
  }
  navigateToChatPage(session) {
    this.router.navigate(['chat'],{ queryParams: { selected: session.practitioner__user_id, redirect: true }});

  }

  goToChat(id){
    this.router.navigate(['chat'],{ queryParams: { selected: id, redirect: true }});

  }

  goToPractitioner(id){
    this.router.navigate(['customer/practitioner-profile'],{ queryParams: { pid: id, redirect: true }});
  }
  getPrice() {
    const session = this.upcomingSession();

    if (!session.start_time) {
      return 'Included';
    }

    const startTime = session.start_time;
    const endTime = session.end_time;

    const [h1, m1] = startTime.split(':').map(Number);
    const [h2, m2] = endTime.split(':').map(Number);

    const h2Final = h2 < h1 ? h2 + 24 : h2;

    const totalS1 = m1 + 60 * h1;
    const totalS2 = m2 + 60 * h2Final;

    if (totalS2 - totalS1 > 30) {
      return `$${session.price}`;
    }
    return 'Included';
  }

  getDuration() {
    const session = this.upcomingSession();

    if (!session.start_time) {
      return '30 min';
    }

    const startTime = session.start_time;
    const endTime = session.end_time;

    const [h1, m1] = startTime.split(':').map(Number);
    const [h2, m2] = endTime.split(':').map(Number);

    const h2Final = h2 < h1 ? h2 + 24 : h2;

    const totalS1 = m1 + 60 * h1;
    const totalS2 = m2 + 60 * h2Final;

    return this.convertToHrs(totalS2 - totalS1);
  }

  getAppointmentDate() {
    const appDate = this.upcomingSession().appointment_date;

    if (!appDate) {
      return '';
    }

    const [y, m, d] = appDate.split('-');
    const month = months[Number(m) - 1];

    return `${Number(d)} ${month}`;
  }

  getAppointmentTime() {
    const time = this.upcomingSession().start_time;

    if (!time) {
      return '';
    }

    const [h, m] = time.split(':');

    if (Number(h) > 12) {
      return `${h - 12}:${m} pm`;
    }
    return `${h}:${m} ${Number(h) === 12 ? 'pm' : 'am'}`;
  }

  convertToHrs(min: number | null) {
    if (min == null) { return ''; }
    const hrs = Math.floor(min / 60);
    const rem = min % 60;
    let res = '';

    if (hrs > 0) {
      res += `${hrs}h`;
    }
    if (rem > 0) {
      res += ` ${rem}min`;
    }
    return res;
  }

  getDate(chatDate: string) {
    const date = new Date(chatDate);
    return `${date.getDate()} ${months[date.getMonth()]}`;
  }

  getTime(chatDate: string) {
    const date = new Date(chatDate);
    let hours = date.getHours();
    let minutes: string | number = date.getMinutes();
    let time = 'am';
    if (hours > 11) {
      time = 'pm';
    }

    if (hours > 12) {
      hours -= 12;
    } else if (hours === 0) {
      hours = 12;
    }

    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    return `${hours}:${minutes} ${time}`;
  }

  ellipsisify(str: string) {
    if (str.length <= 18) {
      return str;
    }
    return `${str.slice(0, 18)}...`;
  }

  selectInterest(interest:any){
    this.selectedInterest=interest;
    console.log(this.selectedInterest);
  }

  deleteInterest(interest: any) {
    const payload = JSON.parse(localStorage.getItem('logedin_data'));
    payload.interest = payload.interest.filter(item => item !== interest.interest);
    this.loader.show();
    this.apiService
      .updateBasicInfo(payload, payload.id)
      .subscribe((response: any) => {
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response)
        );
        this.customer.interest[0] = response.interest;
        this.getCustomerInterestList();
        this.loader.hide();
      });
  }

  ngOnDestroy() {
    this.interestSub?.unsubscribe();
  }
}
