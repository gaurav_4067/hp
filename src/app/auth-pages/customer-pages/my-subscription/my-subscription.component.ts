import { Component, TemplateRef, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/services/AuthService';
import { SubscriptionPlansService } from 'src/app/services/SubscriptionPlansService';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SocialAuthService } from "angularx-social-login";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { GeneralService } from 'src/app/services/GeneralService';
import { YesnoModalComponent } from 'src/app/v1/common/view/yesno-modal/yesno-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from 'src/app/v2/confirm-modal/confirm-modal.component';

@Component({
    selector: 'mysubscription',
    templateUrl: './my-subscription-component.html',
    styleUrls: ['./my-subscription.component.scss'],
    providers: [AuthService],
    encapsulation: ViewEncapsulation.None,
})

export class MySubscriptionComponent implements OnInit {
    @Input() phase2Request: boolean=false;
    selectedItem: string = '0';
    showPackageItems: string = 'Monthly Pricing';
    PlanData: any;
    plan_input = "Monthly Pricing";
    customerName = JSON.parse(localStorage.getItem("user_data"));
    subscriptionPlans = [];
    MonthlyData = [];
    ThreeMonthData = [];
    SixMonthData = [];
    AnnualData = [];
    request: any;
    message: any = "";
    subscriptionDetails;
    showOrderConfirmationPage = false;
    selectedPaymentType = null;
    selectedPlan;
    showAddNewPayment = false;
    isShowBackButton = true;
    buttonText = 'Save';
    planDetails;
    paymentCardDetails;
    modalRef: BsModalRef;
    newSubscriptionDetails;
    stripeDetails;
    logedinData: any;
    isPlanActive:boolean=false;
    showWaitMessage:boolean=false;
    msg:string="Please wait...";
    constructor(private router: Router,
        private authService: AuthService,
        private plansservice: SubscriptionPlansService,
        public apiService: ApiService,
        private loader: NgxSpinnerService,
        private toaster: ToastrService,
        private socialAuthService: SocialAuthService,
        private modalService: BsModalService,
        public myModal:NgbModal,
        public generalService: GeneralService,
    ) { }


    ngOnInit() {
        this.generalService.showProfile=false;
        this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
        
        this.getPlanData(this.plan_input);
        this.getSubscription();
        this.customerpaymentdetails();
    }

    openModal(template: TemplateRef<any>) {
        if(this.planDetails){
            let modalRef = this.myModal.open(YesnoModalComponent, { scrollable: true, centered: true, ariaLabelledBy: 'signup-confirm-title', size: 'lg', windowClass: 'confirm-modal' });
            modalRef.componentInstance.showButton = true;
            modalRef.componentInstance.heading = "Are you sure you want to cancel your Healplace subscription?";
            modalRef.componentInstance.yesButtontext = "Cancel Subscription";
            modalRef.componentInstance.noButtontext = "Keep Subscription";
            modalRef.componentInstance.subscriptionDetails=this.subscriptionDetails;
            // this.modalRef = this.modalService.show(template, {class: 'cancel-subscription modal-sm'});
            
            // document.getElementsByClassName('cancel-subscription')[0].parentElement.style.backgroundColor = 'none !important';
            // document.getElementsByClassName('cancel-subscription')[0].parentElement.style.opacity = 'none !important';
        }
      }

    saveCardDetails(e) {
        this.paymentCardDetails = e;
        console.log('this.paymentCardDetails', this.paymentCardDetails);
        this.showAddNewPayment = false;
        this.addSubscription(true);
    }

    goBackToConfirmScreen(e) {
        this.showAddNewPayment = false;
    }
    choosePaymentType(type) {
        this.selectedPaymentType = type;
        if (type === 'new') {
            this.showAddNewPayment = true;
        }
        if (type === 'paypal') {
            this.toaster.warning('Choose other payment options');
        }
    }

    confirm(): void {
        this.cancelSubscription();
        this.modalRef.hide();
    }
     
    decline(): void {
        this.modalRef.hide();
    }

    getPlanData(plan_input) {
        
        this.loader.show();
        this.plansservice.getsubscriptionplans(plan_input).subscribe(
            response => {
                this.PlanData = response;
                this.subscriptionPlans=this.PlanData.plan.slice(0,3);
                // if (plan_input == '3 Months') {
                //     this.ThreeMonthData = this.PlanData.plan;
                // }
                // if (plan_input == '6 Months') {
                //     this.SixMonthData = this.PlanData.plan;
                // }
                // if (plan_input == 'Monthly Pricing') {
                //     this.MonthlyData = this.PlanData.plan;
                // }
                // if (plan_input == 'Annually') {
                //     this.AnnualData = this.PlanData.plan;
                // }
                this.loader.hide();
            },
            error => {
                console.log(error);
                this.loader.hide();
                this.toaster.error(error.statusText);
            }
        );
    }

    getSubscription() {
        this.loader.show();
        this.plansservice.getsubscription()
            .subscribe((response: any) => {
                if (response && response.length > 0) {
                    this.getPlanDetails(response[0]);
                }
                else{
                    this.msg="Currently you’ve not subscribed for any plan";
                }
                console.log('response', response[0]);
            }, (error) => {
                this.loader.hide();
                this.toaster.error(error.statusText);
                console.log('error', error);
            });
    }

    getPlanDetails(data) {
        
        this.subscriptionDetails = data;
        this.plansservice.getplanDetails(data?.plan_id)
            .subscribe((details: any) => {
                
                if(details.plan.length>0){
                    this.isPlanActive=true;
                }
                else{
                    this.msg="Currently you’ve not subscribed for any plan";
                }

                
                this.planDetails = details.plan[0];
                console.log('details', details);
                this.loader.hide();
            }, (error) => {
                this.loader.hide();
                this.toaster.error(error.statusText);
                console.log('error', error);
            });
    }

    cancelSubscription() {
        this.loader.show();
        const payload = {
            offer_id: this.subscriptionDetails?.offer_id,
            plan_id: this.subscriptionDetails?.plan_id,
            unsubscribe_date: new Date(),
            reason: '',
            customer: JSON.parse(localStorage.getItem("logedin_data")).id,
            parent_id: null
        };
        this.plansservice.removeSubscription(payload, this.subscriptionDetails?.id)
        .subscribe((sub)=>{
            console.log('cancelSubscription', sub);
            this.loader.hide();
            this.toaster.success('Subscription Cancelled Successfully!');
            let logedinData = JSON.parse(localStorage.getItem("logedin_data"));
            logedinData.is_subscribed = false;  
   
            localStorage.setItem(
                `logedin_data`,
                JSON.stringify(logedinData)
            );
            this.generalService.getUserSubscription("Unsubscribed");
            this.router.navigate(['sessiononly']);
            
        }, (error)=>{
            console.log('cancelSubscription error', error);
            this.loader.hide();
            this.toaster.error(error.statusText);
        });
    }

    private messageSource = new BehaviorSubject(this.message);
    currentMessage = this.messageSource.asObservable();
    public set value(v: string) {
        this.message = this.messageSource;
    }

    ChooseClick(data, planid) {
        this.selectedItem = data;
        this.selectedPlan = planid;
        console.log('planid', planid, this.customerName);
        
        this.showOrderConfirmationPage = true;
        this.choosePaymentType('card');
    }

    ShowPackage(data, apiInput) {
        this.showPackageItems = data;
        this.getPlanData(apiInput);

    }

    addSubscription(flag) {
        this.showWaitMessage=true;
        this.stripeCheckout(flag);
    }

    updateSub(){
        if (this.selectedPlan && this.selectedPlan.id) {
            this.loader.show();
            const payload = {
                offer_id: this.selectedPlan.id,
                plan_id: this.selectedPlan.id,
                unsubscribe_date: null,
                reason: '',
                customer: JSON.parse(localStorage.getItem("logedin_data")).id,
                parent_id: null
            };
            this.plansservice.addSubscription(payload)
                .subscribe((sub) => {
                    this.newSubscriptionDetails = sub;
                    console.log('sub', sub);
                    this.customerPayment(this.stripeDetails);
                    // this.toaster.success('Subscription Added Successfully!');
                    // this.loader.hide();
                }, (error) => {
                    console.log('sub error', error);
                    if(error.error && error.error.exception){
                        this.toaster.error(error.error.exception);
                    } else {
                        this.toaster.error(error.error);
                    }
                    this.showWaitMessage=false;
                    this.loader.hide();
                });
        }
    }

    customerpaymentdetails() {
        this.loader.show();
        this.apiService.customerpaymentdetails()
            .subscribe((paymentCardDetails: any) => {
                console.log('paymentCardDetails', paymentCardDetails);
                paymentCardDetails.forEach(element => {
                    if (element && element.is_default) {
                        this.paymentCardDetails = element;
                    }
                });
                this.loader.hide();
            }, (error) => {
                console.log('paymentCardDetails error', error);
                this.toaster.error(error.statusText);
                this.loader.hide();
            });
    }
    stripeCheckout(flag) {
        this.loader.show();
        const m = this.paymentCardDetails.cc_expiry.split('/');
        const payload = {
            'cc_number': this.paymentCardDetails.cc_number,
            'exp_month': flag ? m[0] : this.paymentCardDetails.cc_expiry.slice(0, 2),
            'exp_year': flag ? m[1] : this.paymentCardDetails.cc_expiry.slice(3),
            'cvc': parseInt(flag? this.paymentCardDetails?.cvc : 316),
            'session_amount': this.selectedPlan.sale_price,
            'description': '',
            'country': 'US'
        };
        this.apiService.stripeCheckout(payload)
            .subscribe((stripe: any) => {
                console.log('stripe', stripe);
                if (stripe && stripe.has_paid) {
                    this.stripeDetails = stripe;
                    this.logedinData.is_subscribed = true;
                    localStorage.setItem(
                        `logedin_data`,
                        JSON.stringify(this.logedinData)
                    );
                    this.updateSub();
                } else {
                    this.showWaitMessage=false;
                    this.loader.hide();
                }
            }, (error) => {
                console.log('stripeCheckout error', error);
                this.showWaitMessage=false;
                this.loader.hide();
                this.toaster.error(error.statusText);
            });
    }
    customerPayment(stripe){
        this.loader.show();
        const payload = {
            'transactionid': stripe.transaction_id,
            'haspaid': stripe.has_paid,
            'paymentinfo': '',
            'subscription': this.newSubscriptionDetails.id,
            'customer': JSON.parse(localStorage.getItem("logedin_data")).id,
            'appointment': null
          };
        this.apiService.confirmingAppointment(payload)
            .subscribe((confrimpay: any) => {
                console.log('confrimpay', confrimpay);
                this.toaster.success('Subscription Added Successfully!');
                // this.toaster.success('Amount Paid Successfully!');
                this.loader.hide();
                this.showWaitMessage=false;
                this.generalService.getUserSubscription("Subscribed");
                this.router.navigate(['welcome-customer']);
                let modalRef =this.myModal.open(ConfirmModalComponent, {scrollable:true, centered: true ,ariaLabelledBy: 'signup-confirm-title',  size: 'lg', windowClass: 'confirm-modal' });
                modalRef.componentInstance.showButton = true;
                modalRef.componentInstance.isHighlight=true;
                modalRef.componentInstance.showCurrentPlanSection=true;
                modalRef.componentInstance.message = "";
                modalRef.componentInstance.closeButtonText ="Start Exploring Our Features";
                modalRef.componentInstance.heading = "Thank you for your order. You now have access to all our premium features.";
            }, (error) => {
                this.showWaitMessage=false;
                console.log('confrimpay error', error);
                this.loader.hide();
                this.toaster.error(error.statusText);
            });
    }
}