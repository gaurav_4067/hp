import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerPractitionerProfileComponent } from './customer-practitioner-profile.component';

describe('CustomerPractitionerProfileComponent', () => {
  let component: CustomerPractitionerProfileComponent;
  let fixture: ComponentFixture<CustomerPractitionerProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerPractitionerProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerPractitionerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
