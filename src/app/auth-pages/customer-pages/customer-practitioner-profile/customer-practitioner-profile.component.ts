import { Component, OnInit } from '@angular/core';
import '../../../services/dateExtension';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from '../../../services/ApiService';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { GeneralService } from 'src/app/services/GeneralService';
import { HttpErrorResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from '../customer-booking/ngbdmodalcontent.component';

@Component({
  selector: 'app-customer-practitioner-profile',
  templateUrl: './customer-practitioner-profile.component.html',
  styleUrls: ['./customer-practitioner-profile.component.scss']
})
export class CustomerPractitionerProfileComponent implements OnInit {
  settings = {
    availability: [],    
    isMultiple: false,
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    selectedDates: [],
    startDate: null,
    weekdays: ['sun', 'mon', 'tue', 'wed', 'thurs', 'fri', 'sat'],
  }
  selectedList:any;
  msg:string="Please wait..."
  practitionerDetail:any={"practitioner_expertise":[],"practitioner_profile":[],"practitioner_sessions":[]};
  days = [];
  schedule = [];
  week: string;
  pid:string="";
  constructor(
    private route: ActivatedRoute,
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    private router: Router,
    public generalService: GeneralService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    
    const curr = new Date; // get current date
    const first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    const firstday = new Date(curr.setDate(first)).toUTCString();
    this.settings.startDate = new Date(firstday);
    
    this.settings.availability = this.getTime(0, 24);
    this.route.queryParams.subscribe(params => {
      if (params.redirect) {
        this.generalService.showProfile=true;
        this.pid=params.pid;
        this.generalService.selectedPractitioner = params.pid;
        // this.pid="e98f0e7b-def2-4c09-a863-62bc5f1c7032";
        this.getPractitionerDetail(this.pid);
        this.getDatesHeader();
      }
    });
  }

  getPractitionerDetail(id) {
    this.apiService
      .getPractitionerDetail(id)
      .subscribe((response: any) => {
        if (response) {
          
          this.practitionerDetail = response;
          this.generalService.selectedPractitionerDetail = response.practitioner_profile[0];
          this.generalService.selectedPractitionerDetail.practitioner_overall_rating = response.practitioner_overall_rating;
          this.generalService.selectedPractitionerDetail.practitioner_rating_count = response.practitioner_rating_count;

        }
        else {
          this.msg = "Data not available";
        }
      },
        (errResponse: HttpErrorResponse) => {
          this.msg = "Data not available";
        });
  }


  changeWeek(week) {
    this.days = [];

    this.settings.startDate = week.startdate;
    this.getDatesHeader();
  }
  getTime(startTime, endTime) {
    var time = [];
    var currentTime = startTime;
    while (currentTime < endTime) {
      const suffix = (currentTime >= 12) ? 'pm' : 'am';
      const tempTime = ((currentTime + 11) % 12 + 1);
      time.push({
        displayTime: tempTime + ':00' + suffix,
        time: currentTime + ':00',
      });
      currentTime++;
    }
    return time;
  }

  getMonthName(idx) {
    return this.settings.months[idx];
  };

  getDatesHeader() {
    for (let i = 0; i < 7; i++) {
      var d = this.settings.startDate.addDays(i);
      this.days.push({
        fullDate: d,
        date: d.getDate(),
        day: this.settings.weekdays[d.getDay()],
        month: this.settings.startDate.getMonth() + 1,
        monthName: this.getMonthName(this.settings.startDate.getMonth()),
        utc: d.toJSON().slice(0, 10),
        available: [],
        disable: []
      })
    }
    this.getNavControl();
  }
  getNavControl() {
    const firstday = this.days[0].day + ', ' + this.days[0].monthName + ' ' + this.days[0].date;
    const lstday = this.days.length - 1;
    const lastday = this.days[lstday].day + ', ' + this.days[lstday].monthName + ' ' + this.days[lstday].date;
    this.week = firstday + ' - ' + lastday;
    this.fetchSchedule(this.days[0].utc, this.days[lstday].utc);
  };
  previousWeek() {
    this.days = [];

    this.settings.startDate = this.settings.startDate.addDays(-7);
    this.getDatesHeader();
  }
  nextWeek() {
    this.days = [];
    this.settings.startDate = this.settings.startDate.addDays(7);
    this.getDatesHeader();
  }

  formatDate(d) {
    var date = '' + d.getDate();
    var month = '' + (d.getMonth() + 1);
    var year = d.getFullYear();
    if (date.length < 2) {
      date = '0' + date;
    }
    if (month.length < 2) {
      month = '0' + month;
    }
    return year + '-' + month + '-' + date;
  }

  fetchSchedule(start_date, end_date) {
    this.loader.show();
    this.apiService.getPractitionerAvailabelSchedule(start_date, end_date,this.pid).subscribe((response: any) => {
      this.schedule = response;
      console.log('this.schedule', this.schedule);
      function ConvertNumberToTwoDigitString(n) {
        return n > 9 ? "" + n : "0" + n;
      }
      this.schedule.forEach((item) => {
        let start_time = new Date(item.start_time).getUTCHours();
        let end_time = new Date(item.end_time).getUTCHours();
        const dt = new Date(item.start_time).getDate();
        let time = [];
        while (start_time <= end_time) {
          time.push(ConvertNumberToTwoDigitString(start_time++) + ':00');
        }
        const times = this.days.filter((day) => {
          return day.date === dt;
        });
        if (times && times.length > 0 && time.length > 0 && item.is_from_appointment) {
          times[0].available = times[0].available.concat(time);
        }
        if (times && times.length > 0 && time.length > 0 && !item.is_available) {
          times[0].disable = times[0].disable.concat(time);
        }
      });
      this.loader.hide();
    },
      (errResponse) => {
        this.loader.hide();
      }
    );
  }

  editSchedule() {
    console.log('this.settings.startDate', this.settings.startDate);
    this.router.navigate(['practitioner/edit-schedule/'], { queryParams: { date: moment(this.settings.startDate).format('YYYY-MM-DD') }})
  }

  bookSession() {
    this.modalService.dismissAll();
    const modalRef = this.modalService.open(NgbdModalContent, { centered: true ,ariaLabelledBy: 'modal-basic-title', backdrop: 'static', size: 'lg', windowClass: 'book-session modal-remove-chat' });
    modalRef.componentInstance.selectedList = { id: this.pid, isFromZoomSession: true };
  }
};
