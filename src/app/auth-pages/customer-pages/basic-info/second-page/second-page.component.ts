import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';

@Component({
  selector: 'app-second-page',
  templateUrl: './second-page.component.html',
  styleUrls: ['./second-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SecondPageComponent implements OnInit {
  userData: any;
  interestData = [];
  slideShowData = [];
  logedinData: any;
  interest:any = [];
  isPage:number = 0;
  constructor(
    public apiService : ApiService,
    public router : Router,
    public loader: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if(localStorage.getItem("interest")){
      this.interest = JSON.parse(localStorage.getItem("interest"));
    }
    this.loadData();
  }
  loadData(){
    this.loader.show();
    this.apiService
    .getIntrest()
    .subscribe((res: any) => {
      let response=res.interest_list;
      console.log(response);
      let mapedData = [];
      for (var i = 0; i < response.length; i += 4) {
        let temp = [];
        temp.push(response[i]);
        if(response[i+1]){
          temp.push(response[i+1]);
        }
        if(response[i+2]){
          temp.push(response[i+2]);
        }
        if(response[i+3]){
          temp.push(response[i+3]);
        }
        mapedData.push(temp);
      }
      console.log(mapedData);
      this.slideShowData = mapedData;
      this.interestData = mapedData[0];
      this.loader.hide();
    });
  }
  chooseMe(intrestID: any){
    
    let refContent = document.getElementById(`${intrestID}`) as HTMLInputElement;
    if(refContent.className == "col-md-8 color-fix fix-shadow active"){
      refContent.className = "col-md-8 color-fix";
      if(this.interest.find(obj=>obj == intrestID)){
        let removed = this.interest.filter(obj=>obj != intrestID);
        this.interest = removed;
      }
      localStorage.removeItem("interest");
      localStorage.setItem("interest", JSON.stringify(this.interest));
    }else{
      refContent.className = "col-md-8 color-fix fix-shadow active";
      if(this.interest.length){
        if(!this.interest.find(obj=>obj == intrestID)){
          this.interest.push(intrestID);
        }
      }else{
        this.interest.push(intrestID);
      }
      localStorage.removeItem("interest");
      localStorage.setItem("interest", JSON.stringify(this.interest));
    }
  }
  slideForward(){
    this.isPage = this.isPage + 1;
    setTimeout(() => {
      this.interestData = this.slideShowData[this.isPage];
    }, 100);
  }
  slideBackward(){
    this.isPage = this.isPage - 1;
    setTimeout(() => {
      this.interestData = this.slideShowData[this.isPage];
    }, 100);
  }
  filterIfThere(list: any){
    if(this.interest.find(obj=>obj == list.interest)){
      return true;
    }else{
      return false; 
    }
  }
  checkIfLetMove(){
    if(this.isPage == this.slideShowData.length - 1){
      return false;
    }else{
      return true;
    }
  }
}