import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';

@Component({
  selector: 'app-fifth-page',
  templateUrl: './fifth-page.component.html',
  styleUrls: ['./fifth-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FifthPageComponent implements OnInit {
  userData: any;
  moodsData: any;
  logedinData: any;

  constructor(
    public apiService : ApiService,
    public router : Router,
    public loader : NgxSpinnerService,
    public toastr : ToastrService,
  ) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    //this.loadData();
  }
  loadData(){
    this.loader.show();
    this.apiService
    .getMoods(this.logedinData["id"])
    .subscribe((response: any) => {
      console.log(response);
      this.moodsData = response;
      this.loader.hide();
    });
  }

  selectMood(mood:any){
    localStorage.setItem("mood", mood);
    alert(mood);
  }

  chooseMe(obj:any, isMood:any){
    console.log(obj);
    
    let dummy = [1,2,3,4,5,6];
    let mekeIt = false;
    let i = 1;
    dummy.forEach(element => {
      let refContent = document.getElementById(`myid${i}`) as HTMLInputElement;
      refContent.className = "sd-container";
      // if(refContent.className == "sd-container"){
      //   if(obj == i){
      //     mekeIt = false;
      //   }else{
      //     mekeIt = true;
      //   }
      // }
      i++;
    });
    let refContent = document.getElementById(`myid${obj}`) as HTMLInputElement;
    refContent.className = "sd-container fix-shadow";
    localStorage.setItem("mood", isMood);
    // if(!mekeIt){
    //   if(refContent.className == "sd-container fix-shadow"){
    //     refContent.className = "sd-container";
    //     localStorage.removeItem("mood");
    //   }else{
    //     refContent.className = "sd-container fix-shadow";
    //     localStorage.setItem("mood", isMood);
    //   }
    // }
  }
  profileSubmit(){
    let payload = this.logedinData;
    if(localStorage.getItem("nick_name")){
      payload["nick_name"] = localStorage.getItem("nick_name");
    }
    if(localStorage.getItem("interest")){
      payload["interest"] = JSON.parse(localStorage.getItem("interest"));
    }
    if(localStorage.getItem("is_news_subscribe") == "true"){
      payload["is_news_subscribe"] = true;
    }else{
      payload["is_news_subscribe"] = false;
    }
    if(localStorage.getItem("is_sms_subscribe") == "true"){
      payload["is_sms_subscribe"] = true;
    }else{
      payload["is_sms_subscribe"] = false;
    }
    if(localStorage.getItem("is_email_subscribe") == "true"){
      payload["is_email_subscribe"] = true;
    }else{
      payload["is_email_subscribe"] = false;
    }
    if(localStorage.getItem("notification_type") == "true"){
      payload["notification_type"] = "Email";
    }else{
      payload["notification_type"] = "SMS";
    }
    if(localStorage.getItem("reminder_type") == "true"){
      payload["reminder_type"] = "24";
    }else{
      payload["reminder_type"] = "1";
    }
    if(localStorage.getItem("mood")){
      payload["moods"] = [{
        "mood": localStorage.getItem("mood"),
        "created": new Date(),
      }];
    }

    let user_name = {
      "first_name": this.userData["user"]["first_name"],
      "last_name": this.userData["user"]["last_name"],
      "email": this.userData["user"]["email"],
    };

    let userAddress = {
      "address_line1": null,
      "address_line2": "N/A",
      "address_line3": "N/A",
      "addrees_line4": "N/A",
      "zipcode": null,
      "address_type": null,
      "city": null,
    };
    if(this.userData["city_id"]){
      userAddress.city = this.userData["city_id"];
    }else{
      userAddress.city = null;
    }
    payload["address"] = userAddress;
    payload["user_id"] = user_name;
    delete payload["is_news_subscribe"];
    delete payload["modified"];
    delete payload["created"];
    console.log(payload);
    //return;
    this.loader.show();
    this.apiService
    .updateProfileInfo(payload, payload["id"])
    .subscribe((response: any) => {
      console.log(response);
      localStorage.setItem(
        `logedin_data`,
        JSON.stringify(response)
      );
      // this.loggedUpdate();
      // this.toastr.success('Basic information updated successfully');
      this.loader.hide();
      this.submit();
    });
  }
  submit(){
    let payload = {
      "mood": localStorage.getItem("mood"),
      "customer": this.logedinData["user_id"]["id"],
      description: "",
    };
    console.log(payload);
    //return;
    this.loader.show();
    this.apiService
    .updateMoodInfo(payload, this.logedinData["id"])
    .subscribe((response: any) => {
      console.log(response);
      this.logedinData["moods"] = [{
        mood: response.mood
      }];
      localStorage.setItem(
        `logedin_data`,
        JSON.stringify(this.logedinData)
      );
      // this.toastr.success('Basic information updated successfully');
      this.loader.hide();
      this.loadProfileData();
    });
  }
  loadProfileData(){
    this.loader.show();
    this.apiService
      .pkIdUpdate(this.userData.groups[0].toLowerCase(),"get")
      .subscribe((response: any) => {
        console.log(response);
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response[0])
        );
        this.loader.hide();
        this.loggedUpdate();
      });
  }
  loggedUpdate(){
    this.loader.show();
    let payload = {
      is_previously_logged_in: true
    }
    this.apiService
    .previousLoggedUpdate(payload,this.userData["user"]["pk"])
    .subscribe((response: any) => {
      console.log(response);
      this.userData["is_previously_logged_in"] = true;
      localStorage.setItem(
        `user_data`,
        JSON.stringify(this.userData)
      );
      this.clearStore();
      this.loader.hide();
      this.toastr.success('Basic information updated successfully');
      window.location.reload();
    });
  }
  clearStore(){
    localStorage.removeItem("nick_name");
    localStorage.removeItem("interest");
    localStorage.removeItem("is_news_subscribe");
    localStorage.removeItem("is_sms_subscribe");
    localStorage.removeItem("is_email_subscribe");
    localStorage.removeItem("notification_type");
    localStorage.removeItem("reminder_type");
    localStorage.removeItem("mood");
  }
}