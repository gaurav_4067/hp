import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/GeneralService';

@Component({
  selector: 'app-customer-aboutus',
  templateUrl: './customer-aboutus.component.html',
  styleUrls: ['./customer-aboutus.component.scss']
})
export class CustomerAboutusComponent implements OnInit {

  constructor(private generalService:GeneralService) { }

  ngOnInit(): void {
    this.generalService.showProfile=false;
  }

}
