import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { GeneralService } from 'src/app/services/GeneralService';

@Component({
  selector: 'app-billing-info',
  templateUrl: './billing-info.component.html',
  styleUrls: ['./billing-info.component.scss']
})
export class BillingInfoComponent implements OnInit {
  username: string;
  logedinData: any;
  isSubscribed: any = false;
  cards: any = {};
  userData: any;
  cardDeatils: any;
  constructor(
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    private  generalService:GeneralService
    ) { }
  

  ngOnInit(): void {
    this.generalService.showProfile=false;
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.isSubscribed = this.logedinData["is_subscribed"];
    this.username = this.userData?.user.first_name + ' ' + this.userData?.user.last_name;
    this.getCardData();
  }
  getCardData(){
    this.apiService
      .getCardDetails()
      .subscribe((res: any) => {
        if (res) {
          res.forEach(element => {
            if(element.is_default){
              element.last_four_cc = element.cc_number.slice(-4);
              this.cardDeatils = element;
            }
          });
          console.log(this.cardDeatils)
        }
        this.loader.hide();
      },
        (errResponse: HttpErrorResponse) => {
          this.loader.hide();
        });
  }
}
