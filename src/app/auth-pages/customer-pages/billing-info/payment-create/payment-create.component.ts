import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';

@Component({
  selector: 'app-payment-create',
  templateUrl: './payment-create.component.html',
  styleUrls: ['./payment-create.component.scss']
})
export class PaymentcreateComponent implements OnInit {
  @ViewChild('input2') input2: ElementRef
  @ViewChild('input3') input3: ElementRef
  @ViewChild('input4') input4: ElementRef
  
  paymentForm: FormGroup;
  month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  year = [2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030];
  selectedYear = 'Year';
  selectedMonth = 'Month';
  formSubmitted = false;
  isCardNumberFocused = false;

  @Input() isShowBackButton: boolean;
  buttonText: string;
  @Output() saveCardDetails = new EventEmitter<any>();
  @Output() goBack = new EventEmitter<any>();
  userData: any;
  logedinData: any;
  cardDeatils: any;
  cardMethods: any = [];
  isCard: string;
  selectedCard: any;

  constructor(public apiService: ApiService,
    private fb: FormBuilder,
    public router: Router,
    public thirpApiService: ThirdPApiService,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.buttonText = "Save Card";
    let userAddress = `${this.logedinData.address.address_line3=='N/A' || 'undefined' || 'null'?"":this.logedinData.address.address_line3} ${this.logedinData.address.address_line4=='N/A'|| 'undefined' || 'null'?"":this.logedinData.address.address_line4} ${this.logedinData.address.address_line5=='N/A'|| 'undefined' || 'null'?"":this.logedinData.address.address_line5}`.trim();
    
    this.paymentForm = this.fb.group({
      fullName: [`${this.logedinData.user_id.first_name} ${this.logedinData.user_id.last_name}` , Validators.required],
      cardHolderName: [`${this.logedinData.user_id.first_name} ${this.logedinData.user_id.last_name}`, Validators.required],
      billingAddr: [userAddress ? userAddress : "", Validators.required],
      cardNumPart1: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      cardNumPart2: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      cardNumPart3: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      cardNumPart4: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      city: [this.logedinData.address.address_line3=='N/A'|| 'undefined' || 'null' ?"": this.logedinData.address.address_line3 , Validators.required],
      zipCode: [this.logedinData.address.zip_code ? this.logedinData.address.zip_code :  "", Validators.required],
      cvvNumber: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(3), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
    });
    this.getAllCardInfo();
  }
  getAllCardInfo(){
    this.loader.show();
    this.apiService
      .getCardMethods()
      .subscribe((response: any) => {
        console.log(response);
        this.cardMethods = response;
        this.loader.hide();
      });
  }

  validateExpiry(){
    
    let curYear = new Date().getFullYear();
    if (this.selectedYear !='Year' && this.selectedMonth !='Month') {
      if (parseInt(this.selectedYear, 10) > curYear) {

      } else {
        let curMonth = new Date().getMonth() + 1;
        if (parseInt(this.selectedMonth, 10) < curMonth) {
          this.toastr.error(`Please provide valid expiry.`);
          this.selectedMonth="Month";
        }
      }
    }
  }
  onGoBackBtnClick() {
    this.goBack.emit(true);
  }

  selectMonth(m) {
    // if (m < new Date().getMonth() + 1) {
    //   this.toastr.error(`Please provide valid card details.`);
    //   return;
    // }
    this.selectedMonth = m;
  }

  cardMethod(arg: string){
    console.log(arg);
    this.isCard = arg;
    if(this.isCard){
      let result = this.cardMethods.find(obj=>obj.method === this.isCard);
      if(result){
        this.selectedCard = result;
      }
    }
  }
  createPaymentDetails(paymentDetails) {
    console.log('paymentDetails', paymentDetails)
    this.formSubmitted = true;
    if (this.paymentForm.valid && this.selectedMonth !== 'Month' && this.selectedYear !== 'Year') {
      if(!this.validateCardExpiry(this.selectedMonth,this.selectedYear)){
        this.toastr.error("Please provide valid card validity.");
        return 0;
      }else{
        const payload = {
          "cc_number": paymentDetails.value.cardNumPart1 + paymentDetails.value.cardNumPart2 + paymentDetails.value.cardNumPart3 + paymentDetails.value.cardNumPart4,
          "cc_expiry": this.selectedMonth + '/' + this.selectedYear,
          "is_default": true,
          "account_holder_name": paymentDetails.value.fullName,
          "account_holder_type": "individual",
          "routing_number": null,
          "account_number": null,
          "billing_address": paymentDetails.value.billingAddr,
          "city": paymentDetails.value.city,
          "customer": this.logedinData["id"],
          "zip_code" : paymentDetails.value.zipCode,
          "payment_methods": this.selectedCard["id"],
        };
        this.loader.show();
        this.apiService
          .createCardDetails(payload)
          .subscribe((response: any) => {
            console.log(response);
            this.toastr.success('Payment information created successfully');
            this.loader.hide();
          });
      }
    }
  }
  validateCardExpiry(isMonth:any, isYear:any){
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth()+1;
    if(isYear >= year){
      if(isYear > year){
        return true;
      }else if(isYear == year && isMonth >= month){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }
  cardNumberFocused(){
    this.isCardNumberFocused = true;
  }

  cardNumberFocuseOut(){
    this.isCardNumberFocused = false;
  }

  onInputEntry(event, feild) {
    let input = event.target;
    let length = input.value.length;
    let maxLength = input.attributes.maxlength.value;
  
    if (length >= maxLength) {
      if(feild==2)
        this.input2.nativeElement.focus();
      else if(feild==3)
        this.input3.nativeElement.focus();
      else if(feild==4)
        this.input4.nativeElement.focus();
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  
  }
}
