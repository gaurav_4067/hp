import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';

@Component({
  selector: 'app-billing-history',
  templateUrl: './billing-history.component.html',
  styleUrls: ['./billing-history.component.scss']
})
export class BillingHistoryComponent implements OnInit {
  userData: any;
  logedinData: any;
  billingDetails: any;

  constructor(
    public apiService: ApiService,
    public router: Router,
    public thirpApiService: ThirdPApiService,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.loadData()
  }
  loadData(){
    this.apiService
    .getBillingDetails()
    .subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.billingDetails = [...res["context"], ...res["subscription_payment_history"]];
      }
      this.loader.hide();
    },
    (errResponse: HttpErrorResponse) => {
      this.loader.hide();
    });
  }

  downloadCSV(row:any){
    var bodyContent: any = [];
    bodyContent.push({
      date: row.modified,
      session_type: `${row.duration_min} minutes video session with  ${row.practitioner__user_id__first_name} ${row.practitioner__user_id__last_name}`,
      amount: row.price  
    });

    var titleContent = {
      title: "Billing History",
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true,
      headers: [
        'Date',
        'Plan/Session Type',
        'Price'
      ],
      nullToEmptyString: true,
    };
    new Angular5Csv(bodyContent, "billing_info", titleContent);
  }
}
