import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { TimeoutError } from 'rxjs';
import { ApiService } from 'src/app/services/ApiService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';

@Component({
  selector: 'app-payment-update',
  templateUrl: './payment-update.component.html',
  styleUrls: ['./payment-update.component.scss']
})
export class PaymentUpdateComponent implements OnInit {
  @ViewChild('input2') input2: ElementRef;
  @ViewChild('input3') input3: ElementRef;
  @ViewChild('input4') input4: ElementRef;
  
  paymentForm: FormGroup;
  month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  year = [2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030];
  selectedYear = 'Year';
  selectedMonth = 'Month';
  formSubmitted = false;
  isCardNumberFocused = false;

  @Input() isShowBackButton: boolean;
  buttonText: string;
  @Output() saveCardDetails = new EventEmitter<any>();
  @Output() goBack = new EventEmitter<any>();
  userData: any;
  logedinData: any;
  cardDeatils: any;
  cardMethods: any = [];

  constructor(public apiService: ApiService,
    private fb: FormBuilder,
    public router: Router,
    public thirpApiService: ThirdPApiService,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.apiService
    .getCardDetails()
    .subscribe((res: any) => {
      if (res) {
        res.forEach(element => {
          if(element.is_default){
            this.cardDeatils = element;
            if(this.cardDeatils){
              this.buttonText = "Update Card";
              let userName = `${this.logedinData.user_id.first_name} ${this.logedinData.user_id.last_name}`;
              let userAddress = `${this.logedinData.address.address_line3} ${this.logedinData.address.address_line4} ${this.logedinData.address.address_line5}`;
              if (this.logedinData.address.address_line3 != "N/A") {
                userAddress = `${this.logedinData.address.address_line3} ${this.logedinData.address.address_line4} ${this.logedinData.address.address_line5}`;
              } else {
                userAddress = this.cardDeatils.billing_address;
              }
              if(this.cardDeatils.cc_expiry.slice(1,2) == "/"){
                this.selectedMonth = `${this.cardDeatils.cc_expiry.slice(0,1)}`;
              }else{
                this.selectedMonth = `${this.cardDeatils.cc_expiry.slice(0,2)}`;
              }
              this.selectedYear = `${this.cardDeatils.cc_expiry.substring(this.cardDeatils.cc_expiry.length - 4)}`;
              this.paymentForm = this.fb.group({
                fullName: [userName ? this.cardDeatils.account_holder_name : this.cardDeatils.account_holder_name, Validators.required],
                cardHolderName: [userName ? this.cardDeatils.account_holder_name : this.cardDeatils.account_holder_name, Validators.required],
                billingAddr: [userAddress ? this.cardDeatils.billing_address : this.cardDeatils.billing_address, Validators.required],
                cardNumPart1: [this.cardDeatils.cc_number ? this.cardDeatils.cc_number.slice(0,4) : "", [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
                cardNumPart2: [this.cardDeatils.cc_number ? this.cardDeatils.cc_number.slice(4,8) : "", [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
                cardNumPart3: [this.cardDeatils.cc_number ? this.cardDeatils.cc_number.slice(8,12) : "", [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
                cardNumPart4: [this.cardDeatils.cc_number ? this.cardDeatils.cc_number.slice(12,16) : "", [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
                // city: [this.logedinData.address.address_line3 ? this.cardDeatils.city : this.cardDeatils.city, Validators.required],
                city: [this.logedinData.address.address_line3=='N/A'|| 'undefined' || 'null' ?"": this.logedinData.address.address_line3 , Validators.required],
                zipCode: [this.logedinData.address.zip_code ? this.cardDeatils.zip_code: this.cardDeatils.zip_code, Validators.required],
                cvvNumber: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(3), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
              });
            }
          }
        });
        console.log(this.cardDeatils);
        console.log(this.selectedMonth, this.selectedYear)
      }
      this.loader.hide();
    },
    (errResponse: HttpErrorResponse) => {
      this.loader.hide();
    });
    this.getAllCardInfo();
  }
  getAllCardInfo(){
    this.loader.show();
    this.apiService
      .getCardMethods()
      .subscribe((response: any) => {
        console.log(response);
        this.cardMethods = response;
        this.loader.hide();
      });
  }
  validateExpiry(){
    let curYear = new Date().getFullYear();
    if (this.selectedYear && this.selectedMonth) {
      if (parseInt(this.selectedYear, 10) > curYear) {

      } else {
        let curMonth = new Date().getMonth() + 1;
        if (parseInt(this.selectedMonth, 10) < curMonth) {
          this.toastr.error(`Please provide valid expiry.`);
          this.selectedMonth="Month";
        }
      }
    }
  }
  onGoBackBtnClick() {
    this.goBack.emit(true);
  }
  selectMonth(m) {
    // if (m < new Date().getMonth() + 1) {
    //   this.toastr.error(`Please provide valid card validity.`);
    //   return;
    // }
    this.selectedMonth = m;
  }

  updatePaymentDetails(paymentDetails) {
    console.log('paymentDetails', this.cardDeatils.id)
    this.formSubmitted = true;
    if (this.paymentForm.valid && this.selectedMonth !== 'Month' && this.selectedYear !== 'Year') {
      if(!this.validateCardExpiry(this.selectedMonth,this.selectedYear)){
        this.toastr.error("Please provide valid card validity.");
        return 0;
      }else{
        const payload = {
          "cc_number": paymentDetails.value.cardNumPart1 + paymentDetails.value.cardNumPart2 + paymentDetails.value.cardNumPart3 + paymentDetails.value.cardNumPart4,
          "cc_expiry": this.selectedMonth + '/' + this.selectedYear,
          "is_default": true,
          "account_holder_name": paymentDetails.value.fullName,
          "account_holder_type": "individual",
          "routing_number": null,
          "account_number": null,
          "billing_address": paymentDetails.value.billingAddr,
          "city": paymentDetails.value.city,
          "zip_code" : paymentDetails.value.zipCode,
          "customer": this.logedinData["id"],
          "payment_methods": this.cardDeatils.payment_methods
        };
        console.log(this.logedinData["id"]);
        this.loader.show();
        this.apiService
          .updateCardDetails(this.cardDeatils.id, payload)
          .subscribe((response: any) => {
            console.log(response);
            this.toastr.success('Billing information updated successfully');
            this.loader.hide();
          });
      }
    }
  }
  validateCardExpiry(isMonth:any, isYear:any){
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth()+1;
    if(isYear >= year){
      if(isYear > year){
        return true;
      }else if(isYear == year && isMonth >= month){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }
  cardNumberFocused(){
    this.isCardNumberFocused = true;
  }

  cardNumberFocuseOut(){
    this.isCardNumberFocused = false;
  }

  onDigitInput(event){
    
    let element;
    if (event.code !== 'Backspace')
         element = event.srcElement.nextElementSibling;
 
     if (event.code === 'Backspace')
         element = event.srcElement.previousElementSibling;
 
     if(element == null)
         return;
     else
         element.focus();
 }

 numberOnly(event): boolean {
  const charCode = (event.which) ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;

}
onInputEntry(event, feild) {
  let input = event.target;
  let length = input.value.length;
  let maxLength = input.attributes.maxlength.value;

  if (length >= maxLength) {
    if(feild==2)
      this.input2.nativeElement.focus();
    else if(feild==3)
      this.input3.nativeElement.focus();
    else if(feild==4)
      this.input4.nativeElement.focus();
  }
}
}
