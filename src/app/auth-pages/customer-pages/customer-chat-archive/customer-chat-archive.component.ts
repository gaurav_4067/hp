import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../../services/ChatService';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-customer-chat-archive',
  templateUrl: './customer-chat-archive.component.html',
  styleUrls: ['./customer-chat-archive.component.scss']
})
export class CustomerChatArchiveComponent implements OnInit {

  archiveList: any;
  selectedArchive: any;

  constructor(private chatService: ChatService, private loader: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getArchiveList();
  }

  getArchiveList() {
    this.loader.show();
    this.chatService.getCustomerArchiveChats().subscribe((succes: any) => {
      this.archiveList = succes.response;
      this.loader.hide();
      console.log('this.practitionerList', succes);
    }, (err) => {
      this.loader.hide();
      console.log('err', err);
    })
  }

  deleteArchive() {
    this.loader.show();
    this.chatService.deleteArchiveChat(this.selectedArchive.archive_id).subscribe((success) => {
      console.log('success', success);
      this.loader.hide();
      this.getArchiveList();
    }, (err) => {
      this.loader.hide();
      console.log('err', err);
    })
  }

  selectArchive(item) {
    this.selectedArchive = item;
  }

}
