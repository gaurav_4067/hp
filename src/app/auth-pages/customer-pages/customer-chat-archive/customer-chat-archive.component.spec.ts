import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerChatArchiveComponent } from './customer-chat-archive.component';

describe('CustomerChatArchiveComponent', () => {
  let component: CustomerChatArchiveComponent;
  let fixture: ComponentFixture<CustomerChatArchiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerChatArchiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerChatArchiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
