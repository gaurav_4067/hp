import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/ApiService';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { GeneralService } from '../../../services/GeneralService';

@Component({
  selector: 'app-customer-stats',
  templateUrl: './customer-stats.component.html',
  styleUrls: ['./customer-stats.component.scss']
})
export class CustomerStatsComponent implements OnInit {
  stats: any;
  moods = [];

  metrics = [{
    src: 'assets/images/emojis/Group.png',
    title: 'First Metric',
    desc: 'Keep up the good work',
    change: '+45%'
  }, {
    src: 'assets/images/emojis/Group-2.png',
    title: 'Second Metric',
    desc: 'You have improved your energy level',
    change: '+25%'
  }, {
    src: 'assets/images/emojis/Group-2.png',
    title: 'Third Metric',
    desc: 'You have no check-ins for there metric',
    change: '--'
  }];

  overview = [{ type: 'chats', quantity: 22 },
  { type: 'sessions', quantity: 3 },
  { type: 'check-ins', quantity: 14 },
  { type: 'reviews', quantity: 3 },];

  reviews: any = [];

  activeReview: any;
  activeIndex = 0;
  stars = [];
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  currentMonthName: any;
  currMonth = new Date().getMonth();

  constructor(private apiService: ApiService, config: NgbCarouselConfig, ratingConfig: NgbRatingConfig, public generalService: GeneralService) {
     // customize default values of carousels used by this component tree
     config.interval = 10000;
     config.wrap = false;
     config.keyboard = false;
     config.pauseOnHover = false;
 
     // customize default values of ratings used by this component tree
     ratingConfig.max = 5;
     ratingConfig.readonly = true;

    this.currentMonthName = this.months[this.currMonth];
  }

  ngOnInit(): void {
    this.generalService.showProfile=false;
    // this.activeReview = this.reviews[this.activeIndex];
    // this.stars = Array(this.activeReview.rating).fill(0);
    this.getCustomerStats();
  }

  // validateSelectedMonth(selectedMonth:string){
  //   this.currentMonthName=selectedMonth;
  //   this.currMonth=this.months.findIndex(x=>x==selectedMonth);
  // }

  getCustomerStats() {
    this.apiService.getCustomerStats().subscribe((success: any) => {
      console.log('success', success);
      this.stats = success;
      this.reviews = success.reviews;
      this.activeReview = this.stats.reviews.length > 0 && this.stats.reviews[this.activeIndex];
      this.stars = Array(this.activeReview.overall_rating).fill(0);
      this.moods = this.stats.mood_meter.slice(0, 4);
    }, (err) => {
      console.log('err', err);
    })
  }

  nextReview() {
    this.activeIndex = this.activeIndex === this.reviews.length - 1 ? 0 : this.activeIndex + 1;
    this.activeReview = this.reviews[this.activeIndex];
    this.stars = Array(this.activeReview.overall_rating).fill(0);
  }

  prevReview() {
    this.activeIndex = this.activeIndex === 0 ? this.reviews.length - 1 : this.activeIndex - 1;
    this.activeReview = this.reviews[this.activeIndex];
    this.stars = Array(this.activeReview.overall_rating).fill(0);
  }

}
