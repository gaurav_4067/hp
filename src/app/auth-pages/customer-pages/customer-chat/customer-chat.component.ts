import { Component, OnInit, AfterViewChecked, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Conversations from '@twilio/conversations';
import { ChatService } from '../../../services/ChatService';
import * as RecordRTC from 'recordrtc';
import { GeneralService } from '../../../services/GeneralService';
import { DeleteChatComponent } from '../delete-chat/delete-chat.component';
import { Router, ActivatedRoute } from '@angular/router';
import { PractitionerScheduleService } from '../../../services/practitioner-schedule.service'
import { ReportPractitionerComponent } from '../../practitioner-pages/practitioner-chat/report-practitioner/report-practitioner.component';
import { BlockCustomerComponent } from '../block-customer/block-customer.component';
import { SwitchPractitionerComponent } from '../../practitioner-pages/practitioner-chat/switch-practitioner/switch-practitioner.component'
import { NgbdModalContent } from '../customer-booking/ngbdmodalcontent.component';
import { FollowUpFormComponent } from '../../practitioner-pages/practitioner-chat/follow-up-form/follow-up-form.component';
import { ReviewPractitionerComponent } from '../../practitioner-pages/practitioner-chat/review-practitioner/review-practitioner.component';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { ThirdPApiService } from '../../../services/ThirdPApiService';
import { ZoomMeetingConfig } from '../../../config/config';
import { AuthService } from '../../../services/AuthService';
import { PractitionerSessionService } from 'src/app/services/Practitioner-session.service';
import { ActiveFollowupFormComponent } from 'src/app/v1/practitioner/active-followup-form/active-followup-form.component';

@Component({
  selector: 'app-customer-chat',
  templateUrl: './customer-chat.component.html',
  styleUrls: ['./customer-chat.component.scss']
})
export class CustomerChatComponent implements OnInit {
  model: any;
  isContent: any;
  @ViewChild('ChartRemoveConfirmModal') ChartRemoveConfirmModal: TemplateRef<any>;
  @ViewChild('chatScrollContainer') private chatScrollContainer: ElementRef<any>;
  @ViewChild('practitionerPopTemplate') practitionerPopTemplate: TemplateRef<any>;
  message: any = '';
  userData: any;
  selectedMsg: any;
  client: any;
  channel: any;
  isAvailable: boolean;
  messageData: any = [];
  showEmojiPopup: boolean = false;
  recorder: any;
  blobUrl: any;
  stream: any;
  isRecording: boolean = false;
  isLoading: boolean = false;
  isShowInterest: boolean = false;
  isTyping: boolean = false;
  hiveredMsgId: any = null;
  practitionerList: any = [];
  customerList: any = [];
  selectedUser: any;
  selectedList;
  isCustomerBlocked = false;
  isPractitionerSwitched = false;
  redirect: boolean = false;
  selectedIndex: any;
  mediaURL: any = [];
  selectedChat;
  selectedText;
  savedMsgs: any;
  upcomingSession: any;

  constructor(
    public modalService: NgbModal,
    private chatService: ChatService,
    public generalService: GeneralService,
    public router: Router,
    private practitionerScheduleService: PractitionerScheduleService,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private loader: NgxSpinnerService,
    private toaster: ToastrService,
    public thirdPApiService: ThirdPApiService,
    public auth: AuthService,
    private practionerSession: PractitionerSessionService,
  ) {
    this.route.queryParams.subscribe(params => {
      if (params.redirect) {
        this.redirect = params.redirect;
        this.selectedChat = params.selected;
      }
      this.userData = JSON.parse(localStorage.getItem("user_data"));
      if (params && params.isFromZoomSession && params.pid && this.userData && this.userData.groups[0] === "Customer") {
        console.log('params', params);
        this.selectedList = { id: params.pid, isFromZoomSession: true };
        this.reviewPractitioner();
        this.updateVideoSession(params.aid);
      }
    });
  }

  ngOnInit(): void {
    this.generalService.showProfile=false;
    this.generalService.setPractitionerSwitch(false);
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    if (this.userData.groups[0] === "Customer") {
      this.getPractitionerList();
      this.getPinMessage();
      // this.chatService.getCustomerToken().subscribe((succes: any) => {
      //   console.log('sucess', succes);
      //   Conversations.create(succes.token).then((client) => {
      //     console.log('clientclient', client);
      //     this.setupChatClient(client)
      //   }, (err) => {
      //     console.log('create', err);
      //   })
      // }, (err) => {
      //   console.log('err', err);
      // })
    } else if (this.userData.groups[0] === "Practitioner") {
      this.getPractitioner();
      this.getCustomerList();
      // this.chatService.getPractitionerToken().subscribe((succes: any) => {
      //   console.log('sucess', succes);
      //   Conversations.create(succes.token).then((client) => {
      //     console.log('clientclient', client);
      //     this.setupChatClient(client)
      //   }, (err) => {
      //     console.log('create', err);
      //   })
      // }, (err) => {
      //   console.log('err', err);
      // })
    }
  }

  updateVideoSession(id) {
    const payload = {
      status_code: 'Completed',
      id: id
    };
    this.apiService.updateVideoSessionStatus(payload)
      .subscribe((status) => {
        console.log('status', status);
      }, (error) => {
        console.log('error status', error);
      });
  }

  getPractitioner() {
    let loggedInData = JSON.parse(localStorage.getItem("logedin_data"));
    this.practitionerScheduleService.getPractitioner(loggedInData.id).subscribe((success: any) => {
      this.isAvailable = success.chat_availability === 'R' ? false : true;
    }, (err) => {
      console.log('err', err);
    });
  }

  switchAvailable() {
    let loggedInData = JSON.parse(localStorage.getItem("logedin_data"));
    this.isAvailable = !this.isAvailable;
    const data = {
      chat_availability: this.isAvailable ? 'G' : 'R',
      user_id: loggedInData.user_id
    }
    this.practitionerScheduleService.changeAvailable(data, loggedInData.id).subscribe((success) => {
      console.log('success', success);
    }, (err) => {
      console.log('err', err);
    });
  }

  getChatToken(channelName) {
    if (this.userData.groups[0] === "Customer") {
      this.chatService.getCustomerToken().subscribe((succes: any) => {
        console.log('sucess', succes);
        Conversations.create(succes.token).then((client) => {
          console.log('clientclient', client);
          this.setupChatClient(client, channelName)
        }, (err) => {
          console.log('create', err);
        })
      }, (err) => {
        console.log('err', err);
      })
    } else if (this.userData.groups[0] === "Practitioner") {
      this.chatService.getPractitionerToken().subscribe((succes: any) => {
        console.log('sucess', succes);
        Conversations.create(succes.token).then((client) => {
          console.log('clientclient', client);
          this.setupChatClient(client, channelName)
        }, (err) => {
          console.log('create', err);
        })
      }, (err) => {
        console.log('err', err);
      })
    }
  }


  getPinMessage() {
    this.chatService.viewPinMessage().subscribe((success: any) => {
      if (success.response.length > 0) {
        this.savedMsgs = success.response;
      } else {
        this.savedMsgs = [];
      }
    }, (err) => {
      console.log(err);
    });
  }

  unPinMessage(item) {
    this.chatService.unPinMessage(item.pin_id).subscribe((success) => {
      console.log(success);
      this.getPinMessage();
    }, err => {
      console.log(err);
    })
  }

  checkSessionTime() {
    if (this.upcomingSession && this.upcomingSession.length !== 0 && moment(this.selectedUser.upcoming_session.appointment_date).isSame(moment().format('YYYY-MM-DD'))) {
      const duration = moment.duration(moment(this.selectedUser.upcoming_session.start_time, ["HH:mm"]).diff(moment(moment().format('HH:mm'), ["HH:mm"]))).asMinutes();
      if (duration < 5) {
        return true;
      }
      return false;
    }
    return false;
  }

  startSession(session) {
    console.log('session', session, this.selectedUser);
    const userData = JSON.parse(localStorage.getItem('user_data'));
    const isGroup = userData?.groups[0].toLowerCase();
    const userName = userData?.user.username;
    const email = userData?.user.email;
    this.loader.show();
   if(isGroup === 'customer'){
    this.thirdPApiService.getZoomMeetingList(this.selectedUser.upcoming_session.id)
    .subscribe((response: any) => {
      console.log('response', response);
      this.loader.hide();
      if (response && response.length > 0) {
        window.open(`${ZoomMeetingConfig.ZOOM_SESSION_APP_URL}?user=${userName}&id=${response[0].meeting_id}&auth=${this.auth.getToken()}&email=${email}&group=${isGroup}&pid=${session.practitioner_id}&aid=${session.id}`, '_blank');
      } else {
        this.toaster.error('No meetings id available');
      }
    }, (error) => {
      console.error(error);
      this.loader.hide();
    });
   } else {
    const userData = JSON.parse(localStorage.getItem('user_data'));
    const email = userData?.user.email;
    const payload = {
      duration: session.duration,
      appointment: this.selectedUser.upcoming_session.id,
      email: email
    };
    this.practionerSession.postvideosession(payload)
      .subscribe((res: any) => {
        console.log('res', res);
        this.loader.hide();
        if (res && res.meeting_id) {
          window.open(`${ZoomMeetingConfig.ZOOM_SESSION_APP_URL}?user=${userName}&id=${res.meeting_id}&auth=${this.auth.getToken()}&email=${email}&group=${isGroup}`, '_blank');
        }
      }, (error) => {
        console.log('error', error);
        this.loader.hide();
      });
   }
  }

  checkIfChannelExits(data) {
    return new Promise((resolve, reject) => {
      this.chatService.createChannel(data).subscribe((succes: any) => {
        console.log('sucess', succes);
        resolve(succes);
      }, (err) => {
        console.log('err', err);
        reject(err)
      });
    })
  }

  unBlockCustomer() {
    this.loader.show();
    this.apiService.unBlockCustomer(this.selectedList.block_customer_id)
      .subscribe((report: any) => {
        console.log('unblock customer', report);
        this.loader.hide();
        this.modalService.dismissAll();
        this.router.navigate(['welcome-practitioner']);
      }, (error) => {
        console.log('unblock customer error', error);
        if (error && error.status) {
          this.loader.hide();
          this.modalService.dismissAll();
          this.router.navigate(['welcome-practitioner']);
        } else {
          this.toaster.error(error.statusText);
          this.loader.hide();
        }
      });
  }

  goToChat(list, index) {
    console.log('listlist', list);
    this.selectedIndex = index;
    this.selectedUser = list;
    this.isCustomerBlocked = list.is_blocked;
    this.isLoading = true;
    this.messageData = [];
    if (list.upcoming_session.length !== 0) {
      this.upcomingSession = {
        date: new Date(list.upcoming_session.appointment_date),
        start_time: moment(list.upcoming_session.start_time, ["HH:mm"]).format("h:mm A"),
        status: list.upcoming_session.status_code__status,
        duration: moment.duration(moment(list.upcoming_session.end_time, ["HH:mm"]).diff(moment(list.upcoming_session.start_time, ["HH:mm"]))).asMinutes()
      };
    } else {
      this.upcomingSession = [];
    }
    let channelName;
    let data;
    let loggedInData = JSON.parse(localStorage.getItem("logedin_data"));
    console.log('loggedInData', loggedInData);
    this.generalService.setTitle(list.name || list.nick_name);
    if (this.userData.groups[0] === "Customer") {
      channelName = 'ch-' + list.id.split('-')[1] + loggedInData.id.split('-')[1];
      data = {
        "practitioner_id": list.id,
        "isactive": false,
        "sessioninfo": "testing",
        "isblock": false,
        "channelname": channelName,
        "customer": loggedInData.id,
        "parent_id": null
      }
    } else {
      channelName = 'ch-' + loggedInData.id.split('-')[1] + list.id.split('-')[1];
      // channelName = 'ch-' + '372f' + list.id.split('-')[1];
      data = {
        "practitioner_id": loggedInData.id,
        // "practitioner_id": '398ac134-372f-4309-85a3-363c711d52cb',
        "isactive": false,
        "sessioninfo": "testing",
        "isblock": false,
        "channelname": channelName,
        "customer": list.id,
        "parent_id": null
      }
    }
    console.log('channelName', channelName, data, list);
    this.checkIfChannelExits(data)
      .then((res) => {
        console.log('resres', res);
        this.getChatToken(channelName);
      })
      .catch((err) => {
        if (err.status === 400) {
          // this.isLoading = false;
          this.getChatToken(channelName);
        }
        console.log('errerr', err);
      })
  }

  mouseOver(ev, val) {
    ev.stopPropagation();
    ev.preventDefault();
    this.hiveredMsgId = val;
  }

  isChannelNameAvailable(name) {
    if (name && name != "") {
      return false;
    } else {
      return true
    }
  }
  ngAfterContentChecked() {
    // this.scrollToBottom();
  }

  openPractitionerPopup(ev, list) {
    ev.stopPropagation();
    this.selectedList = list;
    this.modalService.open(this.practitionerPopTemplate, { windowClass: 'modal-practitioner', size: 'md' });
  }

  getPractitionerList() {
    this.chatService.getPractitionerList().subscribe((succes: any) => {
      let practitionerList = succes.data;
      if (practitionerList.length > 0) {
        this.practitionerList = practitionerList.filter((item: any) => item.is_archieve === false);
        if (this.redirect) {
          this.practitionerList.forEach((element, index) => {
            if (element.user_id === this.selectedChat) {
              this.selectedIndex = index;
              this.goToChat(this.practitionerList[this.selectedIndex], this.selectedIndex);
            }
          });
        } else {
          this.selectedIndex = 0;
          this.goToChat(this.practitionerList[0], 0);
        }
      } else {
        this.isShowInterest = true;
      }
    }, (err) => {
      console.log('err', err);
    })
  }

  getCustomerList() {
    this.chatService.getCustomerList().subscribe((succes: any) => {
      let customerList = succes.data.response;
      if (customerList.length > 0) {
        this.customerList = customerList.filter((item: any) => item.is_archieve === false);
        if (this.redirect) {
          this.customerList.forEach((element, index) => {
            if (element.id === this.selectedChat) {
              this.selectedIndex = index;
              this.goToChat(this.customerList[this.selectedIndex], this.selectedIndex);
            }
          });
        } else {
          this.selectedIndex = 0;
          this.goToChat(this.customerList[0], 0)
        }
      } else {
        this.isShowInterest = true;
      }
    }, (err) => {
      console.log('err', err);
    })
  }

  scrollToBottom(): void {
    try {
      this.chatScrollContainer.nativeElement.scroll({
        top: this.chatScrollContainer.nativeElement.scrollHeight,
        left: 0,
        behavior: 'smooth'
      });
    } catch (err) { }
  }

  setupChatClient(client, channelName) {
    this.client = client;
    this.client
      .getConversationByUniqueName(channelName)
      .then(channel => channel)
      .catch(error => {
        if (error.body.code === 50300) {
          return this.client.createConversation({ uniqueName: channelName });
        }
      })
      .then(channel => {
        this.channel = channel;
        return this.channel.join().catch(() => { });
      })
      .then(() => {
        this.isLoading = false;
        // this.setState({ isLoading: false });
        this.channel.getMessages().then(this.messagesLoaded.bind(this));
        this.channel.on('messageAdded', this.messageAdded.bind(this));
        this.channel.on('typingStarted', (participant) => {
          if (participant.identity !== this.userData.user.email) {
            this.isTyping = true;
          }
        });
        this.channel.on('typingEnded', (participant) => {
          if (participant.identity !== this.userData.user.email) {
            this.isTyping = false;
          }
        });
        if (this.channel.createdBy === this.userData.user.email) {
          this.channel.add(this.selectedUser.user_id__email || this.selectedUser.email);
        }
      })
      .catch(this.handleError.bind(this));
  }

  handleError(error) {
    console.error(error);
    // this.client.conversations.conversations("CHc84a76a904ad43f887326c38c3761921.channel");
    this.isLoading = false;
    // this.setState({
    //   error: 'Could not load chat.'
    // });
  }

  deleteChannel() {
    this.channel.delete();
  }

  getImage(item) {
    // let url = await item.media.getContentTemporaryUrl();
    return item.media.getContentTemporaryUrl().then((url) => {
      // log media temporary URL
      console.log('Media temporary URL is ' + url);
      return url;
    });
  }

  async openFile(item) {
    let url = await item.media.getContentTemporaryUrl();
    window.open(url, "_blank");
  }

  async getFile(item, index) {
    console.log('index', index, this.messageData);
    // new Promise((resolve, reject) => {
    // item.media.getContentTemporaryUrl().then((url) => {
    //   console.log('this.url', url);
    //   this.mediaURL.push({
    //     index: index,
    //     url: url
    //   });
    //   // this.messageData[index].imageUrl = url;
    //   // return url;
    //   // resolve(url);
    // });
    // })
    let url = await item.media.getContentTemporaryUrl();
    this.mediaURL.push({
      index: index,
      url: url
    });
    // this.messageData[index].imageUrl = url;
    // console.log('url', url);
    // return url;
    // window.open(url, "_blank");
  }

  getImgByIndex() {
    console.log('this.mediaURL', this.mediaURL);
    for (let j = 0; j < this.mediaURL.length; j++) {
      this.messageData[this.mediaURL[j].index].imageUrl = this.mediaURL[j].url;
    }
    console.log('this.messageData', this.messageData);
    // this.messageData = [...this.messageData]
    // this.mediaURL.forEach((item) => {
    //   this.messageData[item.index].imageUrl = item.url;
    // })
  }

  messagesLoaded(messagePage) {
    console.log('messagePage.items', messagePage.items);
    this.messageData = [];
    if (messagePage.items.length > 0) {
      for (let i = 0; i < messagePage.items.length; i++) {
        this.messageData.push({
          item: messagePage.items[i],
          type: messagePage.items[i].type,
          imageUrl: '',
          text: messagePage.items[i].body,
          author: { id: messagePage.items[i].author, name: messagePage.items[i].author },
          timestamp: messagePage.items[i].dateCreated,
          sid: messagePage.items[i].sid,
          channel_sid: messagePage.items[i].conversation.sid
        });
        if (messagePage.items[i].type === 'media') {
          console.log('asd', messagePage.items[i].media.filename.split('.').pop());
          if (messagePage.items[i].media.filename !== 'blob' && ['png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG'].includes(messagePage.items[i].media.filename.split('.').pop())) {
            this.getFile(messagePage.items[i], i);
          }
        }
      }
    }
    // messagePage.items.forEach((item, index) => {
    //   if (item.type === 'media') {
    //     this.messageData.push({
    //       item: item,
    //       type: item.type,
    //       imageUrl: '',
    //     })
    //     this.getFile(item, index);
    //     console.log('item', item);
    //   }

    // });
    console.log('this.messageData', this.messageData, this.mediaURL);
    setTimeout(() => {
      this.getImgByIndex();
    }, 2000)
    // this.messageData = messagePage.items.map((item) => {
    //   return {
    //     item: item,
    //     imageUrl: this.getFile(item),
    //     type: item.type,
    //     text: item.body,
    //     author: { id: item.author, name: item.author },
    //     timestamp: item.dateCreated,
    //     sid: item.sid,
    //     channel_sid: item.conversation.sid
    //   }
    // });
    // console.log('this.messages', this.messageData);
    // this.scrollToBottom();
  }

  messageAdded(message) {
    console.log('new message', message);
    this.messageData.push({
      item: message,
      type: message.type,
      text: message.body,
      author: { id: message.author, name: message.author },
      timestamp: message.dateCreated,
      sid: message.sid,
      channel_sid: message.conversation.sid
    });
    console.log('this.messageData', this.messageData);
  }

  sendMsg() {
    if(this.message.trim()!==''){
    this.channel.sendMessage(this.message);
    this.message = '';
    }
  }

  selectMsg(message, text) {
    this.selectedMsg = message;
    this.selectedText = text;
  }

  goToArchive() {
    if (this.userData.groups[0] === "Customer") {
      this.router.navigate(['archive-chat']);
    } else if (this.userData.groups[0] === "Practitioner") {
      this.router.navigate(['practitioner-archive']);
    }
  }

  deleteMsg() {
    const message = this.selectedMsg;
    console.log('message', message);
    // if (message.author === this.userData.user.email) {
    message.remove().then((success) => {
      console.log('success', success);
      this.channel.getMessages().then(this.messagesLoaded.bind(this));
    }, (err) => {
      console.log('err', err);
    });
    // }
  }

  addEmoji(event) {
    console.log('emoji', event);
    this.message = `${this.message}${event.emoji.native}`;
    this.showEmojiPopup = !this.showEmojiPopup;
  }

  showEmoji() {
    this.showEmojiPopup = !this.showEmojiPopup;
  }


  onFileSelected(event) {

    const file: File = event.target.files[0];

    if (file) {

      const fileName = file.name;

      const formData = new FormData();

      formData.append("file", file);

      console.log('formData', formData);
      // const upload$ = this.http.post("/api/thumbnail-upload", formData);

      // upload$.subscribe();
      this.channel.sendMessage(formData);
    }
  }

  onKeyDown(e) {
    if (e.keyCode === 13) {
      // this.sendMsg();
    } else {
      // else send the Typing Indicator signal
      this.channel.typing();
    }
    // this.channel.on('typingStarted', function(participant) {
    //   console.log('participant', participant);
    // })
  }

  toggleRecord() {
    // if (this.recordingTimer) {
    //   this.stopRTC();
    // } else {
    //   navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
    //     this.startRTC(stream);
    //   }).catch(error => {
    //     alert(error)
    //   })
    // }
    if (this.isCustomerBlocked) return;
    this.isRecording = !this.isRecording;
    if (this.isRecording) {
      this.startRTC();
    } else {
      this.stopRTC();
    }
  }

  async startRTC() {
    // this.recordWebRTC = new RecordRTC.StereoAudioRecorder(stream, this.options);
    // // this.mediaRecordStream = stream;
    // this.blobUrl = null;
    // this.recordWebRTC.record();
    // this.startCountdown();
    this.stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    this.recorder = new RecordRTC.StereoAudioRecorder(this.stream, {
      type: 'audio'
    });
    this.recorder.record();
  }

  stopRTC() {
    this.recorder.stop((blob) => {
      console.log('blobblob', blob);
      const formData = new FormData();

      formData.append("file", blob);

      this.channel.sendMessage(formData);
      if (this.stream) {
        this.stream.getAudioTracks().forEach(track => track.stop());
        this.stream = null;
      }

      // const mp3Name = encodeURIComponent('audio_' + new Date().getTime() + '.mp3');
    });
    // let blob = await this.recorder.getBlob();
    // invokeSaveAsDialog(blob);
    // this.recordWebRTC.stop((blob) => {
    //   //NOTE: upload on server
    //   this.blobUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(blob));
    //   this.startCountdown(true);
    // })
  }

  startCountdown(clearTime = false) {
    // if (clearTime) {
    //   this.clearStream(this.mediaRecordStream);
    //   this.recordWebRTC = null;
    //   this.recordingTimer = null;
    //   this.mediaRecordStream = null;
    //   clearInterval(this.interval);
    //   return
    // } else {
    //   this.recordingTimer = `00:00`;
    //   clearInterval(this.interval);
    // }

    // this.interval = setInterval(() => {
    //   let timer: any = this.recordingTimer;
    //   timer = timer.split(':');
    //   let minutes = +timer[0];
    //   let seconds = +timer[1];

    //   if (minutes == 10) {
    //     this.recordWebRTC.stopRecording();
    //     clearInterval(this.interval);
    //     return
    //   }
    //   ++seconds;
    //   if (seconds >= 59) {
    //     ++minutes;
    //     seconds = 0;
    //   }

    //   if (seconds < 10) {
    //     this.recordingTimer = `0${minutes}:0${seconds}`;
    //   } else {
    //     this.recordingTimer = `0${minutes}:${seconds}`;
    //   }
    // }, 1000);
  }

  clearStream(stream: any) {
    try {
      stream.getAudioTracks().forEach(track => track.stop());
      stream.getVideoTracks().forEach(track => track.stop());
    } catch (error) {
      //stream error
    }
  }
  archiveChat() {
    this.modalService.dismissAll();
    this.loader.show();
    console.log('selectedList', this.selectedList);
    let loggedInData = JSON.parse(localStorage.getItem("logedin_data"));
    const data = {
      practitioner: this.userData.groups[0] === "Practitioner" ? loggedInData.id : this.selectedList.id,
      is_archieve: true,
      archieved_by: this.userData.groups[0] === "Customer" ? "Customer" : "Practitioner",
      customer: this.userData.groups[0] === "Customer" ? loggedInData.id : this.selectedList.id
    };
    this.chatService.archiveChat(data).subscribe((success) => {
      console.log('success', success);
      if (this.userData.groups[0] === "Customer") {
        this.getPractitionerList();
      } else {
        this.getCustomerList();
      }
      this.loader.hide();
      this.toaster.success('Archived Successfully!');
    }, (err) => {
      this.loader.hide();
      console.log(err);
    });
  }

  reviewPractitioner() {
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(ReviewPractitionerComponent, { windowClass: 'modal-remove-chat',centered:true,scrollable:true, size: 'lg' });
    modelRef.componentInstance.selectedList = this.selectedList;
  }

  viewSessions() { 
    this.modalService.dismissAll();
    this.router.navigate(['/session-dashboard']);
  }

  reviewMe() { }

  followUpForm() { }

  openNotes() { }

  bookSession() {
    
    this.modalService.dismissAll();
    const modalRef = this.modalService.open(NgbdModalContent, { centered: true ,ariaLabelledBy: 'modal-basic-title', backdrop: 'static', size: 'lg', windowClass: 'book-session modal-remove-chat' });
    modalRef.componentInstance.selectedList = this.selectedList;
  }
  switchPractitioner() {
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(SwitchPractitionerComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modelRef.componentInstance.selectedList = this.selectedList;
  }

  blockCustomer() {
    if(!this.selectedList.is_blocked){
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(BlockCustomerComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modelRef.componentInstance.selectedList = this.selectedList;
    }
    else{
      this.unBlockCustomer();
    }
  }

  openFollowUpForm() {
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(FollowUpFormComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modelRef.componentInstance.selectedList = this.selectedList;
  }

  openModal(modalContent: any, isWhere: string) {
    this.isContent = isWhere;
    this.model = this.modalService.open(modalContent, { size: 'sm' });
  }
  closeModal(Event: any) {
    this.model.close();
  }
  pinMsg() {
    console.log('pinmsg');
    this.loader.show();
    let cols = document.getElementsByClassName('popover') as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < cols.length; i++) {
      cols[i].style.display = 'none';
    }
    const payLoad = {
      practitioner : this.selectedUser.id,
      message: this.selectedText,
      is_pinned: true,
      customer: JSON.parse(localStorage.getItem("logedin_data")).id
    }
    this.chatService.pinMessage(payLoad)
    .subscribe((msg)=>{
      this.loader.hide();
      this.toaster.success('Pin Message successfully done');
    }, (error)=>{
      this.loader.hide();
      this.toaster.error(error.statusText);
    });
  }
  deleteChat() {
    this.modalService.dismissAll();
    this.modalService.open(DeleteChatComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
  }
  reportPtactitioner() {
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(ReportPractitionerComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modelRef.componentInstance.selectedList = this.selectedList;
  }

  initActiveFollowupForms(){
    this.modalService.dismissAll();
    let modalRef = this.modalService.open(ActiveFollowupFormComponent, { scrollable: true, centered: true, ariaLabelledBy: 'signup-confirm-title', size: 'lg', windowClass: 'confirm-modal' });
  }
}
