import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';

@Component({
  selector: 'app-view-interests',
  templateUrl: './view-interests.component.html',
  styleUrls: ['./view-interests.component.scss']
})
export class ViewInterestsComponent implements OnInit {
  interests: any;
  userData: any;
  logedinData: any;
  selectedInterest: any = [];

  constructor(
    public apiService: ApiService,
    public router: Router,
    public thirdPApiService: ThirdPApiService,
    public auth: AuthService,
    public generalService: GeneralService,
    private loader: NgxSpinnerService,
    public toastr : ToastrService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    if(this.logedinData){
      localStorage.setItem("interest", JSON.stringify(this.logedinData["interest"]));
      this.logedinData["interest"].forEach(element => {
        this.selectedInterest.push(element);
      });
    }
    this.fetchInterests();
  }
  fetchInterests() {
    this.apiService
      .getIntrest()
      .subscribe((res: any) => {
        if (res) {
          
         this.interests = res.interest_list;
         console.log(res);
        }
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.loader.hide();
      });
  }
  filterIfThere(list:any){
    if(this.logedinData["interest"].find(obj=>obj == list.interest)){
      return true;
    }else{
      return false;
    }
  }
  chooseMe(data:any){
    //
    let refContent = document.getElementById(`${data.id}`) as HTMLInputElement;
    if(refContent.className == "card expertise-card active" || refContent.className == "card expertise-card active ng-star-inserted"){
      refContent.className = "card expertise-card";
      let removed;
      if(this.selectedInterest.find(obj=>obj == data.interest)){
        removed = this.selectedInterest.filter(obj=>obj != data.interest);
        this.selectedInterest = removed;
      }
      localStorage.removeItem("interest");
      localStorage.setItem("interest", JSON.stringify(this.selectedInterest));
    }else{
      refContent.className = "card expertise-card active";
      if(this.selectedInterest.length){
        if(!this.selectedInterest.find(obj=>obj == data.interest)){
          this.selectedInterest.push(data.interest);
        }
      }else{  
        this.selectedInterest.push(data.interest);
      }
      console.log(this.selectedInterest);
      localStorage.removeItem("interest");
      localStorage.setItem("interest", JSON.stringify(this.selectedInterest));
    }
  }
  profileSubmit(){
    let payload = this.logedinData;
    if(localStorage.getItem("interest")){
      payload["interest"] = JSON.parse(localStorage.getItem("interest"));
    }
    console.log(payload);
    //return;
    this.loader.show();
    this.apiService
    .updateBasicInfo(payload,payload["id"])
    .subscribe((response: any) => {
      console.log(response);
      localStorage.setItem(
        `logedin_data`,
        JSON.stringify(response)
      );
      this.toastr.success('Updated successfully');
      this.loader.hide();
      this.router.navigate(["customer-profile"]);
    });
  }
}