import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInterestsComponent } from './view-interests.component';

describe('ViewInterestsComponent', () => {
  let component: ViewInterestsComponent;
  let fixture: ComponentFixture<ViewInterestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewInterestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewInterestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
