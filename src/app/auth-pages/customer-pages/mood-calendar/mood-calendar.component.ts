import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-mood-calendar',
  templateUrl: './mood-calendar.component.html',
  styleUrls: ['./mood-calendar.component.scss']
})
export class MoodCalendarComponent implements OnInit {

  @Input('month') currentMonth: number;
  @Input('moods') data: Array<any>;

  monthMap = [[], [], [], [], [], [], []];

  weekMap = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

  totalMap = {
    0: 31,
    1: 28,
    2: 31,
    3: 30,
    4: 31,
    5: 30,
    6: 31,
    7: 31,
    8: 30,
    9: 31,
    10: 30,
    11: 31,
  }

  constructor() { }

  ngOnInit(): void {
    this.monthMap = [[], [], [], [], [], [], []];
    const today = new Date();
    const year = today.getFullYear();

    const firstDate = new Date(year, this.currentMonth, 1, 0, 0, 0, 0);

    const firstDay = firstDate.getDay();
    const totalDays = this.currentMonth === 1 ? this.lastDayOfFeb(year) : this.totalMap[this.currentMonth];

    console.log(this.data);
    this.fillMonth(firstDay, totalDays);
    console.log(this.monthMap);
  }

  private fillMonth(day, total) {
    let weekDay = day;
    for (let i = 1; i <= total; i++) {
      const theDay = this.monthMap[weekDay];
      weekDay = weekDay === this.monthMap.length - 1 ? 0 : weekDay + 1;
      this.data.find(it => console.log(new Date(it.created).getDate(), i));
      const mood = this.data.find(it => new Date(it.created).getDate() == i);
      theDay.push({
        mood: mood ? mood.mood : '',
        date: i
      });
    }

    for (let i = 0; i < day; i++) {
      this.monthMap[i].unshift({
        mood: null,
        date: Date.now(),
      });
    }
  }

  private lastDayOfFeb(year: number) {
    if (year % 400 === 0) return 29;
    if (year % 100 === 0) return 28;
    if (year % 4 === 0) return 29;
    return 28;
  }

}
