import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../services/ApiService';
import { ThirdPApiService } from '../../../services/ThirdPApiService';
import { ZoomMeetingConfig } from '../../../config/config';
import { AuthService } from '../../../services/AuthService';
import { GeneralService } from '../../../services/GeneralService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

@Component({
  selector: 'app-customer-session',
  templateUrl: './customer-session.component.html',
  styleUrls: ['./customer-session.component.scss']
})
export class CustomerSessionComponent implements OnInit {

  userData: any;
  loggedInData: any;
  name: string;
  customer: any = {};

  constructor(
    public apiService: ApiService,
    public router: Router,
    public thirdPApiService: ThirdPApiService,
    public auth: AuthService,
    public generalService: GeneralService,
    private loader: NgxSpinnerService,
    private toaster: ToastrService,
  ) { }

  ngOnInit() {
    console.log('Customer Dashboard');
    this.generalService.setTitle(null);
    this.userData = JSON.parse(localStorage.getItem('user_data'));
    this.loggedInData = JSON.parse(localStorage.getItem('logedin_data'));
    this.name = this.userData?.user.first_name + ' ' + this.userData?.user.last_name;
    this.fetchDetails();
  }

  fetchDetails() {
    this.loader.show();
    this.apiService
      .getCustomerDetails()
      .subscribe((response: any) => {
        if (response) {
          this.customer = response.stats_value;
        }
        this.loader.hide();
      },
        (errResponse: HttpErrorResponse) => {
          this.loader.hide();
        });
  }

  upcomingSession() {
    const session = this.customer?.upcoming_session?.data;

    if (!session || !session.length) {
      return {};
    }
    return session[0];
  }
  navigateToSessionPage(session) {
    if (this.loggedInData.is_subscribed) {
      const userData = JSON.parse(localStorage.getItem('user_data'));
      const isGroup = userData?.groups[0].toLowerCase();
      const userName = userData?.user.username;
      const email = userData?.user.email;
      this.loader.show();
      this.thirdPApiService.getZoomMeetingList(session.id)
        .subscribe((response: any) => {
          console.log('response', response);
          this.loader.hide();
          if (response && response.length > 0) {
            window.open(`${ZoomMeetingConfig.ZOOM_SESSION_APP_URL}?user=${userName}&id=${response[0].meeting_id}&auth=${this.auth.getToken()}&email=${email}&group=${isGroup}&pid=${session.practitioner_id}&aid=${session.id}`, '_blank');
          } else {
            this.toaster.error('No meetings id available');
          }
        }, (error) => {
          console.error(error);
          this.loader.hide();
        });
    }
    // else {
    //   this.router.navigate(['mysubscription']);
    // }
  }
  navigateToChatPage(session) {
    if (this.loggedInData.is_subscribed) {
      this.router.navigate(['chat'], { queryParams: { selected: session.practitioner__user_id, redirect: true } });
    }
    // else {
    //   this.router.navigate(['mysubscription']);
    // }
  }
  getPrice(session) {
    if (!session.start_time) {
      return 'Included';
    }

    const startTime = session.start_time;
    const endTime = session.end_time;

    const [h1, m1] = startTime.split(':').map(Number);
    const [h2, m2] = endTime.split(':').map(Number);

    const h2Final = h2 < h1 ? h2 + 24 : h2;

    const totalS1 = m1 + 60 * h1;
    const totalS2 = m2 + 60 * h2Final;

    if (totalS2 - totalS1 > 30) {
      return `$${session.price}`;
    }
    return 'Included';
  }

  getDuration(session) {
    if (!session.start_time) {
      return '30 min';
    }

    const startTime = session.start_time;
    const endTime = session.end_time;

    const [h1, m1] = startTime.split(':').map(Number);
    const [h2, m2] = endTime.split(':').map(Number);

    const h2Final = h2 < h1 ? h2 + 24 : h2;

    const totalS1 = m1 + 60 * h1;
    const totalS2 = m2 + 60 * h2Final;

    return this.convertToHrs(totalS2 - totalS1);
  }

  getAppointmentDate(appDate) {

    if (!appDate) {
      return '';
    }

    const [y, m, d] = appDate.split('-');
    const month = months[Number(m) - 1];

    return `${Number(d)} ${month}`;
  }

  getAppointmentTime(time) {

    if (!time) {
      return '';
    }

    const [h, m] = time.split(':');

    if (Number(h) > 12) {
      return `${h - 12}:${m} pm`;
    }
    return `${h}:${m} ${Number(h) === 12 ? 'pm' : 'am'}`;
  }

  convertToHrs(min: number | null) {
    if (min == null) { return ''; }
    const hrs = Math.floor(min / 60);
    const rem = min % 60;
    let res = '';

    if (hrs > 0) {
      res += `${hrs}h`;
    }
    if (rem > 0) {
      res += ` ${rem}min`;
    }
    return res;
  }

  getDate(chatDate: string) {
    const date = new Date(chatDate);
    return `${date.getDate()} ${months[date.getMonth()]}`;
  }

  getTime(chatDate: string) {
    const date = new Date(chatDate);
    let hours = date.getHours();
    let minutes: string | number = date.getMinutes();
    let time = 'am';
    if (hours > 11) {
      time = 'pm';
    }

    if (hours > 12) {
      hours -= 12;
    } else if (hours === 0) {
      hours = 12;
    }

    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    return `${hours}:${minutes} ${time}`;
  }

  ellipsisify(str: string) {
    if (str.length <= 18) {
      return str;
    }
    return `${str.slice(0, 18)}...`;
  }

}
