
import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { ApiService } from '../../services/ApiService';

@Component({
  selector: 'app-view-expertise',
  templateUrl: './view-expertise.component.html',
  styleUrls: ['./view-expertise.component.scss']
})
export class ViewExpertiseComponent implements OnInit, OnDestroy {
  expertises = [];
  selectedExpertises: any = [];
  selectedExpertise:any;

  sub: Subscription;
  chatSessions = [];
  chatSubscription: Subscription;
  userData: any;
  logedinData: any;

  constructor(
    public apiService: ApiService,
    public router: Router,
    public thirdPApiService: ThirdPApiService,
    public auth: AuthService,
    public generalService: GeneralService,
    private loader: NgxSpinnerService,
    public toastr : ToastrService,
    private ref: ElementRef
  ) { }
  
  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    console.log(this.logedinData);
    this.fetchExpertise();
    this.chatSubscription = this.apiService.getCustomerChatSessionList().subscribe((data: any) => {
      const { count, response } = data.data;
      this.chatSessions = [...response];
    });
  }
  fetchExpertise() {
    this.loader.show();
    this.sub = this.apiService.getAllInterest().subscribe((response: any) => {
      let tempData = [];
      this.logedinData["expertise"].forEach(element => {
        let result = response.find(obj=>obj.interest == element);
        if(result){
          tempData.push(result);
        }
      });
      this.expertises = tempData;
      this.loader.hide();
    });
  }
  deleteExpertise(expertise: any) {
    const payload = JSON.parse(localStorage.getItem('logedin_data'));
    payload.expertise = payload.expertise.filter(item => item !== expertise.interest);
    this.loader.show();
    this.apiService
      .editPractitioner(payload.id, payload)
      .subscribe((response: any) => {
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response)
        );
        if(response.expertise){
          this.logedinData["expertise"] = response.expertise;
        }
        this.fetchExpertise();
        this.loader.hide();
      });
  }
  selectExpertise(expertise:any){
    this.selectedExpertise=expertise;
    console.log(this.selectedExpertise);
  }

  goToAddExpertise(){
    this.router.navigate(['/add-expertise'], { state: { expertisesSelected: [...this.expertises] } });
    window.scrollTo(0, 0);
  }
  isSelected(expId: string) {
    return this.selectedExpertises.some(exp => exp.id === expId);
  }
  toggleItem(expertise: any) {
    if (this.isSelected(expertise.id)) {
      this.selectedExpertises = this.selectedExpertises.filter(exp => exp.id !== expertise.id);
      return;
    }
    this.selectedExpertises.push(expertise);
  }
  toggleCls(id: any, display: boolean) {
    const card = this.ref.nativeElement.querySelector(`[id="chat-${id}"]`);
    card.classList[display ? 'add' : 'remove']('active');
  }
  ngOnDestroy() {
    this.sub?.unsubscribe();
    this.chatSubscription?.unsubscribe();
  }
}
