import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/ApiService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { GeneralService } from 'src/app/services/GeneralService';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'src/app/services/AuthService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'addcustomer',
  templateUrl: './admin-add-customer-component.html',
  styleUrls: ['./admin-add-customer-component.scss'],
})

export class AddCustomerComponent implements OnInit {
  public _token: any;
  fieldTextType1: boolean;
  fieldTextType2: boolean;
  invalid_email: boolean;
  email_required: boolean;
  signupForm: FormGroup
  user = {
    username: "",
    email: "",
    password1: "",
    password2: "",
    first_name: "",
    last_name: "",
    phonenumber: "",
    country: [],
    state: [],
    city: [],
    newsletter: false,
    phone_code: "",
  };
  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  passwordError: any[];
  loginModel: any;
  isContent: any;
  countryData: any = [];
  stateData: any = [];
  cityData: any = [];
  interestData: any = [];

  countryConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder:'Select Country *',
    customComparator: ()=>{},
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder:'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  stateConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder:'Select State *',
    customComparator: ()=>{},
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder:'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  cityConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder:'Select City *',
    customComparator: ()=>{},
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder:'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }

  expertiseConfig = {
    displayKey: 'interest',
    search: true,
    height: 'auto',
    placeholder: 'Select Expertise *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'interest',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }

  selectedCountryISO: any;
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    public apiService: ApiService,
    public thirpApiService: ThirdPApiService,
    public modalService: NgbModal,
    public toastr: ToastrService,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
  ) {}

  ngOnInit() {

    this.apiService.getInterest()
    .subscribe((expertise: any) => {
      this.interestData = [...expertise];
    });

    this.loadCountry();
  }
  validateEmail(email) {
    let patternRe =/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(patternRe.test(email.target.value)){
      this.invalid_email = false;
      this.email_required = false;
    }else{
      this.invalid_email = true;
      this.email_required = false;
      if(email.target.value == ''){
        this.email_required = true
        this.invalid_email = false
      }
    }
  }
  // remeberMe(event:any){
    // if(this.user.remember){
    //   localStorage.setItem('is_remember', 'true');
    // }else{
    //   localStorage.setItem('is_remember', 'false');
    // }
  // }
  onlyCapitalLetters (str) { 
    let newStr = "";
    for (let i = 0; i < str.length; i++) {
      if (str[i].match(/[A-Z]/)) {
        newStr += str[i];
      }
    }
    return newStr;
  }
  onlySmallLetters (str) { 
    let newStr = "";
    for (let i = 0; i < str.length; i++) {
      if (str[i].match(/[a-z]/)) {
        newStr += str[i];
      }
    }
    return newStr;
  }
  onlySpecialChar (str) { 
    let newStr = "";
    for (let i = 0; i < str.length; i++) {
      if (str[i].match(/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#]/)) {
        newStr += str[i];
      }
    }
    return newStr;
  }
  sociaLogin(User, isUser){
    this.loader.show();
    let payload;
    if(isUser == "google"){
      payload = {
        access_token:	User.access_token ? User.access_token : User.authToken,
        code:	User.first_issued_at ? User.first_issued_at : User.id,
        id_token:	User.id_token ? User.id_token : User.idToken,
      }
    }else{
      payload = {
        access_token:	User.authToken,
        code:	User.id,
        id_token:	User.id,
      }
    }
    this.apiService
    .sociaLogin(payload,isUser)
    .subscribe((response: any) => {
      // console.log(response);
      if(response){
        localStorage.setItem(
          "access_token",
          response.access_token
        );
        localStorage.setItem(
          "refresh_token",
          response.refresh_token
        );
        delete response.access_token;
        delete response.refresh_token;
        localStorage.setItem(
          "user_data",
          JSON.stringify(response)
        );
        this.loader.hide();
        if(response.is_user_object_created == false){
          this.loader.show();
          this.apiService
          .pkIdUpdate(response.groups[0].toLowerCase(),"post")
          .subscribe((response: any) => {
            console.log(response);
            localStorage.setItem(
              `logedin_data`,
              JSON.stringify(response[0] ? response[0] : response["results"][0])
            );
            this.toastr.success('Login Successfully');
            this.loggedUpdate({
              is_user_object_created: true
            });
          });
        }else{
          this.PKCallInit(response.groups[0].toLowerCase(),"get");
        }
        //window.location.reload();
        // if(response["groups"][0]){
        //   let routeName = this.generalService.routeName(response["groups"][0]);
        //   this.router.navigate([routeName]);
        // }
      }
    },
    (errResponse: HttpErrorResponse) => {
      if(errResponse['error']['non_field_errors']){
        errResponse['error']['non_field_errors'].forEach(element => {
          this.toastr.error(element);
        });
      }else{
        this.toastr.error('Something went wrong please try again');
      }
      this.loader.hide();
    }
  );
  }
  AddCustomer(){
    if (this.user) {
      this.loader.show();
      let payload = {
        username: this.user.email,
        email: this.user.email,
        password1: "welcome@HEALPLACE123#!",
        password2: "welcome@HEALPLACE123#!",
        first_name: this.user.first_name,
        last_name: this.user.last_name,
        phonenumber: this.user.phone_code + this.user.phonenumber,
        city_id: this.user.city['id'],
        is_newsletter: this.user.newsletter,
      }
      console.log(payload);
      this.apiService
        .signup(payload)
        .subscribe((response: any) => {
          console.log(response);
          if(response.email){
            this.toastr.warning(response.email);
          }else if(response.username){
            this.toastr.warning(response.email);
          }else if(response.detail){
            this.router.navigate(["/admin/customer-list"]);
            this.toastr.success('New customer created successfully.')
          }
          this.loader.hide();
        },
        (errResponse: HttpErrorResponse) => {
          if(errResponse['error']['non_field_errors']){
            errResponse['error']['non_field_errors'].forEach(element => {
              this.toastr.error(element);
            });
          }else if(errResponse['error']['password1']){
            errResponse['error']['password1'].forEach(element => {
              this.toastr.error(element);
            });
          }else if(errResponse['error']['phonenumber']){
            errResponse['error']['phonenumber'].forEach(element => {
              this.toastr.error(element);
            });
          }else{
            this.toastr.error('Something went wrong please try again');
          }
          this.loader.hide();
        }
      );
    }
  }
  loggedUpdate(payload){
    this.loader.show();
    let userData = JSON.parse(localStorage.getItem("user_data"));
    this.loader.show();
    this.apiService
    .previousLoggedUpdate(payload, userData["user"]["pk"])
    .subscribe((response: any) => {
      console.log(response);
      userData["is_previously_logged_in"] = true;
      localStorage.setItem(
        `user_data`,
        JSON.stringify(userData)
      );
      this.loader.hide();
    });
  }
  PKCallInit(group, method){
    this.loader.show();
    this.apiService
    .pkIdUpdate(group,method)
    .subscribe((response: any) => {
      console.log(response);
      localStorage.setItem(
        `logedin_data`,
        JSON.stringify(response[0])
      );
      this.toastr.success('Login Successfully');
      this.loader.hide();
      window.location.reload();
    });
  }
  loadCountry(){
    this.loader.show();
    this.thirpApiService
    .getCountries("id,phone_code","no_page")
    .subscribe((response: any) => {
      console.log(response);
      this.countryData = response;
      this.loader.hide();
    });
  }
  loadState(countryId){
    this.loader.show();
    this.thirpApiService
    .getStates(countryId,"id","no_page")
    .subscribe((response: any) => {
      console.log(response);
      this.stateData = response;
      this.loader.hide();
    });
  }
  loadCity(stateId){
    this.loader.show();
    this.thirpApiService
    .getCities(stateId,"id","no_page")
    .subscribe((response: any) => {
      console.log(response);
      this.cityData = response;
      this.loader.hide();
    });
  }
  //on changes events
  countryChanged(data: any){
    console.log(data);
    let countryPre: any = `CountryISO.${data.name.split(' ').join('')}`
    // this.preferredCountries = [countryPre];
    //this.selectedCountryISO = countryPre;
    if(data){
      if(data.phone_code.slice(0, 1) == "+"){
        this.user.phone_code = `${data.phone_code}`;
      }else{
        this.user.phone_code = `+${data.phone_code}`;
      }
      this.stateData = [];
      this.cityData = [];
      this.user.state = [];
      this.user.city = [];
      this.loadState(data.id);
      console.log(this.user);
    }else{
      this.stateData = [];
      this.cityData = [];
      this.user.state = [];
      this.user.city = [];
    }
  }
  stateChanged(data: any){
    console.log(data);
    if(data){
      this.cityData = [];
      this.user.city = [];
      this.loadCity(data.id);
    }else{
      this.cityData = [];
      this.user.city = [];
    }
    console.log(this.user);
  }
  cityChanged(data: any){
    console.log(data);
  }
  phoneChanged(data:any){
    console.log(data);
    console.log(this.user.phonenumber)
  }
  omit_char(arg:any){
    console.log(arg);
  }
  password_visit1() {
    let x = document.getElementById("signup-password1") as HTMLInputElement;
    if (x.type == "password") {
      document.getElementById("eye-open1").className = "fa fa-lg fa-eye field-icon toggle-password view-hide";
      x.type = "text";
    } else {
      document.getElementById("eye-open1").className = "fa fa-lg fa-eye-slash field-icon toggle-password view-hide";
      x.type = "password";
    }
  }
  password_visit2() {
    let x = document.getElementById("signup-password2") as HTMLInputElement;
    if (x.type == "password") {
      document.getElementById("eye-open2").className = "fa fa-lg fa-eye field-icon toggle-password view-hide";
      x.type = "text";
    } else {
      document.getElementById("eye-open2").className = "fa fa-lg fa-eye-slash field-icon toggle-password view-hide";
      x.type = "password";
    }
  }

  expertiseChanged(data) {
    console.log(data);
  }
}