import { HttpClient } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-profile-picture-modal',
  templateUrl: './profile-picture-modal.component.html',
  styleUrls: ['./profile-picture-modal.component.scss']
})
export class ProfilePictureModalComponent implements OnInit, OnDestroy {

  @Input() isPractitioner: boolean;
  @Input() uploadDone: any;
  imageList = [];
  imageSubscription: Subscription;
  errorMessages = {
    INVALID_FILE_TYPE: 'Non-supported image type uploaded',
    MAXIMUM_SIZE: 'You can only upload image of file size less than 2MB',
    DIMENSIONS: 'The image should be 400 x 400 pixels at least',
  };
  validation = {
    type: false,
    size: false,
    dimensions: false,
  };

  @ViewChild('fileInput') input;
  userData: any;
  logedinData: any;
  profilePic: any;
  curatedChoosen: any;
  result: any;
  constructor(
    private http: HttpClient,
    private apiService: ApiService,
    public toastr: ToastrService,
    public router: Router,
    private loader: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('user_data'));
    this.logedinData = JSON.parse(localStorage.getItem('logedin_data'));
    this.imageSubscription = this.apiService.getCuratedProfilePictures().subscribe((result: any) => {
      this.imageList = [...result];
    });
  }

  ngOnDestroy() {
    this.imageSubscription?.unsubscribe();
  }

  onUploadButtonClick() {
    this.input.nativeElement.click();
  }

  onFileChanged(event) {
    const files = event.target.files;
    this.resetValidation();
    if (files[0]) {
      const file = files[0];
      this.profilePic = file;
      if (!this.validFileType(file)) {
        this.toastr.error(this.errorMessages.INVALID_FILE_TYPE);
      } else {
        this.validation.type = true;
      }

      if (!this.validFileSize(file.size)) {
        this.toastr.error(this.errorMessages.MAXIMUM_SIZE);
      } else {
        this.validation.size = true;
      }

      const reader = new FileReader();
      const self = this;
      reader.readAsDataURL(file);
      reader.onload = e => {
        // Initiate the JavaScript Image object.
        const image: any = new Image();
        image.src = e.target.result;

        // Validate the File Height and Width.
        image.onload = function () {
          const height = this.height;
          const width = this.width;
          if (height < 400 || width < 400) {
            self.toastr.error(self.errorMessages.DIMENSIONS);
            self.validation.dimensions = false;
            self.triggerEnable();
            return false;
          }
          self.validation.dimensions = true;
          this.curatedChoosen = undefined;
          self.triggerEnable();
          return true;
        };

      };
      this.triggerEnable();
    }
  }

  private validFileType(file: File): boolean {
    return [
      'image/bmp',
      'image/gif',
      'image/jpeg',
      'image/png',
      'image/tiff',
    ].includes(file.type);
  }

  private validFileSize(size: number): boolean {
    return size <= (1024 * 1024 * 2);
  }

  private resetValidation() {
    this.validation.dimensions = false;
    this.validation.size = false;
    this.validation.type = false;
  }

  private triggerEnable() {
    const { type, size, dimensions } = this.validation;

    return type && size && dimensions;
  }

  async saveCuratedProfile(data: any) {
    const refContent = document.getElementById(`${data.id}`) as HTMLInputElement;
    refContent.className = 'fix-shadow';
    this.curatedChoosen = data;
    try {
      var binArray = []
      var datEncode = "";
  
      for (let i=0; i < data.image.length; i++) {
          binArray.push(data.image[i].charCodeAt(0).toString(2)); 
      } 
      for (let j=0; j < binArray.length; j++) {
          var pad = padding_left(binArray[j], '0', 8);
          datEncode += pad + ' '; 
      }
      function padding_left(s, c, n) { if (! s || ! c || s.length >= n) {
          return s;
      }
      var max = (n - s.length)/c.length;
      for (var i = 0; i < max; i++) {
          s = c + s; } return s;
      }
      console.log(binArray);
      this.profilePic = binArray;
    } catch (err) {
      console.error(err);
    }
  }
  saveProfile() {
    if (this.profilePic) {
      const formData: FormData = new FormData();
      formData.append('id', this.logedinData.id);
      formData.append('profile_image', this.profilePic);
      console.log(this.curatedChoosen);
      if (this.curatedChoosen) {
        let resultOut;
        this.getBase64ImageFromUrl(this.curatedChoosen.image)
        .then(result => resultOut = result)
        .catch(err => console.error(err));
        console.log(resultOut);
          if(resultOut){
            formData.append('profile_image', resultOut);
            formData.append('is_curated_images', 'true');
            formData.append('is_profile_pic_approved', 'true');
          }
      } else {
        formData.append('is_curated_images', 'false');
        formData.append('is_profile_pic_approved', 'false');
      }
      formData.append('user_id', this.logedinData.user_id.id);
      if (this.isPractitioner) {
        this.loader.show();
        this.imageSubscription = this.apiService
          .updatePractitionerProfilePicture(this.logedinData.id, formData)
          .subscribe((result: any) => {
            console.log(result);
            this.loader.hide();
            // this.router.navigate(['/practitioner-profile']);
            // window.scrollTo(0, 0);
            if (this.uploadDone) {
              //this.uploadDone();
            }
            this.toastr.success("Profile image uploaded successfully.");
            this.router.navigate(['/practitioner-profile']);
          });
      } else {
        this.loader.show();
        this.imageSubscription = this.apiService
          .updateProfilePicture(this.logedinData.id, formData)
          .subscribe((result: any) => {
            if (this.uploadDone) {
              //this.uploadDone();
            }
            this.loader.hide();
            this.toastr.success("Profile image uploaded successfully.");
            this.router.navigate(['/customer-profile']);
          });
      }
    }
  }
  async getBase64ImageFromUrl(imageUrl) {
    var res = await fetch(imageUrl);
    var blob = await res.blob();
  
    return new Promise((resolve, reject) => {
      var reader  = new FileReader();
      reader.addEventListener("load", function () {
          resolve(reader.result);
      }, false);
  
      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })
  }
}
