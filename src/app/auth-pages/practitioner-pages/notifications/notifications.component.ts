import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  duration: any = 2;
  method: any = 3;
  reminder: any = 4;
  userData: any;
  logedinData: any;

  news_and_updates: any;
  notification_type: any;
  reminder_type: any;
  constructor(
    public apiService: ApiService,
    public router: Router,
    public thirpApiService: ThirdPApiService,
    public auth: AuthService,
    public generalService: GeneralService,
    private loader: NgxSpinnerService,
    public toastr : ToastrService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.generalService.showProfile=false;
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.loadData();
  }
  loadData(){
    this.loader.show();
    this.apiService
    .getManageNotification(this.userData.groups[0], this.logedinData["id"])
    .subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.news_and_updates = res.news_and_updates;
        this.notification_type = res.notification_type;
        this.reminder_type = res.reminder_type;
      }
      this.loader.hide();
    },
    (errResponse: HttpErrorResponse) => {
      this.loader.hide();
    });
  }
  updateNotification(){
    this.loader.show();
    let payload = {
      "is_sms_subscribe": this.logedinData["is_sms_subscribe"],
      "is_email_subscribe": this.logedinData["is_email_subscribe"],
      "news_and_updates": this.news_and_updates,
      "notification_type": this.notification_type,
      "reminder_type": this.reminder_type,
    };
    if(this.notification_type == "SMS"){
      payload.is_sms_subscribe = true;
      payload.is_email_subscribe = false;
    }else if(this.notification_type == "Both"){
      payload.is_sms_subscribe = true;
      payload.is_email_subscribe = true;
    }else{
      payload.is_sms_subscribe = false;
      payload.is_email_subscribe = true;
    }
    this.apiService
    .updateManageNotification(this.userData.groups[0], this.logedinData["id"],payload)
    .subscribe((res: any) => {
      if (res) {
        this.news_and_updates = res.news_and_updates;
        this.notification_type = res.notification_type;
        this.reminder_type = res.reminder_type;
        this.logedinData["is_email_subscribe"] = res.is_email_subscribe;
        this.logedinData["is_sms_subscribe"] = res.is_sms_subscribe;
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(this.logedinData)
        );
      }
      this.loader.hide();
    },
    (errResponse: HttpErrorResponse) => {
      this.loader.hide();
    });
  }
  newsUpdate(obj){

  }
}
