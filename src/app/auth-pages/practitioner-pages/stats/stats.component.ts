import { Component, ElementRef, OnInit } from '@angular/core';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ApiService } from '../../../services/ApiService';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {
  data: any = [];
  chatStat = [];
  dataSubscription: Subscription;
  ratingsLabels = ['5 stars', '4 stars', '3 stars', '2 stars', '1 stars'];
  ratingsData = [
    {
      data: [10, 10, 10, 10, 10],
      label: 'Series A',

      barThickness: 18, barPercentage: 0.1,
      categoryPercentage: 0.1
    }
  ];
  stats: any;

  chatsData = [
    {
      data: [65, 59, 80, 81, 56, 55, 88],
      label: 'Series B',
      stack: 'a',
      barThickness: 18, barPercentage: 0.1,
      categoryPercentage: 0.1
    },
    {

      data: [30, 20, 0, 0, 0, 0, 0],
      label: 'Series A',
      stack: 'a',
      barThickness: 18, barPercentage: 0.1,
      categoryPercentage: 0.1
    },

  ];
  currentRate = 4;
  constructor(ratingConfig: NgbRatingConfig, private apiService: ApiService,private ref: ElementRef,) {
    // customize default values of ratings used by this component tree
    ratingConfig.max = 5;
    ratingConfig.readonly = true;
  }

  ngOnInit(): void {
    this.getPractitionerStats();
    const [startDate, endDate] = this.createDates();
    this.dataSubscription = this.apiService.getDashboardData(startDate, endDate).subscribe((data: any) => {
      this.data = data;
      console.log(data);
      this.mergeAndSetChatStat();
      
    });
  }
  
  

  mergeAndSetChatStat() {
    const chat = this.data.stats_value.chat;
    const form = this.data.stats_value.followupform;

    this.chatStat = [];

    for (let i = 0; i < chat.length; i++) {
      this.chatStat.push({ ...chat[i], ...form[i] });
    }
  }
  getPractitionerStats() {
    this.apiService.getPractitionerStats().subscribe((success) => {
      console.log('success', success);
      this.stats = success;
    }, (err) => {
      console.log('err', err);
    })
  }
  shortDay(num: number) {
    return [
      'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa',
    ][num];
  }

  createDates(): Array<string> {
    let yourDate = new Date();
    const offset = yourDate.getTimezoneOffset();
    yourDate = new Date(yourDate.getTime() - offset * 60 * 1000);
    const todayDate = yourDate.toISOString().split('T')[0];
    // const lastDate = new Date(yourDate);
    // lastDate.setDate(lastDate.getDate() + 6);
    // const endDate = lastDate.toISOString().split('T')[0];
    let firstday = new Date(yourDate.setDate(yourDate.getDate() - yourDate.getDay()));
    let lastday = new Date(yourDate.setDate(yourDate.getDate() - yourDate.getDay()+6));
    const startDate = firstday.toISOString().split('T')[0];
    const endDate = lastday.toISOString().split('T')[0];
    return [todayDate, todayDate];
  }

  showStats(day) {
    const bar = this.ref.nativeElement.querySelector(`[id="${day}-bar"]`);
    const formBar = this.ref.nativeElement.querySelector(`[id="${day}-form-bar"]`);
    const cloud = this.ref.nativeElement.querySelector(`[id="${day}-cloud"]`);

    const data = this.chatStat.find(d => d.day === day);

    bar.style['background-color'] = '#00BAD5';
    bar.style['border-radius'] = '8px';
    formBar.style['border-radius'] = '8px';
    formBar.style['background-color'] = '#FBD036';
    formBar.style.display = 'block';
    formBar.style.height = `${88 * data.form_count / Math.max(data.chat_count, data.form_count)}px`;

    cloud.style.display = 'block';
    cloud.style.top = '-' + (88 * this.percentage(data) - 49) + 'px';
  }

  hideStats(day) {
    const bar = this.ref.nativeElement.querySelector(`[id="${day}-bar"]`);
    const formBar = this.ref.nativeElement.querySelector(`[id="${day}-form-bar"]`);
    const cloud = this.ref.nativeElement.querySelector(`[id="${day}-cloud"]`);

    bar.style['background-color'] = 'transparent';
    formBar.style['background-color'] = 'transparent';
    formBar.style.display = 'none';
    formBar.style.height = '0px';

    cloud.style.display = 'none';
    cloud.style.top = '0px';
  }

  percentage(data) {
    const maxVal = Math.max(...this.chatStat.map(c => c.chat_count), ...this.chatStat.map(c => c.form_count));
    return Math.max(data.chat_count, data.form_count) / maxVal;
  }
}
