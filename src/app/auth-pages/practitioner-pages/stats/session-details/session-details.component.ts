import { Component, OnInit, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { GeneralService } from '../../../../services/GeneralService';

@Component({
  selector: 'app-session-details',
  templateUrl: './session-details.component.html',
  styleUrls: ['./session-details.component.scss'],
  providers: [NgbCarouselConfig]
})
export class SessionDetailsComponent implements OnInit {

  @Input() reviews: any;
  @Input() sessions: any;
  currentRate = 4;

  constructor(config: NgbCarouselConfig, ratingConfig: NgbRatingConfig, public generalService: GeneralService) {
    // customize default values of carousels used by this component tree
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;

    // customize default values of ratings used by this component tree
    ratingConfig.max = 5;
    ratingConfig.readonly = true;
  }

  ngOnInit(): void {
  }

}
