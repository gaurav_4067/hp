import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { FormGroup } from '@angular/forms';
import { AuthService } from '../../../services/AuthService';
import { ApiService } from '../../../services/ApiService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { GeneralService } from '../../../services/GeneralService';
import { CommonService } from '../../../services/CommonService';
import { TabsetConfig } from 'ngx-bootstrap/tabs';
import { AddLiveClassComponent } from 'src/app/v1/common/view/add-live-class/add-live-class.component';
import { AddChatSessionComponent } from 'src/app/v1/common/view/add-chat-session/add-chat-session.component';
export function getTabsetConfig(): TabsetConfig {
  return Object.assign(new TabsetConfig(), { type: 'pills', isKeysAllowed: true });
}

@Component({
  selector: 'app-practitioner-sessions',
  templateUrl: './practitioner-sessions.component.html',
  styleUrls: ['./practitioner-sessions.component.scss'],
  providers: [{ provide: TabsetConfig, useFactory: getTabsetConfig }]
})
export class PractitionerSessionsComponent implements OnInit {
  sessionType:string='1on1';
  @Input() resource: any;
  @Output() changeBack = new EventEmitter<any>();
  username: string;
  expertise: string;
  profileimage: string;
  isContent: any;
  addSessionModel: any;
  sessions: any = [];
  reload: any;
  loginData: any;

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    public apiService: ApiService,
    public modalService: NgbModal,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    public commonService: CommonService,
  ) { }

  ngOnInit(): void {
    this.loader.show();
    this.fetchSessions();
    let userData = JSON.parse(localStorage.getItem("user_data"));
    this.username = userData?.user.first_name + ' ' + userData?.user.last_name;
    this.loginData = JSON.parse(localStorage.getItem("logedin_data"));
    this.expertise = this.loginData?.expertise;
    this.profileimage = this.loginData?.profile_image;
    this.reload = this.commonService.getUpdate().subscribe
      (message => {
        this.ngOnInit();
      });
  }

  ngOnDestroy() {
    this.reload?.unsubscribe();
  }

  fetchSessions() {
    this.apiService
      .getPractitionerSessions()
      .subscribe((response: any) => {
        if (response) {
          this.sessions = response.results;
        }
        this.loader.hide();
      },
        (errResponse: HttpErrorResponse) => {
          this.loader.hide();
        });
  }

  openAddSessionModal(modalContent: any, isWhere: string) {
    this.changeBack.emit(true);
    this.isContent = isWhere;
    this.addSessionModel = this.modalService.open(modalContent, { windowClass: 'modal-holder add-new-session add-session-modal' });
  }

  closeAddSessionModal(Event: any) {
    console.log('sdsdds')
    this.addSessionModel.close();
  }

  initAddLiveClass(){
    let modalRef = this.modalService.open(AddLiveClassComponent, { scrollable: true, centered: true, ariaLabelledBy: 'signup-confirm-title', size: 'lg', windowClass: 'confirm-modal' });
  }

  initAddChatSession(){
    let modalRef = this.modalService.open(AddChatSessionComponent, { scrollable: true, centered: true, ariaLabelledBy: 'signup-confirm-title', size: 'lg', windowClass: 'confirm-modal' });
  }

}
