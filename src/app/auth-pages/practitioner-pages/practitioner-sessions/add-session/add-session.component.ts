import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../../services/AuthService';
import { ApiService } from '../../../../services/ApiService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { GeneralService } from '../../../../services/GeneralService';
import { CommonService } from '../../../../services/CommonService';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-session',
  templateUrl: './add-session.component.html',
  styleUrls: ['./add-session.component.scss']
})
export class AddSessionComponent implements OnInit {
  @Input() resource: any;
  @Input() title: string;
  @Input() buttonText: string;
  @Input() type: string;
  @Input() editSession: any;
  @Input() session: any;
  @Input() sessions: any;
  @Output() changeBack = new EventEmitter<any>();
  @Output() closeModal = new EventEmitter<any>();
  @Output() reloadSessions = new EventEmitter<any>();

  isContent: any;
  confirmSessionModel: any;
  expertises: any;
  durations = [30,60,90,120];
  addSessionForm = new FormGroup({
    title: new FormControl('', Validators.required),
    duration_min: new FormControl('', Validators.required),
    expertise: new FormControl('', Validators.required),
    price: new FormControl('',  Validators.compose(
      [Validators.minLength(1),Validators.maxLength(4), Validators.required])),
  })

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    public apiService: ApiService,
    public modalService: NgbModal,
    private loader: NgxSpinnerService,
    public generalService: GeneralService,
    private commonService: CommonService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    if (this.editSession) {
      this.addSessionForm.setValue({
        title: this.session.title,
        duration_min: this.session.duration_min,
        expertise: this.session.expertise || '',
        price: this.session.price,
      });
    }
    this.expertises = JSON.parse(localStorage.getItem("logedin_data")).expertise;
    console.log(this.expertises)
    // this.loader.show();
    // this.apiService.getInterest().subscribe((response: any) => {
    //   this. expertises = [...response];
    //   this.loader.hide();
    // },
    //   (errResponse: HttpErrorResponse) => {

    //     this.loader.hide();
    //   }
    // );
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  openConfirmSessionModal(modalContent: any, isWhere: string, session: any,sessions:any) {
    
    if (this.sessions.filter(i => i.expertise[0] == this.addSessionForm.value.expertise && parseInt(i.duration_min, 10) == parseInt(this.addSessionForm.value.duration_min)).length>0) {
      this.toastr.warning("This session is already scheduled");
    }
    else {
      if (parseInt(this.addSessionForm.value.price, 10) == 0) {
        this.toastr.error(`Please enter valid amount`);
        return;
      }
      else {
        this.changeBack.emit(true);
        this.isContent = isWhere;
        const updatedSession = this.addSessionForm.value;
        this.session = { ...session, ...updatedSession };
        this.confirmSessionModel = this.modalService.open(modalContent, { windowClass: 'modal-holder confirm-session add-session-modal' });
      }
    }
  }

  closeConfirmSessionModal(Event: any) {
    this.confirmSessionModel.close();
  }
  back() {
    this.closeModal.emit(true);
  }
  saveSession(session: any) {
    this.loader.show();
    let userData = JSON.parse(localStorage.getItem("user_data"));
    let practitionerId = JSON.parse(localStorage.getItem("logedin_data")).id;

    let data = { ...session, ... { "practitioner": practitionerId } }
   
    let api;
    data.expertise = [this.addSessionForm.value.expertise];
    if (this.editSession) {
      data.expertise = this.addSessionForm.value.expertise;
      api = this.apiService
        .editPractitionerSession(data, this.editSession.id);
    } else {
      api = this.apiService
        .addPractitionerSession(data);
    }

    api.subscribe((response: any) => {
      this.commonService.sendUpdate('reload');
      this.confirmSessionModel.close();
      this.loader.hide();
    },
      (errResponse: HttpErrorResponse) => {
        this.reloadSessions.emit(true);
        this.confirmSessionModel.close();
        this.loader.hide();
      }
    );
  }
}
