import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { PractitionerScheduleService } from '../../../services/practitioner-schedule.service';
import { ChatService } from '../../../services/ChatService';
import { GeneralService } from '../../../services/GeneralService';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from '../../customer-pages/customer-booking/ngbdmodalcontent.component';
import { FollowUpFormComponent } from '../practitioner-chat/follow-up-form/follow-up-form.component';
import { BlockCustomerComponent } from '../../customer-pages/block-customer/block-customer.component';
import { SwitchPractitionerComponent } from '../practitioner-chat/switch-practitioner/switch-practitioner.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ReviewPractitionerComponent } from '../practitioner-chat/review-practitioner/review-practitioner.component';
import { ReportPractitionerComponent } from '../practitioner-chat/report-practitioner/report-practitioner.component';
import { DeleteChatComponent } from '../../customer-pages/delete-chat/delete-chat.component';
import { ApiService } from 'src/app/services/ApiService';
import { ActiveFollowupFormComponent } from 'src/app/v1/practitioner/active-followup-form/active-followup-form.component';

@Component({
  selector: 'app-available-patient',
  templateUrl: './available-patient.component.html',
  styleUrls: ['./available-patient.component.scss']
})
export class AvailablePatientComponent implements OnInit {
  @ViewChild('practitionerPopTemplate') practitionerPopTemplate: TemplateRef<any>;
  
  isAvailable: boolean;
  userData: any;
  logedin_data: any;
  customerList: any = [];
  selectedList: any;
  practitionerList: any = [];
  selectedIndex: any;
  isShowInterest: boolean = false;
  redirect: boolean = false;
  selectedChat;
  constructor(
    private modalService: NgbModal,
    private practitionerScheduleService: PractitionerScheduleService, 
    private loader: NgxSpinnerService,private chatService: ChatService,
    private toaster: ToastrService,
    private apiService: ApiService,
    public generalService: GeneralService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.generalService.setTitle(null);
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedin_data = JSON.parse(localStorage.getItem("logedin_data"));
    this.getPractitioner();
    this.getCustomerList();
  }

  getCustomerList() {
    this.chatService.getCustomerList().subscribe((succes: any) => {
      this.customerList = succes.data.response;
      this.customerList = this.customerList.filter((item: any) => item.is_archieve === false);
    }, (err) => {
      console.log('err', err);
    })
  }

  goToChat(list, index) {
    console.log('list', list);
    this.router.navigate(['/chat'], { queryParams: { selected: list.id, redirect: true }})
  }

  getPractitioner() {
    this.practitionerScheduleService.getPractitioner(this.logedin_data.id).subscribe((success: any) => {
      this.isAvailable = success.chat_availability === 'R' ? false : true;
    }, (err) => {
      console.log('err', err);
    });
  }

  openPractitionerPopup(ev, list) {
    ev.stopPropagation();
    this.selectedList = list;
    this.modalService.open(this.practitionerPopTemplate, { windowClass: 'modal-practitioner', size: 'md' });
  }
  
  switchAvailable() {
    const data = {
      chat_availability: !this.isAvailable ? 'G' : 'R',
      user_id: this.logedin_data.user_id
    }
    this.practitionerScheduleService.changeAvailable(data, this.logedin_data.id).subscribe((success) => {
      console.log('success', success);
    }, (err) => {
      console.log('err', err);
    });
  }

  bookSession() {
    this.modalService.dismissAll();
    const modalRef = this.modalService.open(NgbdModalContent, { ariaLabelledBy: 'modal-basic-title', backdrop: 'static', size: 'lg', windowClass: 'customer-booking-modal' });
    modalRef.componentInstance.selectedList = this.selectedList;
  }

  openFollowUpForm() {
    
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(FollowUpFormComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modelRef.componentInstance.selectedlist = this.selectedList;
  }

  blockCustomer() {
    if(!this.selectedList.is_blocked){
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(BlockCustomerComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modelRef.componentInstance.selectedList = this.selectedList;
    }
    else{
      this.unBlockCustomer();
    }
  }

  switchPractitioner() {
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(SwitchPractitionerComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modelRef.componentInstance.selectedList = this.selectedList;
  }

  isChannelNameAvailable(name) {
    if (name && name != "") {
      return false;
    } else {
      return true
    }
  }

  archiveChat() {
    this.modalService.dismissAll();
    this.loader.show();
    console.log('selectedList', this.selectedList);
    let loggedInData = JSON.parse(localStorage.getItem("logedin_data"));
    const data = {
      practitioner: this.userData.groups[0] === "Practitioner" ? loggedInData.id : this.selectedList.id,
      is_archieve: true,
      archieved_by: this.userData.groups[0] === "Customer" ? "Customer" : "Practitioner",
      customer: this.userData.groups[0] === "Customer" ? loggedInData.id : this.selectedList.id
    };
    this.chatService.archiveChat(data).subscribe((success) => {
      console.log('success', success);
      if (this.userData.groups[0] === "Customer") {
        this.getPractitionerList();
      } else {
        this.getCustomerList();
      }
      this.loader.hide();
      this.toaster.success('Archived Successfully!');
    }, (err) => {
      this.loader.hide();
      console.log(err);
    });
  }

  viewSessions() { 
    this.modalService.dismissAll();
    this.router.navigate(['/session-dashboard']);
  }

  getPractitionerList() {
    this.chatService.getPractitionerList().subscribe((succes: any) => {
      let practitionerList = succes.data;
      if (practitionerList.length > 0) {
        this.practitionerList = practitionerList.filter((item: any) => item.is_archieve === false);
        if (this.redirect) {
          this.practitionerList.forEach((element, index) => {
            if (element.user_id === this.selectedChat) {
              this.selectedIndex = index;
              this.goToChat(this.practitionerList[this.selectedIndex], this.selectedIndex);
            }
          });
        } else {
          this.selectedIndex = 0;
          this.goToChat(this.practitionerList[0], 0);
        }
      } else {
        this.isShowInterest = true;
      }
    }, (err) => {
      console.log('err', err);
    })
  }

  reviewPractitioner() {
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(ReviewPractitionerComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modelRef.componentInstance.selectedList = this.selectedList;
  }

  reportPtactitioner() {
    this.modalService.dismissAll();
    const modelRef = this.modalService.open(ReportPractitionerComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
    modelRef.componentInstance.selectedList = this.selectedList;
  }

  deleteChat() {
    this.modalService.dismissAll();
    this.modalService.open(DeleteChatComponent, { windowClass: 'modal-remove-chat', size: 'lg' });
  }

  unBlockCustomer() {
    this.loader.show();
    this.apiService.unBlockCustomer(this.selectedList.block_customer_id)
      .subscribe((report: any) => {
        console.log('unblock customer', report);
        this.loader.hide();
        this.modalService.dismissAll();
        this.router.navigate(['welcome-practitioner']);
      }, (error) => {
        console.log('unblock customer error', error);
        if (error && error.status) {
          this.loader.hide();
          this.modalService.dismissAll();
          this.router.navigate(['welcome-practitioner']);
        } else {
          this.toaster.error(error.statusText);
          this.loader.hide();
        }
      });
  }

  initActiveFollowupForms(){
    this.modalService.dismissAll();
    let modalRef = this.modalService.open(ActiveFollowupFormComponent, { scrollable: true, centered: true, ariaLabelledBy: 'signup-confirm-title', size: 'lg', windowClass: 'confirm-modal' });
  }
}
