import {Component, EventEmitter, Output} from '@angular/core';
import {NgbDate, NgbCalendar, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-week-selection',
  templateUrl: './week-selection.component.html',
  styleUrls: ['./week-selection.component.scss']
})
export class WeekSelectionComponent {

  hoveredDate: NgbDate | null = null;

  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  @Output() weekRange = new EventEmitter<any>();

  constructor(private calendar: NgbCalendar, public formatter: NgbDateParserFormatter) {
    let yourDate = new Date();
    let firstday = new Date(yourDate.setDate(yourDate.getDate() - yourDate.getDay()));
    let lastday = new Date(yourDate.setDate(yourDate.getDate() - yourDate.getDay()+6));
    const startDate = firstday.toISOString().split('T')[0];
    const endDate = lastday.toISOString().split('T')[0];

    let fromDate = new Date();
    this.fromDate = new NgbDate(fromDate.getFullYear(),fromDate.getMonth()+1,fromDate.getDate() - fromDate.getDay());
    this.toDate = new NgbDate(fromDate.getFullYear(),fromDate.getMonth()+1,yourDate.getDate() - yourDate.getDay()+6)
  }

  onDateSelection(date: NgbDate) {
    let fromDate=new Date(date.year+'-'+date.month+'-'+date.day);
    this.fromDate=new NgbDate(fromDate.getFullYear(),fromDate.getMonth()+1,fromDate.getDate());
    const toDate=new Date(fromDate.getTime()+6*24*60*60*1000)
    this.toDate=new NgbDate(toDate.getFullYear(),toDate.getMonth()+1,toDate.getDate())
    this.weekRange.emit({startdate: new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day), enddate: new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day)});
   }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }
}




