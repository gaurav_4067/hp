import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import '../../../../services/dateExtension';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from '../../../../services/ApiService';
import { Router, ActivatedRoute } from '@angular/router';
import { PractitionerScheduleService } from '../../../../services/practitioner-schedule.service';
// import * as moment from 'moment';
import * as moment from 'moment-timezone';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-schedule',
  templateUrl: './edit-schedule.component.html',
  styleUrls: ['./edit-schedule.component.scss']
})
export class EditScheduleComponent implements OnInit {

  settings = {
    availability: [],
    isMultiple: false,
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    selectedDates: [],
    startDate: null,
    endDate: null,
    weekdays: ['sun', 'mon', 'tue', 'wed', 'thurs', 'fri', 'sat'],
  }
  selectedIndex: any;
  text: any = '';
  days = [];
  schedule = [];
  week: string;
  timeSchedule: any = [
    '12:00 am', '12:30 am', '01:00 am', '01:30 am', '02:00 am', '02:30 am', '03:00 am', '03:30 am', '04:00 am', '04:30 am', '05:00 am', '05:30 am', '06:00 am', '06:30 am', '07:00 am', '07:30 am', '08:00 am', '08:30 am', '09:00 am', '09:30 am', '10:00 am', '10:30 am', '11:00 am', '11:30 am', '12:00 pm', '12:30 pm', '01:00 pm', '01:30 pm', '02:00 pm', '02:30 pm', '03:00 pm', '03:30 pm', '04:00 pm', '04:30 pm', '05:00 pm', '05:30 pm', '06:00 pm', '06:30 pm', '07:00 pm', '07:30 pm', '08:00 pm', '08:30 pm', '09:00 pm', '09:30 pm', '10:00 pm', '10:30 pm', '11:00 pm', '11:30 pm'
  ]
  scheduleList: any = [];
  selectedDate: any;

  @ViewChild('CopyToAllModal') CopyToAllModal: TemplateRef<any>;
  @ViewChild('DuplicateScheduleModal') DuplicateScheduleModal: TemplateRef<any>;

  constructor(
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute,
    private practitionerScheduleService: PractitionerScheduleService,
    public modalService: NgbModal,
    private toastr: ToastrService
  ) {
    this.route.queryParams.subscribe(params => {
      if (params.date) {
        this.selectedDate = params.date;
      }
    });
  }

  ngOnInit(): void {
    this.onInit();
  }

  onInit(){
    var firstday;
    var currentDate;
    if (this.selectedDate) {
      firstday = new Date(this.selectedDate).toUTCString();
    } else {
      currentDate = new Date();
      firstday = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay() - 6)).toUTCString();
    }
    this.settings.startDate = new Date(firstday);
    this.getDatesHeader()
      .then(() => {
        this.getNavControl();
      })
    this.settings.availability = this.getTime(0, 24);
  }

  isDateExpired(list) {
    const date = new Date(list.utc);
    return new Date().setHours(0, 0, 0, 0) <= date.setHours(0, 0, 0, 0);
  }

  changeWeek(week) {
    this.days = [];

    this.settings.startDate = week.startdate;
    this.settings.endDate = week.enddate;
    this.getDatesHeader()
      .then(() => {
        this.getNavControl();
      })
  }
  getTime(startTime, endTime) {
    var time = [];
    var currentTime = startTime;
    while (currentTime < endTime) {
      const suffix = (currentTime >= 12) ? 'pm' : 'am';
      const tempTime = ((currentTime + 11) % 12 + 1);
      time.push({
        displayTime: tempTime + ':00' + suffix,
        time: currentTime + ':00',
      });
      currentTime++;
    }
    return time;
  }

  getMonthName(idx) {
    return this.settings.months[idx];
  };

  getDatesHeader() {
    return new Promise((resolve, reject) => {
      for (let i = 0; i < 7; i++) {
        let d = this.settings.startDate.addDays(i);
        this.days.push({
          fullDate: d,
          date: d.getDate(),
          day: this.settings.weekdays[d.getDay()],
          month: d.getMonth() + 1,
          monthName: this.getMonthName(d.getMonth()),
          utc: d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate(),
          available: [],
          disable: [],
          isAdd: false,
          checked: false
        })
      }
      resolve(true);
    })
  }
  getNavControl() {
    const firstday = this.days[0].day + ', ' + this.days[0].monthName + ' ' + this.days[0].date;
    const lstday = this.days.length - 1;
    const lastday = this.days[lstday].day + ', ' + this.days[lstday].monthName + ' ' + this.days[lstday].date;
    this.week = firstday + ' - ' + lastday;
    this.fetchSchedule(this.days[0].utc, this.days[lstday].utc);
  };
  previousWeek() {
    this.days = [];

    this.settings.startDate = this.settings.startDate.addDays(-7);
    this.getDatesHeader()
      .then(() => {
        this.getNavControl();
      })
  }
  nextWeek() {
    this.days = [];
    this.settings.startDate = this.settings.startDate.addDays(7);
    this.getDatesHeader()
      .then(() => {
        this.getNavControl();
      })
  }

  formatDate(d) {
    var date = '' + d.getDate();
    var month = '' + (d.getMonth() + 1);
    var year = d.getFullYear();
    if (date.length < 2) {
      date = '0' + date;
    }
    if (month.length < 2) {
      month = '0' + month;
    }
    return year + '-' + month + '-' + date;
  }

  fetchSchedule(start_date, end_date) {
    this.loader.show();
    const end = new Date(end_date);
    end_date = end.setDate(new Date(end_date).getDate() + 1);
    end_date = new Date(end_date).toJSON().slice(0, 10);
    this.apiService.getPractitionerSchedule(start_date, end_date).subscribe((response: any) => {
      this.schedule = response;

      this.schedule.forEach((item) => {
        let date = new Date(item.start_time);
        let d = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        let practioner = item.practitioner;
        this.days.forEach((day) => {
          if (day.utc === d) {
            const start = new Date(item.start_time).getUTCHours();
            const end = new Date(item.end_time).getUTCHours();
            const startTime = new Date(item.start_time).getUTCMinutes();
            const endTime = new Date(item.end_time).getUTCMinutes();
            const startClock = (start >= 12) ? 'PM' : 'AM';
            const endClock = (end >= 12) ? 'PM' : 'AM'
            const available = {
              start: ((start + 11) % 12 + 1) + ':' + (startTime === 0 ? '00' : startTime) + ' ' + startClock,
              end: ((end + 11) % 12 + 1) + ':' + (endTime === 0 ? '00' : endTime) + ' ' + endClock,
              start_time: item.start_time,
              end_time: item.end_time,
              practioner: practioner,
              id: item.id
            }
            day.available.push(available);
          }
        });
      });
      this.scheduleList = this.days;
      this.loader.hide();
    },
      (errResponse) => {
        this.loader.hide();
      }
    );
  }

  addNewSchedule(i) {
    if (this.days[i].available.length !== 0)
      this.scheduleList[i].isAdd = !this.days[i].isAdd;
  }

  removeSchedule(i, j) {
    if (this.scheduleList[i].available.length === 1) {
      this.scheduleList[i].available.splice(j, 1);
    } else {
      this.scheduleList[i].available.splice(j, 1);
      this.scheduleList[i].isAdd = !this.days[i].isAdd;
    }
  }

  isTimeExpired(time, i){
    if(moment(this.scheduleList[i].fullDate).format('L') === moment(new Date()).format('L')){
      let date = moment(this.scheduleList[i].fullDate).format('L');
      const currentDate = moment(new Date()).format('HH:mm');
      const d = moment(date + ' ' + time).format();
      if(currentDate < moment(d).format('HH:mm')){
        console.log('if', moment(d).format('HH:mm'), currentDate);
        return false;
      } else {
        console.log('else', moment(d).format('HH:mm'), currentDate);
        return true;
      }
    } else {
      return false;
    }
  }

  addTime(text, time, i, j) {
    
    const currentDate = new Date();
    const selectedTime = time;
    const d = moment(currentDate).format('L');
    const date = moment(d + ' ' + selectedTime).format();
    let isPresent = false;
    if (j === undefined || j === null) {
      if (text === 'From') {
        if (this.scheduleList[i].available.length > 0) {
          this.scheduleList[i].available.forEach((item) => {
            if (item.start && moment(item.start, ["h:mm A"]).format('HH:mm') <= moment(date).format('HH:mm') && moment(date).format('HH:mm') < moment(item.end, ["h:mm A"]).format('HH:mm')) {
              isPresent = true;
            } else if (moment(item.start_time).format('HH:mm') <= moment(date).format('HH:mm') && moment(date).format('HH:mm') < moment(item.end_time).format('HH:mm')) {
              isPresent = true;
            }
          })
        }
        if (isPresent) return;
        if(this.isTimeExpired(selectedTime, i)) {
          this.toastr.error(`Schedule can not be created for past time.`);
          return;
      };
        this.scheduleList[i].available.push({
          start_time: date,
          end_time: moment(date).add(30, 'minutes').format(),
          isNew: true
        });
      } else if (text === 'To') {
        if (this.scheduleList[i].available.length > 0) {
          this.scheduleList[i].available.forEach((item) => {
            console.log(item, moment(item.start, ["h:mm A"]).format('HH:mm'), moment(date).format('HH:mm'), moment(item.end, ["h:mm A"]).format('HH:mm'), moment(item.start_time).format('HH:mm'), moment(item.end_time).format('HH:mm'));
            if (item.start && moment(item.start, ["h:mm A"]).format('HH:mm') <= moment(date).format('HH:mm') && moment(date).format('HH:mm') < moment(item.end, ["h:mm A"]).format('HH:mm')) {
              isPresent = true;
            } else if (moment(item.start_time).format('HH:mm') <= moment(date).format('HH:mm') && moment(date).format('HH:mm') <= moment(item.end_time).format('HH:mm')) {
              isPresent = true;
            }
          })
        }
        if (isPresent) return;
        if(this.isTimeExpired(selectedTime, i)) {
            this.toastr.error(`Schedule can not be created for past time.`);
            return;
        };
        this.scheduleList[i].available.push({
          start_time: moment(date).subtract(30, 'minutes').format(),
          end_time: date,
          isNew: true
        });
      }
    } else {
      if (text === 'From') {
        const end = this.scheduleList[i].available[j].end_time;
        const duration = moment.duration(moment(end).diff(moment(date)));
        const hours = duration.asHours();
        if (this.scheduleList[i].available.length > 0) {
          this.scheduleList[i].available.forEach((item) => {
            if (moment(item.start, ["h:mm A"]).format('HH:mm') <= moment(date).format('HH:mm') && moment(date).format('HH:mm') <= moment(item.end, ["h:mm A"]).format('HH:mm')) {
              isPresent = true;
            }
          })
        }
        if (isPresent) return;
        if (hours > 2) {
          this.toastr.error(`Oops! You can't schedule session more than 2 hours.`);
          return;
        }
        if (date >= end) return;
        this.scheduleList[i].available[j].start_time = date;
        this.scheduleList[i].available[j].start = moment(date).format('LT')
      } else if (text === 'To') {
        const start = this.scheduleList[i].available[j].start_time;
        const duration = moment.duration(moment(date).diff(moment(start)));
        const hours = duration.asHours();
        if (this.scheduleList[i].available.length > 0) {
          this.scheduleList[i].available.forEach((item) => {
            if (moment(item.start, ["h:mm A"]).format('HH:mm') <= moment(date).format('HH:mm') && moment(date).format('HH:mm') <= moment(item.end, ["h:mm A"]).format('HH:mm')) {
              isPresent = true;
            }
          })
        }
        if (isPresent) return;
        if (hours > 2) {
          this.toastr.error(`Oops! You can't schedule session more than 2 hours.`);
          return;
        }
        if (start >= date) return;
        this.scheduleList[i].available[j].end_time = date;
        this.scheduleList[i].available[j].end = moment(date).format('LT');
      }
      this.scheduleList[i].available[j].isUpdated = true;
    }
    this.scheduleList[i].isAdd = false;

  }

  saveSchedule() {
    let data = {};
    let create_data = [];
    let update_data = [];
    this.scheduleList.forEach((schedule) => {
      // let create_data = {};
      // let update_data = {};
      if (schedule.available.length !== 0) {
        let newArr = [];
        let updateArr = [];
        schedule.available.forEach((item) => {
          if (item.isNew) {
            const arr = [];
            arr.push(moment(item.start_time).format('HH:mm'));
            arr.push(moment(item.end_time).format('HH:mm'))
            arr.push(moment.tz.guess());
            newArr.push(arr);
          } else if (item.isUpdated) {
            const arr = [];
            arr.push(moment(item.start_time).format('HH:mm'));
            arr.push(moment(item.end_time).format('HH:mm'))
            update_data.push({
              'record_id': item.id,
              'update_times': arr
            });
          }
        })
        if (newArr.length > 0) {
          create_data.push({
            [schedule.utc]: newArr
          })
        }
      }
    });

    data = {
      'create_data': create_data,
      'update_data': update_data,
      'delete_data': []
    }

    this.practitionerScheduleService.saveAvailablity(data).subscribe((success) => {
      this.router.navigate(['practitioner/schedule']);
    }, (err) => {
      console.log('errerr', err);
    });
  }

  selectItem(i) {
    this.scheduleList[i].checked = !this.scheduleList[i].checked;
  }

  openCopyToAllConfirmModal(i) {
    this.selectedIndex = i;
    this.modalService.open(this.CopyToAllModal, { windowClass: 'modal-copy-to-all', size: 'md' });
  }

  openDuplicateScheduleConfirmModal() {
    this.modalService.open(this.DuplicateScheduleModal, { windowClass: 'modal-copy-to-all', size: 'md' });
  }

  duplicateSchedule() {
    const dates = [];
    this.scheduleList.forEach((item) => {
      if (item.checked) {
        dates.push(item.utc)
      }
    })

    if (dates.length === 0) return;

    this.practitionerScheduleService.duplicateSchedule({ dates: dates }).subscribe((success) => {
      this.modalService.dismissAll();
      this.router.navigate(['practitioner/schedule']);
    }, (err) => {
      console.log('err', err);
    })
  }

  copySchedule() {
    const available = this.scheduleList[this.selectedIndex].available;
    let newArr = [];
    available.forEach((item) => {
      const arr = [];
      arr.push(moment(item.start_time).format('HH:mm'));
      arr.push(moment(item.end_time).format('HH:mm'));
      arr.push(moment.tz.guess());
      newArr.push(arr);
    });

    const create_data = {
      'create_data': [],
      'update_data': [],
      'delete_data': []
    };

    this.scheduleList.forEach((item, index) => {
      if (item.available.length > 0) {
        item.available.forEach((schedule) => {
          create_data.delete_data.push(schedule.id);
        });
      }
    });

    this.scheduleList.forEach((item, index) => {
      create_data.create_data.push({
        [this.scheduleList[index].utc]: newArr
      })
    });

    this.practitionerScheduleService.saveAvailablity(create_data).subscribe((success) => {
      this.modalService.dismissAll();
      //this.onInit();
      
      //this.selectItem(this.selectedIndex);
      //this.router.navigate(['practitioner/schedule']);
    }, (err) => {
      console.log('err', err);
    })
  }
}
