import { Component, OnInit } from '@angular/core';
import '../../../services/dateExtension';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from '../../../services/ApiService';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { PractitionerScheduleService } from '../../../services/practitioner-schedule.service';
import { ToastrService } from 'ngx-toastr';

declare const gapi;

@Component({
  selector: 'app-practitioner-schedule',
  templateUrl: './practitioner-schedule.component.html',
  styleUrls: ['./practitioner-schedule.component.scss']
})
export class PractitionerScheduleComponent implements OnInit {
  settings = {
    availability: [],
    isMultiple: false,
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    selectedDates: [],
    startDate: null,
    weekdays: ['sun', 'mon', 'tue', 'wed', 'thurs', 'fri', 'sat'],
  }
  days = [];
  fromCalender = [];
  schedule = [];
  week: string;
  constructor(
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    private router: Router,
    private practitionerScheduleService: PractitionerScheduleService,
    private toastr: ToastrService,
  ) {
  }

  updateSigninStatus() {
    console.log('gapi', gapi);
    gapi.client.calendar.events.list({
      'calendarId': 'primary',
      'timeMin': (new Date(this.settings.startDate)).toISOString(),
      'timeMax': (new Date(this.settings.startDate.addDays(6))).toISOString(),
      'showDeleted': false,
    }).then((response) => {
      if(response.result.items && response.result.items.length >0){
        this.addMeetingsFromCalenderToHealplace(response.result.items);
        this.syncAppSessions(response.result.items);
      }
      console.log('Upcoming events:', response.result.items);
    });
  }

  ngOnInit(): void {
    const curr = new Date; // get current date
    const first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    const firstday = new Date(curr.setDate(first)).toUTCString();
    this.settings.startDate = new Date(firstday);
    this.getDatesHeader();
    this.settings.availability = this.getTime(0, 24);
    gapi.load('client:auth2', this.initClient);
  }

  initClient() {
    gapi.client.init({
      clientId: '28219092747-19gnucp7ov2kr5giak3ig8p5o5dh9lbb.apps.googleusercontent.com',
      discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"],
      scope: "https://www.googleapis.com/auth/calendar.events"
    }).then((res) => {
      console.log('ressss', res);
    }, (error) => {
      console.log('error', error);
    });
  }

  changeWeek(week) {
    this.days = [];

    this.settings.startDate = week.startdate;
    this.getDatesHeader();
  }
  getTime(startTime, endTime) {
    var time = [];
    var currentTime = startTime;
    while (currentTime < endTime) {
      const suffix = (currentTime >= 12) ? 'pm' : 'am';
      const tempTime = ((currentTime + 11) % 12 + 1);
      time.push({
        displayTime: tempTime + ':00' + suffix,
        time: currentTime + ':00',
      });
      currentTime++;
    }
    return time;
  }

  getMonthName(idx) {
    return this.settings.months[idx];
  };

  getDatesHeader() {
    for (let i = 0; i < 7; i++) {
      var d = this.settings.startDate.addDays(i);
      this.days.push({
        fullDate: d,
        date: d.getDate(),
        day: this.settings.weekdays[d.getDay()],
        month: this.settings.startDate.getMonth() + 1,
        monthName: this.getMonthName(this.settings.startDate.getMonth()),
        utc: d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate(),
        available: [],
        disable: []
      })
    }
    this.getNavControl();
  }
  getNavControl() {
    const firstday = this.days[0].day + ', ' + this.days[0].monthName + ' ' + this.days[0].date;
    const lstday = this.days.length - 1;
    const lastday = this.days[lstday].day + ', ' + this.days[lstday].monthName + ' ' + this.days[lstday].date;
    this.week = firstday + ' - ' + lastday;
    this.fetchSchedule(this.days[0].utc, this.days[lstday].utc);
  };
  previousWeek() {
    this.days = [];

    this.settings.startDate = this.settings.startDate.addDays(-7);
    this.getDatesHeader();
  }
  nextWeek() {
    this.days = [];
    this.settings.startDate = this.settings.startDate.addDays(7);
    this.getDatesHeader();
  }

  formatDate(d) {
    var date = '' + d.getDate();
    var month = '' + (d.getMonth() + 1);
    var year = d.getFullYear();
    if (date.length < 2) {
      date = '0' + date;
    }
    if (month.length < 2) {
      month = '0' + month;
    }
    return year + '-' + month + '-' + date;
  }

  fetchSchedule(start_date, end_date) {
    this.loader.show();
    this.apiService.getPractitionerSchedule(start_date, end_date).subscribe((response: any) => {
      this.schedule = response;
      console.log('this.schedule', this.schedule);
      function ConvertNumberToTwoDigitString(n) {
        return n > 9 ? "" + n : "0" + n;
      }
      this.schedule.forEach((item) => {
        let start_time = new Date(item.start_time).getUTCHours();
        let end_time = new Date(item.end_time).getUTCHours();
        const dt = new Date(item.start_time).getDate();
        let time = [];
        while (start_time <= end_time) {
          time.push(ConvertNumberToTwoDigitString(start_time++) + ':00');
        }
        const times = this.days.filter((day) => {
          return day.date === dt;
        });
        if (times && times.length > 0 && time.length > 0 && item.is_from_appointment) {
          times[0].available = times[0].available.concat(time);
        }
        if (times && times.length > 0 && time.length > 0 && !item.is_available) {
          times[0].disable = times[0].disable.concat(time);
        }
      });
      this.loader.hide();
    },
      (errResponse) => {
        this.loader.hide();
      }
    );
  }

  editSchedule() {
    this.router.navigate(['practitioner/edit-schedule/'], { queryParams: { date: moment(this.settings.startDate).format('YYYY-MM-DD') } })
  }
  integrateGoogleCalender() {
    gapi.auth2.getAuthInstance().signIn().then((res) => {
      console.log('res', res);
      this.updateSigninStatus();
      console.log('this.schedule', this.schedule);
    });
    let signin = gapi.auth2.getAuthInstance().isSignedIn.get();
    console.log("signin", signin);
  }

  syncAppSessions(cevents){
    this.schedule.forEach((item) => {
      if(!this.isDuplicate(cevents, item, 'api')){
        if(item && (item.is_available || item.is_from_appointment)){
          const payload = {
            start:{
              dateTime: item.start_time,
              timeZone: moment.tz.guess()
            },
            end: {
              dateTime: item.end_time,
              timeZone: moment.tz.guess()
            }
          };
          console.log('syncAppSessions', payload);
          let request = gapi.client.calendar.events.insert({
            'calendarId': 'primary',
            'resource': payload
          });
          
          request.execute((event)=> {
            console.log('Event created: ' + event.htmlLink);
          });
        }
      }
    });
  }

  addMeetingsFromCalenderToHealplace(eventsFromCalender) {
    eventsFromCalender.forEach((item) => {
      let date = new Date(item.start.dateTime);
      let d = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
      let newArr = [];
      if(!this.isDuplicate(this.schedule, item, 'calender')){
        this.days.forEach((day) => {
          if (day.utc === d) {
            const arr = [];
            arr.push(moment(item.start.dateTime).format('HH:mm'));
            arr.push(moment(item.end.dateTime).format('HH:mm'))
            arr.push(moment.tz.guess());
            newArr.push(arr);
            this.fromCalender.push({
              [day.utc]: newArr
            });
            console.log('available', this.fromCalender);
          }
        });
      }
    });

    const data = {
      'create_data': this.fromCalender,
      'update_data': [],
      'delete_data': []
    }
   setTimeout(()=>{
    if(this.fromCalender && this.fromCalender.length>0){
      this.loader.show();
    this.practitionerScheduleService.saveAvailablity(data).subscribe((success) => {
      this.loader.hide();
      this.toastr.success('Session Added Successfully');
      this.router.navigate(['practitioner/schedule']);
      this.settings = {
        availability: [],
        isMultiple: false,
        months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        selectedDates: [],
        startDate: null,
        weekdays: ['sun', 'mon', 'tue', 'wed', 'thurs', 'fri', 'sat'],
      }
      this.days = [];
      this.fromCalender = [];
      this.schedule = [];
      this.ngOnInit();
    }, (err) => {
      this.loader.hide();
      console.log('errerr', err);
    });
    }
   },1000);
  }

  isDuplicate(collection, date, from){
    const date2 = from === 'api' ? new Date(date.start_time) : new Date(date.start.dateTime);
    const d2 = date2.getUTCDate() + '-' + date2.getUTCMonth() + '-' + date2.getUTCFullYear();
    const m = date2.getUTCMinutes();
    const h = date2.getUTCHours();
    return collection.some(element => {
      const date1 = from === 'api' ? new Date(element.start.dateTime) : new Date(element.start_time);
      const d = date1.getUTCDate() + '-' + date1.getUTCMonth() + '-' + date1.getUTCFullYear();
      const min = date1.getUTCMinutes();
      const hours = date1.getUTCHours();
      return d === d2 && hours === h && min === m;
    });
  }
};

