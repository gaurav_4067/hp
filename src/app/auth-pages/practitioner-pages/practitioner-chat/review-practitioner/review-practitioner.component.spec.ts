import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewPractitionerComponent } from './review-practitioner.component';

describe('ReviewPractitionerComponent', () => {
  let component: ReviewPractitionerComponent;
  let fixture: ComponentFixture<ReviewPractitionerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewPractitionerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewPractitionerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
