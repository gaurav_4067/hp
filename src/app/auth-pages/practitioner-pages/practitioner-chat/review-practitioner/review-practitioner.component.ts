import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-review-practitioner',
  templateUrl: './review-practitioner.component.html',
  styleUrls: ['./review-practitioner.component.scss']
})
export class ReviewPractitionerComponent implements OnInit {
  option = null;
  currentStep = 'one';
  shortDescription;
  isEnableReportBtn = false;
  pid;
  cid;
  rate = 0;
  max = 5;
  ratings = [0,1,2,3,4,5];
  isReadonly = false;
  dynamic = 0;
  qDynamic = 0
  mood;
  @Input() selectedList;
  logedinData: any;

  constructor(
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    public router: Router,
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    this.pid = this.selectedList.id;
    this.cid = JSON.parse(localStorage.getItem("logedin_data")).id;
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
  }
  confirmSelection(event: KeyboardEvent) {
    if (event.keyCode === 13 || event.key === 'Enter') {
      this.isReadonly = true;
    }
  }
  onShortDescription(val) {
    if (val) {
      this.isEnableReportBtn = true;
    } else {
      this.isEnableReportBtn = false;
    }
  }
  completeSession(step){
    console.log(step);
    console.log(this.selectedList);
    if(this.selectedList.isFromZoomSession){
      this.currentStep = step;
      this.isEnableReportBtn = false;
      //this.stripeTranfer("sessionId");
    } else {
      this.completetCheckIn();
    }
  }
  stripeTranfer(sessionId){
    let payload = {
      "connect_id": this.logedinData.stripe_connect_id,
      "country": "US",
      "session_id": sessionId,
      "is_licensed": false,
    };
    this.apiService
      .stripeTransfers(payload)
      .subscribe((response: any)=>{
        console.log(response);
      });
  }
  ratingSelected(rate){
    this.dynamic = rate;
    this.isEnableReportBtn = true;
  }
  qRatingSelected(rate){
    this.qDynamic = rate;
    this.isEnableReportBtn = true;
  }
  completetCheckIn(){
    const payload = {
      overall_rating: this.rate,
      review: this.shortDescription,
      customer_id: this.cid,
      is_recommended: false,
      practitioner: this.pid
    };
    this.loader.show();
    this.apiService.reviewPractitioner(payload)
    .subscribe((review)=>{
      console.log(review);
      console.log('review', review);
      this.toastr.success('Review Details Submitted')
      this.loader.hide();
      if(this.selectedList.isFromZoomSession){
        this.addMood();
      } else {
        this.activeModal.close();
      }
    }, (error)=>{
      console.log('review error', error);
      this.loader.hide();
      this.toastr.error(error.statusText);
    });
  }
  addMood(){
    const payload = {
      mood: this.mood,
      description:this.shortDescription,
      customer: JSON.parse(localStorage.getItem("logedin_data")).user_id.id
    };
    this.apiService.AddMoodInfo(payload)
    .subscribe((mood)=>{
      console.log('mood', mood);
      this.loader.hide();
      this.activeModal.close();
    }, (error)=>{
      console.log('mood error', error);
      this.loader.hide();
      this.toastr.error(error.statusText);
    });
  }
  goBack(step){
    this.currentStep = step;
  }
  moodOptionSlection(mood){
    this.isEnableReportBtn = true;
    this.mood = mood;
  }
}
