import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { ApiService } from '../../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GeneralService } from '../../../../services/GeneralService';

@Component({
  selector: 'app-report-practitioner',
  templateUrl: './report-practitioner.component.html',
  styleUrls: ['./report-practitioner.component.scss']
})
export class ReportPractitionerComponent implements OnInit {
  @ViewChild('file') file: ElementRef;
  option = null;
  currentStep = 'one';
  shortDescription;
  isEnableReportBtn = false;
  fileData;
  reason = {
    'first': "Practitioner wasn't a good match",
    'second': 'Something else'
  };
pid;

  @Input() selectedList;

  constructor(
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    public router: Router,
    public activeModal: NgbActiveModal,
    public generalService: GeneralService,
    ) { }

  ngOnInit(): void {
    this.pid = this.selectedList.id;
  }
  optionSelected(option) {
    this.option = option;
  }
  addDetails() {
    this.currentStep = 'two';
  }
  closeModal() {
    this.activeModal.close();
  }

  onFileSelected(event) {
    let file = event.target.files[0];
    this.fileData = file;
  }

  onShortDescription(val) {
    if (val) {
      this.isEnableReportBtn = true;
    } else {
      this.isEnableReportBtn = false;
    }
  }

  reportPractitioner() {
    let formData;
    let payload;
    if (this.fileData) {
      formData = new FormData();
      formData.append("practitioner", this.pid);
      formData.append("reason", this.reason[this.option]);
      formData.append("reason_description", this.shortDescription);
      formData.append("screen_shot", this.fileData);
    } else {
      payload = {
        practitioner: this.pid,
        reason: this.reason[this.option],
        reason_description: this.shortDescription,
      } 
    }
    this.loader.show();
    this.apiService.reportPractitioner(this.fileData ? formData : payload)
    .subscribe((report:any)=>{
      console.log('report', report);
      this.loader.hide();
      this.currentStep = 'three';
    }, (error)=>{
      console.log('report error', error);
      this.loader.hide();
      this.toastr.error(error.statusText);
    });
  }

  backToPlatform() {
    this.activeModal.close();
    this.router.navigate(['welcome-customer']);
  }

  resetSelectedFile() 
  {  
    
    this.file.nativeElement.value = "";
    this.fileData={};
  }
}
