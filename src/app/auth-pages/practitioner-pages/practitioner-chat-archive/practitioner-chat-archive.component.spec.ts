import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PractitionerChatArchiveComponent } from './practitioner-chat-archive.component';

describe('PractitionerChatArchiveComponent', () => {
  let component: PractitionerChatArchiveComponent;
  let fixture: ComponentFixture<PractitionerChatArchiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PractitionerChatArchiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PractitionerChatArchiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
