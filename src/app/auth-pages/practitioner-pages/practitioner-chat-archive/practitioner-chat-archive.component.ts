import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../../services/ChatService';
import { NgxSpinnerService } from 'ngx-spinner';
import { GeneralService } from '../../../services/GeneralService';

@Component({
  selector: 'app-practitioner-chat-archive',
  templateUrl: './practitioner-chat-archive.component.html',
  styleUrls: ['./practitioner-chat-archive.component.scss']
})
export class PractitionerChatArchiveComponent implements OnInit {

  archiveList: any;
  selectedArchive: any;

  constructor(private chatService: ChatService, private loader: NgxSpinnerService, public generalService: GeneralService) { }

  ngOnInit(): void {
    this.getArchiveList();
  }

  getArchiveList() {
    this.loader.show();
    let loggedInData = JSON.parse(localStorage.getItem("logedin_data"));
    this.chatService.getPractitionerArchiveChats(loggedInData.id).subscribe((succes: any) => {
      this.archiveList = succes.response;
      this.loader.hide();
    }, (err) => {
      this.loader.hide();
    })
  }

  deleteArchive() {
    this.loader.show();
    this.chatService.deleteArchiveChat(this.selectedArchive.id).subscribe((success) => {
      this.loader.hide();
      this.getArchiveList();
    }, (err) => {
      this.loader.hide();
    })
  }

  selectArchive(item) {
    this.selectedArchive = item;
  }

}
