import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/services/AuthService';
import { PractitionerSessionService } from 'src/app/services/Practitioner-session.service';
import { GeneralService } from '../../services/GeneralService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ZoomMeetingConfig } from '../../config/config';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/ApiService';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

@Component({
  selector: 'session-dashboard',
  templateUrl: './practitioner-session.component.html',
  styleUrls: ['./practitioner-session.component.scss']
})
export class PractitionerSessionComponent implements OnInit {
  sessionMode:boolean=true;
  hideMessage = [];
  upcomingSessions: any;
  completedSessions: any;
  latestupcomingSessions: any;
  showUpComingSession: boolean = true;
  customerChatList: any;
  customer__interest: string = "";
  free: string = "";
  timeDifference: string = "";
  dayandMonth: string = "";
  timeStart: any;
  timeEnd: any;
  difference: any;
  ChangeSessionrequest: any;
  videoRequest: any;
  isShowSession: any;
  selectedSession;

  @ViewChild('statusPopUp') statusPopUp: TemplateRef<any>;

  constructor(
    private practionerSession: PractitionerSessionService,
    public modalService: NgbModal,
    public generalService: GeneralService,
    public apiService:ApiService,
    private loader: NgxSpinnerService,
    private toaster: ToastrService,
    public auth: AuthService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.getPractitionerSessions();
  }

  showPopup(data) {
    this.selectedSession = data;
    console.log('this.selectedSession', this.selectedSession);

    // this.modalService.open(this.statusPopUp, { windowClass: 'modal-customer-status', size: 'md' });
  }

  closeMessage() {
    this.modalService.dismissAll();
  }

  getPractitionerSessions() {
    this.loader.show();
    this.practionerSession.getpractitionersessions()
      .subscribe((response: any) => {
        this.loader.hide();
        this.upcomingSessions = response.change_status_sessions_list.data;
        this.completedSessions = response.completed_sessions.data;
        if (response.upcoming_sessions && response.upcoming_sessions?.data.length > 1) {
          this.latestupcomingSessions = response.upcoming_sessions?.data.slice(0, 2);
        } else {
          this.latestupcomingSessions = response.upcoming_sessions?.data;
        }
      },
        error => {
          console.log(error);
          this.loader.hide();
        }
      );
  }

  getPrice(session) {
    if (!session.start_time) {
      return 'Free';
    }

    const startTime = session.start_time;
    const endTime = session.end_time;

    const [h1, m1] = startTime.split(':').map(Number);
    const [h2, m2] = endTime.split(':').map(Number);

    const h2Final = h2 < h1 ? h2 + 24 : h2;

    const totalS1 = m1 + 60 * h1;
    const totalS2 = m2 + 60 * h2Final;

    if (totalS2 - totalS1 > 30) {
      return `$${session.practitioner_session.price}`;
    }
    return 'Free';
  }

  getDuration(session) {
    if (!session.start_time) {
      return '30 min';
    }

    const startTime = session.start_time;
    const endTime = session.end_time;

    const [h1, m1] = startTime.split(':').map(Number);
    const [h2, m2] = endTime.split(':').map(Number);

    const h2Final = h2 < h1 ? h2 + 24 : h2;

    const totalS1 = m1 + 60 * h1;
    const totalS2 = m2 + 60 * h2Final;

    return this.convertToHrs(totalS2 - totalS1);
  }

  getAppointmentDate(appDate) {

    if (!appDate) {
      return '';
    }

    const [y, m, d] = appDate.split('-');
    const month = months[Number(m) - 1];

    return `${Number(d)} ${month}`;
  }

  getAppointmentTime(time) {

    if (!time) {
      return '';
    }

    const [h, m] = time.split(':');

    if (Number(h) > 12) {
      return `${h - 12}:${m} pm`;
    }
    return `${h}:${m} ${Number(h) === 12 ? 'pm' : 'am'}`;
  }

  convertToHrs(min: number | null) {
    if (min == null) { return ''; }
    const hrs = Math.floor(min / 60);
    const rem = min % 60;
    let res = '';

    if (hrs > 0) {
      res += `${hrs}h`;
    }
    if (rem > 0) {
      res += ` ${rem}min`;
    }
    return res;
  }

  getDate(chatDate: string) {
    const date = new Date(chatDate);
    return `${date.getDate()} ${months[date.getMonth()]}`;
  }

  getTime(chatDate: string) {
    const date = new Date(chatDate);
    let hours = date.getHours();
    let minutes: string | number = date.getMinutes();
    let time = 'am';
    if (hours > 11) {
      time = 'pm';
    }

    if (hours > 12) {
      hours -= 12;
    } else if (hours === 0) {
      hours = 12;
    }

    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    return `${hours}:${minutes} ${time}`;
  }

  ellipsisify(str: string) {
    if (str.length <= 18) {
      return str;
    }
    return `${str.slice(0, 18)}...`;
  }

  ShowUpComing() {
    this.showUpComingSession = !this.showUpComingSession;
  }

  changeSessionStatus(flag) {
    this.modalService.dismissAll();
   if(flag === 'Message'){
    this.router.navigate(['chat'],{ queryParams: { selected: this.selectedSession.customer_id, redirect: true }});
   }else{
    this.loader.show();
    const payload = {
      is_cancel: flag === 'Decline' ? true : false,
      is_reschedule: flag === 'ReSchedule' ? true : false,
      customer: this.selectedSession.customer_id,
      practitioner_session: this.selectedSession.practitioner_session.id,
      start_time: this.selectedSession.start_time,
      appointment_date: this.selectedSession.appointment_date
    };
    this.practionerSession.approvecustomersession(payload, this.selectedSession.id)
      .subscribe((res) => {
        console.log('res', res);
        this.loader.hide();
        this.toaster.success('Session Status Updated Successfully!');
        if(flag === 'Approve'){
          const userData = JSON.parse(localStorage.getItem('user_data'));
          const email = userData?.user.email;
          const payload1 = {
            duration: this.selectedSession.practitioner_session.duration_min,
            appointment: this.selectedSession.id,
            email: email
          }
          this.practionerSession.postvideosession(payload1)
          .subscribe((response: any) => {
            console.log('meeting response', response);
            this.sendEmail(response.meeting_id,response.appointment);
            this.loader.hide();
          }, (errorResponse) => {
            console.log('errorResponse', errorResponse);
            this.loader.hide();
          });
        }
        this.getPractitionerSessions();
      }, (error) => {
        console.log('error', error);
        this.loader.hide();
      });
   }
  }

  sendEmail(meeting_id,appointment_id){
    this.apiService.sendEmail(meeting_id,appointment_id)
    .subscribe((response: any) => {
      console.log('sendEmail response', response);
      //this.loader.hide();
    }, (errorResponse) => {
      console.log('errorResponse', errorResponse);
      //this.loader.hide();
    });
  }

  startSession(data) {
    const userData = JSON.parse(localStorage.getItem('user_data'));
    const isGroup = userData?.groups[0].toLowerCase();
    const userName = userData?.user.username;
    const email = userData?.user.email;
    this.loader.show();
    const payload = {
      duration: data.practitioner_session.duration_min,
      appointment: data.id,
      email: email
    };
    this.practionerSession.postvideosession(payload)
      .subscribe((res: any) => {
        console.log('res', res);
        this.loader.hide();
        if (res && res.meeting_id) {
          window.open(`${ZoomMeetingConfig.ZOOM_SESSION_APP_URL}?user=${userName}&id=${res.meeting_id}&auth=${this.auth.getToken()}&email=${email}&group=${isGroup}`, '_blank');
        }
      }, (error) => {
        console.log('error', error);
        this.loader.hide();
      });
  }

  gotoChat(data) {
    this.router.navigate(['chat'],{ queryParams: { selected: data.customer_id, redirect: true }});
  }

  isShowStartSession(session) {
    const now = new Date();
    const hoursNow = now.getHours();
    const minNow = now.getMinutes();
    const date1 = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
    if (session.appointment_date === date1) {
      const [h, m] = session.start_time.split(':');
      if (Number(h) == hoursNow) {
        const diff = Number(m) - minNow;
        if (diff < 6) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  validateSessionApproval(){
    console.log(this.selectedSession);
    //this.selectedSession.appointment_date
  }
}

