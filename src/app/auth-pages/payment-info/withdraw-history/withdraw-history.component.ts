import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';

@Component({
  selector: 'app-withdraw-history',
  templateUrl: './withdraw-history.component.html',
  styleUrls: ['./withdraw-history.component.scss']
})
export class WithdrawHistoryComponent implements OnInit {
  withdrawlists: any = [];
  username: string;
  logedinData: any;
  responseMsg="Please wait...";
  constructor(
    public apiService: ApiService,
    public router: Router,
    public auth: AuthService,
    public generalService: GeneralService,
    private loader: NgxSpinnerService,
    public toastr : ToastrService,
    private toaster: ToastrService,
  ) { }

  ngOnInit(): void {
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    let userData = JSON.parse(localStorage.getItem("user_data"));
    this.username = userData?.user.first_name + ' ' + userData?.user.last_name;
    if(this.logedinData.stripe_connect_id){
      this.fetchWithdrawList();      
    }
  }

  fetchWithdrawList() {
    this.apiService
      .getWithdrawList(this.logedinData.stripe_connect_id)
      .subscribe((response: any) => {
        if (response) {
          
         this.withdrawlists = response.msg;

         console.log(this.withdrawlists);
        }
        else{
          this.responseMsg="No data Found";
        }
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.loader.hide();
        this.responseMsg="No data Found";
      });
  }
  downloadBulkCSV() {
    var bodyContent: any = [];
    var currentdate = new Date();
    var datetime = "Last Sync: " + currentdate.getDate() + "/"
      + (currentdate.getMonth() + 1) + "/"
      + currentdate.getFullYear() + " @ "
      + currentdate.getHours() + ":"
      + currentdate.getMinutes() + ":"
      + currentdate.getSeconds();
    let fileNanme = `Withdraw History ${datetime}`;
    this.withdrawlists.forEach(arg => {
      bodyContent.push({
        date: arg.created,
        bankaccount: "Visa Ending in " + arg.last_4_numbers,
        amount: arg.amount
      });
    }); var titleContent = {
      title: "Withdraw History Report",
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true,
      headers: [
        'Date',
        'Bank Account/Card Number',
        'Amount'
      ],
      nullToEmptyString: true,
    };
    new Angular5Csv(bodyContent, fileNanme, titleContent);
  }
  
  downloadCSV(list: any){
    console.log(list);
    
    if(list){
      var currentdate = new Date(); 
      var datetime = "Last Sync: " + currentdate.getDate() + "/"
      + (currentdate.getMonth()+1)  + "/" 
      + currentdate.getFullYear() + " @ "  
      + currentdate.getHours() + ":"  
      + currentdate.getMinutes() + ":" 
      + currentdate.getSeconds();
      let fileNanme = `Withdraw History ${datetime}`;
      let bodyContent = [{
        date: list.created,
        bankaccount: "Visa Ending in "+list.last_4_numbers,
        amount: list.amount
      }];
      var titleContent = {
        title: "Withdraw History Report",
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        useBom: true,
        headers: [
          'Date',
          'Bank Account/Card Number',
          'Amount'
        ],
        nullToEmptyString: true,
      };
      new Angular5Csv(bodyContent, fileNanme, titleContent);
    }
  }
}
