import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/ApiService';
import { AuthService } from 'src/app/services/AuthService';
import { GeneralService } from 'src/app/services/GeneralService';

@Component({
  selector: 'app-payment-info',
  templateUrl: './payment-info.component.html',
  styleUrls: ['./payment-info.component.scss']
})
export class PaymentInfoComponent implements OnInit {
  username: string;
  expertise: any;
  eranings: any;
  logedinData: any;
  total_earnings: any;
  total_withdraw: any;
  bankDetails: any;

  constructor(
    public apiService: ApiService,
    public router: Router,
    public auth: AuthService,
    public generalService: GeneralService,
    private loader: NgxSpinnerService,
    public toastr : ToastrService,
  ) { }

  ngOnInit(): void {
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    let userData = JSON.parse(localStorage.getItem("user_data"));
    this.username = userData?.user.first_name + ' ' + userData?.user.last_name;
    this.expertise = this.logedinData.expertise;
    if(this.logedinData.stripe_connect_id){
      this.fetchEarnings();
    }
    this.fetchBankDetails();
  }
  fetchBankDetails(){
    this.loader.show();
    this.apiService
    .getBankAccount()
    .subscribe((response: any) => {
      if(response.results.length){
        this.bankDetails = response.results[0];
      }
      this.loader.hide();
    },
    (errResponse: HttpErrorResponse) => {
      this.loader.hide();
    });
  }
  fetchEarnings() {
    this.loader.show();
    this.apiService
      .getEarnings(this.logedinData.stripe_connect_id)
      .subscribe((res: any) => {
        if (res) {
          this.eranings = res;
          console.log(this.eranings);
          this.total_earnings = res['earnings'];
          this.total_withdraw = res['funds available for withdraw'];
          console.log(this.eranings);
        }
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.loader.hide();
      });
  }

  withdrawAmount(){
    const payload = {
      "connect_id": this.logedinData.stripe_connect_id,
      "country": "US",
      "payout_amount": this.total_withdraw
    };
    this.loader.show();
    this.apiService
      .withdrawAmount(payload)
      .subscribe((res: any) => {
        this.toastr.success('Funds withdraw successfully.');
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.toastr.error('Something went wrong.');
        this.loader.hide();
      });
  }

}
