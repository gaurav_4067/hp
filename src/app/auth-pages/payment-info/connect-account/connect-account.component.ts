import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import { ThirdPApiService } from '../../../services/ThirdPApiService';
import { NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-connect-account',
  templateUrl: './connect-account.component.html',
  styleUrls: ['./connect-account.component.scss'],
})
export class ConnectAccountComponent implements OnInit {
  readonly DT_FORMAT = 'MM/DD/YYYY';

  parse(value: string): NgbDateStruct {
    if (value) {
      value = value.trim();
      let mdt = moment(value, this.DT_FORMAT)
    }
    return null;
  }
  format(date: NgbDateStruct): string {
    if (!date) return '';
    let mdt = moment([date.year, date.month - 1, date.day]);
    if (!mdt.isValid()) return '';
    return mdt.format(this.DT_FORMAT);
  }

  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  countryData: any = [];
  stateData: any = [];
  cityData: any = [];
  payload: any;
  stripe_connect_id: any;
  isEdit: boolean = false;
  countryConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select Country *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  stateConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select State *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  cityConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select City *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  userData = JSON.parse(localStorage.getItem("user_data"));
  loginData = JSON.parse(localStorage.getItem("logedin_data"));
  add_location = this.loginData?.address?.address_line3 +' '+this.loginData?.address?.addrees_line4+' '+this.loginData?.address?.address_line5;
  connectAccountForm = new FormGroup({
    id_number: new FormControl('', Validators.required),
    email: new FormControl(this.loginData.user_id.email ? this.loginData.user_id.email : '', Validators.required),
    phone: new FormControl(this.loginData.phone_number, Validators.required),
    state: new FormControl(this.loginData?.address?.addrees_line4 ? this.loginData.address.addrees_line4 :'', Validators.required),
    postal_code: new FormControl(this.loginData?.address?.zipcode ? this.loginData.address.zipcode :'', Validators.required),
    line1: new FormControl('', Validators.required),
    country: new FormControl(this.loginData?.address?.address_line5 ? this.loginData.address.address_line5 :'', Validators.required),
    city: new FormControl(this.loginData?.address?.address_line3 ? this.loginData.address.address_line3 :'', Validators.required),
    first_name: new FormControl(this.loginData.user_id.first_name ? this.loginData.user_id.first_name : '', Validators.required),
    last_name: new FormControl(this.loginData.user_id.last_name ? this.loginData.user_id.last_name : '', Validators.required),
    dob: new FormControl('', Validators.required),
  });
  isCompleteProcess: boolean = false;
  isMaxDate: { year: any; month: any; day: any; };
  selectedAccount: any;
  isCountryData: any;
  
  constructor(public apiService: ApiService,
    public thirpApiService: ThirdPApiService,
    private ngbCalendar: NgbCalendar,
    private dateAdapter: NgbDateAdapter<string>,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    public router: Router,
  ) { }

  ngOnInit(): void {
    var today: any = new Date();
    var dd: any = today.getDate() - 1;
    var mm: any = today.getMonth() + 1;
    var yyyy: any = today.getFullYear() - 13;
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    this.isMaxDate = { year: parseInt(yyyy), month: parseInt(mm), day: parseInt(dd) };
    const connect_id = this.loginData.stripe_connect_id;
    if (connect_id != null) {
      this.isEdit = true;
      this.loader.show();
      this.apiService
      .getConnectAccount(connect_id)
      .subscribe((res: any) => {
        const response = res.connected_account;
        this.selectedAccount = response;
        if(response["currently_due"]){
          response["currently_due"].forEach(element => {
            if(element == "Adding Bank Account is Pending"){
              this.toastr.warning(element);
              this.isCompleteProcess = true;
            }else{
              this.toastr.warning(element);
              this.isCompleteProcess = true;
            }
          });
        }
        this.connectAccountForm.patchValue({
          id_number: "*********",
          email: response?.email,
          state: response?.state,
          postal_code: response?.postal_code,
          line1: response?.line1,
          country: response?.country,
          city: response?.city,
          first_name: response?.first_name,
          last_name: response?.last_name,
          dob: response?.dob,
          phone: response?.phone,
        });
        this.loadCountry();
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.loader.hide();
      });
    }else{
      this.loadCountry();
      this.isEdit = false;
    }
  }
  loadCountry() {
    this.loader.show();
    this.thirpApiService
      .getCountries("id,iso2", "no_page")
      .subscribe((response: any) => {
        this.countryData = response;
        if(this.isEdit){
          if("bank_accounts" in this.selectedAccount === true){
            if(this.selectedAccount.bank_accounts.length && this.selectedAccount.country){
              let result = this.countryData.find(obj=>obj.iso2 === this.selectedAccount.country)
              this.isCountryData = result;
              this.connectAccountForm.patchValue({
                country: result["name"],
              });
              this.ifloadState(result["id"]);
            }
          }
        }else{
          if(this.loginData.address.address_line5){
            let result = this.countryData.find(obj=>obj.name === this.loginData.address.address_line5)
            this.isCountryData = result;
            this.ifloadState(result["id"]);
          }
        }
        this.loader.hide();
      });
  }
  ifloadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.stateData = response;
        if(this.isEdit){
          if("bank_accounts" in this.selectedAccount === true){
            if(this.selectedAccount.bank_accounts && this.selectedAccount.state){
              let result = this.stateData.find(obj=>obj.name === this.selectedAccount.state)
              this.ifloadCity(result["id"]);
            }
          }
        }else{
          if(this.loginData.address.addrees_line4){
            let result = this.stateData.find(obj=>obj.name === this.loginData.address.addrees_line4)
            this.ifloadCity(result["id"]);
          }
        }
        this.loader.hide();
      });
  }
  ifloadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        console.log(response);
        this.cityData = response;
        this.loader.hide();
      });
  }
  loadState(countryId) {
    this.loader.show();
    this.thirpApiService
      .getStates(countryId, "id,state_code", "no_page")
      .subscribe((response: any) => {
        this.stateData = response;
        this.loader.hide();
      });
  }
  loadCity(stateId) {
    this.loader.show();
    this.thirpApiService
      .getCities(stateId, "id", "no_page")
      .subscribe((response: any) => {
        this.cityData = response;
        this.loader.hide();
      });
  }
  //on changes events
  countryChanged(data: any) {
    console.log(data)
    this.isCountryData = data;
    if (data) {
      this.stateData = [];
      this.cityData = [];
      this.loadState(data.id);
    } else {
      this.stateData = [];
      this.cityData = [];
    }
  }
  stateChanged(data: any) {
    if (data) {
      this.cityData = [];
      this.loadCity(data.id);
    } else {
      this.cityData = [];
    }
  }
  cityChanged(data: any) {
  }

  saveBankDetails() {
    if (this.isEdit) {
      this.editConnectAccount();
    } else {
      this.addConnectAccount();
    }
  }

  addConnectAccount() {
    this.loader.show();
    console.log(this.isCountryData);
    console.log(this.connectAccountForm.value);
    this.payload = { ...this.connectAccountForm.value, ...{
        "id": this.loginData.id,
        "phone": this.connectAccountForm.value.phone 
      } 
    };
    if(this.connectAccountForm.value.country && this.connectAccountForm.value.country["name"]){
      this.payload["country"] = this.isCountryData["iso2"];
    }else{
      this.payload["country"] = this.isCountryData["iso2"];
    }
    if(this.connectAccountForm.value.state && this.connectAccountForm.value.state["name"]){
      this.payload["state"] = this.connectAccountForm.value.state["name"];
    }else{
      this.payload["state"] = this.connectAccountForm.value.state;
    }
    if(this.connectAccountForm.value.city && this.connectAccountForm.value.city["name"]){
      this.payload["city"] = this.connectAccountForm.value.city["name"];
    }else{
      this.payload["city"] = this.connectAccountForm.value.city;
    }
    // this.payload["line1"] = `${this.payload["country"]} ${this.payload["city"]}`;
    console.log(this.payload);
    //return;
    this.apiService
      .addConnectAccount(this.payload)
      .subscribe((response: any) => {
        this.toastr.success('Connect account created successfully.');
        this.loader.hide();
        this.stripe_connect_id = response.connect_id;
        this.updatePractDetailsAdd(this.payload);
      },
      (errResponse: HttpErrorResponse) => {
        this.toastr.error('Something went wrong.');
        this.loader.hide();
      });
  }

  editConnectAccount() {
    this.loader.show();
    const loginData = JSON.parse(localStorage.getItem("logedin_data"));
    const updatedFormValues = {};
    const address = ['city', 'state', 'country', 'line1', 'postal_code'];
    this.connectAccountForm['_forEachChild']((control, name) => {
      if (control.dirty) {
        if (address.indexOf(name) > -1) {
          if(!updatedFormValues['address']) {
            updatedFormValues['address'] = {};
          }
          updatedFormValues['address'][name] = control.value;
        } else {
          updatedFormValues[name] = control.value;
        }
      }
    });
    if(this.connectAccountForm.value.country && this.connectAccountForm.value.country["name"]){
      this.connectAccountForm.value.country = this.isCountryData["iso2"];
    }else{
      this.connectAccountForm.value.country = this.isCountryData["iso2"];
    }
    if(this.connectAccountForm.value.state && this.connectAccountForm.value.state["name"]){
      this.connectAccountForm.value.state = this.connectAccountForm.value.state["name"];
    }
    if(this.connectAccountForm.value.city && this.connectAccountForm.value.city["name"]){
      this.connectAccountForm.value.city = this.connectAccountForm.value.city["name"];
    }
    const data = {
      "params": {
        ...updatedFormValues, ... {
          phone: this.connectAccountForm.value.phone,
          first_name: this.connectAccountForm.value.first_name,
          last_name: this.connectAccountForm.value.last_name,
          email: this.connectAccountForm.value.email,
          dob: this.connectAccountForm.value.dob,
          address: {
            "city": this.connectAccountForm.value.city,
            "state": this.connectAccountForm.value.state,
            "country": this.connectAccountForm.value.country,
            "line1": this.connectAccountForm.value.line1,
            "postal_code": this.connectAccountForm.value.postal_code,
          }
        },
      },
      "connect_id": loginData.stripe_connect_id
      // "acct_1J8i8C2Zw2nwBsBW"
    }
    console.log(data);
    //return;
    this.apiService
      .editConnectAccount(data)
      .subscribe((response: any) => {
        // localStorage.setItem(
        //   `logedin_data`,
        //   JSON.stringify(response)
        // );
        this.updatePractDetailsEdit(data);
        this.toastr.success('Connect account updated successfully.');
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.toastr.error('Something went wrong.');
        this.loader.hide();
      });
  }

  updatePractDetailsAdd(changes) {
    let payload = this.loginData;
    payload["address"]["address_line2"] = changes.line1;
    payload["address"]["address_line3"] = changes.city;
    payload["address"]["address_line4"] = changes.state;
    payload["address"]["address_line5"] = this.isCountryData["name"];
    if (changes.dob.day.length < 2) {
      changes.dob.day = `0${changes.dob.day}`; '0' + changes.dob.day;
    }
    if (changes.dob.month.length < 2) {
      changes.dob.month = `0${changes.dob.day}`; '0' + changes.dob.month;
    }
    payload["dob"] = `${changes.dob["year"]}-${changes.dob["month"]}-${changes.dob["day"]}`;
    payload["stripe_connect_id"] = this.stripe_connect_id;
    console.log(payload);
    this.loader.show();
    this.apiService
      .editPractitionerPatch(this.loginData.id, payload)
      .subscribe((response: any) => {
        console.log(response);
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response)
        );
        this.router.navigate(["/bank-details"]);
        this.loader.hide();
      });
  }
  
  updatePractDetailsEdit(changes) {
    let payload = this.loginData;
    payload["address"]["address_line2"] = changes["params"]["address"].line1;
    payload["address"]["address_line3"] = changes["params"]["address"].city;
    payload["address"]["address_line4"] = changes["params"]["address"].state;
    payload["address"]["address_line5"] = this.isCountryData["name"];
    payload["address"]["zipcode"] = changes["params"]["address"].postal_code;
    if (changes["params"].dob.day.length < 2) {
      changes["params"].dob.day = `0${changes["params"].dob.day}`;
    }
    if (changes["params"].dob.month.length < 2) {
      changes["params"].dob.month = `0${changes["params"].dob.month}`;
    }
    payload["dob"] = `${changes["params"].dob["year"]}-${changes["params"].dob["month"]}-${changes["params"].dob["day"]}`;
    payload["stripe_connect_id"] = changes.connect_id;
    console.log(payload);
    this.loader.show();
    this.apiService
      .editPractitionerPatch(this.loginData.id, payload)
      .subscribe((response: any) => {
        console.log(response);
        localStorage.setItem(
          `logedin_data`,
          JSON.stringify(response)
        );
        this.loader.hide();
      });
  }
}

