import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../services/ApiService';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ThirdPApiService } from 'src/app/services/ThirdPApiService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.scss']
})
export class BankDetailsComponent implements OnInit {
  isEdit: boolean = false;
  bankDetails: any;
  userData: any;
  logedinData: any;
  addBankDetailsForm = new FormGroup({
    routing_number: new FormControl('', Validators.required),
    account_holder_type: new FormControl('', Validators.required),
    account_number: new FormControl('', Validators.required),
    account_holder_name: new FormControl('', Validators.required),
    bank_name: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
  });

  countryConfig = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select Country *',
    customComparator: () => { },
    limitTo: 0,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
    clearOnSelection: false,
    inputDirection: 'ltr',
  }
  countryData: any;
  isCountryChosen: any;

  constructor(
    public apiService: ApiService,
    private loader: NgxSpinnerService,
    public toastr: ToastrService,
    public thirpApiService: ThirdPApiService,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    // this.loader.show();
    this.apiService
      .getBankAccount()
      .subscribe((response: any) => {
        this.bankDetails = response.results[0];
        if(this.bankDetails){
          this.isEdit = true;
          this.thirpApiService
          .getCountries("id,iso2", "no_page")
          .subscribe((response: any) => {
            this.countryData = response;
            let result = this.countryData.find(element => element.iso2 == this.bankDetails["country"]);
            this.isCountryChosen = result;
            this.addBankDetailsForm.setValue({
              routing_number: this.bankDetails.routing_number,
              account_holder_type: this.bankDetails.account_holder_type,
              account_number: this.bankDetails.account_number,
              account_holder_name: this.bankDetails.account_holder_name,
              bank_name: this.bankDetails.bank_name,
              country: result ? result["name"] : "",
            });
            console.log(this.addBankDetailsForm);
            this.loader.hide();
          });
        }else{
          this.isEdit = false;
          this.loadCountry();
        }
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.loader.hide();
      });
  }
  loadCountry() {
    this.loader.show();
    this.thirpApiService
      .getCountries("id,iso2", "no_page")
      .subscribe((response: any) => {
        this.countryData = response;
        this.loader.hide();
      });
  }
  countryChanged(data: any) {
    console.log(data);
    this.isCountryChosen = data;
  }
  saveBankDetails() {
    this.loader.show();
    if (this.isEdit) {
      this.editBankDetails();
    } else {
      this.addBankDetails();
    }
  }

  addBankDetails() {
    this.loader.show();
    let data = { ...this.addBankDetailsForm.value, ... { "practitioner": this.logedinData.id, "is_default": true, "is_active": true } };
    data["country"] = data["country"]["iso2"];
    this.apiService
      .addBankAccount(data)
      .subscribe((response: any) => {
        if(response){
          this.externalBankAccount(response, "add");
        }
        // this.toastr.success('Bank account created successfully.');
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.toastr.error('Something went wrong.');
        this.loader.hide();
      });
  }
  editBankDetails() {
    let updatedFormValues = {};
    this.addBankDetailsForm['_forEachChild']((control, name) => {
      if (control.dirty) {
        updatedFormValues[name] = control.value;
      }
    });
    this.loader.show();
    let data = { ...this.addBankDetailsForm.value, ... { "practitioner": this.logedinData.id, "is_default": true, "is_active": true } };
    data["country"] = this.isCountryChosen["iso2"];
    this.apiService
      .editBankAccount(data, this.bankDetails.id)
      .subscribe((response: any) => {
        if(response){
          this.externalBankAccount(response, "update");
        }
        // this.toastr.success('Bank account updated successfully.');
        this.loader.hide();
      },
      (errResponse: HttpErrorResponse) => {
        this.toastr.error('Something went wrong.');
        this.loader.hide();
      });
  }
  externalBankAccount(data, isRequest){
    this.loader.show();
    console.log(this.isCountryChosen);
    let payload = {
      "account_holder_name": data.account_holder_name,       
      "routing_number": data.routing_number,   
      "account_number": data.account_number,   
      "country": this.isCountryChosen["iso2"],
      "connect_id": this.logedinData.stripe_connect_id ? this.logedinData.stripe_connect_id : "",   
    }   
    this.apiService
    .externalBankAccount(payload)
    .subscribe((response: any) => {
      if(isRequest == "add"){
        this.toastr.success('Bank account created successfully.');
        this.router.navigate(["/document-verification"]);
      }else{
        this.toastr.success('Bank account updated successfully.');
      }
      this.loader.hide();
    },
    (errResponse: HttpErrorResponse) => {
      this.toastr.error('Something went wrong.');
      this.loader.hide();
    });
  }
}
