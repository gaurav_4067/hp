import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ActivationStart } from '@angular/router';
import { AuthService } from 'src/app/services/AuthService';
import { ApiService } from 'src/app/services/ApiService';
import { DOCUMENT } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GeneralService } from '../../services/GeneralService';
import { SessiononlyComponent} from '../../auth-pages/common-pages/sessiononly/sessiononly.component';
import { ContactAdminComponent } from '../../auth-pages/contact-admin/contact-admin.component';
import { SocialAuthService } from "angularx-social-login";

@Component({
  selector: 'app-auth-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [AuthService]
})
export class AuthNavbarComponent implements OnInit {
  public _token: any;
  hideHeader: any = true;
  model: any;
  isContent: any;
  configData: any;
  userData: any;
  isGroup: any;
  title: any;
  logedinData: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    public apiService: ApiService,
    private modalService: NgbModal,
    public generalService: GeneralService,
    private socialAuthService: SocialAuthService,
  ) {}

  ngOnInit() {
    this.generalService.showProfile=false;
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.isGroup = this.userData?.groups[0];
    //this.isSubscribed = this.logedinData["is_subscribed"];
    
    if (this.authService.isAuthenticated() && (this.logedinData["is_subscribed"] || this.isGroup == 'Practitioner' || this.isGroup == 'Admin' )) {
      this.router.navigateByUrl(this.router.url);
    }
    else{
      //this.router.navigate(["/home"]);
    }
    this.router.events.subscribe((data) => {
      this.router.events.subscribe(data => {
        if (data instanceof ActivationStart) {
          this.title = data.snapshot.data.title;
        }
      });
    });
    //this.userData = JSON.parse(localStorage.getItem("user_data"));
    this.logedinData = JSON.parse(localStorage.getItem("logedin_data"));
    //this.isGroup = this.userData?.groups[0];
    this.configData = {
      suppressScrollX: true,
      wheelSpeed: 0.3
    };
  }
  openSendMessageModal(){
    this.modalService.open(ContactAdminComponent, { backdrop: false, windowClass: 'contact-admin modal-send-msg', size: 'lg' });
  }
  getTitle(){
    return this.generalService.title ? this.generalService.title : this.isGroup == 'Practitioner' ?  'Hi Dr. ' + this.transform(this.logedinData?.user_id?.first_name) : `Welcome ` + this.transform(this.userData?.user?.first_name);
  }
  transform(value:string): string {
    let first = value.substr(0,1).toUpperCase();
    return first + value.substr(1); 
  }
  search(ev){
    this.modalService.dismissAll();
    console.log('ev', ev.target.value, this.isGroup);
    if(this.isGroup === 'Customer' && ev.target.value){
      const modalRef = this.modalService.open(SessiononlyComponent, {centered:true, scrollable:true, windowClass: 'search-practitioner', size: 'lg'});
      modalRef.componentInstance.query = ev.target.value;
    }
  }
  closeAllModal() {
    this.modalService.dismissAll();
  }
  navigateToPlan(){
    this.generalService.planAndDiscountSubject.next({canReload: true});
  }
  logout(){
    this.generalService.showProfile=false;
    this.apiService
      .getAppLogout()
      .subscribe((response: any) => {
        console.log(response.detail);
      });
    this.closeAllModal();
    this.socialAuthService.signOut();
    localStorage.removeItem("access_token");
    localStorage.removeItem("user_data");
    localStorage.removeItem("refresh_token");
    localStorage.removeItem("logedin_data");
    window.location.reload();
  }
}
